#!/usr/bin/env python

# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import sys
import os.path

from mettle.genes import ZipBuddy


def package_build():

    bsver    = os.path.expandvars('${BITSMITHS_VERSION}')
    dest_dir = '../release'
    zip_file = '../bin/bitsmiths-package.zip'
    zb       = ZipBuddy(zip_file)

    zb.clean_dir(dest_dir.replace('/', os.path.sep))
    zb.create_dir(dest_dir.replace('/', os.path.sep))

    zb.add_exclude_path('../products/contman/cpp/db')

    zb.add_text_as_file('.', 'version', bsver)

    # zb.add_files('cpp/include', '../products/contman/cpp',     ['.h'], True)
    zb.add_files('cpp/include', '../products/lib/cpp',         ['.h'], True)
    # zb.add_files('cpp/include', '../products/monitor/cpp',     ['.h'], True)

    # zb.add_files('cpp',         '../bin/cpp',    ['.a', '.lib', '.so', '.dll'], True)

    # zb.add_files('sql/crest',        '../products/crest/mettledb/sqldef',         ['.*'], True)
    # zb.add_files('sql/crest',        '../products/crest/sql',                     ['.*'], False)
    zb.add_files('sql/fura',         '../products/fura/mettledb/sqldef',          ['.*'], True)
    zb.add_files('sql/fura',         '../products/fura/sql',                      ['.*'], False)
    zb.add_files('sql/loco',         '../products/loco/mettledb/sqldef',          ['.*'], True)
    zb.add_files('sql/loco',         '../products/loco/sql',                      ['.*'], False)
    # zb.add_files('sql/contman',      '../products/contman/mettledb/sqldef',       ['.*'], True)
    # zb.add_files('sql/monitor',      '../products/monitor/mettledb/sqldef',       ['.*'], True)
    zb.add_files('sql/audit',        '../products/audit/mettledb/sqldef',         ['.*'], True)

    if sys.platform.startswith('win'):
        zb.add_post_execute('cmd /c "cd %s&&winrar -y x %s"' % (
            dest_dir.replace('/', os.path.sep), zip_file.replace('/', os.path.sep)))
    else:
        zb.add_post_execute('sh -c "cd %s&&unzip -o %s -d ."' % (
            dest_dir.replace('/', os.path.sep), zip_file.replace('/', os.path.sep)))

    zb.run()


if __name__ == '__main__':
    package_build()
