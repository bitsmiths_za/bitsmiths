#******************************************************************************#
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
#******************************************************************************#
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
#******************************************************************************#

UNAME     = $(OS)
EXTRAFLAG = release

ifeq ($(UNAME), Windows_NT)
	MAKE       = make.exe
	PY         = python.exe
	CMD_SEP    = &&
else
	MAKE       = make
	PY         = python3
	CMD_SEP    = ;
endif

MK_CMD      = cd $(^) && mettle-genes makefiles_proj.yml
BUILD_CMD   = cd $(^) && cd _mk_sln && $(MAKE) $(EXTRAFLAG)
RUN_CMD     =
PACKAGE_CMD = ./create-package.py && cd ../bin && sha256sum bitsmiths-package.zip > bitsmiths-package.chksum
SOURCES     = bslib audit loco fura monitor

# crest contman

ifeq ($(CLEAN), clean)
   RUN-CMD = $(CLEAN-CMD)
endif

.PHONY: help
help:
	@echo
	@echo "  Usage: make [target]"
	@echo "  ----------------------------------------"
	@echo "  full         : run [mk, clean, release and package]"
	@echo "  release      : build all targets in release mode"
	@echo "  debug        : build all targets in debug mode"
	@echo "  clean        : clean all targets"
	@echo "  mk           : create all the make files"
	@echo "  package      : package all build binaries"
	@echo
	@echo "  bslib        : build library"
	@echo "  audit        : build auditing"
	@echo "  loco         : build correspondence"
	@echo "  fura         : build function user role auths"
	@echo "  monitor      : build monitor"
	@echo
	@echo "  help         : this help"
	@echo

#	@echo "  crest        : build reporting"
#	@echo "  contman      : build content management"


.PHONY: full
full: mk clean release package

.PHONY: package
package:
	@echo -------------------------
	@echo Create Package - start
	@echo -------------------------
	$(PACKAGE_CMD)
	@echo -------------------------
	@echo Create Package - done
	@echo -------------------------

.PHONY: release
release: EXTRAFLAG = release
release: RUN_CMD   = $(BUILD_CMD)
release: $(SOURCES)
	@echo
	@echo Release build done
	@echo

# Optional debug make
.PHONY: debug
debug: EXTRAFLAG = debug
debug: RUN_CMD   = $(BUILD_CMD)
debug: $(SOURCES)
	@echo
	@echo Debug build done
	@echo

# Generate all the makefiles
.PHONY: mk
mk: RUN_CMD   = $(MK_CMD)
mk: $(SOURCES)
	@echo
	@echo Makefile generation done
	@echo

# Clean all the projects
.PHONY: clean
clean: EXTRAFLAG = clean
clean: RUN_CMD   = $(BUILD_CMD)
clean: bslib
	@echo
	@echo Clean done
	@echo


# clean: $(SOURCES)
bslib: CMD = $(or $(RUN_CMD),$(BUILD_CMD))
bslib: ../products/lib/build
	@echo
	@echo -  Bitsmiths Library
	@echo "$(CMD)"
	@echo "$(RUN_CMD)"
	@echo "$(BUILD_CMD)"
	@echo
	$(CMD)

audit: CMD = $(or $(RUN_CMD),$(BUILD_CMD))
audit: ../products/audit/build
	@echo
	@echo -  Bitsmiths Audit
	@echo
	$(CMD)

loco: CMD = $(or $(RUN_CMD),$(BUILD_CMD))
loco: ../products/loco/build
	@echo
	@echo -  Bitsmiths Loco
	@echo
	$(CMD)

fura: CMD = $(or $(RUN_CMD),$(BUILD_CMD))
fura: ../products/fura/build
	@echo
	@echo -  Bitsmiths Fura
	@echo
	$(CMD)

monitor:CMD = $(or $(RUN_CMD),$(BUILD_CMD))
monitor: ../products/monitor/build
	@echo
	@echo -  Bitsmiths Monitor
	@echo
	$(CMD)


# crest:CMD = $(or $(RUN_CMD),$(BUILD_CMD))
# crest: ../products/crest/build
# 	@echo
# 	@echo -  Bitsmiths Crest
# 	@echo
# 	$(CMD)

# contman:CMD = $(or $(RUN_CMD),$(BUILD_CMD))
# contman: ../products/contman/build
# 	@echo
# 	@echo -  Bitsmiths Content Management
# 	@echo
# 	$(CMD)
