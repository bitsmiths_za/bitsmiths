#!/bin/bash

#******************************************************************************#
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
#******************************************************************************#
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
#******************************************************************************#

source ../externs/mettle/bash/mettle-lib.sh

CLI_VER=

function splitVerParts()
{
  local verSpaced=$(echo $CLI_VER | tr "." " ")

  IFS=', ' read -r -a verParts <<< "$verSpaced"

  if [ ${#verParts[@]} -ne 4 ]; then
    mlib_raise "$0" "$LINENO" "Version string not valid, expected format is \"verCodename.verMaj.verMed.verMin\" eg: Codename.0.4.2"
  fi
}

function writeVersionFile()
{
  local verFile="./package-version.txt"
  local dateVal=`date +%Y-%m-%d`

  echo "  - writting new version file [${verFile}]"

  cat > $verFile << _EOF_
${verParts[0]}.${verParts[1]}.${verParts[2]}.${verParts[3]}.${dateVal}
_EOF_

  if [ $? -ne 0 ]; then
    mlib_raise "$0" "$LINENO" "Could not cat/create [$verFile], rc:$?"
  fi
}


function updatePYPIVersion()
{
  local pypath=../products
  local projver="${verParts[1]}.${verParts[2]}.${verParts[3]}"

  declare -a local setupFiles=(\
 $pypath/lib/python3/setup.cfg\
 $pypath/audit/python3/setup.cfg\
 $pypath/loco/python3/setup.cfg\
 $pypath/fura/python3/setup.cfg\
)

  for pr in ${setupFiles[@]}; do
    echo "  - updating config file [${pr}] version to [${projver}]"

    if [ ! -f "$pr" ]; then
      mlib_raise "$0" "$LINENO" "File [${pr}] not found."
    fi

    mlib_exec "$0" "$LINENO" "sed -i \"s|version =.*|version = ${projver}|\" \"$pr\""
  done
}


function updateTSDependancies()
{
  local ppath=../products
  local projver="${verParts[1]}.${verParts[2]}.${verParts[3]}"

  declare -a local packageFiles=(\
 $ppath/crest/ts/crest/lib/package\
 $ppath/fura/ts/fura/projects/bs-fura-lib/package\
 $ppath/fura/ts/fura/projects/bs-fura-std/package\
 $ppath/fura/ts/fura/package\
 $ppath/lib/ts/bs/projects/bs-lib/package\
 $ppath/lib/ts/bs/package\
  )

  echo "  - updating type script package version to [${projver}]"

  for pr in ${packageFiles[@]}; do
    pfile=$pr.json

    if [ ! -f "$pfile" ]; then
      mlib_raise "$0" "$LINENO" "Package file [${pfile}] not found."
    fi

    echo "     $pfile"

    mlib_exec "$0" "$LINENO" "sed -i \"s|.*\x22version\x22.*|  \x22version\x22: \x22${projver}\x22,|\" \"$pfile\""
  done
}


function main()
{
  echo ""
  echo " Prepare Version [${CLI_VER}]"
  echo ""

  splitVerParts

  writeVersionFile

  updatePYPIVersion

  updateTSDependancies

  echo ""
  echo "Version staged ok"
  echo ""
}


function usageAndExit()
{
  echo ""
  echo "Usage: $0 -v VER [-h]"
  echo "------------------------------------------"
  echo " -v VER  : New version number ie: CodeName.1.0.1"
  echo " -h      : This help."
  echo ""

  exit 2
}


while getopts ":hv:" opt; do
  case $opt in
    v)
      export CLI_VER="$OPTARG"
      ;;
    h)
      usageAndExit
      ;;
    \?)
      usageAndExit
      ;;
    *)
      usageAndExit
      ;;
  esac
done

if [ "$CLI_VER" == "" ]; then
  usageAndExit
fi

main
