#!/bin/bash

#******************************************************************************#
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
#******************************************************************************#
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
#******************************************************************************#

source ../externs/mettle/bash/mettle-lib.sh

METTLE_VER=
SHORT_VER=

function readVersion()
{
  local pver=`pip show bitsmiths-mettle | grep "Version:"`

  if [ ! "$pver" ]; then
    mlib_raise "$0" "$LINENO" "Could not determine bitsmiths-mettle version... is it installed?"
  fi

  local verSpaced=$(echo $pver | cut -f 2 -d' ')
  local verParts=

  IFS='.' read -r -a verParts <<< "$verSpaced"

  if [ ${#verParts[@]} -ne 3 ]; then
    mlib_raise "$0" "$LINENO" "Mettle version string not valid, expected format is \"verMaj.verMed.verMin\" eg: 1.5.2"
  fi

  METTLE_VER="${verParts[0]}.${verParts[1]}.${verParts[2]}"
  SHORT_VER="${verParts[0]}.${verParts[1]}"

  echo "  - Mettle Version: $METTLE_VER"
}

function updateMettleDefFiles()
{
  local ppath=../products

 # $ppath/contman/mettlebraze\
 # $ppath/monitor/mettlebraze\
 # $ppath/monitor/mettledb\
 # $ppath/crest/mettlebraze\
 # $ppath/crest/mettledb\

 declare -a local geneDirs=(\
 $ppath/lib/build\
 $ppath/audit/build\
 $ppath/audit/mettledb\
 $ppath/loco/build\
 $ppath/loco/mettledb\
 $ppath/fura/build\
 $ppath/fura/mettlebraze\
 $ppath/fura/mettledb\
)

  for pr in ${geneDirs[@]}; do
    if [ ! -d "$pr" ]; then
      mlib_raise "$0" "$LINENO" "Directory [${pr}] not found."
    fi

    for mfile in `ls ${pr}/*.yml`; do
      echo "     $mfile"
      mlib_exec "$0" "$LINENO" "sed -i \"s|version   \:.*|version   \: $SHORT_VER|\" \"$mfile\""
    done

  done

}

function updateTSDependancies()
{
  local ppath=../products
  local ver=${METTLE_VER}

  declare -a local depFiles=(\
 $ppath/fura/ts/fura/package\
 $ppath/crest/ts/crest/lib/package\
)

  declare -a local depSubProjFiles=(\
 $ppath/fura/ts/fura/projects/bs-fura-lib/package\
 $ppath/fura/ts/fura/projects/bs-fura-std/package\
)

  echo " - updating type script dependancies [${ver}]"

 for pr in ${depSubProjFiles[@]}; do
    pfile=$pr.json

    if [ ! -f "$pfile" ]; then
      mlib_raise "$0" "$LINENO" "Package file [${pfile}] not found."
    fi

    local comma=","

    echo "     $pfile"

    for mpkg in braze db io lib; do
      mlib_exec "$0" "$LINENO" "sed -i \"s|.*\@bitsmiths/mettle-${mpkg}\x22.*|    \x22@bitsmiths/mettle-${mpkg}\x22: \x22~${ver}\x22,|\" \"$pfile\""
    done
  done
}

function main()
{
  echo ""
  echo " Syncronizing Mettle Version"
  echo ""

  readVersion

  updateMettleDefFiles

  # updateTSDependancies

  echo ""
  echo "Version sync done"
  echo ""
}


function usageAndExit()
{
  echo ""
  echo "Usage: $0 [-h]"
  echo ""
  echo "Syncronize the mettle version in the externs folder"
  echo "into all the mettledb and mettlebraze python files."
  echo ""
  echo "It also updates all the package dependancies/pylint"
  echo "and a bunch of other mettle version dependancies to the"
  echo "externs folder."
  echo ""
  echo " -h      : This help."
  echo ""

  exit 2
}


while getopts ":h" opt; do
  case $opt in
    h)
      usageAndExit
      ;;
    \?)
      usageAndExit
      ;;
    *)
      usageAndExit
      ;;
  esac
done

main
