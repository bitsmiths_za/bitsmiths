
# ------------------------------------------------------------------------------
# To extern mettle from a remote SFTP server, use below
# ------------------------------------------------------------------------------

# EXTERN_MODE=sftp
# IDENTITY_KEY=/home/<user>/keys/user.bitsmiths.id_rsa
# METTLE_PATH=bsuser@ipaddress\:ubuntu_1604/mettle/1.4.0/*


# ------------------------------------------------------------------------------
# To extern mettle from a local file system releae, use below
# ------------------------------------------------------------------------------

EXTERN_MODE=link
METTLE_PATH=/home/<user>/sandbox/bs/mettle/release
