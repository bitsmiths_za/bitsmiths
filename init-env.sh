#******************************************************************************#
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
#******************************************************************************#
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
#******************************************************************************#

export LOCAL_PATH=`pwd`
export LOCAL_HOST=`hostname`
export LOCAL_USER=`whoami`

export LOCAL_PY=$LOCAL_PATH/bs-venv/bin/python

INIT_ENV_FORCE_REFRESH=0

export BS_DB_CONN_URL="postgres"

# -- Environment Variables
export BITSMITHS_VER_CODENAME=N/A
export BITSMITHS_VER_MAJ=0
export BITSMITHS_VER_MED=0
export BITSMITHS_VER_MIN=0
export BITSMITHS_VERSION="${BITSMITHS_VER_MAJ}.${BITSMITHS_VER_MED}.${BITSMITHS_VER_MIN}"

export PYTHONPATH="${LOCAL_PATH}/products/lib/python3"\
:"${LOCAL_PATH}/products/audit/python3"\
:"${LOCAL_PATH}/products/loco/python3"\
:"${LOCAL_PATH}/products/fura/python3"\
:"${LOCAL_PATH}/products/monitor/python3"\


alias gohome='cd $LOCAL_PATH'
alias goaudit='cd $LOCAL_PATH/products/audit'
alias goloco='cd $LOCAL_PATH/products/loco'
alias gofura='cd $LOCAL_PATH/products/fura'
alias golib='cd $LOCAL_PATH/products/lib'
alias gomonitor='cd $LOCAL_PATH/products/monitor'

#:"${LOCAL_PATH}/products/crest/py"

function initPythonVirtualEnv
{
  cd "$LOCAL_PATH"

  local installPip=0

  if [ ! -d bs-venv ]; then
    echo ""
    echo " - Initializing python3.7 virtual environment for bitsmiths..."
    echo ""

    python3.7 -m venv bs-venv

    installPip=1
  fi

  source "${LOCAL_PATH}/bs-venv/bin/activate"

  if [ $installPip -eq 1 ] || [ $INIT_ENV_FORCE_REFRESH -eq 1 ]; then
    pip install wheel
    pip install -r \"$LOCAL_PATH/products/pip-requirements.txt\" --upgrade
    pip install -r \"$LOCAL_PATH/products/pip-dev-requirements.txt\" --upgrade
  fi
}


function initVersionInfo()
{
  local vparts=(`cat "${LOCAL_PATH}/build/package-version.txt" | tr "." "\n"`)

  export BITSMITHS_VER_CODENAME=${vparts[0]}
  export BITSMITHS_VER_MAJ=${vparts[1]}
  export BITSMITHS_VER_MED=${vparts[2]}
  export BITSMITHS_VER_MIN=${vparts[3]}
  export BITSMITHS_VERSION="${BITSMITHS_VER_MAJ}.${BITSMITHS_VER_MED}.${BITSMITHS_VER_MIN}"
}


function usageAndExit()
{
  echo ""
  echo " Init Bitsmiths Environment"
  echo ""
  echo "Usage: $0 [ -r -? ]"
  echo "---------------------------------------------------------------------"
  echo " -r    : Force virtual environment refreshes"
  echo " -?    : This help"
  echo ""
  echo ""
  echo " To intialize your environment run >> . ./`basename $0`"
  echo ""

  exit 2
}


function main()
{
  set -e

  initVersionInfo

  initPythonVirtualEnv

  pre-commit install

  alias sql_pg='psql -U bs'

  echo ""
  echo " - Bitsmiths Envirnoment Initialized : ${BITSMITHS_VERSION} - ${BITSMITHS_VER_CODENAME}"
  echo ""

  set +e
}


while getopts "r" opt; do
  case $opt in
    r)  INIT_ENV_FORCE_REFRESH=1;;
    \?) usageAndExit;;
    *)  usageAndExit;;
  esac
done

main
