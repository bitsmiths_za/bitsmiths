#!/usr/bin/env python

# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import sys
import os
import os.path
import yaml

with open('../../../externs/mettle/version', 'rt') as fh:
    mettleVer      = fh.read().strip()
    mettleVerParts = mettleVer.split('.')

with open('../../../build/package-version.yml', 'rt') as fh:
    pv    = yaml.safe_load(fh.read())
    bsver = '%d.%d' % (pv['version']['maj'], pv['version']['med'])

eggVer = 'py%d.%d.egg' % (sys.version_info.major, sys.version_info.minor)

sys.path.append(('../../../externs/mettle/py3/mettle-%s.%s-%s' % (
    mettleVerParts[0], mettleVerParts[1], eggVer)).replace('/', os.path.sep))

import mettle.mfgen


# ------------------------------------------------------------------------------
# GenMettle
# ------------------------------------------------------------------------------
def GenMettle(mfgen):

    mfgen.gen('mettle', {
        'makefile.dir' : 'mettledb',
        'mettlegen'    : '../../externs/mettle/py3/mettlegen-%s.%s-%s' % (
            mettleVerParts[0], mettleVerParts[1], eggVer),
        'proj_paths'   : ['mettledb/proj.yml'],
    })

    mfgen.gen('mettle', {
        'makefile.dir' : 'mettlebraze',
        'mettlegen'    : '../../externs/mettle/py3/mettlegen-%s.%s-%s' % (
            mettleVerParts[0], mettleVerParts[1], eggVer),
        'proj_paths'   : ['mettlebraze/contman.py'],
    })


# ------------------------------------------------------------------------------
# GenCPP
# ------------------------------------------------------------------------------
def GenCPP(mfgen):
    cppGen = mfgen.getGenerator('cpp')

    cppGen._destDirs['lib'] = '../../bin/cpp/lib'
    cppGen._destDirs['so']  = '../../bin/cpp/lib'
    cppGen._destDirs['exe'] = '../../bin/cpp/exe'

    cppGen._osCompiler['win']   = 'mingw'
    cppGen._osCompiler['linux'] = 'gnu'

    pth    = 'cpp/bs/contman'
    incl   = ['cpp', '../lib/cpp', '../../externs/mettle/cpp/include']

    mfgen.gen('cpp', {
        'makefile.dir' : pth,
        'dest.out'     : 'lib',
        'dest.name'    : 'libbs_contman',
        'srcPath'      : [
                            pth + '/*',
                            pth + '/braze/*',
                            pth + '/db/tables/*',
                            pth + '/db/dao/postgresql/*',
                         ],
        'incPath'      : incl + ['../../externs/lua5'],
    })

    if sys.platform.startswith('linux'):
        mfgen.gen('cpp', {
            'makefile.dir' : pth,
            'dest.out'     : 'so',
            'dest.name'    : 'libbs_contman',
            'srcPath'      : [
                                pth + '/*',
                                pth + '/braze/*',
                                pth + '/db/tables/*',
                                pth + '/db/dao/postgresql/*',
                             ],
        })


# ------------------------------------------------------------------------------
# GenPython3
# ------------------------------------------------------------------------------
def GenPython3(mfgen):

    if mfgen._windows() and not os.path.exists(os.path.join(mfgen._config['extern-path'], 'python3')):
        mfgen.warning("Python3 path not set [%s], no PYD makefiles will be generated" % (
            os.path.join(mfgen._config['extern-path'], 'python3')))
        return

    pyGen = mfgen.getGenerator('py3')

    pyGen._destDirs['egg'] = '../../bin/py3'

    mfgen.gen('py3', {
        'makefile.dir' : 'py',
        'dest.out'     : 'egg',
        'dest.name'    : 'bs_contman',
        'dest.descr'   : 'Bitsmiths Content Management Package',
        'ver'          : bsver,
        'author'       : 'Bitsmiths (Pty) Ltd.',
        'author_email' : 'nicolas@bitsmiths.co.za, steven@bitsmiths.co.za, darryl@bitsmiths.co.za',
        'license'      : 'MIT',
        'url'          : 'https://bitbucket.org/bitsmiths_za/bitsmiths',
    })


# ------------------------------------------------------------------------------
# GenSolution
# ------------------------------------------------------------------------------
def GenSolution(mfgen):
    projSolution = {
        'makefile.dir' : 'build',
        'targets'      : mfgen._allGen,
        'order'        : ['mettle', 'cpp', 'py3'],
    }

    mfgen.gen('solution', projSolution)


# ------------------------------------------------------------------------------
# main
# ------------------------------------------------------------------------------
if __name__ == '__main__':
    mfgen    = mettle.mfgen.generator('..')

    mfgen.initialize(['build.yml'], True)

    GenMettle(mfgen)
    GenCPP(mfgen)
    GenPython3(mfgen)
    GenSolution(mfgen)
