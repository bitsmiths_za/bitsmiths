/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "bs/contman/contentmanager.h"
#include "bs/contman/macros.h"
#include "bs/contman/xcontentmanager.h"

#include "bs/contman/db/dao/commentsDAO.h"
#include "bs/contman/db/dao/objectsDAO.h"
#include "bs/contman/db/dao/refsDAO.h"

#include "bs/lib/podcommit.h"

#include "mettle/lib/filemanager.h"
#include "mettle/lib/logger.h"
#include "mettle/lib/macros.h"

#include "mettle/db/iconnect.h"

#include <stdint.h>
#include <inttypes.h>


using namespace Mettle::Lib;
using namespace BS::Lib;
using namespace BS::ContMan::Braze;
using namespace BS::ContMan::DB;

#define SOURCE "BS.ContMan.ContentManager"

namespace BS { namespace ContMan {

static void convObjectDB2Obj(const tObjects &src, bObject &dest)
{
   dest.id            = src.id;
   dest.name          = src.name;
   dest.mimeType      = src.mimeType;
   dest.sourceID      = src.sourceID;
   dest.sourceValue   = src.sourceValue;
   dest.objSize       = src.objSize;
   dest.status        = src.status;
   dest.createdBy     = src.createdBy;
   dest.createdDate   = src.createdDate;
   dest.modifiedBy    = src.modifiedBy;
   dest.modifiedDate  = src.modifiedDate;
}

static void convObjectObj2DB(const bObject &src, tObjects &dest)
{
   if (src.id.length() > 0)
      dest.id = src.id.toInt64();
   else
      dest.id = 0;

   dest.name          = src.name;
   dest.mimeType      = src.mimeType;
   dest.sourceID      = src.sourceID;
   dest.sourceValue   = src.sourceValue;
   dest.objSize       = src.objSize;
   dest.status        = src.status;
   dest.createdBy     = src.createdBy;
   dest.createdDate   = src.createdDate;
   dest.modifiedBy    = src.modifiedBy;
   dest.modifiedDate  = src.modifiedDate;
}

static void convRefObj2DB(const bRef &src, tRefs &dest)
{
   if (src.objectId.length() > 0)
      dest.objectId = src.objectId.toInt64();
   else
      dest.objectId = 0;

   dest.id            = src.refId;
   dest.value         = src.value;
   dest.createdBy     = src.createdBy;
   dest.createdDate   = src.createdDate;
   dest.modifiedBy    = src.modifiedBy;
   dest.modifiedDate  = src.modifiedDate;
}

static void convRefDB2Obj(const tRefs &src, bRef &dest)
{
   dest.objectId      = src.objectId;
   dest.refId         = src.id;
   dest.value         = src.value;
   dest.createdBy     = src.createdBy;
   dest.createdDate   = src.createdDate;
   dest.modifiedBy    = src.modifiedBy;
   dest.modifiedDate  = src.modifiedDate;
}

static void convCommentDB2Obj(const tComments &src, bComment &dest)
{
   dest.objectId      = src.objectId;
   dest.commentId     = src.id;
   dest.comment       = src.value;
   dest.createdBy     = src.createdBy;
   dest.createdDate   = src.createdDate;
}

ContentManager::ContentManager(Pod *pod) throw()
{
   _pod = pod;
}

ContentManager::~ContentManager() throw()
{
}

void ContentManager::validateCredentials()
{
   if (_user.isEmpty() || _sysUser.isEmpty())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Invalid security credentials!");
}

void ContentManager::logoff()
{
   PLOG(_pod)->info("CMAN: Logoff - Start(usr:%s/sysUsr:%s)", _user.c_str(), _sysUser.c_str());

   _user.clear();
   _passwd.clear();
   _sysUser.clear();
   _sysPasswd.clear();

   PLOG(_pod)->info("CMAN: Logoff - Done");
}

void ContentManager::logon(const String &endPoint,
                           const String &user,
                           const String &passwd,
                           const String &sysUser,
                           const String &sysPasswd)
{
   PLOG(_pod)->info("CMAN: Logon - Start(usr:%s/sysUsr:%s)", _user.c_str(), _sysUser.c_str());

   logoff();

   _user      = user;
   _passwd    = passwd;
   _sysUser   = sysUser;
   _sysPasswd = sysPasswd;

   PLOG(_pod)->info("CMAN: Logon - Done(usr:%s/sysUsr:%s)", _user.c_str(), _sysUser.c_str());
}

bObject *ContentManager::objLoad(const String &objectId)
{
   POD_COMMIT(_pod);

   validateCredentials();

   PLOG(_pod)->info("CMAN: ObjLoad - Start(usr:%s/sysUsr:%s/id:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str());

   dObjects obj(DBCON(_pod));

   obj.rec.id = objectId.toInt64();

   if (!obj.trySelectOne())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to load Object (%s), the record does not exist.", objectId.c_str());

   bObject::Safe oo(new bObject());

   convObjectDB2Obj(obj.rec, *oo.obj);

   PLOG(_pod)->info("CMAN: ObjLoad - Done(id:%" PRId64 ")", obj.rec.id);

   return oo.forget();
}

unsigned int ContentManager::objLoadData(      MemoryBlock *dest,
                                         const String      &objectId)
{
   POD_COMMIT(_pod);

   validateCredentials();

   PLOG(_pod)->info("CMAN: objLoadData - Start(usr:%s/sysUsr:%s/id:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str());

   dest->clear();

   dObjects obj(DBCON(_pod));

   obj.rec.id = objectId.toInt64();

   if (!obj.trySelectOne())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to load Object (%s) data, the record does not exist.", objectId.c_str());

   readContent(&obj.rec, dest);

   PLOG(_pod)->info("CMAN: objLoadData - Done(id:%s/size:%u)", objectId.c_str(), dest->size());

   return dest->size();
}

void ContentManager::internalObjSave(const bObject     &obj,
                                     const MemoryBlock *data,
                                           String      &outId)
{
   validateCredentials();

   PLOG(_pod)->info("CMAN: internalObjSave - Start(usr:%s/sysUsr:%s/id:%s/name:%s/dataSize:%d)", _user.c_str(), _sysUser.c_str(), obj.id.c_str(), obj.name.c_str(), data ? data->size() : 0);

   dObjects dbObj(DBCON(_pod));

   convObjectObj2DB(obj, dbObj.rec);

   if (dbObj.rec.name.isEmpty())
      dbObj.rec.name = STR_UNKNOWN;

   if (idNull(obj.id))
   {
      dbObj.rec.createdBy   = _user;
      dbObj.rec.createdDate = DateTime::now();
      dbObj.rec.objSize     = data ? data->size() : 0;

      dbObj.rec.modifiedBy.clear();
      dbObj.rec.modifiedDate.clear();

      dbObj.insert();

      outId = dbObj.rec.id;
   }
   else
   {
      if (!dbObj.trySelectOne(obj.id.toInt64()))
         throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to load Object (%s) data, the record does not exist.", obj.id.c_str());

      if (!obj.name.isEmpty())
         dbObj.rec.name = obj.name;

      dbObj.rec.mimeType    = obj.mimeType;
      dbObj.rec.sourceID    = obj.sourceID;
      dbObj.rec.sourceValue = obj.sourceValue;
      dbObj.rec.status      = obj.status;

      dbObj.rec.modifiedBy   = _user;
      dbObj.rec.modifiedDate = DateTime::now();

      dbObj.update();

      outId = dbObj.rec.id;
   }

   writeContent(&dbObj.rec, data);

   dbObj.update();

   PLOG(_pod)->info("CMAN: internalObjSave - Done(id:%s/name:%s)", outId.c_str(), obj.name.c_str());
}

String ContentManager::objSave(const bObject     &obj,
                               const MemoryBlock *data)
{
   POD_COMMIT(_pod);

   String outId;

   internalObjSave(obj, data, outId);

   COMMIT;

   return outId;
}

String ContentManager::objSave(const bObject     &obj,
                               const MemoryBlock *data,
                               const bRef         &ref)
{
   POD_COMMIT(_pod);

   String outId;

   internalObjSave(obj, data, outId);

   internalRefSave(outId, ref, true);

   COMMIT;

   return outId;
}

String ContentManager::objSave(const bObject     &obj,
                               const MemoryBlock *data,
                               const bRef::List  &refs)
{
   POD_COMMIT(_pod);

   String outId;

   internalObjSave(obj, data, outId);

   _FOREACH(bRef, r, refs)
      internalRefSave(outId, *r.obj, true);

   COMMIT;

   return outId;
}

void ContentManager::objDelete(const String &objectId)
{
   POD_COMMIT(_pod);

   validateCredentials();

   PLOG(_pod)->info("CMAN: objDelete - Start(usr:%s/sysUsr:%s/id:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str());

   dObjects dbObj(DBCON(_pod));

   dbObj.rec.id = objectId.toInt64();

   if (!dbObj.trySelectOne())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to delete Object (%s), the record does not exist.", objectId.c_str());

   dRefsDeleteByObjectId(DBCON(_pod)).exec(dbObj.rec.id);
   dCommentsDeleteByObjectId(DBCON(_pod)).exec(dbObj.rec.id);

   deleteContent(&dbObj.rec);

   dbObj.deleteOne();

   COMMIT;

   PLOG(_pod)->info("CMAN: objDelete - Done(usr:%s/sysUsr:%s/id:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str());
}

void ContentManager::internalRefSave(const String   &objectId,
                                     const bRef     &ref,
                                     const bool      isNewObj)
{
   validateCredentials();

   PLOG(_pod)->info("CMAN: internalRefSave - Start(usr:%s/sysUsr:%s/id:%s/ref:%s/val:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str(), ref.refId.c_str(), ref.value.c_str());

   dRefs dbRef(DBCON(_pod));

   convRefObj2DB(ref, dbRef.rec);

   if (dbRef.rec.objectId == 0)
      dbRef.rec.objectId = objectId.toInt64();

   if (!isNewObj && dbRef.trySelectOne())
   {
      dbRef.rec.value        = ref.value;
      dbRef.rec.modifiedBy   = _user;
      dbRef.rec.modifiedDate = DateTime::now();

      dbRef.update();
   }
   else
   {
      dbRef.rec.createdBy   = _user;
      dbRef.rec.createdDate = DateTime::now();

      dbRef.rec.modifiedBy.clear();
      dbRef.rec.modifiedDate.clear();

      dbRef.insert();
   }

   PLOG(_pod)->info("CMAN: internalRefSave - Done(objectid:%" PRId64 "/refid:%s)", dbRef.rec.objectId, dbRef.rec.id.c_str());
}

unsigned int ContentManager::objRefLoad(      bRef::List  &outRefs,
                                        const String      &objectId)
{
   POD_COMMIT(_pod);

   validateCredentials();

   PLOG(_pod)->info("CMAN: objRefLoad - Start(usr:%s/sysUsr:%s/id:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str());

   dRefsByObjectId  qry(DBCON(_pod));
   bRef            *oref;

   outRefs.clear();
   qry.exec(objectId.toInt64());

   while (qry.fetch())
   {
      oref = outRefs.append(new bRef());
      convRefDB2Obj(qry.orec, *oref);
   }

   PLOG(_pod)->info("CMAN: objRefLoad - Done(id:%s/cnt:%u)", objectId.c_str(), outRefs.count());

   return outRefs.count();
}

bRef *ContentManager::objRefLoad(const String &objectId,
                                 const String &refId)
{
   POD_COMMIT(_pod);

   validateCredentials();

   PLOG(_pod)->info("CMAN: objRefLoad - Start(usr:%s/sysUsr:%s/id:%s/refId:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str(), refId.c_str());

   dRefs       dbRec(DBCON(_pod));
   bRef::Safe  oref(new bRef());

   if (!dbRec.trySelectOne(refId, objectId.toInt64()))
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to load bRef (%s) for Object (%s), the record was not found.", refId.c_str(), objectId.c_str());

   convRefDB2Obj(dbRec.rec, *oref.obj);

   PLOG(_pod)->info("CMAN: objRefLoad - Done(id:%s/refId:%s)", objectId.c_str(), refId.c_str());

   return oref.forget();
}

void ContentManager::objRefSave(const String &objectId,
                                const String &refId,
                                const String &refValue)
{
   POD_COMMIT(_pod);

   bRef ref;

   ref.refId = refId;
   ref.value = refValue;

   internalRefSave(objectId, ref, false);

   COMMIT;
}

void ContentManager::objRefSave(const String  &objectId,
                                const bRef    &inRef)
{
   POD_COMMIT(_pod);

   internalRefSave(objectId, inRef, false);

   COMMIT;
}

void ContentManager::objRefSave(const String     &objectId,
                                const bRef::List &inRefs)
{
   POD_COMMIT(_pod);

   _FOREACH(bRef, oref, inRefs)
      internalRefSave(objectId, *oref.obj, false);

   COMMIT;
}

void ContentManager::objRefDelete(const String &objectId,
                                  const String &refId)
{
   POD_COMMIT(_pod);

   validateCredentials();

   PLOG(_pod)->info("CMAN: objRefDelete - Start(usr:%s/sysUsr:%s/id:%s/ref:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str(), refId.c_str());

   dRefs dbRef(DBCON(_pod));

   if (!dbRef.trySelectOne(refId, objectId.toInt64()))
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to delete Object Ref (%s/%s), the record does not exist.", objectId.c_str(), refId.c_str());

   dbRef.deleteOne();

   COMMIT;

   PLOG(_pod)->info("CMAN: objRefDelete - Done(usr:%s/sysUsr:%s/id:%s/ref:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str(), refId.c_str());
}

void ContentManager::internalCommentSave(const String   &objectId,
                                         const bComment &comm)
{
   validateCredentials();

   PLOG(_pod)->info("CMAN: internalCommentSave - Start(usr:%s/sysUsr:%s/id:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str());

   if (objectId.isEmpty() || comm.comment.isEmpty())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to empty comment.");

   dComments dbRec(DBCON(_pod));

   dbRec.insert(objectId.toInt64(),
                comm.comment,
                _user,
                DateTime::now());

   PLOG(_pod)->info("CMAN: internalCommentSave - Done(usr:%s/sysUsr:%s/id:%" PRId64 "/commentId:%" PRId64 ")", _user.c_str(), _sysUser.c_str(), dbRec.rec.objectId, dbRec.rec.id);
}

void ContentManager::objCommentSave(const String &objectId,
                                    const String &comment)
{
   POD_COMMIT(_pod);

   bComment tmp;

   tmp.comment = comment;

   internalCommentSave(objectId, tmp);

   COMMIT;
}

void ContentManager::objCommentSave(const String                    &objectId,
                                    const Mettle::Braze::StringList &commentList)
{
   POD_COMMIT(_pod);

   bComment tmp;

   _FOREACH(String, c, commentList)
   {
      tmp.clear();
      tmp.comment = *(c.obj);

      internalCommentSave(objectId, tmp);
   }

   COMMIT;
}

void ContentManager::objCommentDelete(const String &objectId,
                                      const String &commentId)
{
   POD_COMMIT(_pod);

   validateCredentials();

   PLOG(_pod)->info("CMAN: objCommentDelete - Start(usr:%s/sysUsr:%s/id:%s/comm:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str(), commentId.c_str());

   dComments dbCom(DBCON(_pod));

   if (!dbCom.trySelectOne(commentId.toInt64()) || dbCom.rec.objectId != objectId.toInt64())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to delete Object Comment (%s/%s), the record does not exist.", objectId.c_str(), commentId.c_str());

   dbCom.deleteOne();

   COMMIT;

   PLOG(_pod)->info("CMAN: objCommentDelete - Done(usr:%s/sysUsr:%s/id:%s/comm:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str(), commentId.c_str());
}

unsigned int ContentManager::objCommentLoad(      bComment::List &outComments,
                                            const String              &objectId)
{
   POD_COMMIT(_pod);

   validateCredentials();

   PLOG(_pod)->info("CMAN: objCommentLoad - Start(usr:%s/sysUsr:%s/id:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str());

   dCommentsByObjectId  qry(DBCON(_pod));
   bComment            *ocom;

   outComments.clear();
   qry.exec(objectId.toInt64());

   while (qry.fetch())
   {
      ocom = outComments.append(new bComment());
      convCommentDB2Obj(qry.orec, *ocom);
   }

   PLOG(_pod)->info("CMAN: objCommentLoad - Done(id:%s/cnt:%u)", objectId.c_str(), outComments.count());

   return outComments.count();
}

bComment *ContentManager::objCommentLoad(const String &objectId,
                                         const String &commentId)
{
   POD_COMMIT(_pod);

   validateCredentials();

   PLOG(_pod)->info("CMAN: objCommentLoad - Start(usr:%s/sysUsr:%s/id:%s/commId:%s)", _user.c_str(), _sysUser.c_str(), objectId.c_str(), commentId.c_str());

   dComments       dbRec(DBCON(_pod));
   bComment::Safe  ocom(new bComment());

   if (!dbRec.trySelectOne(commentId.toInt64()) || dbRec.rec.objectId != objectId.toInt64())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to load bComment (%s) for Object (%s), the record was not found.", commentId.c_str(), objectId.c_str());

   PLOG(_pod)->info("CMAN: objCommentLoad - Done(id:%s/commId:%s)", objectId.c_str(), commentId.c_str());

   return ocom.forget();
}

bool ContentManager::idNull(const Mettle::Lib::String &id) const
{
   return id.isEmpty() || id == "0";
}


unsigned int ContentManager::objSearch(      bObject::List  &outObjs,
                                       const String::List   &sourceIds,
                                       const String::List   &sourceValues,
                                       const String::List   &status,
                                       const String::List   &mimeTypes,
                                       const String::List   &refIds,
                                       const String::List   &refValues,
                                       const String         &createdBy,
                                       const DateTime       &dateCreatedFrom,
                                       const DateTime       &dateCreatedTo,
                                       const String         &modifiedBy,
                                       const DateTime       &dateModifiedFrom,
                                       const DateTime       &dateModifiedTo)
{
   POD_COMMIT(_pod);
   char dbgDateFrom[32];
   char dbgDateTo[32];

   outObjs.clear();

   PLOG(_pod)->info("CMAN: objSearch - Start(dateFrom:%s, dateTo:%s, sourceIds:%u, sourceValues:%u)", dateCreatedFrom.toString(dbgDateFrom), dateCreatedTo.toString(dbgDateTo), sourceIds.count(), sourceValues.count());

   bObject        *obj;
   dObjectsSearch  qry(DBCON(_pod));
   unsigned int    idx;
   String          jot;
   String          jot2;

   if (sourceIds.count() != 0 && sourceValues.count() != 0 && sourceIds.count() != sourceValues.count())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "objSearch: both sourceIds length and sourceValues must be the same if both are used in conjunction.");

   if (sourceIds.count() > 0 && sourceValues.count() > 0)
   {
      qry.irec.criteria += " AND (0=1 ";

      for (idx = 0; idx < sourceIds.count(); idx++)
         qry.irec.criteria += String().format(" OR (o.sourceID = '%s' AND o.sourceValue = '%s')", cleanStrCrit(sourceIds[idx], jot), cleanStrCrit(sourceValues[idx], jot2));

      qry.irec.criteria += ")";
   }
   else if (sourceIds.count() == 1)
   {
      dynStrCrit(qry.irec.criteria, "o.sourceID", sourceIds[0]);
   }
   else if (sourceIds.count() > 1)
   {
      qry.irec.criteria += String().format(" AND o.sourceID IN ('%s'", cleanStrCrit(sourceIds[0], jot));

      for (idx = 1; idx < sourceIds.count(); idx++)
         qry.irec.criteria += String().format(",'%s'", cleanStrCrit(sourceIds[idx], jot));

      qry.irec.criteria += ")";
   }
   else if (sourceValues.count() == 1)
   {
      dynStrCrit(qry.irec.criteria, "o.sourceValue", sourceValues[0]);
   }
   else if (sourceValues.count() > 1)
   {
      qry.irec.criteria += String().format(" AND o.sourceValue IN ('%s'", cleanStrCrit(sourceValues[0], jot));

      for (idx = 1; idx < sourceValues.count(); idx++)
         qry.irec.criteria += String().format(",'%s'", cleanStrCrit(sourceValues[idx], jot));

      qry.irec.criteria += ")";
   }

   if (status.count() == 1)
   {
      dynStrCrit(qry.irec.criteria, "o.status", status[0]);
   }
   else if (status.count() > 1)
   {
      qry.irec.criteria += String().format(" AND o.status IN ('%s'", cleanStrCrit(status[0], jot));

      for (idx = 1; idx < status.count(); idx++)
         qry.irec.criteria += String().format(",'%s'", cleanStrCrit(status[idx], jot));

      qry.irec.criteria += ")";
   }

   if (mimeTypes.count() == 1)
   {
      dynStrCrit(qry.irec.criteria, "o.mimeType", mimeTypes[0]);
   }
   else if (mimeTypes.count() > 1)
   {
      qry.irec.criteria += String().format(" AND o.mimeType IN ('%s'", cleanStrCrit(mimeTypes[0], jot));

      for (idx = 1; idx < mimeTypes.count(); idx++)
         qry.irec.criteria += String().format(",'%s'", cleanStrCrit(mimeTypes[idx], jot));

      qry.irec.criteria += ")";
   }

   if (!createdBy.isEmpty())
      dynStrCrit(qry.irec.criteria, "o.createdBy", createdBy);

   if (!modifiedBy.isEmpty())
      dynStrCrit(qry.irec.criteria, "o.modifiedBy", modifiedBy);

   if (!dateCreatedFrom.isNull())
      qry.irec.criteria += String().format(" AND o.createdDate >= %s", _pod->_dbCon->dateTimeToStr(&dateCreatedFrom, &jot));

   if (!dateCreatedTo.isNull())
      qry.irec.criteria += String().format(" AND o.createdDate <= %s", _pod->_dbCon->dateTimeToStr(&dateCreatedTo, &jot));

   if (!dateModifiedFrom.isNull())
      qry.irec.criteria += String().format(" AND o.modifiedDate >= %s", _pod->_dbCon->dateTimeToStr(&dateModifiedFrom, &jot));

   if (!dateModifiedTo.isNull())
      qry.irec.criteria += String().format(" AND o.modifiedDate <= %s", _pod->_dbCon->dateTimeToStr(&dateModifiedTo, &jot));

   if (refIds.count() != 0 && refValues.count() != 0 && refIds.count() != refValues.count())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "objSearch: both refIds length and refValues must be the same if both are used in conjunction.");

   if (refIds.count() != 0 || refValues.count() != 0)
   {
      qry.irec.tableJoin = " JOIN contman.Refs r ON r.objectId = o.id ";

      if (refIds.count() > 0 && refValues.count() > 0)
      {
         qry.irec.criteria += " AND (0=1 ";

         for (idx = 0; idx < refIds.count(); idx++)
            qry.irec.criteria += String().format(" OR (r.id = '%s' AND r.value = '%s')", cleanStrCrit(refIds[idx], jot), cleanStrCrit(refValues[idx], jot2));

         qry.irec.criteria += ")";
      }
      else if (refIds.count() == 1)
      {
         dynStrCrit(qry.irec.criteria, "r.id", refIds[0]);
      }
      else if (refIds.count() > 1)
      {
         qry.irec.criteria += String().format(" AND r.id IN ('%s'", cleanStrCrit(refIds[0], jot));

         for (idx = 1; idx < refIds.count(); idx++)
            qry.irec.criteria += String().format(",'%s'", cleanStrCrit(refIds[idx], jot));

         qry.irec.criteria += ")";
      }
      else if (refValues.count() == 1)
      {
         dynStrCrit(qry.irec.criteria, "r.value", refValues[0]);
      }
      else if (refValues.count() > 1)
      {
         qry.irec.criteria += String().format(" AND r.value IN ('%s'", cleanStrCrit(refValues[0], jot));

         for (idx = 1; idx < refValues.count(); idx++)
            qry.irec.criteria += String().format(",'%s'", cleanStrCrit(refValues[idx], jot));

         qry.irec.criteria += ")";
      }
   }

   if (qry.irec.criteria.isEmpty())
      throw xContentManager(__FILE__, __LINE__, SOURCE, "objSearch: No criteria has been defined.");

   PLOG(_pod)->debug("CMAN: objSearch (tableJoin:%s, criteria:%s)", qry.irec.tableJoin.c_str(), qry.irec.criteria.c_str());

   qry.exec();

   while (qry.fetch())
   {
      obj = outObjs.append(new bObject());
      convObjectDB2Obj(qry.orec, *obj);
   }

   PLOG(_pod)->info("CMAN: objSearch - Done(cnt:%u)", outObjs.count());

   return outObjs.count();
}


const char *ContentManager::cleanStrCrit(const String &source,
                                               String &jotter)

{
   if (source.length() > 2048)
      throw xContentManager(__FILE__, __LINE__, SOURCE, "cleanStrCrit: - input source to large (%d bytes)", source.length());

   jotter = source;

   if (jotter.isEmpty())
      return jotter.c_str();

   for (char *p = (char *) jotter.c_str(); *p; ++p)
      if (*p == '%' || *p == '\n' || *p == '\r' || *p == '\f' || *p == '\'')
         *p = ' ';

   jotter.replace("'", "''");

   return jotter.c_str();
}

void ContentManager::dynStrCrit(      String &crit,
                                const String &ident,
                                const String &input)
{
   if (input.isEmpty())
      return;

   String jot;

   cleanStrCrit(input, jot);

   jot.replace('*', '%');

   if (jot.find("%") >= 0)
      crit += String().format(" AND %s ILIKE '%s'", ident.c_str(), jot.c_str());
   else
      crit += String().format(" AND %s = '%s'", ident.c_str(), jot.c_str());
}


#undef SOURCE

}}
