/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_CONTMAN_CONTENTMANAGER_H_
#define __BS_CONTMAN_CONTENTMANAGER_H_

#include "bs/lib/pod.h"

#include "bs/contman/braze/contmanstruct.h"

#include "mettle/lib/memoryblock.h"

#include "mettle/braze/stdtypelists.h"

namespace BS { namespace ContMan {

namespace DB
{
   class tObjects;
}

/** \class ContentManager cotentmanager.h bs/contman/contentmanager.h
 * \brief Base class for content management.
 *
 *  This base class provides the standard set of calls to manage content. Derived
 *  objects will typically implement the write/read methods.
 */
class ContentManager
{
public:

   virtual ~ContentManager() noexcept;

   virtual void logon(
      const Mettle::Lib::String &endPoint,
      const Mettle::Lib::String &user,
      const Mettle::Lib::String &passwd,
      const Mettle::Lib::String &sysUser,
      const Mettle::Lib::String &sysPasswd);

   virtual void logoff();

   BS::ContMan::Braze::bObject *objLoad(
      const Mettle::Lib::String &objectId);

   unsigned int objLoadData(
            Mettle::Lib::MemoryBlock *dest,
      const Mettle::Lib::String      &objectId);

   Mettle::Lib::String objSave(
      const BS::ContMan::Braze::bObject &obj,
      const Mettle::Lib::MemoryBlock    *data);

   Mettle::Lib::String objSave(
      const BS::ContMan::Braze::bObject &obj,
      const Mettle::Lib::MemoryBlock    *data,
      const BS::ContMan::Braze::bRef    &ref);

   Mettle::Lib::String objSave(
      const BS::ContMan::Braze::bObject    &obj,
      const Mettle::Lib::MemoryBlock       *data,
      const BS::ContMan::Braze::bRef::List &refs);

   unsigned int objSearch(
            BS::ContMan::Braze::bObject::List  &outObjs,
      const Mettle::Lib::String::List          &sourceIds,
      const Mettle::Lib::String::List          &sourceValues,
      const Mettle::Lib::String::List          &status,
      const Mettle::Lib::String::List          &mimeTypes,
      const Mettle::Lib::String::List          &refIds,
      const Mettle::Lib::String::List          &refValues,
      const Mettle::Lib::String                &createdBy,
      const Mettle::Lib::DateTime              &dateCreatedFrom,
      const Mettle::Lib::DateTime              &dateCreatedTo,
      const Mettle::Lib::String                &modifiedBy,
      const Mettle::Lib::DateTime              &dateModifiedFrom,
      const Mettle::Lib::DateTime              &dateModifiedTo);

   void objDelete(
      const Mettle::Lib::String &objectId);

   unsigned int objRefLoad(
            BS::ContMan::Braze::bRef::List  &outRefs,
      const Mettle::Lib::String             &objectId);

   BS::ContMan::Braze::bRef *objRefLoad(
      const Mettle::Lib::String &objectId,
      const Mettle::Lib::String &refId);

   void objRefSave(
      const Mettle::Lib::String &objectId,
      const Mettle::Lib::String &refId,
      const Mettle::Lib::String &refValue);

   void objRefSave(
      const Mettle::Lib::String             &objectId,
      const BS::ContMan::Braze::bRef::List  &inRefs);

   void objRefSave(
      const Mettle::Lib::String      &objectId,
      const BS::ContMan::Braze::bRef &inRef);

   void objRefDelete(
      const Mettle::Lib::String &objectId,
      const Mettle::Lib::String &refId);

   unsigned int objCommentLoad(
            BS::ContMan::Braze::bComment::List &outComments,
      const Mettle::Lib::String                &objectId);

   BS::ContMan::Braze::bComment *objCommentLoad(
      const Mettle::Lib::String &objectId,
      const Mettle::Lib::String &commentId);

   void objCommentSave(
      const Mettle::Lib::String &objectId,
      const Mettle::Lib::String &comment);

   void objCommentSave(
      const Mettle::Lib::String        &objectId,
      const Mettle::Braze::StringList  &commentList);

   void objCommentDelete(
      const Mettle::Lib::String &objectId,
      const Mettle::Lib::String &commentId);

protected:

   BS::Lib::Pod        *_pod;
   Mettle::Lib::String  _user;
   Mettle::Lib::String  _passwd;
   Mettle::Lib::String  _sysUser;
   Mettle::Lib::String  _sysPasswd;

   virtual void validateCredentials();

   virtual void writeContent(
            BS::ContMan::DB::tObjects *obj,
      const Mettle::Lib::MemoryBlock  *data) = 0;

   virtual void readContent(
      const BS::ContMan::DB::tObjects *obj,
            Mettle::Lib::MemoryBlock  *data) = 0;

   virtual void deleteContent(
      const BS::ContMan::DB::tObjects *obj) = 0;

   bool idNull(
      const Mettle::Lib::String &id) const;

   void internalObjSave(
      const BS::ContMan::Braze::bObject  &obj,
      const Mettle::Lib::MemoryBlock     *data,
            Mettle::Lib::String          &outId);

   void internalRefSave(
      const Mettle::Lib::String       &objetId,
      const BS::ContMan::Braze::bRef  &ref,
      const bool                       isNew);

   void internalCommentSave(
      const Mettle::Lib::String           &objetId,
      const BS::ContMan::Braze::bComment  &comm);

   const char *cleanStrCrit(
      const Mettle::Lib::String &source,
            Mettle::Lib::String &jotter);

   void dynStrCrit(
            Mettle::Lib::String &crit,
      const Mettle::Lib::String &ident,
      const Mettle::Lib::String &input);

protected:

   ContentManager(
      BS::Lib::Pod *pod) noexcept;
};

}}


#endif
