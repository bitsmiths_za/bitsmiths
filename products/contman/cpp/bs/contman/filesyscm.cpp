/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "bs/contman/filesyscm.h"
#include "bs/contman/macros.h"
#include "bs/contman/xcontentmanager.h"

#include "bs/contman/db/tables/objects.h"

#include "bs/lib/podcommit.h"

#include "mettle/lib/filehandle.h"
#include "mettle/lib/filemanager.h"
#include "mettle/lib/logger.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/memoryblock.h"

#include "mettle/io/filestream.h"
#include "mettle/io/memorystream.h"

#include "mettle/db/iconnect.h"

#include <stdint.h>
#include <inttypes.h>

using namespace Mettle::Lib;
using namespace BS::Lib;
using namespace BS::ContMan::DB;

#define SOURCE "BS.ContMan.FileSysCM"

namespace BS { namespace ContMan {

static void GenerateDirPath(tObjects *obj, String &dirOnly)
{
   Date now = Date::now();

   String year, month, day, id;

   year.format("%4.4d", now.year());
   month.format("%2.2d", now.month());
   day.format("%2.2d", now.day());
   id.format("%" PRId64, obj->id);

   dirOnly.joinDirs(year.c_str(), month.c_str(), day.c_str(), 0);
   obj->dirPath.joinDirs(year.c_str(), month.c_str(), day.c_str(), id.c_str(), 0);
}

FileSysCM::FileSysCM(const char *cmPath,
                           Pod  *pod) throw()
          :ContentManager(pod)
{
   _cmPath = cmPath;
}

FileSysCM::~FileSysCM() throw()
{
}

void FileSysCM::writeContent(
         tObjects    *obj,
   const MemoryBlock *data)
{
   if (!data || data->size() < 1)
      return;

   String     fullPath;
   String     dirOnly;
   String     dirPath;
   FileHandle fh;

   if (obj->dirPath.isEmpty())
      GenerateDirPath(obj, dirOnly);

   dirPath.joinDirs(_cmPath.c_str(),  dirOnly.c_str(), 0);
   fullPath.joinDirs(_cmPath.c_str(), obj->dirPath.c_str(), 0);

   if (!FileManager::pathExists(dirPath.c_str()))
      FileManager::createDir(dirPath.c_str());

   fh.create(fullPath.c_str(), FileHandle::Binary);
   fh.write(data->_block, data->_size);
   fh.close();

   obj->objSize = data ? data->size() : 0;
}

void FileSysCM::readContent(
   const tObjects    *obj,
         MemoryBlock *data)
{
   String       fullPath;
   FileHandle   fh;

   fullPath.joinDirs(_cmPath.c_str(), obj->dirPath.c_str(), 0);

   if (!FileManager::pathExists(fullPath.c_str()))
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to load Object (%" PRId64 ") data, content is missing. - [Path '%s' not found!]", obj->id, fullPath.c_str());

   fh.open(fullPath.c_str(), FileHandle::Binary);

   Mettle::IO::FileStream   fs(&fh);
   Mettle::IO::MemoryStream ms;

   fs.poop(&ms, 0);

   data->_block  = (uint8_t*)      ms._memory;
   data->_size   = (unsigned int)  ms._sizeUsed;

   ms._memory    = 0;
   ms._sizeUsed  = 0;
}

void FileSysCM::deleteContent(
   const tObjects *obj)
{
   if (obj->dirPath.isEmpty())
      return;

   String fullPath;

   fullPath.joinDirs(_cmPath.c_str(), obj->dirPath.c_str(), 0);

   FileManager::removeFile(fullPath.c_str(), true);
}

void FileSysCM::logon(const String &endPoint,
                      const String &user,
                      const String &passwd,
                      const String &sysUser,
                      const String &sysPasswd)
{
   ContentManager::logon(endPoint, user, passwd, sysUser, sysPasswd);

   if (!FileManager::pathExists(_cmPath.c_str()))
      throw xContentManager(__FILE__, __LINE__, SOURCE, "File system contenet management root path [%s] not found!", _cmPath.c_str());
}


#undef SOURCE

}}
