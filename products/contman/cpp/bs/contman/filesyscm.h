/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_CONTMAN_FILESYSCM_H_
#define __BS_CONTMAN_FILESYSCM_H_

#include "bs/contman/contentmanager.h"

namespace BS { namespace ContMan {

class FileSysCM : public ContentManager
{
public:

   /** \brief Constructor.
    *
    *  \param cmPath The content management root file path.
    *  \param pod The pod object to use.
    */
   FileSysCM(const char *cmPath,
             BS::Lib::Pod *pod) noexcept;

   virtual ~FileSysCM() noexcept;

   void logon(const Mettle::Lib::String &endPoint,
              const Mettle::Lib::String &user,
              const Mettle::Lib::String &passwd,
              const Mettle::Lib::String &sysUser,
              const Mettle::Lib::String &sysPasswd);

protected:

   Mettle::Lib::String _cmPath;

   virtual void writeContent(
            BS::ContMan::DB::tObjects *obj,
      const Mettle::Lib::MemoryBlock  *data);

   virtual void readContent(
      const BS::ContMan::DB::tObjects *obj,
            Mettle::Lib::MemoryBlock  *data);

   virtual void deleteContent(
      const BS::ContMan::DB::tObjects *obj);
};

}}


#endif
