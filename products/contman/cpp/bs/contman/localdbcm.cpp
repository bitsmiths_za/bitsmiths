/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "bs/contman/localdbcm.h"
#include "bs/contman/macros.h"
#include "bs/contman/xcontentmanager.h"

#include "bs/contman/db/tables/objects.h"
#include "bs/contman/db/dao/contentDAO.h"

#include "bs/lib/podcommit.h"

#include "mettle/lib/logger.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/memoryblock.h"

#include "mettle/db/iconnect.h"

#include <stdint.h>
#include <inttypes.h>

using namespace Mettle::Lib;
using namespace BS::Lib;
using namespace BS::ContMan::DB;

#define SOURCE "BS.ContMan.LocalDBCM"

namespace BS { namespace ContMan {

LocalDBCM::LocalDBCM(Pod  *pod) throw()
          :ContentManager(pod)
{
}

LocalDBCM::~LocalDBCM() throw()
{
}

void LocalDBCM::writeContent(
         tObjects    *obj,
   const MemoryBlock *data)
{
   if (!data || data->size() < 1)
      return;

   dContent cont(DBCON(_pod));

   if (!cont.trySelectOne(obj->id))
   {
      cont.rec.objectId       = obj->id;
      cont.rec.content.eat((MemoryBlock *)data);
      cont.insert();
   }
   else
   {
      cont.rec.content.eat((MemoryBlock *)data);
      cont.update();
   }
}

void LocalDBCM::readContent(
   const tObjects     *obj,
         MemoryBlock  *data)
{
   dContent cont(DBCON(_pod));

   if (!cont.trySelectOne(obj->id))
      throw xContentManager(__FILE__, __LINE__, SOURCE, "Unabled to load Object (%" PRId64 ") data, content is missing.", obj->id);

   data->eat(&cont.rec.content);
}

void LocalDBCM::deleteContent(
   const tObjects *obj)
{
   dContent cont(DBCON(_pod));

   cont.deleteOne(obj->id);
}


#undef SOURCE

}}
