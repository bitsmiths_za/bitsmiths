/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_CONTMAN_MACROS_H_
#define __BS_CONTMAN_MACROS_H_

#define DBCON(p)                       p->_dbCon
#define POD_COMMIT(p)                  BS::Lib::PodCommit _mcPodCommit(p)
#define COMMIT                         _mcPodCommit.commit()
#define MAKE_DB_LOCK(time, retrys)     Mettle::DB::DBLock _dbLock(time, retrys)
#define DEFAULT_DB_LOCK                Mettle::DB::DBLock _dbLock(100, 20)
#define DB_LOCK                        &_dbLock

#define PLOG(p)                        p->_log->trace(__FILE__, __LINE__)
#define PLOGSPOTX(p, x)                p->_log->spot(__FILE__, __LINE__, x)
#define PLOGSPOT(p)                    p->_log->spot(__FILE__, __LINE__)

#define STR_UNKNOWN  "<Unknown>"

#endif
