/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "bs/contman/server.h"

#include "bs/contman/contentmanager.h"

#include "mettle/lib/common.h"
#include "mettle/lib/logger.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/string.h"

#include "mettle/braze/stdtypelists.h"

#define MODULE   "BS.ContMan.Server"

using namespace Mettle::Lib;
using namespace Mettle::IO;
using namespace Mettle::Braze;

using namespace BS::ContMan::Braze;

namespace BS { namespace ContMan {

#define CMAN_LOGIN(cred) _contMan->logon(cred.endPoint, cred.user, cred.passwd, cred.sysUser, cred.sysPasswd)


Server::Server(BS::Lib::Pod              *pod,
               Mettle::Braze::ITransport *transPort)
{
   _transPort    = transPort;
   _pod          = pod;
   _contMan      = 0;
}

Server::~Server()
{
   _destruct();
}

void Server::_destruct()
{
   _DELETE(_contMan)
}

void Server::_slackTimeHandler()
{
}

void Server::_construct()
{
   _destruct();

   _contMan = _initContentManager();
}

ContentManager *Server::_initContentManager()
{
   return 0;
}

bool Server::_shutdownServer()
{
   return false;
}

bool Server::ping()
{
   String addr;

   _pod->_log->info("ping (%s)", _transPort->clientAddress(&addr)->c_str());

   return true;
}

bObject *Server::objLoad(/* i  */ const bCredentials             &cred,
                         /* i  */ const Mettle::Lib::String      &objectId)
{
   CMAN_LOGIN(cred);
   return _contMan->objLoad(objectId);
}

Mettle::Lib::MemoryBlock *Server::objLoadData(/* i  */ const bCredentials             &cred,
                                              /* i  */ const Mettle::Lib::String      &objectId)
{
   MemoryBlock::Safe mb(new MemoryBlock());

   CMAN_LOGIN(cred);
   _contMan->objLoadData(mb.obj, objectId);

   return mb.forget();
}

Mettle::Lib::String *Server::objSave(/* i  */ const bCredentials             &cred,
                                     /* i  */ const bObject                  &obj,
                                     /* i  */ const Mettle::Lib::MemoryBlock &cont)
{
   String::Safe objid(new String());

   CMAN_LOGIN(cred);
   *objid.obj = _contMan->objSave(obj, &cont);

   return objid.forget();
}

Mettle::Lib::String *Server::objSaveRefs(/* i  */ const bCredentials             &cred,
                                         /* i  */ const bObject                  &obj,
                                         /* i  */ const Mettle::Lib::MemoryBlock &cont,
                                         /* i  */ const bRef::List               &refs)
{
   String::Safe objid(new String());

   CMAN_LOGIN(cred);
   *objid.obj = _contMan->objSave(obj, &cont, refs);

   return objid.forget();
}

bObject::List *Server::objSearch(/* i  */ const bCredentials              &cred,
                                 /* i  */ const Mettle::Braze::StringList &sourceIds,
                                 /* i  */ const Mettle::Braze::StringList &sourceValues,
                                 /* i  */ const Mettle::Braze::StringList &status,
                                 /* i  */ const Mettle::Braze::StringList &mimeTypes,
                                 /* i  */ const Mettle::Braze::StringList &refIds,
                                 /* i  */ const Mettle::Braze::StringList &refValues,
                                 /* i  */ const Mettle::Lib::String      &createdBy,
                                 /* i  */ const Mettle::Lib::DateTime    &dateCreatedFrom,
                                 /* i  */ const Mettle::Lib::DateTime    &dateCreatedTo,
                                 /* i  */ const Mettle::Lib::String      &modifiedBy,
                                 /* i  */ const Mettle::Lib::DateTime    &dateModifiedFrom,
                                 /* i  */ const Mettle::Lib::DateTime    &dateModifiedTo)
{
   bObject::List::Safe res(new bObject::List());

   CMAN_LOGIN(cred);

   String::List apiSourceIds;
   String::List apiSourceValues;
   String::List apiStatus;
   String::List apiMimeTypes;
   String::List apiRefIds;
   String::List apiRefValues;

   _FOREACH(String, s, sourceIds)
      apiSourceIds.append(new String(*s.obj));

   _FOREACH(String, s, sourceValues)
      apiSourceValues.append(new String(*s.obj));

   _FOREACH(String, s, status)
      apiStatus.append(new String(*s.obj));

   _FOREACH(String, s, mimeTypes)
      apiMimeTypes.append(new String(*s.obj));

   _FOREACH(String, s, refIds)
      apiRefIds.append(new String(*s.obj));

   _FOREACH(String, s, refValues)
      apiRefValues.append(new String(*s.obj));

   _contMan->objSearch(
      *res.obj,
      apiSourceIds,
      apiSourceValues,
      apiStatus,
      apiMimeTypes,
      apiRefIds,
      apiRefValues,
      createdBy,
      dateCreatedFrom,
      dateCreatedTo,
      modifiedBy,
      dateModifiedFrom,
      dateModifiedTo);

   return res.forget();
}

void Server::objDelete(/* i  */ const bCredentials             &cred,
                       /* i  */ const Mettle::Lib::String      &objectId)
{
   CMAN_LOGIN(cred);
   _contMan->objDelete(objectId);
}

bRef::List *Server::refLoadAll(/* i  */ const bCredentials             &cred,
                              /* i  */ const Mettle::Lib::String      &objectId)
{
   bRef::List::Safe res(new bRef::List());

   CMAN_LOGIN(cred);
   _contMan->objRefLoad(*res.obj, objectId);

   return res.forget();
}

bRef *Server::refLoad(/* i  */ const bCredentials             &cred,
                      /* i  */ const Mettle::Lib::String      &objectId,
                      /* i  */ const Mettle::Lib::String      &refId)
{
   CMAN_LOGIN(cred);
   return _contMan->objRefLoad(objectId, refId);
}

void Server::refSave(/* i  */ const bCredentials             &cred,
                     /* i  */ const Mettle::Lib::String      &objectId,
                     /* i  */ const Mettle::Lib::String      &refId,
                     /* i  */ const Mettle::Lib::String      &refValue)
{
   CMAN_LOGIN(cred);
   return _contMan->objRefSave(objectId, refId, refValue);
}

void Server::refSaveList(/* i  */ const bCredentials             &cred,
                         /* i  */ const Mettle::Lib::String      &objectId,
                         /* i  */ const bRef::List               &refs)
{
   CMAN_LOGIN(cred);
   return _contMan->objRefSave(objectId, refs);
}

void Server::refDelete(/* i  */ const bCredentials             &cred,
                       /* i  */ const Mettle::Lib::String      &objectId,
                       /* i  */ const Mettle::Lib::String      &refId)
{
   CMAN_LOGIN(cred);
   return _contMan->objRefDelete(objectId, refId);
}

bComment::List *Server::commentLoadAll(/* i  */ const bCredentials             &cred,
                                       /* i  */ const Mettle::Lib::String      &objectId)
{
   bComment::List::Safe res(new bComment::List());

   CMAN_LOGIN(cred);
   _contMan->objCommentLoad(*res.obj, objectId);

   return res.forget();
}

bComment *Server::commentLoad(/* i  */ const bCredentials             &cred,
                              /* i  */ const Mettle::Lib::String      &objectId,
                              /* i  */ const Mettle::Lib::String      &commentId)
{
   CMAN_LOGIN(cred);
   return _contMan->objCommentLoad(objectId, commentId);
}

void Server::commentSave(/* i  */ const bCredentials             &cred,
                         /* i  */ const Mettle::Lib::String      &objectId,
                         /* i  */ const Mettle::Lib::String      &comment)
{
   CMAN_LOGIN(cred);
   _contMan->objCommentSave(objectId, comment);
}

void Server::commentSaveList(/* i  */ const bCredentials              &cred,
                             /* i  */ const Mettle::Lib::String       &objectId,
                             /* i  */ const Mettle::Braze::StringList &commentList)
{
   CMAN_LOGIN(cred);
   _contMan->objCommentSave(objectId, commentList);
}

void Server::commentDelete(/* i  */ const bCredentials             &cred,
                           /* i  */ const Mettle::Lib::String      &objectId,
                           /* i  */ const Mettle::Lib::String      &commentId)
{
   CMAN_LOGIN(cred);
   _contMan->objCommentDelete(objectId, commentId);
}

#undef MODULE

}}
