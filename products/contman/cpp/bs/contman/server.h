/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_MONITOR_SERVER_H_
#define __BS_MONITOR_SERVER_H_

#include "bs/lib/iprovider.h"
#include "bs/lib/pod.h"

#include "bs/contman/braze/sicontman.h"

#include "mettle/braze/itransport.h"
#include "mettle/braze/stdtypelists.h"

#include "mettle/lib/memoryblock.h"

namespace BS { namespace ContMan {

class ContentManager;

class Server : public BS::ContMan::Braze::siContMan
{
public:

   Server(BS::Lib::Pod               *pod,
          Mettle::Braze::ITransport  *transPort);

   ~Server();

   void _construct();

   void _destruct();

   bool _shutdownServer();

   void _slackTimeHandler();

   bool ping();

   BS::ContMan::Braze::bObject *objLoad(
      /* i  */ const BS::ContMan::Braze::bCredentials  &cred,
      /* i  */ const Mettle::Lib::String               &objectId);

   Mettle::Lib::MemoryBlock *objLoadData(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId);

   Mettle::Lib::String *objSave(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const BS::ContMan::Braze::bObject      &obj,
      /* i  */ const Mettle::Lib::MemoryBlock         &cont);

   Mettle::Lib::String *objSaveRefs(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const BS::ContMan::Braze::bObject      &obj,
      /* i  */ const Mettle::Lib::MemoryBlock         &cont,
      /* i  */ const BS::ContMan::Braze::bRef::List   &refs);

   BS::ContMan::Braze::bObject::List *objSearch(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Braze::StringList        &sourceIds,
      /* i  */ const Mettle::Braze::StringList        &sourceValues,
      /* i  */ const Mettle::Braze::StringList        &status,
      /* i  */ const Mettle::Braze::StringList        &mimeTypes,
      /* i  */ const Mettle::Braze::StringList        &refIds,
      /* i  */ const Mettle::Braze::StringList        &refValues,
      /* i  */ const Mettle::Lib::String              &createdBy,
      /* i  */ const Mettle::Lib::DateTime            &dateCreatedFrom,
      /* i  */ const Mettle::Lib::DateTime            &dateCreatedTo,
      /* i  */ const Mettle::Lib::String              &modifiedBy,
      /* i  */ const Mettle::Lib::DateTime            &dateModifiedFrom,
      /* i  */ const Mettle::Lib::DateTime            &dateModifiedTo);

   void objDelete(
      /* i  */ const BS::ContMan::Braze::bCredentials   &cred,
      /* i  */ const Mettle::Lib::String                &objectId);

   BS::ContMan::Braze::bRef::List *refLoadAll(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId);

   BS::ContMan::Braze::bRef *refLoad(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId,
      /* i  */ const Mettle::Lib::String              &refId);

   void refSave(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId,
      /* i  */ const Mettle::Lib::String              &refId,
      /* i  */ const Mettle::Lib::String              &refValue);

   void refSaveList(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId,
      /* i  */ const BS::ContMan::Braze::bRef::List   &refs);

   void refDelete(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId,
      /* i  */ const Mettle::Lib::String              &refId);

   BS::ContMan::Braze::bComment::List *commentLoadAll(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId);

   BS::ContMan::Braze::bComment *commentLoad(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId,
      /* i  */ const Mettle::Lib::String              &commentId);

   void commentSave(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId,
      /* i  */ const Mettle::Lib::String              &comment);

   void commentSaveList(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId,
      /* i  */ const Mettle::Braze::StringList        &commentList);

   void commentDelete(
      /* i  */ const BS::ContMan::Braze::bCredentials &cred,
      /* i  */ const Mettle::Lib::String              &objectId,
      /* i  */ const Mettle::Lib::String              &commentId);

protected:

   virtual ContentManager *_initContentManager();

   Mettle::Braze::ITransport   *_transPort;
   BS::Lib::Pod                *_pod;
   ContentManager              *_contMan;
};

}}

#endif
