# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.Braze'

    def _Name(self):
        return 'ContMan'

    def _Object(self, brazeproj):
        import mettlegen

        proj = mettlegen.BrazeProject(
            brazeproj,
            self._Name(),
            self._Version(),
            'named')

        # ----------------------------------
        # Language Generators
        # ----------------------------------
        proj.generators['C++'].init(True, r'../cpp/bs/contman/braze', {
            'braze.struct'        : 'b|',
            'braze.couplet'       : '|_Couplet',
            'server.marshaler'    : 'sm|',
            'server.interface'    : 'si|',
            'client.marshaler'    : 'cm|',
            'client.interface'    : 'ci|',
            'namespace'           : 'BS.ContMan.Braze',
            'includepath.tables'  : 'bs|contman|db|tables',
            'includepath.braze'   : 'bs|contman|braze',
            'casing' : {
                'class'  : 'pascal',
                'method' : 'camel',
                'member' : 'camel',
                'file'   : 'lower',
            },
        })

        proj.generators['Python3'].init(True, '../py/bs_contman/braze', {
            'braze.struct'       : 'b|',
            'braze.couplet'      : '|_Couplet',
            'client.marshaler'   : '|ClientMarshaler',
            'client.interface'   : '|ClientInterface',
            'client.serverImpl'  : '|ServerImpl',
            'server.marshaler'   : '|ServerMarshaler',
            'server.interface'   : '|ServerInterface',
            'namespace'          : 'bs_contman.braze',
            'casing' : {
                'class'  : 'pascal',
                'method' : 'camel',
                'member' : 'camel',
            },
        })

        # ----------------------------------
        # Structs
        # ----------------------------------
        proj.struct('Object', fields = (
            ('id',           'string'),
            ('name',         'string'),
            ('mimeType',     'string'),
            ('sourceID',     'string'),
            ('sourceValue',  'string'),
            ('objSize',      'int32'),
            ('status',       'string'),
            ('createdBy',    'string'),
            ('createdDate',  'datetime'),
            ('modifiedBy',   'string'),
            ('modifiedDate', 'datetime'),
        ))

        proj.struct('Ref', fields = (
            ('objectId',     'string'),
            ('refId',        'string'),
            ('value',        'string'),
            ('createdBy',    'string'),
            ('createdDate',  'datetime'),
            ('modifiedBy',   'string'),
            ('modifiedDate', 'datetime'),
        ))

        proj.struct('Comment', fields = (
            ('objectId',    'string'),
            ('commentId',   'string'),
            ('comment',     'string'),
            ('createdBy',   'string'),
            ('createdDate', 'datetime'),
        ))

        proj.struct('Credentials', fields = (
            ('endPoint',  'string'),
            ('user',      'string'),
            ('passwd',    'string'),
            ('sysUser',   'string'),
            ('sysPasswd', 'string'),
        ))


        # ----------------------------------
        # Remote Calls
        # ----------------------------------
        proj.remoteCall('ping', returns = ('', 'bool'))

        proj.remoteCall(
            'objLoad',
            returns = ('', 'braze:Object'),
            argsIn  = (
               ('cred',       'braze:Credentials'),
               ('objectId',   'string'),
            )
        )

        proj.remoteCall('objLoadData',
            returns = ('', 'memblock'),
            argsIn  = (
               ('cred',       'braze:Credentials'),
               ('objectId',   'string'),
        ))

        proj.remoteCall('objSave',
            returns = ('', 'string'),
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('obj',      'braze:Object'),
               ('cont',     'memblock'),
        ))

        proj.remoteCall('objSaveRefs',
            returns = ('', 'string'),
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('obj',      'braze:Object'),
               ('cont',     'memblock'),
               ('refList',  'braze:Ref[]'),
        ))

        proj.remoteCall('objSearch',
            returns = ('', 'braze:Object[]'),
            argsIn  = (
                ('cred',             'braze:Credentials'),
                ('sourceIds',        'string[]'),
                ('sourceValues',     'string[]'),
                ('status',           'string[]'),
                ('mimeTypes',        'string[]'),
                ('refIds',           'string[]'),
                ('refValues',        'string[]'),
                ('createdBy',        'string'),
                ('dateCreatedFrom',  'datetime'),
                ('dateCreatedTo',    'datetime'),
                ('modifiedBy',       'string'),
                ('dateModifiedFrom', 'datetime'),
                ('dateModifiedTo',   'datetime'),
        ))

        proj.remoteCall('objDelete',
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('objectId', 'string'),
        ))

        proj.remoteCall('refLoadAll',
            returns = ('', 'braze:Ref[]'),
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('objectId', 'string'),
        ))

        proj.remoteCall('refLoad',
            returns = ('', 'braze:Ref'),
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('objectId', 'string'),
               ('refId',    'string'),
        ))

        proj.remoteCall('refSave',
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('objectId', 'string'),
               ('refId',    'string'),
               ('refValue', 'string'),
        ))

        proj.remoteCall('refSaveList',
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('objectId', 'string'),
               ('refList',  'braze:Ref[]'),
        ))

        proj.remoteCall('refDelete',
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('objectId', 'string'),
               ('refId',    'string'),
        ))

        proj.remoteCall('commentLoadAll',
            returns = ('', 'braze:Comment[]'),
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('objectId', 'string'),
        ))

        proj.remoteCall('commentLoad',
            returns = ('', 'braze:Comment'),
            argsIn  = (
               ('cred',      'braze:Credentials'),
               ('objectId',  'string'),
               ('commentId', 'string'),
        ))

        proj.remoteCall('commentSave',
            argsIn  = (
               ('cred',     'braze:Credentials'),
               ('objectId', 'string'),
               ('comment',  'string'),
        ))

        proj.remoteCall('commentSaveList',
            argsIn  = (
               ('cred',        'braze:Credentials'),
               ('objectId',    'string'),
               ('commentList', 'string[]'),
        ))

        proj.remoteCall('commentDelete',
            argsIn  = (
               ('cred',      'braze:Credentials'),
               ('objectId',  'string'),
               ('commentId', 'string'),
        ))

        return proj


if __name__ == '__main__':
    bp = _MettleObject()

    print ('Project Type = %s' % bp._MettleFileType())
    print ('Project Name = %s' % bp._Name())
    print ('Version      = %f' % bp._Version())
