# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import time

from bs_contman.xContentManager import xContentManager

from bs_contman.braze.ContManClientInterface import ContManClientInterface


class ContManServerImpl(ContManClientInterface):
    """
    The contment management server implementation.
    """

    MODULE = '[ContManServerImpl]'

    def __init__(self, pod, cm):
        """
        Constructor.

        :param pod: The pod to use.
        :param cm: The content manager object to use.
        """
        self._shutdown = False
        self._pod      = pod
        self._cm       = cm


    def _shutdownServer(self):
        return self._shutdown


    def _slackTimeHandler(self):
        pass


    def _construct(self):
        self._destruct()


    def _destruct(self):
        pass


    def _credLogin(self, cred):
        """
        Log into the server with credential.

        :param cred: The credential to use.
        :return: The contment manager object for convenience.
        """
        self._cm.logon(cred.endPoint,
                       cred.user,
                       cred.passwd,
                       cred.sysUser,
                       cred.sysPasswd)

        return self._cm


    # ------------------------------------------------------------------------
    # SERVER INTERFACE METHODS
    # ------------------------------------------------------------------------


    def ping(self):
        """
        Ping method to test is server is up.

        :return: Always return true
        """
        time.sleep(0.5)
        return True


    def objLoad(self, cred, objectId):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :return: bs_contman.braze.bObject
        """
        return self._credLogin(cred).objLoad(objectId)


    def objLoadData(self, cred, objectId):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :return: byte[]
        """
        return self._credLogin(cred).objLoadData(objectId)


    def objSave(self, cred, obj, cont):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param obj: bs_contman.braze.bObject, input
        :param cont: byte[], input
        :return: string
        """
        return self._credLogin(cred).objSave(obj, cont)


    def objSaveRefs(self, cred, obj, cont, refList):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param obj: bs_contman.braze.bObject, input
        :param cont: byte[], input
        :param refList: bs_contman.braze.bRef.List, input
        :return: string
        """
        return self._credLogin(cred).objSave(obj, cont, refList)


    def objSearch(
          self,
          cred,
          sourceIds,
          sourceValues,
          status,
          mimeTypes,
          refIds,
          refValues,
          createdBy,
          dateCreatedFrom,
          dateCreatedTo,
          modifiedBy,
          dateModifiedFrom,
          dateModifiedTo):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param sourceIds: mettle.braze.StringList, input
        :param sourceValues: mettle.braze.StringList, input
        :param status: mettle.braze.StringList, input
        :param mimeTypes: mettle.braze.StringList, input
        :param refIds: mettle.braze.StringList, input
        :param refValues: mettle.braze.StringList, input
        :param createdBy: string, input
        :param dateCreatedFrom: datetime.datetime, input
        :param dateCreatedTo: datetime.datetime, input
        :param modifiedBy: string, input
        :param dateModifiedFrom: datetime.datetime, input
        :param dateModifiedTo: datetime.datetime, input
        :return: bs_contman.braze.bObject.List
        """
        sources = {}
        refs    = {}

        if sourceIds and sourceValues:
            if len(sourceIds) != sourceValues:
                raise xContentManager('Length of sourceIds and sourceValues must equal.')

            for idx in range(len(sourceIds)):
                sources[sourceIds[idx]] = sourceValues[idx]

        if refIds and refValues:
            if len(refIds) != refValues:
                raise xContentManager('Length of refIds and refValues must equal.')

            for idx in range(len(refIds)):
                refs[refIds[idx]] = refValues[idx]

        return self._credLogin(cred).objSearch(
            sources,
            refs,
            sourceIds,
            sourceValues,
            refIds,
            refValues,
            status,
            mimeTypes,
            createdBy,
            dateCreatedFrom,
            dateCreatedTo,
            modifiedBy,
            dateModifiedFrom,
            dateModifiedTo)


    def objDelete(self, cred, objectId):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        """
        self._credLogin(cred).objDelete(objectId)


    def refLoadAll(self, cred, objectId):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :return: bs_contman.braze.bRef.List
        """
        return self._credLogin(cred).refLoadAll(objectId)


    def refLoad(self, cred, objectId, refId):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :param refId: string, input
        :return: bs_contman.braze.bRef
        """
        return self._credLogin(cred).refLoad(objectId, refId)


    def refSave(self, cred, objectId, refId, refValue):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :param refId: string, input
        :param refValue: string, input
        """
        self._credLogin(cred).refSave(objectId, refId, refValue)


    def refSaveList(self, cred, objectId, refList):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :param refs: bs_contman.braze.bRef.List, input
        """
        self._credLogin(cred).refSaveList(objectId, refList)


    def refDelete(self, cred, objectId, refId):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :param refId: string, input
        """
        self._credLogin(cred).refDelete(objectId, refId)


    def commentLoadAll(self, cred, objectId):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :return: bs_contman.braze.bComment.List
        """
        return self._credLogin(cred).commentLoadAll(objectId)


    def commentLoad(self, cred, objectId, commentId):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :param commentId: string, input
        :return: bs_contman.braze.bComment
        """
        return self._impl.commentLoad(objectId, commentId)


    def commentSave(self, cred, objectId, comment):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :param comment: string, input
        """
        self._credLogin(cred).commentSave(objectId, comment)


    def commentSaveList(self, cred, objectId, comments):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :param comments: bs_contman.braze.bComment.List, input
        """
        self._credLogin(cred).commentSaveList(objectId, comments)


    def commentDelete(self, cred, objectId, commentId):
        """
        :param cred: bs_contman.braze.bCredentials, input
        :param objectId: string, input
        :param commentId: string, input
        """
        self._credLogin(cred).commentDelete(objectId, commentId)
