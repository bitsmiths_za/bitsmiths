# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime

from bs_lib.AutoTransaction import AutoTransaction
from bs_lib                 import QueryBuilder

from bs_contman.braze.bComment  import bComment
from bs_contman.braze.bObject   import bObject
from bs_contman.braze.bRef      import bRef

from bs_contman.db.tables.tComments import tComments
from bs_contman.db.tables.tObjects  import tObjects
from bs_contman.db.tables.tRefs     import tRefs

from bs_contman.xContentManager import xContentManager


class ContentManager:
    """
    This is the base abstract content management class which will
    must be overloaded an interfaced to actual content management
    systems.
    """
    STR_UNKNOWN = '<Unknown>'

    def __init__(self):
        """
        Constructor.
        """
        self._pod = None
        self._log = None

        self._endPoint  = ''
        self._user      = ''
        self._passwd    = ''
        self._sysUsr    = ''
        self._sysPasswd = ''


    def __del__(self):
        """
        Destructor.
        """
        self.destroy()


    def initialize(self, pod, endPoint):
        """
        Virtual method to initialize the content management object.

        :param pod: The pod to use.
        :param endPoint: The end point for the content management.
        """
        self._pod      = pod
        self._log      = pod._log
        self._endPoint = endPoint


    def destroy(self):
        """
        Virtual method to destroy the content management object.
        """
        self._pod = None


    def id(self) -> str:
        """
        Pure virtual method, the identifier of the content management overload.

        :return: The overload name.
        """
        raise Exception('id() not implemented.')


    def name(self) -> str:
        """
        Pure virtual method, the name of the content management overload.

        :return: The overload name.
        """
        raise Exception('name() not implemented.')


    def logon(self,
              endPoint: str,
              usr: str,
              passwd: str,
              sysUsr=None,
              sysPasswd=None):
        """
        Virtual method that logs onto the system.

        :param endPoint: The connection end point string/url.
        :param usr: The user who is logging on.
        :param passwd: The password for user.
        :param sysUsr: Optionally give the system user loggin.
        :param sysPasswd: Optionally give the system user password.
        """
        self._log.debug("logon - start [cm_id:%s, usr:%s, sysUsr:%s]" % (
            self.id(), usr, str(sysUsr)))

        self.logoff()

        self._usr       = usr
        self._passwd    = passwd
        self._sysUer    = sysUsr
        self._sysPasswd = sysPasswd

        self._log.debug("logon - done [usr:%s, sysUsr:%s]" % (self._usr, str(self._sysUsr)))


    def logoff(self):
        """
        Log off of the contment management sysem.
        """
        self._log.debug("logoff - start [usr:%s, sysUsr:%s]" % (self._usr, str(self._sysUsr)))

        self._usr      = ''
        self._passwd    = ''
        self._sysUsr   = ''
        self._sysPasswd = ''

        self._log.debug("logoff - done [usr:%s, sysUsr:%s]" % (self._usr, str(self._sysUsr)))


    def objLoad(self, objectId: str) -> bObject:
        """
        Loads an object by its identifier.

        :param objectId: The object identifier to load.
        :return: The braze object loaded.
        """
        with AutoTransaction(self._pod):
            self._validateCredentials()

            self._log.info("objLoad - start [usr:%s, sysUsr:%s, id:%s]" % (
                self._user, self._sysUser, objectId))

            obj = self._pod.dao.dObjects(self._pod.dbcon)

            if not obj.trySelectOneDeft(int(objectId)):
                raise xContentManager("Object not found [%s]" % objectId)

            return self._convertObjectDB2Braze(obj.rec)


    def objSave(self, obj: bObject, data: bytes, refList: list = None) -> str:
        """
        Save an object, effectively and upsert.

        :param obj: The braze object data to save.
        :param data: The byte data to save.
        :param refs: Optionally give a list bRef object to save.
        :return: The object identifier.
        """
        with AutoTransaction(self._pod) as at:
            self._validateCredentials()

            outId = self._internalObjSave(obj, data)

            if refList:
                for refRec in refList:
                    self._internalRefSave(outId, refRec, True)

            at.commit()

            return outId


    def objDelete(self, objectId: str):
        """
        Delete an object and all its associated data.

        :param objectId: The object identifier to delete.
        """
        with AutoTransaction(self._pod) as at:
            self._validateCredentials()

            self._log.info("objDelete - start [usr:%s, sysUsr:%s, id:%s]" % (
                str(self._user), str(self._sysUser), objectId))

            dbObj = self._pod.dao.dObjects(self._pod.dbcon)

            if not dbObj.try_select_one_deft(int(objectId)):
                raise xContentManager('Unabled to delete Object, record not found. [objectId:%s]' % (
                    objectId))

            self._pod.dao.dRefsDeleteByObjectId(self._pod.dbcon).exec_deft(dbObj.rec.id)
            self._pod.dao.dCommentsDeleteByObjectId(self._pod.dbcon).exec_deft(dbObj.rec.id)

            self._deleteContent(dbObj.rec)
            dbObj.deleteOne()
            at.commit()

            self._log.info("objDelete - done [id:%s]" % (objectId))


    def objLoadData(self, objectId: str) -> bytes:
        """
        Loads the object data.

        :param objectId: The object identifier of the data to load.
        :return: The object byte data.
        """
        with AutoTransaction(self._pod):
            self._validateCredentials()

            self._log.info("objLoadData - start [usr:%s, sysUsr:%s, id:%s]" % (
                self._user, self._sysUser, objectId))

            obj = self._pod.dao.dObjects(self._pod.dbcon)

            if not obj.trySelectOneDeft(int(objectId)):
                raise xContentManager("Object not found [%s]" % objectId)

            res = self._readContent(obj.rec.id)

            self._log.info("objLoadData - done [id:%s, size:%d])" % (
                objectId, len(res)))

            return res


    def objRefLoad(self, objectId: str, refId: str = None) -> bRef.List:
        """
        Loads all the references for an object.

        :param objectId: The object identifier to load for.
        :param refId: Optionally give the ref to specficially load for.
        :return: The list of loaded references.
        """
        with AutoTransaction(self._pod):
            self._validateCredentials()

            self._log.info("objRefLoad - start [usr:%s, sysUsr:%s, id:%s, refId:%s]" % (
                str(self._user), str(self._sysUser), objectId, str(refId)))

            res = bRef.List()

            if refId:
                dbRec = self._pod.dao.dRefs(self._pod.dbcon)

                if not dbRec.try_select_one_deft(refId, int(objectId)):
                    raise xContentManager("Reference not found. [refId:%s, objectId:%s]" % (
                        refId, objectId))

                res.append(self._convRefObjDB2Braze(dbRec.rec))

            else:
                qry = self._pod.dao.dRefsByObjectId(self._pod.dbcon)

                qry.exec_deft(int(objectId))

                while qry.fetch():
                    res.append(self._convRefObjDB2Braze(qry.orec))

            self._log.info("objRefLoad - done [id:%s, refId:%s, cnt:%d]" % (
                objectId, str(refId), len(res)))

            return res


    def objRefSave(self, objectId: str, refId: str, refValue: str):
        """
        Save an object reference, effectively an upsert.

        :param objectId: The object identifer to save the refence against.
        :param refId: The reference identifier.
        :param refValue: The reference value.
        """
        with AutoTransaction(self._pod) as at:
            self._validateCredentials()

            ref = bRef()

            ref.refId = refId
            ref.value = refValue

            self._internalRefSave(objectId, ref, False)

            at.commit()


    def objRefSaveList(self, objectId: str, refList: bRef.List):
        """
        Save an object reference, effectively an upsert.

        :param objectId: The object identifer to save the refence against.
        :param refList: List of reference to save.
        """
        with AutoTransaction(self._pod) as at:
            self._validateCredentials()

            for rec in refList:
                self._internalRefSave(objectId, rec, False)

            at.commit()


    def objRefDelete(self, objectId: str, refId: str):
        """
        Delete an object reference.

        :param objectId: The object identifer to save the refence against.
        :param refId: The reference identifier.
        """
        with AutoTransaction(self._pod) as at:
            self._validateCredentials()

            self._log.info("objRefDelete - start [id:%s, ref:%s]" % (objectId, refId))

            dbRef = self._pod.dao.dRefs(self._pod.dbcon)

            if not dbRef.try_select_one_deft(refId, int(objectId)):
                raise xContentManager("Object Ref not found. [objectId:%s, refId:%s]" % (
                    objectId, refId))

            dbRef.delete_one()

            self._log.info("objRefDelete - done [id:%s, ref:%s]" % (objectId, refId))
            at.commit()


    def objCommentLoad(self, objectId: str, commentId: str = None) -> bComment.List:
        """
        Load all or a single comment about an object.

        :param objectId: The object identifier to load for.
        :param commentId: Optionally give the comment to specficially load for.
        :return: The list of loaded comments.
        """
        with AutoTransaction(self._pod):
            self._validateCredentials()

            self._log.info("objCommentLoad - start [usr:%s, sysUsr:%s, id:%s, commentId:%s]" % (
                str(self._user), str(self._sysUser), objectId, str(commentId)))

            res = bComment.List()

            if commentId:
                dbRec = self._pod.dao.dComments(self._pod.dbcon)

                if dbRec.try_select_one_deft(int(commentId)):
                    if dbRec.rec.objectId != int(objectId):
                        raise xContentManager('Comment does not belong to object. [commentId:%s, objectId:%s]' % (
                            commentId, objectId))

                    res.append(self._convCommentDB2Braze(dbRec.rec))
            else:
                qry = self._pod.dao.dCommentsByObjectId(self._pod.dbcon)

                qry.exec_deft(int(objectId))

                while qry.fetch():
                    res.append(self._convCommentDB2Braze(qry.orec))

            self._log.info("objCommentLoad - done [id:%s, commentId:%s, cnt:%d]" % (
                objectId, str(commentId), len(res)))

            return res


    def objCommentSave(self, objectId: str, comment: str) -> str:
        """
        Save a comment against an object.

        :param objectId: The object identifier.
        :param comment: The comment to save.
        :return str: The comment id.
        """
        with AutoTransaction(self._pod) as at:
            self._validateCredentials()

            res = self._internalCommentSave(objectId, comment)

            at.commit()

            return res


    def objCommentSaveList(self, objectId: str, commentList) -> str:
        """
        Save a comment against an object.

        :param objectId: The object identifier.
        :param commentList: The comment list to save.
        :return str: The comment id.
        """
        with AutoTransaction(self._pod) as at:
            self._validateCredentials()

            for comm in commentList:
                self._internalCommentSave(objectId, comm)

            at.commit()


    def objCommentDelete(self, objectId: str, commentId: str):
        """
        Delete an object comment.

        :param objectId: The object identifier to load for.
        :param commentId: Optionally give the comment to specficially load for.
        """
        with AutoTransaction(self._pod) as at:
            self._validateCredentials()

            self._log.info("objCommentDelete - start [usr:%s, sysUsr:%s, id:%s, comm:%s]" % (
                str(self._user), str(self._sysUser), objectId, commentId))

            dbCom = self._pod.dao.dComments(self._pod.dbcon)

            if not dbCom.try_select_one_deft(int(commentId)) or dbCom.rec.objectId != int(objectId):
                raise xContentManager("Comment not found. [objectId:%s, commentId:%s]" % (
                    objectId, commentId))

            dbCom.delete_one()

            at.commit()

            self._log.info("objCommentDelete - done [id:%s, comm:%s]" % (
                objectId, commentId))


    def objSearch(
          self,
          sources          : dict = None,
          refIds           : dict = None,
          sourceKeyList    : list = None,
          sourceValList    : list = None,
          refKeyList       : list = None,
          refValList       : list = None,
          statusList       : list = None,
          mimeTypeList     : list = None,
          createdBy        : str  = None,
          modifiedBy       : str  = None,
          dateCreatedFrom  : datetime.datetime = None,
          dateCreatedTo    : datetime.datetime = None,
          dateModifiedFrom : datetime.datetime = None,
          dateModifiedTo   : datetime.datetime = None) -> bObject.List:
        """
        Virtual method to search for objects.

        :param sources: Dictionary of source key/values to search on.
        :param refIds: Dictionary of ref key/values to search on.
        :param sourceKeyList: List of source keys to search on.
        :param sourceValList: List of source values to search on.
        :param refKeyList: List of ref keys to search on.
        :param refValList: List of ref values to search on.
        :param statusList: List of status strings to search on.
        :param mimeTypeList: List of mime types to search on.
        :param createdBy: Search on the created by.
        :param modifiedBy: Search on the modified by.
        :param dateCreatedFrom: Search of date created from.
        :param dateCreatedTo: Search of date create to.
        :param dateModifiedFrom: Search on date modified from.
        :param dateModifiedTo: Search on date modified to.
        :return: The list of matching objects.
        """
        with AutoTransaction(self._pod):
            self._validateCredentials()

            self._log.info("objSearch - start [sources:%s, refIds:%s, status:%s...]" % (
                str(sources), str(refIds), str(statusList)))

            res = bObject.List()
            qry = self._pod.dao.dObjectsSearch(self._pod.dbcon)

            self._log.info("objSearch - done [cnt:%d]" % (len(res)))

            if sources:
                qry.irec.criteria += " AND (0=1 "

                for k, v in sources.items():
                    qry.irec.criteria += " OR (o.sourceID = '%s' AND o.sourceValue = '%s')" % (
                        QueryBuilder.cleanStrCrit(k), QueryBuilder.cleanStrCrit(v))

                qry.irec.criteria += ")"

            if sourceKeyList:
                qry.irec.criteria += QueryBuilder.dynList(sourceKeyList, "o", "sourceID")

            if sourceValList:
                qry.irec.criteria += QueryBuilder.dynList(sourceValList, "o", "sourceValue")

            if refIds or refKeyList or refValList:
                qry.irec.tableJoin = " JOIN contman.Refs r ON r.objectId = o.id "

            if refIds:
                qry.irec.criteria += " AND (0=1 "

                for k, v in refIds.items():
                    qry.irec.criteria += " OR (r.id = '%s' AND r.value = '%s')" % (
                        QueryBuilder.cleanStrCrit(k), QueryBuilder.cleanStrCrit(v))

                qry.irec.criteria += ")"

            if refKeyList:
                qry.irec.criteria += QueryBuilder.dynList(refKeyList, "r", "id")

            if refValList:
                qry.irec.criteria += QueryBuilder.dynList(refValList, "r", "value")

            if statusList:
                qry.irec.criteria += QueryBuilder.dynList(statusList, "o", "status")

            if mimeTypeList:
                qry.irec.criteria += QueryBuilder.dynList(mimeTypeList, "o", "mimeType")

            if createdBy:
                qry.irec.criteria += QueryBuilder.dynCrit(createdBy, "o", "createdBy")

            if modifiedBy:
                qry.irec.criteria += QueryBuilder.dynCrit(modifiedBy, "o", "modifiedBy")

            if dateCreatedFrom and dateCreatedFrom != datetime.datetime.min:
                qry.irec.criteria += " AND o.createdDate >= %s" % self._pod._dbCon.datetime4SQLInject(dateCreatedFrom)

            if dateCreatedTo and dateCreatedTo != datetime.datetime.min:
                qry.irec.criteria += " AND o.createdDate <= %s" % self._pod._dbCon.datetime4SQLInject(dateCreatedTo)

            if dateModifiedFrom and dateModifiedFrom != datetime.datetime.min:
                qry.irec.criteria += " AND o.modifiedDate >= %s" % self._pod._dbCon.datetime4SQLInject(dateModifiedFrom)

            if dateModifiedTo and dateModifiedTo != datetime.datetime.min:
                qry.irec.criteria += " AND o.modifiedDate <= %s" % self._pod._dbCon.datetime4SQLInject(dateModifiedTo)

            if not qry.irec.criteria:
                raise xContentManager("objSearch: No criteria has been defined.")

            self._log.debug(" - objSearch [tableJoin:%s, criteria:%s]" % (
                qry.irec.tableJoin, qry.irec.criteria))

            qry.exec()

            while qry.fetch():
                res.append(self._convertObjectDB2Braze(qry.orec))

            self._log.info("objSearch - done [cnt:%d]" % len(res))

            return res


    def _validateCredentials(self):
        """
        Virtual method to ensure the credentials are valid.
        """
        if not self._user or not self._sysUser:
            raise xContentManager("Invalid security credentials!")


    def _convertObjectDB2Braze(self, rec: tObjects) -> bObject:
        """
        Converts a DB Object to a Braze Object.

        :param rec: The table object record.
        :return: The braze object record.
        """
        return bObject('' if not rec.id else str(rec.id),
                       rec.name,
                       rec.mimeType,
                       rec.sourceID,
                       rec.sourceValue,
                       rec.objSize,
                       rec.status,
                       rec.createdBy,
                       rec.createdDate,
                       rec.modifiedBy,
                       rec.modifiedDate)


    def _convertObjectBraze2DB(self, rec: bObject) -> tObjects:
        """
        Converts a Braze Object to a DB Object.

        :param rec: The braze object record.
        :return: The db object record.
        """
        res = tObjects()

        if rec.id:
            res.id = int(rec.id)

        res.name          = rec.name
        res.mimeType      = rec.mimeType
        res.sourceID      = rec.sourceID
        res.sourceValue   = rec.sourceValue
        res.objSize       = rec.objSize
        res.status        = rec.status
        res.createdBy     = rec.createdBy
        res.createdDate   = rec.createdDate
        res.modifiedBy    = rec.modifiedBy
        res.modifiedDate  = rec.modifiedDate

        return res


    def _convRefObjDB2Braze(self, rec: tRefs) -> bRef:
        """
        Converts a DB Ref to a Braze Ref.

        :param rec: The db ref record.
        :return: The braze ref record.
        """
        return bRef(
            str(rec.objectId),
            rec.id,
            rec.value,
            rec.createdBy,
            rec.createdDate,
            rec.modifiedBy,
            rec.modifiedDate)


    def _convRefObjBraze2DB(self, rec: bRef) -> tRefs:
        """
        Converts a Braze Ref to a DB Ref.

        :param rec: The braze ref record.
        :return: The db ref record.
        """
        res = tRefs()

        if rec.objectId:
            res.objectId = int(rec.objectId)

        res.id            = rec.refId
        res.value         = rec.value
        res.createdBy     = rec.createdBy
        res.createdDate   = rec.createdDate
        res.modifiedBy    = rec.modifiedBy
        res.modifiedDate  = rec.modifiedDate

        return res


    def _convCommentDB2Braze(self, rec: tComments) -> bComment:
        """
        Converts a DB Comment to a Braze Comment.

        :param rec: The db comment record.
        :return: The braze comment record.
        """
        return bComment(
            str(rec.objectId),
            rec.id,
            rec.value,
            rec.createdBy,
            rec.createdDate)


    def _readContent(self, rec: tObjects) -> bytes:
        """
        Pure virtual method to read the contentds of and object.

        :param rec: The object who's data is to be read.
        :return bytes: The object contents.
        """
        raise Exception('_readContent() not implemented')


    def _writeContent(self, rec: tObjects, data: bytes) -> bool:
        """
        Pure virtual method to write new object content data.

        :param rec: The table object record.
        :param data: The content data to write.
        :return: True if the record was modified in the write.
        """
        raise Exception('_writeContent() not implemented')


    def _deleteContent(self, rec: tObjects):
        """
        Pure virtual method to delete the object content data.

        :param rec: The table object record.
        """
        raise Exception('_deleteContent() not implemented')


    def _internalObjSave(self, obj: bObject, data: bytes) -> str:
        """
        Virtual method to save an object internally.

        :param obj: The braze object structure to save.
        :param data: The byte data to save.
        :return: The object identifier.
        """
        self._log.info("internalObjSave - start[usr:%s, sysUsr:%s, id:%d, name:%s, dataSize:%d]" % (
            str(self._user), str(self._sysUser), obj.id, obj.name, 0 if not data else len(data)))

        dbObj     = self._pod.dao.dObjects(self._pod.dbcon)
        dbObj.rec = self._convertObjectBraze2DB(obj)

        if not dbObj.rec.name:
            dbObj.rec.name = self.STR_UNKNOWN

        if not obj.id:
            dbObj.rec.createdBy    = self._user or self.STR_UNKNOWN
            dbObj.rec.createdDate  = datetime.datetime.now()
            dbObj.rec.objSize      = 0 if not data else len(data)
            dbObj.rec.modifiedBy   = ''
            dbObj.rec.modifiedDate = datetime.datetime.min

            dbObj.insert()

            outId  = str(dbObj.rec.id)
            obj.id = outId
        else:
            if not dbObj.trySelectOne(int(obj.id)):
                raise xContentManager("Unabled to load Object data, record not found. [id:%s]" % (
                    obj.id))

            if obj.name:
                dbObj.rec.name = obj.name

            dbObj.rec.mimeType    = obj.mimeType
            dbObj.rec.sourceID    = obj.sourceID
            dbObj.rec.sourceValue = obj.sourceValue
            dbObj.rec.status      = obj.status

            dbObj.rec.modifiedBy   = self._user or self.STR_UNKNOWN
            dbObj.rec.modifiedDate = datetime.datetime.now()

            dbObj.update()

            outId = str(dbObj.rec.id)

        if self._writeContent(dbObj.rec, data):
            dbObj.update()

        self._log.info("internalObjSave - done[id:%s]" % (outId))

        return outId


    def _internalRefSave(self, objectId: str, ref: bRef, isNewObj: bool):
        """
        Virtual method that saves an internal refeference.

        :param objectId: The object identifier.
        :param ref: The braze reference object.
        :param isNewObj: If true, assumes new records.
        """
        self._log.info("_internalRefSave - start [usr:%s, sysUsr:%s, id:%s, ref:%s]" % (
            str(self._user), str(self._sysUser), objectId, str(ref)))

        dbRef     = self._pod.dao.dRefs(self._pod.dbcon)
        dbRef.rec = self._convRefObjBraze2DB(ref)

        if not dbRef.rec.objectId:
            dbRef.rec.objectId = int(objectId)

        if not isNewObj and dbRef.try_select_one():
            dbRef.rec.value        = ref.value
            dbRef.rec.modifiedBy   = self._user or self.STR_UNKNOWN
            dbRef.rec.modifiedDate = datetime.datetime.now()
            dbRef.update()
        else:
            dbRef.rec.createdBy    = self._user or self.STR_UNKNOWN
            dbRef.rec.createdDate  = datetime.datetime.now()
            dbRef.rec.modifiedBy   = ''
            dbRef.rec.modifiedDate = datetime.datetime.min
            dbRef.insert()

        self._log.info('_internalRefSave - done[rec:%s]' % (str(self.dbRef.rec)))


    def _internalCommentSave(self, objectId: str, comm: str) -> str:
        """
        Virtual method that saves a comment.

        :param objectId: The object identifier.
        :param comm: The comment to save.
        :return: The comment id.
        """
        self._log.info("_internalCommentSave - start [usr:%s, sysUsr:%s, id:%s, ref:%s]" % (
            str(self._user), str(self._sysUser), objectId, str(comm)))

        if not objectId or not comm.comment:
            raise xContentManager("Cannot save empty comment.")

        dbRec = self._pod.dao.dComments(self._pod.dbcon)

        dbRec.insert_deft(
            int(objectId),
            comm.comment,
            self._user or self.STR_UNKNOWN,
            datetime.datetime.now())

        self._log.info("_internalCommentSave - done [rec:%s]" % (str(dbRec.rec)))

        return str(dbRec.rec.id)
