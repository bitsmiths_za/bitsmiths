# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import os
import os.path

from .ContentManager import ContentManager

from bs_contman                  import Constants
from bs_contman.xContentManager  import xContentManager

from bs_contman.db.tables.tObjects  import tObjects


class FileSys(ContentManager):
    """
    File system content management object.
    """

    def __init__(self):
        """
        Constructor.
        """
        ContentManager.__init__(self)

        self._cmPath = ''


    def id(self) -> str:
        """
        Overload method.
        """
        return Constants.CONTMAN_ID_FILE_SYS


    def name(self) -> str:
        """
        Overload method.
        """
        return 'File System'


    def initialize(self, pod, endPoint):
        """
        Overload.

        :param pod: The pod to use.
        :param endPoint: Expected to be a directory path.
        """
        ContentManager.initialize(self, pod, endPoint)

        if not os.path.exists(endPoint) or not os.path.isdir(endPoint):
            raise xContentManager('File system path not found. [endPoint:%s]' % (endPoint))

        self._cmPath = endPoint


    def logon(self, endPoint: str, usr: str, passwd: str, sysUsr=None, sysPasswd=None):
        """
        Overload.
        """
        ContentManager.logon(endPoint, usr, passwd, sysUsr, sysPasswd)

        if not os.path.exists(self._cmPath) or not os.path.isdir(self._cmPath):
            raise xContentManager('Root file system path not found. [%s]' % (self._cmPath))


    def _validateCredentials(self):
        """
        Overloaded method.
        """
        pass


    def _readContent(self, rec: tObjects) -> bytes:
        """
        Overloaded method.

        :param rec: The object who's data is to be read.
        :return bytes: The object contents.
        """
        fullPath = os.path.join(self._cmPath, rec.dirPath)

        if not os.path.exists(fullPath) or not os.path.isfile(fullPath):
            raise xContentManager("Unabled to load Object, content missing! [id:%d, path:%s]" % (
                rec.id, fullPath))

        with open(fullPath, 'rb') as fh:
            return fh.read()


    def _writeContent(self, rec: tObjects, data: bytes) -> bool:
        """
        Overloaded method.

        :param rec: The table object record.
        :param data: The content data to write.
        :return: True if the record was modified in the write.
        """
        if not data:
            return False

        if not rec.dirPath:
            dpath = self._generateDirPath(rec)

            if not os.path.exists(dpath):
                os.makedirs(dpath, exist_ok=True)

        with open(rec.dirPath, 'rb') as fh:
            fh.write(data)

        return True


    def _deleteContent(self, rec: tObjects):
        """
        Overloaded method.

        :param rec: The table object record.
        """
        if not rec.dirPath:
            return

        fullPath = os.path.join(self._cmPath, rec.dirPath)

        if os.path.exists(fullPath) and os.path.isfile(fullPath):
            os.remove(fullPath)


    def _generateDirPath(self, obj: tObjects) -> str:
        """
        Generate the directory path for the object.

        :param obj: The object table record.
        :return: The generated directory path.
        """
        td = datetime.date.today()

        year  = "%4.4d" % td.year
        month = "%2.2d" % td.month
        day   = "%2.2d" % td.day
        dpath = os.path.join(year, month, day)

        obj.dirPath = os.path.join(dpath, str(obj.id))

        return dpath
