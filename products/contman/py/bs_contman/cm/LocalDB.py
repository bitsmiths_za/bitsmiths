# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

from .ContentManager import ContentManager

from bs_contman                  import Constants
from bs_contman.xContentManager  import xContentManager

from bs_contman.db.tables.tObjects  import tObjects


class LocalDB(ContentManager):
    """
    Local database content management object.
    """

    def __init__(self):
        """
        Constructor.
        """
        ContentManager.__init__(self)


    def id(self) -> str:
        """
        Overload method.
        """
        return Constants.CONTMAN_ID_LOCAL_DB


    def name(self) -> str:
        """
        Overload method.
        """
        return 'Local Database'


    def _validateCredentials(self):
        """
        Overloaded method.
        """
        pass


    def _readContent(self, rec: tObjects) -> bytes:
        """
        Overloaded method.

        :param rec: The object who's data is to be read.
        :return bytes: The object contents.
        """
        cont = self._pod.dao.dContent(self._pod.dbcon)

        if not cont.trySelectOneDeft(rec.id):
            raise xContentManager("Content not found. [objectId:%s]" % (str(rec.id)))

        return cont.rec.content


    def _writeContent(self, rec: tObjects, data: bytes) -> bool:
        """
        Overloaded method.

        :param rec: The table object record.
        :param data: The content data to write.
        :return: True if the record was modified in the write.
        """
        if not data:
            return False

        cont = self._pod.dao.dContent(self._pod.dbcon)

        if not cont.trySelectOneDeft(rec.id):
            cont.rec.objectId  = rec.id
            cont.rec.content   = data
            cont.insert()
        else:
            cont.rec.content = data
            cont.update()

        return True


    def _deleteContent(self, rec: tObjects):
        """
        Overloaded method.

        :param rec: The table object record.
        """
        cont = self._pod.dao.dContent(self._pod.dbcon)

        cont.deleteOneDeft(rec.id)
