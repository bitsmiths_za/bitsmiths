# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

from bs_contman.Constants import Constants

from .ContentManager import ContentManager
from .FileSys        import FileSys
from .LocalDB        import LocalDB
from .stub           import Stub


def get_content_manager(pod, contman_id: str, endPoint: str) -> ContentManager:
    """
    Factory to load a content manager by id.

    :param pod: The pod to use.
    :param contman_id: Content manage identifier.
    :param endPoint: The end point for the content management object.
    """
    res = None

    if contman_id == Constants.CONTMAN_ID_LOCAL_DB:
        res = LocalDB()
    elif contman_id == Constants.CONTMAN_ID_FILE_SYS:
        res = FileSys()
    elif contman_id == Constants.CONTMAN_ID_STUB:
        res = Stub()

    if not res:
        raise Exception('Content Management [%s] not expected.' % contman_id)

    res.initialize(pod, endPoint)

    return res
