#!/usr/bin/env python

# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import sys
import os
import os.path
import yaml

with open('../../../externs/mettle/version', 'rt') as fh:
    mettleVer      = fh.read().strip()
    mettleVerParts = mettleVer.split('.')

with open('../../../build/package-version.yml', 'rt') as fh:
    pv    = yaml.safe_load(fh.read())
    bsver = '%d.%d' % (pv['version']['maj'], pv['version']['med'])

eggVer = 'py%d.%d.egg' % (sys.version_info.major, sys.version_info.minor)

sys.path.append(('../../../externs/mettle/py3/mettle-%s.%s-%s' % (
    mettleVerParts[0], mettleVerParts[1], eggVer)) .replace('/', os.path.sep))

import mettle.mfgen


def gen_mettle(mfgen):
    """
    Generate mettle makefile for code generators.

    :param mfgen: (mfgen.generator) The make file generator.
    """
    mfgen.gen('mettle', {
        'makefile.dir' : 'mettledb',
        'mettlegen'    : '../../externs/mettle/py3/mettlegen-%s.%s-%s' % (
            mettleVerParts[0], mettleVerParts[1], eggVer),
        'proj_paths'   : ['mettledb/proj.py'],
    })

    mfgen.gen('mettle', {
        'makefile.dir' : 'mettlebraze',
        'mettlegen'    : '../../externs/mettle/py3/mettlegen-%s.%s-%s' % (
            mettleVerParts[0], mettleVerParts[1], eggVer),
        'proj_paths'   : ['mettlebraze/crest.py'],
    })


def gen_python3(mfgen):
    """
    Generate makefiles for python egg.

    :param mfgen: (mfgen.generator) The make file generator.
    """
    if mfgen._windows() and not os.path.exists(os.path.join(mfgen._config['extern-path'], 'python3')):
        mfgen.warning("Python3 path not set [%s], no PYD makefiles will be generated" % (
            os.path.join(mfgen._config['extern-path'], 'python3')))
        return

    pyGen = mfgen.getGenerator('py3')

    pyGen._destDirs['egg'] = '../../bin/py3'

    mfgen.gen('py3', {
        'makefile.dir' : 'py',
        'dest.out'     : 'egg',
        'dest.name'    : 'bs_crest',
        'dest.descr'   : 'Bitsmiths Custom Reporting Enquriy Scheduling Transmission [CREST] Package',
        'ver'          : bsver,
        'author'       : 'Bitsmiths (Pty) Ltd.',
        'author_email' : 'nicolas@bitsmiths.co.za, steven@bitsmiths.co.za, darryl@bitsmiths.co.za',
        'license'      : 'MIT',
        'url'          : 'https://bitbucket.org/bitsmiths_za/bitsmiths',
    })



def gen_angular(mfgen):
    """
    Generate angular makefile for type script.

    :param mfgen: (mfgen.generator) The make file generator.
    """
    angGen = mfgen.getGenerator('angular')

    angGen._destDirs['lib'] = '../../bin/ts'

    # mfgen.gen('angular', {
    #     'makefile.dir' : 'ts/crest/lib',
    #     'dest.name'    : 'bs-crest-lib',
    #     'dest.out'     : 'lib',
    #     'ver'          : '%s.0' % bsver,
    #     'node_modules' : '',
    # })

    # mfgen.gen('angular', {
    #     'makefile.dir' : 'ts/fura/std',
    #     'dest.name'    : 'bs-fura-std',
    #     'dest.out'     : 'lib',
    #     'ver'          : '%s.0' % bsver,
    #     'node_modules' : '',
    # })


def gen_solution(mfgen):
    """
    Generate solution  makefile for all products.

    :param mfgen: (mfgen.generator) The make file generator.
    """
    mfgen.getGenerator('solution')

    projSolution = {
        'makefile.dir' : 'build',
        'targets'      : mfgen._allGen,
        'order'        : ['mettle', 'py3', 'angular'],
    }

    mfgen.gen('solution', projSolution)


# ------------------------------------------------------------------------------
# main
# ------------------------------------------------------------------------------
if __name__ == '__main__':
    mfgen    = mettle.mfgen.generator('..')

    mfgen.initialize(['build.yml'], True)

    gen_mettle(mfgen)
    gen_python3(mfgen)
    gen_angular(mfgen)
    gen_solution(mfgen)
