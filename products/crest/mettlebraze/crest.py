# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.Braze'

    def _Name(self):
        return 'Crest'

    def _Object(self, brazeproj):
        import mettlegen

        proj = mettlegen.BrazeProject(
            brazeproj,
            self._Name(),
            self._Version(),
            'named')

        AUTH_LOGGED_IN = '_LOGGED_IN_'

        # ----------------------------------
        # Language Generators
        # ----------------------------------
        proj.generators['Python3'].init(True, '../py/bs_crest/braze', {
            'braze.struct'       : 'b|',
            'braze.couplet'      : '|_Couplet',
            'client.marshaler'   : '|ClientMarshaler',
            'client.interface'   : '|ClientInterface',
            'client.serverImpl'  : '|ServerImpl',
            'server.marshaler'   : '|ServerMarshaler',
            'server.interface'   : '|ServerInterface',
            'namespace'          : 'bs_crest.braze',
            'casing' : {
                'class'  : 'pascal',
                'method' : 'camel',
                'member' : 'camel',
            },
        })

        proj.generators['TypeScript'].init(True, '../ts/crest/lib/src/braze', {
            'braze.struct'       : 'b|',
            'braze.couplet'      : '|_Couplet',
            'client.marshaler'   : '|ClientMarshaler',
            'client.interface'   : '|ClientInterface',
            'import.dbpath'      : 'Crest:../db/tables;'
        })

        proj.dbProject('Crest', '../mettledb/proj.py')

        # ----------------------------------
        # Couplets
        # ----------------------------------

        # ----------------------------------
        # Structs
        # ----------------------------------
        proj.struct('Parameter', fields = (
            ('id',     'string', {'null' : False, 'max' : 128}),
            ('value',  'string', {'null' : True,  'max' : 512}),
        ))

        # ----------------------------------
        # Remote Calls
        # ----------------------------------
        proj.remoteCall('ping', returns = ('', 'bool'))

        proj.remoteCall(
            'configCreate',
            auth    = 'crest.cfg-maint.create',
            returns = ('', 'db:Crest.Config'),
            argsIn  = (
                ('rec',     'db:Crest.Config'),
        ))

        proj.remoteCall(
            'configRead',
            auth    = 'crest.cfg-maint.read',
            returns = ('', 'db:Crest.Config[]'),
            argsIn  = (
                ('cfgId', 'string', {'max' : 256}),
        ))

        proj.remoteCall(
            'configUpdate',
            auth    = 'crest.cfg-maint.update',
            returns = ('', 'db:Crest.Config'),
            argsIn  = (
                ('rec',    'db:Crest.Config'),
        ))

        proj.remoteCall(
            'configDelete',
            auth    = 'crest.cfg-maint.delete',
            argsIn  = (
                ('cfgId', 'string', {'max' : 256, 'null' : False}),
        ))

        proj.remoteCall(
            'enquiryRun',
            auth     = 'crest.enquiry.run',
            returns  = ('', 'db:Crest.Gen.Search.out'),
            argsIn   = (
                ('enquiryId', 'string', {'null' : False, 'max' : 128}),
                ('params',    'braze:Parameter[]'),
        ))

        proj.remoteCall(
            'enquirySearchHist',
            auth     = 'crest.enquiry.search',
            returns  = ('', 'db:Crest.Gen.Search.out[]'),
            argsIn   = (
                ('dateFrom',  'date',   {'null' : False}),
                ('dateTo',    'date',   {'null' : False}),
                ('enquiryId', 'string', {'null' : True, 'max' : 128}),
                ('status',    'char[]', {'max' : 16}),
                ('reqBy',     'string', {'null' : True, 'max' : 128}),
        ))

        proj.remoteCall(
            'enquiryUserRoles',
            auth     = AUTH_LOGGED_IN,
            returns  = ('', 'db:Crest.Enquiry.ByUserRole.out[]')
        )

        proj.remoteCall(
            'enquiryUserHist',
            auth     = 'crest.enquiry.nav',
            returns  = ('', 'db:Crest.Gen.Search.out[]'),
            argsIn   = (
                ('dateFrom', 'date', {'null' : False}),
                ('dateTo',   'date', {'null' : False}),
        ))

        proj.remoteCall(
            'lookupCreate',
            auth    = 'crest.lookup-maint.create',
            returns = ('', 'db:Crest.Lookup'),
            argsIn  = (
                ('rec',     'db:Crest.Lookup'),
        ))

        proj.remoteCall(
            'lookupRead',
            auth    = 'crest.lookup-maint.read',
            returns = ('', 'db:Crest.Lookup[]'),
            argsIn  = (
                ('lookupId', 'string', {'max' : 128}),
        ))

        proj.remoteCall(
            'lookupUpdate',
            auth    = 'crest.lookup-maint.update',
            returns = ('', 'db:Crest.Lookup'),
            argsIn  = (
                ('rec',    'db:Crest.Lookup'),
        ))

        proj.remoteCall(
            'lookupDelete',
            auth    = 'crest.lookup-maint.delete',
            argsIn  = (
                ('lookupId', 'string', {'max' : 128, 'null' : False}),
        ))

        proj.remoteCall(
            'paramCreate',
            auth    = 'crest.param-maint.create',
            returns = ('', 'db:Crest.Param'),
            argsIn  = (
                ('rec',     'db:Crest.Param'),
        ))

        proj.remoteCall(
            'paramRead',
            auth    = 'crest.param-maint.read',
            returns = ('', 'db:Crest.Param[]'),
            argsIn  = (
                ('paramId', 'string', {'max' : 128}),
        ))

        proj.remoteCall(
            'paramUpdate',
            auth    = 'crest.param-maint.update',
            returns = ('', 'db:Crest.Param'),
            argsIn  = (
                ('rec',    'db:Crest.Param'),
        ))

        proj.remoteCall(
            'paramDelete',
            auth    = 'crest.param-maint.delete',
            argsIn  = (
                ('paramId', 'string', {'max' : 128, 'null' : False}),
        ))

        return proj


if __name__ == '__main__' :
    bp = _MettleObject()

    print('Project Type = %s' % bp._MettleFileType())
    print('Project Name = %s' % bp._Name())
    print('Version      = %f' % bp._Version())
