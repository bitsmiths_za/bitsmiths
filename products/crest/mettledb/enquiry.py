# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.DB.Table'

    def _Name(self):
        return 'Enquiry'

    def _Object(self, dbtable):
        import mettlegen

        table = mettlegen.db.Table(self._Name(), self._Version())

        table.selectOne   = True
        table.selectAll   = False
        table.update      = True
        table.insert      = True
        table.deleteOne   = True
        table.deleteAll   = False

        table.column('id',           'string',    {'max' : 128})
        table.column('caption',      'string',    {'max' : 128})
        table.column('outCaption',   'string',    {'max' : 256, 'null' : True})  # The output caption, using input args
        table.column('groupDescr',   'string',    {'max' : 128})
        table.column('queryId',      'string',    {'max' : 128})
        table.column('sortBy',       'string',    {'max' : 4096, 'null' : True})  # sort by columns, json eg: [{column:accountId, order:ASC}, {column:balance, order:DESC}]  # noqa
        table.column('keepFor',      'int32')     # how many days to keep this enquiry output for, 0 = forever, negative is for how many hours.  # noqa
        table.column('otFmt',        'string',    {'null' : True})  # eg {'*':{'pg-layout':'landscape'}}
        table.column('audId',        'string',    {'max' : 128})
        table.column('tmStamp',      'timestamp')

        table.primaryKey('id')

        table.foreignKey('FK_QR', 'Query', 'queryId')

        table.index('IX_QR', 'queryId')

        p = table.sqlProc('ByUserRole')
        p.inField('roleId', 'column:EnquiryRole.roleId')
        p.inField('siteId', 'column:EnquiryRole.siteId')
        p.outField('id',          'this')
        p.outField('caption',     'this')
        p.outField('outCaption',  'this')
        p.outField('groupDescr',  'this')
        p.outField('allowView',   'column:EnquiryRole.allowView')
        p.outField('allowRun',    'column:EnquiryRole.allowRun')
        p.outField('allowRerun',  'column:EnquiryRole.allowRerun')
        p.outField('allowSearch', 'column:EnquiryRole.allowSearch')
        p.outField('allowDelete', 'column:EnquiryRole.allowDelete')
        p.sql['std'] = '''\
SELECT
  e.id,
  e.caption,
  e.outCaption,
  e.groupDescr,
  er.allowView,
  er.allowRun,
  er.allowRerun,
  er.allowSearch,
  er.allowDelete
FROM
  crest.enquiry e
    JOIN
  crest.enquiryrole er
    ON e.id = er.enquiryId
WHERE
  er.roleId    = :roleId AND
  er.siteId    = :siteId AND
  er.allowView = 'Y'
ORDER BY
  e.groupDescr,
  e.caption
'''

        p = table.sqlProc('ForReport')
        p.inField('reportId', 'column:Report.id')
        p.outField('',   'table:this')
        p.sql['std'] = '''\
SELECT
  e.*
FROM
  crest.ReportItem ri
    JOIN
  crest.enquiry e
    ON e.id = ri.enquiryId
WHERE
  ri.reportId = :reportId
ORDER BY
  ri.ordIdx
'''



        return table


if __name__ == '__main__':
    tbl = _MettleObject()

    print('Mettle File Type = %s' % tbl._MettleFileType())
    print('Object Name      = %s' % tbl._Name())
    print('Version          = %f' % tbl._Version())
