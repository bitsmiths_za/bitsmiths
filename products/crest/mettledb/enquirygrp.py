# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.DB.Table'

    def _Name(self):
        return 'EnquiryGrp'

    def _Object(self, dbtable):
        import mettlegen

        table = mettlegen.db.Table(self._Name(), self._Version())

        table.selectOne   = True
        table.selectAll   = False
        table.update      = True
        table.insert      = True
        table.deleteOne   = True
        table.deleteAll   = False

        table.column('id',           'int32')   # also the order index
        table.column('enquiryId',    'string',    {'max' : 128})
        table.column('grpCols',      'string',    {'max' : 4096, 'null' : True})  # group by columns, json eg: [{column:accountId, hide:false}] # noqa
        table.column('sumCols',      'string',    {'max' : 4096, 'null' : True})  # sum columns, json eg: [{column:balance, eq:sum}, {column:trans, eq:avg}] # noqa
        table.column('header',       'string',    {'max' : 4096, 'null' : True})  # sum columns, json eg: [{span:name, txt:'ACCOUNT: {accountId}'}] # noqa
        table.column('footer',       'string',    {'max' : 4096, 'null' : True})  # sum columns, json eg: [{span:name,', txt:'TOTAL'}] # noqa
        table.column('headTag',      'string',    {'max' : 128,  'null' : True})
        table.column('footTag',      'string',    {'max' : 128,  'null' : True})
        table.column('audId',        'string',    {'max' : 128})
        table.column('tmStamp',      'timestamp')

        table.primaryKey('id', 'enquiryId')

        table.foreignKey('FK_EN', 'Enquiry', 'enquiryId')

        table.index('IX_EN', 'enquiryId')

        p = table.sqlProc('ByEnquiry')
        p.inField('enquiryId', 'this')
        p.outField('',         'table:this')
        p.sql['std'] = '''\
SELECT
  eg.*
FROM
  crest.enquirygrp eg
WHERE
  eg.enquiryId = :enquiryId
ORDER BY
  eg.id
'''


        return table


if __name__ == '__main__':
    tbl = _MettleObject()

    print('Mettle File Type = %s' % tbl._MettleFileType())
    print('Object Name      = %s' % tbl._Name())
    print('Version          = %f' % tbl._Version())
