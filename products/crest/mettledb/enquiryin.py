# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.DB.Table'

    def _Name(self):
        return 'EnquiryIn'

    def _Object(self, dbtable):
        import mettlegen

        table = mettlegen.db.Table(self._Name(), self._Version())

        table.selectOne   = True
        table.selectAll   = False
        table.update      = True
        table.insert      = True
        table.deleteOne   = True
        table.deleteAll   = False

        table.column('id',           'string',    {'max' : 128})  # Soft FK to query in
        table.column('enquiryId',    'string',    {'max' : 128})
        table.column('orderIdx',     'int32')
        table.column('defVal',       'string',    {'max' : 128, 'null' : True})
        table.column('hide',         'char',      {'in' : {'Y' : 'Yes', 'N' : 'No'}})
        table.column('audId',        'string',    {'max' : 128})
        table.column('tmStamp',      'timestamp')

        table.primaryKey('id', 'enquiryId')

        table.foreignKey('FK_EN', 'Enquiry', 'enquiryId')

        table.index('IX_EN', 'enquiryId')

        p = table.sqlProc('ByEnquiry')
        p.inField('enquiryId', 'this')
        p.outField('',         'table:this')
        p.sql['std'] = '''\
SELECT
  ei.*
FROM
  crest.enquiryin ei
WHERE
  ei.enquiryId = :enquiryId
ORDER BY
  ei.orderIdx
'''

        p = table.sqlProc('ForRun')
        p.inField('enquiryId',       'this')
        p.outField('orderIdx',       'this')
        p.outField('defVal',         'this')
        p.outField('hide',           'this')
        p.outField('caption',        'string')
        p.outField('id',             'string')
        p.outField('paramType',      'column:Param.paramType')
        p.outField('paramSrc',       'column:Param.paramSrc')
        p.outField('paramPrecision', 'column:Param.paramPrecision')
        p.outField('lookupId',       'column:Param.lookupId')
        p.sql['std'] = '''\
SELECT
  coalesce(ei.orderIdx, qi.orderIdx),
  coalesce(ei.defVal, p.defVal),
  coalesce(ei.hide, 'N'),
  coalesce(qi.qryCaption, p.caption),
  qi.id,
  p.paramType,
  p.paramSrc,
  p.paramPrecision,
  p.lookupId
FROM
  crest.enquiry e
    JOIN
  crest.query q
    ON q.id = e.queryId
    JOIN
  crest.queryin qi
    ON qi.queryId = q.id
    JOIN
  crest.param p
    ON p.id = qi.paramId
    LEFT JOIN
  crest.enquiryin ei
    on ei.id = qi.id
WHERE
  e.id = :enquiryId
ORDER BY
  1,
  qi.id
'''


        return table


if __name__ == '__main__':
    tbl = _MettleObject()

    print('Mettle File Type = %s' % tbl._MettleFileType())
    print('Object Name      = %s' % tbl._Name())
    print('Version          = %f' % tbl._Version())
