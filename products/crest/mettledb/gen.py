# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.DB.Table'

    def _Name(self):
        return 'Gen'

    def _Object(self, dbtable):
        import mettlegen

        table = mettlegen.db.Table(self._Name(), self._Version())

        table.selectOne   = True
        table.selectAll   = False
        table.lockOne     = True
        table.update      = True
        table.insert      = True
        table.deleteOne   = True
        table.deleteAll   = False

        statusTypes = {
            'P' : 'Pending',
            'B' : 'Busy',
            'C' : 'Complete',
            'F' : 'Failed',
            'A' : 'Abort',
        }

        table.column('id',           'seq64')
        table.column('siteId',       'int32')
        table.column('enquiryId',    'string',    {'max' : 128,  'null' : True})  # Soft FK to enquiry
        table.column('reportId',     'string',    {'max' : 128,  'null' : True})  # Soft FK to report
        table.column('schedId',      'string',    {'max' : 128,  'null' : True})  # Soft FK to reportsched
        table.column('status',       'char',      {'in' : statusTypes})
        table.column('param',        'string',    {'null' : True}) # JSON input params eg: [{id:accountId, value:20}, {id:fromDate, value:2017-07-20}]  # noqa
        table.column('reqBy',        'string',    {'max' : 128})
        table.column('reqDate',      'datetime')
        table.column('dateStart',    'datetime',  {'null' : True})
        table.column('dateEnd',      'datetime',  {'null' : True})
        table.column('genTime',      'double')
        table.column('totalRows',    'int32')
        table.column('caption',      'string',    {'null' : True})  # The final caption of the report/enquiry

        table.primaryKey('id')

        # table.foreignKey('FK_EN', 'Enquiry', 'enquiryId')
        # table.foreignKey('FK_RP', 'Report', 'reportId')

        table.index('IX_EN', 'enquiryId')
        # table.index('IX_RP', 'reportId')
        table.index('IX_RB', 'siteId', 'reqBy')

        p = table.sqlProc('Pending')
        p.inField('maxRecs',    'int32')
        p.outField('',         'table:this')
        p.sql['std'] = '''\
SELECT
  g.*
FROM
  crest.gen g
WHERE
  g.status = 'P'
ORDER BY
  g.id
LIMIT
  :maxRecs
'''

        p = table.sqlProc('Search')
        p.inField('siteId',     'int32')
        p.inField('maxRecs',    'int32')
        p.inField('dateFrom',   'datetime')
        p.inField('dateTo',     'datetime')
        p.inField('criteria',   'dynamic', {'max' : 4096})
        p.outField('id',        'this')
        p.outField('enquiryId', 'this')
        p.outField('reportId',  'this')
        p.outField('status',    'this')
        p.outField('reqBy',     'this')
        p.outField('reqDate',   'this')
        p.outField('dateStart', 'this')
        p.outField('dateEnd',   'this')
        p.outField('genTime',   'this')
        p.outField('totalRows', 'this')
        p.sql['std'] = '''\
SELECT
  g.id,
  g.enquiryId,
  g.reportId,
  g.status,
  g.reqBy,
  g.reqDate,
  g.dateStart,
  g.dateEnd,
  g.genTime,
  g.totalRows
FROM
  crest.gen g
WHERE
  g.siteId   = :siteId   AND
  (:dateFrom IS NULL OR g.reqDate >= :dateFrom) AND
  (:dateTo   IS NULL OR g.reqDate <= :dateTo)
  [criteria]
ORDER BY
  g.id DESC
LIMIT
  :maxRecs
'''

        return table


if __name__ == '__main__':
    tbl = _MettleObject()

    print('Mettle File Type = %s' % tbl._MettleFileType())
    print('Object Name      = %s' % tbl._Name())
    print('Version          = %f' % tbl._Version())
