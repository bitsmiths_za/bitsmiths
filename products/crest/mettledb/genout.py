# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.DB.Table'

    def _Name(self):
        return 'GenOut'

    def _Object(self, dbtable):
        import mettlegen

        table = mettlegen.db.Table(self._Name(), self._Version())

        table.selectOne   = True
        table.selectAll   = False
        table.lockOne     = True
        table.update      = True
        table.insert      = True
        table.deleteOne   = True
        table.deleteAll   = False

        statuses = {
            'P' : 'Pending',
            'B' : 'Busy',
            'C' : 'Complete',
            'F' : 'Failed',
            'A' : 'Abort',
        }

        table.column('genId',        'int64')
        table.column('outTypeId',    'string',    {'max' : 32})
        table.column('status',       'char',      {'in' : statuses})
        table.column('dateStart',    'datetime',  {'null' : True})
        table.column('dateEnd',      'datetime',  {'null' : True})
        table.column('genTime',      'double')
        table.column('fileSize',     'int32')
        table.column('errMsg',       'string',    {'null' : True})
        table.column('audId',        'string',    {'max' : 128})
        table.column('tmStamp',      'timestamp')

        table.primaryKey('genId', 'outTypeId')

        table.foreignKey('FK_GE', 'Gen',     'genId')
        table.foreignKey('FK_OT', 'OutType', 'outTypeId')

        table.index('IX_ST', 'genId', 'outTypeId', 'status')

        p = table.sqlProc('ByGen')
        p.inField('genId',    'this')
        p.inField('criteria', 'dynamic', {'max' : 1024})
        p.outField('',         'table:this')
        p.sql['std'] = '''\
SELECT
  g.*
FROM
  crest.genout g
WHERE
  g.genId = :genId
  [criteria]
ORDER BY
  g.id,
  g.outTypeId
'''


        p = table.sqlProc('DelByGen')
        p.inField('genId',    'this')
        p.outField('',         'table:this')
        p.sql['std'] = '''\
DELETE FROM
  crest.genout
WHERE
  genId = :genId
'''

        p = table.sqlProc('Regenerate')
        p.inField('genId',    'this')
        p.outField('',         'table:this')
        p.sql['std'] = '''\
UPDATE
  crest.genout
SET
  status = 'P'
WHERE
  genId  = :genId AND
  status = 'C'
'''

        p = table.sqlProc('Pending')
        p.inField('maxRecs',    'int32')
        p.outField('',         'table:this')
        p.sql['std'] = '''\
SELECT
  g.*
FROM
  (
    select
      go.genid,
      max(go.outTypeId) as outTypeId
    from
      crest.genout go
        join
      crest.gen g
        on g.id = go.genId
    where
      go.status = 'P' AND
      g.status  = 'C'
    group by
      go.genid
    order by
      go.genid
    limit
      :maxRecs
  ) x
    JOIN
  crest.genout g
    ON g.genId     = x.genId AND
       g.outTypeId = x.outTypeId
WHERE
  g.status = 'P'
ORDER BY
  g.genId
'''


        return table


if __name__ == '__main__':
    tbl = _MettleObject()

    print('Mettle File Type = %s' % tbl._MettleFileType())
    print('Object Name      = %s' % tbl._Name())
    print('Version          = %f' % tbl._Version())
