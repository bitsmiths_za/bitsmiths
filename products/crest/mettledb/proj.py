# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.DB'

    def _Name(self):
        return 'BS.Crest'

    def _Object(self, dbproj):
        import mettlegen

        proj = mettlegen.DBProject(dbproj, self._Name(), self._Version())

        proj.tableList = [
            'config.py',
            'enquiry.py',
            'enquiryin.py',
            'enquiryout.py',
            'enquirygrp.py',
            'enquiryrole.py',
            'gen.py',
            'genmetric.py',
            'genout.py',
            'gentmit.py',
            'lookup.py',
            'lookupitem.py',
            'outtype.py',
            'param.py',
            'query.py',
            'queryin.py',
            'queryout.py',
            'report.py',
            'reportitem.py',
            'reportsched.py',
            'reportschedtmit.py',
            'tmitcfg.py',
            'tmitgroup.py',
            'tmitgroupitem.py',
            'tmittype.py',
        ]


        # ----------------------------------
        # Language Generators
        # ----------------------------------
        proj.generators['Python3'].init(True, '../py/bs_crest/db', {
            'class.qryout'       : 'o|',
            'class.dao'          : 'd|',
            'class.qryin'        : 'i|',
            'class.table'        : 't|',
            'class.couplet'      : '|_Couplet',
            'namespace'          : 'bs_crest.db',
            'db.schema'          : 'crest',
            'casing' : {
                'class'  : 'pascal',
                'method' : 'camel',
                'member' : 'camel',
            },
        })

        proj.generators['TypeScript'].init(True, '../ts/crest/lib/src/db', {
            'class.qryout'       : 'o|',
            'class.dao'          : 'd|',
            'class.qryin'        : 'i|',
            'class.table'        : 'c|',
            'class.couplet'      : '|_Couplet',
        })

        proj.generators['Sql'].init(False, 'sqldef', {
            'db.schema'           : 'crest',
        })

        # ----------------------------------
        # POSTGRESQL - Database
        # ----------------------------------
        proj.databases['postgresql'].enabled  = True
        proj.databases['postgresql'].generators['Python3'].init(True)
        proj.databases['postgresql'].generators['Sql'].init(True)

        return proj


if __name__ == '__main__':
    bp = _MettleObject()

    print('Mettle File Type = %s' % bp._MettleFileType())
    print('Object Name      = %s' % bp._Name())
    print('Version          = %f' % bp._Version())
