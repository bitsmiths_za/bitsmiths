# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.DB.Table'

    def _Name(self):
        return 'QueryOut'

    def _Object(self, dbtable):
        import mettlegen

        table = mettlegen.db.Table(self._Name(), self._Version())

        table.selectOne   = True
        table.selectAll   = False
        table.update      = True
        table.insert      = True
        table.deleteOne   = True
        table.deleteAll   = False

        outTypes = {
            'T' : 'Text',
            'D' : 'Date',
            'Z' : 'DateTime',
            'N' : 'Number',
            'F' : 'Float',
            'P' : 'Percentile',
            'Y' : 'YesNo',
            'A' : 'Auto',
        }

        table.column('id',           'string',    {'max' : 128})
        table.column('queryId',      'string',    {'max' : 128})
        table.column('caption',      'string',    {'max' : 128, 'null' : True})
        table.column('outType',      'char',      {'in' : outTypes})
        table.column('outPrecision', 'int16')
        table.column('orderIdx',     'int32')
        table.column('audId',        'string',    {'max' : 128})
        table.column('tmStamp',      'timestamp')

        table.primaryKey('id', 'queryId')

        table.foreignKey('FK_QR', 'Query', 'queryId')

        table.index('IX_QR', 'queryId')

        p = table.sqlProc('ByQuery')
        p.inField('queryId', 'this')
        p.outField('',       'table:this')
        p.sql['std'] = '''\
SELECT
  qo.*
FROM
  crest.queryout qo
WHERE
  qo.queryId = :queryId
ORDER BY
  qo.orderIdx
'''


        return table


if __name__ == '__main__':
    tbl = _MettleObject()

    print('Mettle File Type = %s' % tbl._MettleFileType())
    print('Object Name      = %s' % tbl._Name())
    print('Version          = %f' % tbl._Version())
