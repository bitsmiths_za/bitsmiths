# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.DB.Table'

    def _Name(self):
        return 'ReportSched'

    def _Object(self, dbtable):
        import mettlegen

        table = mettlegen.db.Table(self._Name(), self._Version())

        table.selectOne   = True
        table.selectAll   = False
        table.lockOne     = True
        table.update      = True
        table.insert      = True
        table.deleteOne   = True
        table.deleteAll   = False

        statusTypes = {
            'A' : 'Active',
            'D' : 'Disabled',
        }

        intTypes = {
            'H' : 'Hours',
            'D' : 'Days',
            'W' : 'Weeks',
            'M' : 'Months'
        }

        table.column('reportId',     'string',    {'max' : 128})
        table.column('schedId',      'string',    {'max' : 128})
        table.column('siteId',       'int32')
        table.column('status',       'char',      {'in' : statusTypes})
        table.column('caption',      'string',    {'max' : 256})
        table.column('param',        'string',    {'null' : True})  # Input params for all enquiries.

        table.column('intType',      'char',      {'in' : intTypes})
        table.column('intVal',       'int32')
        table.column('intLast',      'datetime',  {'null' : True})  # last interval run
        table.column('intNext',      'datetime',  {'null' : True})  # last interval run)

        table.column('audId',        'string',    {'max' : 128})
        table.column('tmStamp',      'timestamp')

        table.primaryKey('reportId', 'schedId', 'siteId')

        table.foreignKey('FK_RP', 'Report',     'reportId')
        table.foreignKey('FK_ST', 'Fura.Site',  'siteId')

        table.index('IX_ST', 'status',  'intNext')

        p = table.sqlProc('Pending')
        p.inField('valueDate',  'datetime')
        p.outField('',          'table:this')
        p.sql['std'] = '''\
SELECT
  rs.*
FROM
  crest.reportsched rs
WHERE
  rs.status   = 'A'      AND
  rs.intNext IS NOT NULL AND
  rs.intNext <= :valueDate
ORDER BY
  rs.intNext
'''



        return table


if __name__ == '__main__':
    tbl = _MettleObject()

    print('Mettle File Type = %s' % tbl._MettleFileType())
    print('Object Name      = %s' % tbl._Name())
    print('Version          = %f' % tbl._Version())
