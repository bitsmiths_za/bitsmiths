# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


class _MettleObject:
    def _Version(self):
        return 2.0

    def _MettleFileType(self):
        return 'Mettle.DB.Table'

    def _Name(self):
        return 'ReportSchedTmit'

    def _Object(self, dbtable):
        import mettlegen

        table = mettlegen.db.Table(self._Name(), self._Version())

        table.selectOne   = True
        table.selectAll   = False
        table.update      = True
        table.insert      = True
        table.deleteOne   = True
        table.deleteAll   = False

        table.column('id',           'seq64')
        table.column('reportId',     'string',    {'max' : 128})
        table.column('schedId',      'string',    {'max' : 128})
        table.column('siteId',       'int32')

        table.column('outTypeId',    'string',    {'max' : 32})
        table.column('tmitGroupId',  'string',    {'max' : 128})

        table.column('audId',        'string',    {'max' : 128})
        table.column('tmStamp',      'timestamp')

        table.primaryKey('id')

        table.foreignKey('FK_RS', 'ReportSched',  'reportId',    'schedId', 'siteId')
        table.foreignKey('FK_TG', 'TmitGroup',    'tmitGroupId', 'siteId')
        table.foreignKey('FK_OT', 'OutType',      'outTypeId')

        table.index('IX_RS', 'reportId', 'schedId', 'siteId')
        table.index('IX_TG', 'tmitGroupId', 'siteId')
        table.index('IX_OT', 'outTypeId')

        p = table.sqlProc('ForSchedule')
        p.inField('reportId',     'this')
        p.inField('schedId',      'this')
        p.inField('siteId',       'this')
        p.outField('outTypeId',   'this')
        p.outField('tmitGroupId', 'this')
        p.outField('tmitTypeId',  'column:TmitGroupItem.tmitTypeId')
        p.sql['std'] = '''\
SELECT
  rst.outTypeId,
  rst.tmitGroupId,
  tgi.tmitTypeId
FROM
  crest.ReportSchedTmit rst
    JOIN
  crest.TmitGroupItem tgi
    ON tgi.tmitGroupId = rst.tmitGroupId AND
       tgi.siteId      = rst.siteId
WHERE
  rst.reportId = :reportId AND
  rst.schedId  = :schedId  AND
  rst.siteId   = :siteId
ORDER BY
  1,2,3
'''


        return table


if __name__ == '__main__':
    tbl = _MettleObject()

    print('Mettle File Type = %s' % tbl._MettleFileType())
    print('Object Name      = %s' % tbl._Name())
    print('Version          = %f' % tbl._Version())
