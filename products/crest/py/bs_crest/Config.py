# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #
import datetime
import os.path


class Config:
    """
    Class that loads config values on demand from the database. The config values are cached
    for as long as the config is set to.
    """

    def __init__(self, pod):
        """
        Constructor.

        :param pod: (Pod) The pod to use.
        """
        self._pod      = pod
        self.cacheTime = 60  # min
        self.cache     = {}


    def load(self, values, ignoreMissing=False, expandEnvVars=True):
        """
        Loads config values and caches them.

        :param values: (list) List of string values to read.
        :param ignoreMissing: (bool) If true, simply ignores missing parameters.
        :param expandEnvVars: If set to false, won't attempt to expand the env vars.
        :return: (dict) The config loaded.
        """
        fc = None

        if type(values) == str:
            values = [values]

        for val in values:
            if self.cache.get(val):
                continue

            if fc is None:
                fc = self._pod.dao.dConfig(self._pod.dbcon)

            if fc.trySelectOneDeft(val):
                if expandEnvVars:
                    self.cache[val] = os.path.expandvars(fc.rec.value)
                else:
                    self.cache[val] = fc.rec.value
            else:
                if not ignoreMissing:
                    raise Exception('Config id [%s] not found' % val)

        return self.cache


    def read(self, value, expandEnvVars=True):
        """
        Reads a single value from the config.

        :param value: (string) The config value to read.
        :return: (string) The value.
        """
        val = self.cache.get(value)

        if val is None:
            self.load(value, False, expandEnvVars)

        return self.cache[value]


    def _initSiteConfig(self, dtnow):
        """
        Initialize and return a site config if required.

        :param dtnow: (datetime) Current datetime.
        :return: (dict) The config.
        """
        cfg = self.cache

        if not cfg.get('__cache__') or dtnow > cfg['__cache__']:
            cfg.clear()
            cfg['__cache__'] = dtnow + datetime.timedelta(minutes=self.cacheTime)

        return cfg
