# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import json

from mettle.braze.ServerMultiMarsh  import ServerMultiMarsh
from mettle.lib.xMettle             import xMettle

from bs_fura.Access         import Access
from bs_fura.ErrCode        import ErrCode
from bs_fura.Dao            import Dao as FuraDao
from bs_fura.FuraServerImpl import FuraServerImpl
from bs_fura.WebToken       import WebToken
from bs_fura.xFura          import xFura


class CrestServer(ServerMultiMarsh):
    """!
    Crest server..

    To be used by a Faclon/Flask service.
    """
    MODULE         = '[CrestServer]'
    AUTH_LOGGED_IN = '_LOGGED_IN_'

    def __init__(self, prov, trans, cfgOptions):
        """!
        Constructor.

        @param prov         Provider                : The provider object.
        @param trans        mettle.braze.ITransport : The transport object this server will use.
        @param cfgOptions   dict                    : The config file options.
        """
        self._cfg      = None
        self._log      = None
        self._tok      = None

        self._dbcon    = None
        self._pod      = None
        self._furaDao  = None
        self._webToken = None

        self._furaSI   = None
        self._crestSI  = None

        self._furaSM   = None
        self._crestSM  = None

        self._cfg      = prov.newConfig(cfgOptions)
        self._log      = prov.newLogger(options={'name' : ['crestserver', 'web-server', 'server']})
        self._dbcon    = prov.newDBConnector(None, options={
            'db-type' : 'postgresql',
            'db-conn-string' : self._cfg['databases']['postgresql']['crest']})
        self._pod      = prov.newPod(self._cfg, self._log, self._dbcon)

        from bs_fura.db.dao  import postgresql as pdao   # TODO make this dynamic later.

        self._furaDao  = FuraDao(self._dbcon, pdao)
        furaCfg        = self._furaDao.cfg.load(self._furaDao, 0, 'token.webkey')
        self._webToken = WebToken(json.loads(furaCfg['token.webkey']))
        self._furaSI   = FuraServerImpl(self._log, self._furaDao, self._webToken)

        smList = []

        from bs_fura.braze.FuraServerMarshaler import FuraServerMarshaler

        self._furaSM   = FuraServerMarshaler(self._furaSI)

        smList.append(self._furaSM)

        from bs_crest.CrestServerImpl            import CrestServerImpl
        from bs_crest.braze.CrestServerMarshaler import CrestServerMarshaler

        self._crestSI  = CrestServerImpl(prov, self._pod, self._furaSI)
        self._crestSM  = CrestServerMarshaler(self._crestSI)
        smList.append(self._crestSM)

        ServerMultiMarsh.__init__(self, smList, trans, self._log)


    def __del__(self):
        """!
        Destructor.
        """
        if self._pod:
            self._pod.destroy()
            self._pod = None


    def tokenSet(self, tok):
        self._tok = tok


    def tokenGet(self):
        return '' if self._tok is None else self._tok


    def auth(self, rpcName, rpcAuth, rpcArgs):
        """!
        Overload base method.

        This call should raise an exception if the authentication fails.

        @param rpcName     string : The rpc call name.
        @param rpcAuth     string : The rpc authentication method, sort of a factory string.
        @param rpcArgs     dict   : The rpc args in a dictionary.
        @return            dict   : A auth data object for the server impl to possibly use.
        """
        self._log.debug('CrestServer.auth - start [rpc:%s, auth:%s]' % (rpcName, rpcAuth))

        if not rpcAuth:
            raise Exception('Authorize has no method! [rpc:%s]' % str(rpcName))

        if not self._tok:
            raise xMettle("Token has not been set! [rpc:%s, rpcAuth:%s]!" % (
                str(rpcName), str(rpcAuth)), self.MODULE, xMettle.eCode.TokenInvalid)

        claim = self._webToken.readUsrToken(self._tok)

        self._dbcon.rollback()

        try:
            with Access(self._furaDao, claim['site'], claim['usr'], rpcName) as acc:
                self._tok     = self._webToken.refreshUsrToken(acc, claim, self._tok)
                claim['role'] = acc._role

                if rpcAuth == self.AUTH_LOGGED_IN:
                    return claim

                acc.required(rpcAuth, rpcArgs.get('eaTok'))

            self._log.debug('CrestServer.auth - done [success, usrid:%s, rpc:%s, auth:%s]' % (
                str(claim.get('usr')), rpcName, rpcAuth))

        except xFura as xf:
            self._log.debug('CrestServer.auth - done [failed:%s, usrid:%s, rpc:%s, auth:%s]' % (
                str(xf), str(claim.get('usr')), rpcName, rpcAuth))

            ec = xf.getErrorCode()

            if ec == ErrCode.AccessDenied:
                raise xMettle(
                    "Access denied [ec:%d, reason:%s]!" % (ec.value, ec.name),
                    self.MODULE, xMettle.eCode.CredDenied)

            raise

        except Exception as x:
            self._log.debug('CrestServer.auth - done [failed:%s, usrid:%s, rpc:%s, auth:%s]' % (
                str(x), str(claim.get('usr')), rpcName, rpcAuth))
            raise
        finally:
            self._dbcon.commit()

        return claim
