# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import copy
import time

from mettle.lib.xMettle import xMettle

from bs_crest.braze.CrestServerInterface import CrestServerInterface

from bs_crest.GenManager     import GenManager
from bs_crest                import StdErrors

from bs_crest.db.tables.tConfig             import tConfig
from bs_crest.db.tables.oEnquiryByUserRole  import oEnquiryByUserRole
from bs_crest.db.tables.oGenSearch          import oGenSearch
from bs_crest.db.tables.tLookup             import tLookup
from bs_crest.db.tables.tParam              import tParam

from bs_lib.AutoTransaction import AutoTransaction
from bs_lib.Pod             import Pod
from bs_lib                 import QueryBuilder

from bs_triceraclops.Audit  import Audit


class CrestServerImpl(CrestServerInterface):

    MODULE = '[CrestServerImpl]'

    def __init__(self, log, dao, wt):
        """
        Constructor.

        :param log: (Logger) The logging object to use.
        :param dao: (Dao) Fura dao object.
        """
        self._shutdown = False
        self._log      = log
        self._cdao     = dao
        self._pod      = None
        self._audit    = None


    def _shutdownServer(self):
        return self._shutdown


    def _slackTimeHandler(self):
        pass


    def _construct(self):
        self._destruct()

        # Todo later, make a provider for this or something.
        from bs_triceraclops.db.dao import postgresql as pdao

        self._pod      = self._initPod(self._cdao.dbcon, self._log)
        self._audit    = Audit(self._cdao.dbcon, pdao)
        self._tokData  = None


    def _destruct(self):
        self._audit = None

        if self._pod:
            self._pod.dbcon = None
            self._pod       = None


    def _setRPCTokenData(self, tdata):
        self._tokData  = tdata


    def _initPod(self, dbcon, log):
        p = Pod()

        p.initialize({}, self._log, self._cdao.dbcon)

        return p


    def _siteId(self):
        if not self._tokData:
            raise Exception('Auth user required for RPC call, but no user is logged in!')

        siteId = self._tokData.get('site')

        if siteId is None or type(siteId) != int:
            raise Exception('Auth site is not set!')

        return siteId


    def _usrId(self):
        if not self._tokData:
            raise Exception('Auth user required for RPC call, but no user is logged in!')

        usrId = self._tokData.get('usr')

        if not usrId:
            raise Exception('Auth user is not set!')

        return usrId


    def _roleId(self):
        if not self._tokData:
            raise Exception('Auth user required for RPC call, but no user is logged in!')

        role = self._tokData.get('role')

        if not role:
            raise Exception('Auth role is not set!')

        return role

    # ------------------------------------------------------------------------
    # SERVER INTERFACE METHODS
    # ------------------------------------------------------------------------
    def ping(self):
        """
        Ping method to test is server is up.
        """
        time.sleep(0.5)
        return True


    def configCreate(self, rec):
        """
        Create a new config.

        :param rec: (tConfig) New config .
        :return: (tConfig) Created recrod.
        """
        with AutoTransaction(self._audit, self._pod) as at:
            dc        = self._cdao.dao.dConfig(self._pod.dbcon)
            rec.audId = self._usrId()

            dc.insert(rec)
            self._audit.aud(1, self.MODULE, rec.audId).diff(None, rec, 'crest.Config')
            at.commit()

            return rec


    def configRead(self, cfgId):
        """
        Read a config or list of configs.

        :param cfgId: (string) Id or pattern to to search for.
        :return: (tConfig) The matching config records.
        """
        with AutoTransaction(self._pod):
            qry = self._cdao.dao.dConfigSearch(self._pod.dbcon)
            res = tConfig.List()

            qry.irec.criteria += QueryBuilder.dynCrit(cfgId, 'c', 'id')

            qry.exec().fetchAll(res)

            return res


    def configUpdate(self, rec):
        """
        Update a config value.

        :param rec: (tConfig) The record to update.
        :return: (tConfig) The updated rec.
        """
        with AutoTransaction(self._audit, self._pod) as at:
            dc        = self._cdao.dao.dConfig(self._pod.dbcon)
            rec.audId = self._usrId()

            dc.lockOneDeft(rec.id, self._pod.stdDBLock())

            if dc.rec.tmStamp != rec.tmStamp:
                raise xMettle(StdErrors.STALE, self.MODULE)

            bef = copy.copy(dc.rec)

            dc.update(rec)
            self._audit.aud(1, self.MODULE, rec.audId).diff(bef, rec, 'crest.Config')
            at.commit()

            return rec


    def configDelete(self, cfgId):
        """
        Delete a config entry.

        :param cfgId: (string) The config to delete.
        """
        with AutoTransaction(self._audit, self._pod) as at:
            dc = self._cdao.dao.dConfig(self._pod.dbcon)
            dc.selectOneDeft(cfgId)
            dc.deleteOneDeft(cfgId)
            self._audit.aud(1, self.MODULE, self._usrId()).diff(dc.rec, None, 'crest.Config')
            at.commit()


    def enquiryRun(self, enquiryId, params):
        """
        Run an enquiry.

        :param enquiryId: (string) The enquiry to run.
        :param params: (   bParameter) : The input parameters.
        :return: (oGenSearch) The generation record.
        """
        with AutoTransaction(self._pod) as at:
            with GenManager(self._pod, self._siteId(), self._usrId(), self._roleId()) as gm:
                res = gm.enquiryRun(enquiryId, params)
                at.commit()
                return res


    def enquirySearchHist(self, dateFrom, dateTo, enquiryId, status, reqBy):
        """
        Search through general enquiry run history.

        :param dateFrom: (datetime) Date to search from.
        :param dateTo: (datetime) Date to search to.
        :param enquiryId: (string) Optionally give the enquiry to search for, wild cards ok.
        :param status: (CharList) Optionally give a list of statuses to search for.
        :param reqBy: (string) Optionally give a user to search for, wild cards ok.
        :return: (oGenSearch) The enquiry generations matched.
        """
        with AutoTransaction(self._pod):
            qry = self._pod.dao.dGenSearch(self._pod.dbcon)
            res = oGenSearch.List()

            qry.irec.siteId    = self._siteId()
            qry.irec.dateFrom  = datetime.datetime(dateFrom.year, dateFrom.month, dateFrom.day,  0,  0,  0)
            qry.irec.dateTo    = datetime.datetime(dateTo.year,   dateTo.month,   dateTo.day,   23, 59, 59)
            qry.irec.maxRecs   = 1024
            qry.irec.criteria += ' and g.enquiryId is not null'
            qry.irec.criteria += QueryBuilder.dynCrit(enquiryId, 'g', 'enquiryId')
            qry.irec.criteria += QueryBuilder.dynList(status,    'g', 'status')
            qry.irec.criteria += QueryBuilder.dynCrit(reqBy,     'g', 'reqBy')

            qry.exec.fetchAll(res)

            return res


    def enquiryUserRoles(self):
        """
        Get the logged in users enquiry role functions.

        :return: (oEnquiryByUserRole) The enquiry role relations for the logged in user.
        """
        with AutoTransaction(self._pod):
            qry = self._pod.dao.dEnquiryByUseRole(self._pod.dbcon)
            res = oEnquiryByUserRole.List()

            qry.execDeft(self._roleId(), self._siteId()).fetchAll(res)

            return res


    def enquiryUserHist(self, dateFrom, dateTo):
        """
        Retrieves the logged in users enquiry history.

        :param dateFrom: (datetime) From date to search for.
        :param dateTo: (datetime) To date to search for.
        :return: (oGenSearch) The matched records.
        """
        with AutoTransaction(self._pod):
            qry = self._pod.dao.dGenSearch(self._pod.dbcon)
            res = oGenSearch.List()

            qry.irec.siteId    = self._siteId()
            qry.irec.dateFrom  = datetime.datetime(dateFrom.year, dateFrom.month, dateFrom.day,  0,  0,  0)
            qry.irec.dateTo    = datetime.datetime(dateTo.year,   dateTo.month,   dateTo.day,   23, 59, 59)
            qry.irec.maxRecs   = 1024
            qry.irec.criteria += ' and g.enquiryId is not null'
            qry.irec.criteria += QueryBuilder.dynCrit(self._usrId(), 'g', 'reqBy')

            qry.exec.fetchAll(res)

            return res


    def lookupCreate(self, rec):
        """
        Create a new lookup.

        :param rec: (tLookup) New lookup.
        :return: (tLookup) Created record.
        """
        with AutoTransaction(self._audit, self._pod) as at:
            dl        = self._cdao.dao.dLookup(self._pod.dbcon)
            rec.audId = self._usrId()

            dl.insert(rec)
            self._audit.aud(1, self.MODULE, rec.audId).diff(None, rec, 'crest.Lookup')
            at.commit()

            return rec


    def lookupRead(self, lookupId):
        """
        Read a lookup or list of lookups.

        :param lookupId: (string) Id or pattern to to search for.
        :return: (tLookup) The matching lookup records.
        """
        with AutoTransaction(self._pod):
            qry = self._cdao.dao.dLookupSearch(self._pod.dbcon)
            res = tLookup.List()

            qry.irec.criteria += QueryBuilder.dynCrit(lookupId, 'l', 'id')

            qry.exec().fetchAll(res)

            return res


    def lookupUpdate(self, rec):
        """
        Update a lookp value.

        :param rec: (tLookup) The record to update.
        :return: (tLookup) The updated rec.
        """
        with AutoTransaction(self._audit, self._pod) as at:
            dl        = self._cdao.dao.dLookup(self._pod.dbcon)
            rec.audId = self._usrId()

            dl.lockOneDeft(rec.id, self._pod.stdDBLock())

            if dl.rec.tmStamp != rec.tmStamp:
                raise xMettle(StdErrors.STALE, self.MODULE)

            bef = copy.copy(dl.rec)

            dl.update(rec)
            self._audit.aud(1, self.MODULE, rec.audId).diff(bef, rec, 'crest.Lookup')
            at.commit()

            return rec


    def lookupDelete(self, lookupId):
        """
        Delete a lookup entry.

        :param lookupId: (string) The lookup to delete.
        """
        with AutoTransaction(self._audit, self._pod) as at:
            dl = self._cdao.dao.dLookup(self._pod.dbcon)
            dl.selectOneDeft(lookupId)
            dl.deleteOneDeft(lookupId)
            self._cdao.dao.dLookupItemDeleteByLookup(self._pod.dbcon).execDeft(lookupId)
            self._audit.aud(1, self.MODULE, self._usrId()).diff(dl.rec, None, 'crest.Lookup')
            at.commit()


    def paramCreate(self, rec):
        """
        Create a new param.

        :param rec: (tParam) New param.
        :return: (tParam) Created param.
        """
        with AutoTransaction(self._audit, self._pod) as at:
            dp        = self._cdao.dao.dParam(self._pod.dbcon)
            rec.audId = self._usrId()

            dp.insert(rec)
            self._audit.aud(1, self.MODULE, rec.audId).diff(None, rec, 'crest.Param')
            at.commit()

            return rec


    def paramRead(self, paramId):
        """
        Read a parameters or list of parameters.

        :param paramId: (string) Id or pattern to to search for.
        :return: (tParam) The matching param records.
        """
        with AutoTransaction(self._pod):
            qry = self._cdao.dao.dParamSearch(self._pod.dbcon)
            res = tParam.List()

            qry.irec.criteria += QueryBuilder.dynCrit(paramId, 'p', 'id')

            qry.exec().fetchAll(res)

            return res


    def paramUpdate(self, rec):
        """
        Upate a param record.

        :param rec: (tParam) The record to update.
        :return: (tParam) The updated rec.
        """
        with AutoTransaction(self._audit, self._pod) as at:
            dp        = self._cdao.dao.dParam(self._pod.dbcon)
            rec.audId = self._usrId()

            dp.lockOneDeft(rec.id, self._pod.stdDBLock())

            if dp.rec.tmStamp != rec.tmStamp:
                raise xMettle(StdErrors.STALE, self.MODULE)

            bef = copy.copy(dp.rec)

            dp.update(rec)
            self._audit.aud(1, self.MODULE, rec.audId).diff(bef, rec, 'crest.Param')
            at.commit()

            return rec


    def paramDelete(self, paramId):
        """
        Delete a parameter record.

        :param paramId: (string) input
        """
        with AutoTransaction(self._audit, self._pod) as at:
            dp = self._cdao.dao.dParam(self._pod.dbcon)
            dp.selectOneDeft(paramId)
            dp.deleteOneDeft(paramId)
            self._audit.aud(1, self.MODULE, self._usrId()).diff(dp.rec, None, 'crest.Param')
            at.commit()
