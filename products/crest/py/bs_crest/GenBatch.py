# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os
import os.path
import datetime
import traceback
import operator
import functools
import json

from bs_lib import Common

from bs_crest.Config      import Config
from bs_crest.InputVar    import InputVar
from bs_crest.LookupCache import LookupCache
from bs_crest             import TypeUtil

from bs_crest.db.tables.tGen      import tGen
from bs_crest.db.tables.tParam    import tParam
from bs_crest.db.tables.tQueryOut import tQueryOut


_SORT_INDEXES_ = []


def _genBatch_Sort_Compare(x, y):
    """
    Does the sorting out the query list output.

    :param x: (list) Row of data.
    :param y: (list) Row of data.
    :return: The compare result.
    """
    for si in _SORT_INDEXES_:
        if si[0] == 'a':
            if x[si[1]] < y[si[1]]:
                return -1
            elif x[si[1]] > y[si[1]]:
                return 1
        else:
            if x[si[1]] < y[si[1]]:
                return 1
            elif x[si[1]] > y[si[1]]:
                return -1

    return 0


class GenBatch:
    """
    This object generates the actual enquriy and report
    objects.

    It uses can use a read only database connection.
    """
    MODULE = '[GenBatch]'


    def __init__(self, pod, dao):
        """
        Constructor.

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) Crest Dao object.
        """
        self._pod         = pod
        self._log         = pod.log
        self._cdao        = dao
        self._lookupCache = LookupCache(dao)
        self._ivar        = None

        self._startTM    = datetime.datetime.min
        self._endTM      = datetime.datetime.min
        self._genPath    = ''
        self._genRec     = tGen()
        self._genCaption = ''
        self._genOutCapt = ''
        self._runObjs    = []
        self._outFH      = None
        self._reportRec  = None


    def run(self, genId):
        """
        Generate the output for the specified genreation id.

        :param genId: (int) The gen id to generate for.
        """
        self._startTM = datetime.datetime.now()

        self._log.info('run - started [genId:%d]' % genId)

        totRows = 0

        try:
            self._initialize(genId)

            for ro in self._runObjs:
                totRows += self._runObject(ro)

            self._writeGenResult(genId, totRows)

        except Exception as x:
            strace = traceback.format_exc()
            self._log.error('run - failed [genId:%d, error:%s, trace:%s]' % (
                genId, str(x), strace))
            self._writeGenResult(genId, totRows, x, strace)


        self._log.info('run - done [genId:%d]' % genId)


    def _initialize(self, genId):
        """
        Initialize the batch and ensure all paths, settings, and configs are
        valid for processesing.

        :param genId: (int) The gen pk.
        """
        conf = Config(self._cdao)
        cfg  = conf.load(['output.path'])

        self._genPath = Common.readDict(cfg, 'output.path',    str)

        if not os.path.exists(self._genPath) or not os.path.isdir(self._genPath):
            raise Exception('Generation path not found/valid [%s]' % self._genPath)

        dg = self._cdao.dao.dGen(self._cdao.dbcon)
        dg.selectOneDeft(genId)

        if dg.rec.status != tGen.Status_Couplet.keyBusy:
            raise Exception('Gen record is in unexpected status [genId:%d, status:%s]' % (
                genId, tGen.Status_Couplet.getValue(dg.rec.status)))

        if dg.rec.enquiryId != '' and dg.rec.reportId != '':
            raise Exception('Gen record cannot be both an enquiry and a report [genId:%d]' % (genId))

        self._genPath = os.path.join(self._genPath, dg.rec.reqDate.strftime(TypeUtil.MONTH_FMT))

        if not os.path.exists(self._genPath):
            os.makedirs(self._genPath)

        self._genRec = dg.rec
        self._ivar   = InputVar(self._genRec)

        if dg.rec.enquiryId != '':
            de = self._cdao.dao.dEnquiry(self._cdao.dbcon)

            de.selectOneDeft(dg.rec.enquiryId)

            self._runObjs.append(self._initRunObject(dg.rec, de.rec))

            self._genCaption = de.rec.caption
            self._genOutCapt = de.rec.caption if de.rec.outCaption == '' else de.rec.outCaption

        elif dg.rec.reportId != '':
            dr  = self._cdao.dao.dReport(self._cdao.dbcon)
            der = self._cdao.dao.dEnquiryForReport(self._cdao.dbcon)
            res = []

            dr.selectOneDeft(dg.rec.reportId)

            if 0 == der.execDeft(dg.rec.reportId).fetchAll(res):
                raise Exception('Report for gen record has no linked enquiries! [genId:%d, reportId:%s]' % (
                    genId, dg.rec.reportId))

            for rec in res:
                self._runObjs.append(self._initRunObject(dg.rec, rec))

            self._reportRec  = dr.rec
            self._genCaption = dr.rec.caption
            self._genOutCapt = dr.rec.caption if dr.rec.outCaption == '' else dr.rec.outCaption

        else:
            raise Exception('Gen item is neither an enquiry or a report [genId:%d]' % (genId))


    def _writeGenResult(self, genId, totRows, ex=None, strace=None):
        """
        Writes the exception to the output info file.

        :param genId: (int) The gen object id.
        :param totRows: (int) Total rows from generation.
        :param ex: (Exception) The exception object.
        :param strace: (string) The exception stack trace
        """
        self._closeOutFile()

        self._endTM = datetime.datetime.now()

        if not os.path.exists(self._genPath):
            self._log.warning('Could not write result, genpath does not exist [genId:%d]' % genId)
            return

        obj = {
            'ok'          : True if not ex else False,
            'ver'         : '1.0',
            'genId'       : genId,
            'startTime'   : self._startTM.strftime(TypeUtil.DATETIME_FMT),
            'endTime'     : '' if self._endTM == datetime.date.min else self._endTM.strftime(TypeUtil.DATETIME_FMT),
            'totTime'     : 0.0 if self._endTM == datetime.date.min else (self._endTM - self._startTM).total_seconds(),
            'totalRows'   : totRows,
            'exception'   : '' if not ex else str(ex),
            'stackTrace'  : '' if not ex else str(strace),
            'queries'     : [],
            'caption'     : self._genCaption.strip(),
        }

        for ro in self._runObjs:
            obj['queries'].append({
                'queryId'        : ro['query']['id'],
                'name'           : ro['name'],
                'caption'        : self._substOutCaption(ro, ro['caption']),
                'enqId'          : ro['enqId'],
                'queryRows'      : ro['query']['totRows'],
                'queryStartTime' : ro['query']['startTM'].strftime(TypeUtil.DATETIME_FMT),
                'queryEndTime'   : ro['query']['endTM'].strftime(TypeUtil.DATETIME_FMT),
                'queryTotTime'   : 0.0 if ro['query']['endTM'] == datetime.datetime.min else (
                    ro['query']['endTM'] - ro['query']['startTM']).total_seconds(),
                'procStartTime'  : ro['input']['startTM'].strftime(TypeUtil.DATETIME_FMT),
                'procEndTime'    : ro['input']['endTM'].strftime(TypeUtil.DATETIME_FMT),
                'procTotTime'    : 0.0 if ro['input']['endTM'] == datetime.datetime.min else (
                    ro['input']['endTM'] - ro['input']['startTM']).total_seconds(),
            })


        with open(os.path.join(self._genPath, '%d.result' % genId), 'wt') as fh:
            fh.write(json.dumps(obj))


    def _initRunObject(self, genRec, enqRec):
        """
        Create a run object from an enquiry record.

        :param genRec: (tGen) The gen object.
        :param enqRec: (tEnquiry) The enquiry object.
        :return: The run object
        """
        self._log.debug('_initRunObjectFromEnquiry - start[genId:%d, enqId:%s]' % (genRec.id, enqRec.id))

        ro = {
            'genId'   : genRec.id,
            'name'    : enqRec.caption,
            'caption' : enqRec.caption if not enqRec.outCaption else enqRec.outCaption,
            'query'   : None,
            'enqId'   : enqRec.id,
            'input'   : None,
            'idx'     : len(self._runObjs),
            'tmStamp' : datetime.datetime.now(),
        }

        enq = {
            'id'      : enqRec.id,
            'name'    : enqRec.caption,
            'caption' : enqRec.caption if not enqRec.outCaption else enqRec.outCaption,
            'params'  : None if not genRec.param  else json.loads(genRec.param),
            'sortBy'  : None if not enqRec.sortBy else json.loads(enqRec.sortBy),
            'in'      : [],
            'out'     : [],
            'grps'    : [],
            'startTM' : datetime.datetime.min,
            'endTM'   : datetime.datetime.min,
        }

        self._cdao.dao.dEnquiryInByEnquiry(self._cdao.dbcon).execDeft(enqRec.id).fetchAll(enq['in'])
        self._cdao.dao.dEnquiryOutByEnquiry(self._cdao.dbcon).execDeft(enqRec.id).fetchAll(enq['out'])
        self._cdao.dao.dEnquiryGrpByEnquiry(self._cdao.dbcon).execDeft(enqRec.id).fetchAll(enq['grps'])

        ro['input'] = enq

        self._addQueryToRunObject(ro, enqRec.queryId)

        self._validateAndMapRunObject(ro)

        self._validateSortBy(ro)

        self._validateGroups(ro)

        self._log.debug('_initRunObjectFromEnquiry - done[genId:%d, enquiryId:%s]' % (genRec.id, enqRec.id))

        return ro


    def _addQueryToRunObject(self, ro, queryId):
        """
        Adds the query details to the run object.

        :param ro: (dict) The run object.
        :param queryId: (string) The query id.
        """
        dq = self._cdao.dao.dQuery(self._cdao.dbcon)

        dq.selectOneDeft(queryId)

        qry = {
            'id'      : dq.rec.id,
            'caption' : dq.rec.caption,
            'qry'     : dq.rec.qry,
            'in'      : [],
            'out'     : [],
            'mapIn'   : None,
            'mapOut'  : None,
            'startTM' : datetime.datetime.min,
            'endTM'   : datetime.datetime.min,
            'cols'    : [],
            'rows'    : [],
            'totRows' : 0,
        }

        self._cdao.dao.dQueryInForExecute(self._cdao.dbcon).execDeft(dq.rec.id).fetchAll(qry['in'])
        self._cdao.dao.dQueryOutByQuery(self._cdao.dbcon).execDeft(dq.rec.id).fetchAll(qry['out'])

        if len(qry['out']) < 1:
            raise Exception('Query [%s (%s)] has no output columns' % (qry['caption'], qry['id']))

        ro['query'] = qry


    def _validateAndMapRunObject(self, ro):
        """
        Ensures a run object is valid before we begin running or generating output. It also
        creates the column in/out mappings while it is validating.

        :param ro: (dict) The run object.
        """
        mapin  = {}
        mapout = {}

        for irec in ro['query']['in']:
            mapin[irec.id] = {'qryin' : irec, 'inpin' : None, 'parin' : None, 'value' : None}

        idx = 0

        if ro['input']['params']:
            for prec in ro['input']['params']:
                if type(prec) != dict:
                    raise Exception('Invalid parameter found, type is not a dictionary [param:%s, type:%s, idx:%d]' % (
                        str(prec), str(type(prec)), idx))

                pid   = prec.get('id')
                pval  = prec.get('value')
                idx  += 1

                if not pid or not pval:
                    raise Exception('Invalid generation parameter found [id:%s, value:%s, idx:%d]' % (
                        str(pid), str(pval), idx))

                pid  = str(pid)
                pval = str(pval)

                mo = mapin.get(pid)

                if not mo:
                    self._log.warning('Param input does not match any query input [id:%s, value:%s, idx:%d, queryId:%s]' % (
                        pid, pval, idx, str(ro['query'].get('id'))))
                    continue

                if self._ivar.isVar(pval):
                    val = TypeUtil.toType(self._ivar.getVarValue(pval, pid), mo['qryin'].paramType)
                else:
                    val = TypeUtil.toType(pval, mo['qryin'].paramType)

                if not val or val == datetime.datetime.min or val == datetime.date.min:
                    raise Exception('Param input value not valid [id:%s, val:%s, typeExcpted:%s]' % (
                        pid, pval, mo['qryin'].paramType))

                mo['value'] = val


        for irec in ro['input']['in']:
            mo = mapin.get(irec.id)

            if not mo:
                raise Exception('Input [id:%s, enqId:%s] not found for query [%s]' % (
                    irec.id, ro['enqId'], ro['query']['id']))

            if not mo['value'] is not None:
                continue

            if len(irec.defVal) < 1:
                continue

            if self._ivar.isVar(irec.defVal):
                val = TypeUtil.toType(self._ivar.getVarValue(irec.defVal, irec.id), mo['qryin'].paramType)
            else:
                val = TypeUtil.toType(irec.defVal, mo['qryin'].paramType)

            if val is None or val == datetime.datetime.min or val == datetime.date.min:
                raise Exception('Default input value not valid [id:%s, val:%s, typeExcpted:%s]' % (
                    irec.id, irec.defVal, mo['qryin'].paramType))

            mo['value'] = val


        for mid, mo in mapin.items():
            if mo['value'] is None:
                mo['value'] = TypeUtil.toType('', mo['qryin'].paramType)
                # raise Exception('Query parameter [id:%s] has not been set by the enquiry input!' % (mid))

        idx = 0

        for orec in ro['input']['out']:

            if orec.queryOutId.count(':') == 1:
                sp = orec.queryOutId.split(':')
                qryoutid        =\
                orec.queryOutId = sp[0]
                mapname         = sp[1]
            else:
                qryoutid =\
                mapname  = orec.queryOutId

            if mapout.get(mapname):
                raise Exception('Duplicate enquiry out column [id:%s, queryOutId:%s, orderIdx:%d] detected [queryId:%s]' % (
                    orec.enquiryId, orec.queryOutId, orec.orderIdx, ro['query']['id']))

            mobj = {
                'qryout' : None,
                'inpout' : orec,
                'idx'    : idx,
                'col'    : 0,
                'outPre' : None,
                'mapid'  : mapname,
            }

            mapout[mapname] = mobj

            for qrec in ro['query']['out']:
                if qrec.id != qryoutid:
                    continue

                mobj['qryout'] = qrec
                break

            if not mobj['qryout']:
                raise Exception('Enquiry out column [id:%s, caption:%s, orderIdx:%d] does not link to parent query output \
[queryId:%s]' % (orec.enquiryId, orec.caption, orec.orderIdx, ro['query']['id']))

            if mobj['qryout'].outType == tQueryOut.OutType_Couplet.keyFloat or\
               mobj['qryout'].outType == tQueryOut.OutType_Couplet.keyPercentile:
                if mobj['inpout'].outPrecision >= 0:
                    mobj['outPre'] = mobj['inpout'].outPrecision
                elif mobj['qryout'].outPrecision >= 0:
                    mobj['outPre'] = mobj['qryout'].outPrecision

            if orec.lookupId != '':
                self._lookupCache.cache(orec.lookupId)

            idx += 1

        ro['query']['mapIn']  = mapin
        ro['query']['mapOut'] = mapout


    def _validateSortBy(self, ro):
        """
        Validate that the sort by clause maps the actual output columns.

        :param ro: (dict) Run object.
        """
        if ro['input']['sortBy'] is None:
            return

        sb  = ro['input']['sortBy']
        idx = 0

        if type(sb) != list:
            raise Exception('SortBy not a valid type, exepcted a list of dictionaries [type:%s]' % (
                str(type(sb))))

        for sitem in sb:
            idx += 1

            if type(sitem) != dict:
                raise Exception('SortBy not a valid type, element not a dictionary [element:%d]' % (idx))

            col   = sitem.get('column')
            order = sitem.get('order')

            if not col or type(col) != str:
                raise Exception('SortBy not valid, "column" not found [elment:%d]' % (idx))

            if not order or type(order) != str:
                raise Exception('SortBy not valid, "order" not found [element:%d]' % (idx))

            if order.lower() not in ['asc', 'desc']:
                raise Exception('SortBy not valid, order value not expected [order:%s, element:%d]' % (order, idx))

            mo = ro['query']['mapOut'].get(col)

            if not mo:
                raise Exception('SortBy not valid, column not found in output [column:%s, element:%d]' % (col, idx))


    def _validateGroups(self, ro):
        """
        Validate that the groups are all ok.

        :param ro: (dict) Run object.
        """
        if not ro['input']['grps']:
            return

        grps = ro['input']['grps']

        for gitem in grps:
            try:
                gitem.grpCols = None if not gitem.grpCols else json.loads(gitem.grpCols)
                gitem.sumCols = None if not gitem.sumCols else json.loads(gitem.sumCols)
                gitem.header  = None if not gitem.header  else json.loads(gitem.header)
                gitem.footer  = None if not gitem.footer  else json.loads(gitem.footer)

                if not gitem.header and not gitem.footer:
                    raise Exception('Header and footer both empty, at least one is required.')

                if gitem.grpCols:
                    if type(gitem.grpCols) != list:
                        raise Exception('grpCols [%s] is not a list' % str(type(gitem.grpCols)))

                    cidx  = 0
                    mlist = []
                    gdict = {}

                    for col in gitem.grpCols:
                        par = 'grpCols.col[%d].' % cidx

                        if type(col) != dict:
                            raise Exception('%s is not a dictionary' % (par[:-1]))

                        cname = Common.readDict(col, 'column',    str,  par)
                        hide  = Common.readDict(col, 'hide',      bool, par, optional = True)
                        mo    = ro['query']['mapOut'].get(cname)

                        if not mo:
                            raise Exception('%scolumn [%s] not found in output' % (par, cname))

                        if hide:
                            mo['inpout'].hidden = True

                        mlist.append(mo)
                        gdict[cname] = col
                        cidx += 1

                    setattr(gitem, 'mlist', mlist)
                    setattr(gitem, 'gdict', gdict)


                if gitem.sumCols:
                    if type(gitem.sumCols) != list:
                        raise Exception('sumCols [%s] is not a list' % str(type(gitem.sumCols)))

                    cidx  = 0
                    sdict = {}

                    for col in gitem.sumCols:
                        par = 'sumCols.col[%d].' % cidx

                        if type(col) != dict:
                            raise Exception('%s is not a dictionary' % (par[:-1]))

                        cname = Common.readDict(col, 'column',    str,  par)
                        mo    = ro['query']['mapOut'].get(cname)

                        if not mo:
                            raise Exception('%scolumn [%s] not found in output' % (par, cname))

                        sdict[cname] = mo
                        cidx += 1

                    setattr(gitem, 'sdict', sdict)


                visCols = 0

                for name, mo in ro['query']['mapOut'].items():
                    if not mo['inpout'].hidden or mo['inpout'].hidden == 'N':
                        visCols += 1
                        mo['inpout'].hidden = False

                if gitem.header:
                    if type(gitem.header) != list:
                        raise Exception('header [%s] is not a list' % str(type(gitem.header)))

                    cidx    = 0
                    totspan = 0
                    hlist   = []

                    for col in gitem.header:
                        par = 'header[%d].' % cidx

                        if type(col) != dict:
                            raise Exception('%s is not a dictionary' % (par[:-1]))

                        cname = Common.readDict(col, 'column',  str,  par)
                        text  = Common.readDict(col, 'text',    str,  par, optional=True)
                        sumc  = Common.readDict(col, 'sum',     str,  par, optional=True)
                        avgc  = Common.readDict(col, 'avg',     str,  par, optional=True)
                        span  = Common.readDict(col, 'span',    int,  par, optional=True)

                        if text == '':
                            text        = None
                            col['text'] = text

                        if sumc == '':
                            sumc        = None
                            col['sum'] = sumc

                        if avgc == '':
                            avgc       = None
                            col['avg'] = avgc

                        if not text and not sumc and not avgc:
                            raise Exception('%scolumn [%s] requires text, sum, or avg attribute' % (par, cname))

                        if cname != '*':
                            mo = ro['query']['mapOut'].get(cname)

                            if not mo:
                                raise Exception('%scolumn [%s] not found in output' % (par, cname))

                            if not span:
                                col['span'] = 1
                                span        = 1

                            if span < 1:
                                raise Exception('%scolumn [%s] has an invalid span [%s]' % (par, cname, str(span)))

                            totspan += span
                            hlist.append(mo)
                        else:
                            totspan += visCols
                            hlist.append(visCols)

                        if totspan > visCols:
                            raise Exception('%s total span [%d] greater than the number of visible columns [%s] - col [%s]' % (
                                par[:-1], totspan, visCols, str(col)))

                        if (sumc or avgc) and not gitem.sumCols:
                            raise Exception('%s.column [%s] sum/avg detected, but no sum columns have been defined!' % (
                                par[:-1], cname))

                        if sumc:
                            fnd = False

                            for col in gitem.sumCols:
                                if sumc == col['column']:
                                    fnd = True
                                    break

                            if not fnd:
                                raise Exception('%scolumn [%s, sum:%s] is not a sum column!' % (par[:-1], cname, sumc))

                        if avgc:
                            fnd = False

                            for col in gitem.sumCols:
                                if avgc == col['column']:
                                    fnd = True
                                    break

                            if not fnd:
                                raise Exception('%scolumn [%s, avg:%s] is not a sum column!' % (par[:-1], cname, avgc))

                        cidx += 1

                    setattr(gitem, 'hlist', hlist)

                if gitem.footer:
                    if type(gitem.footer) != list:
                        raise Exception('footer [%s] is not a list' % str(type(gitem.footer)))

                    cidx    = 0
                    totspan = 0
                    flist   = []

                    for col in gitem.footer:
                        par = 'footer[%d].' % cidx

                        if type(col) != dict:
                            raise Exception('%s is not a dictionary' % (par[:-1]))

                        cname = Common.readDict(col, 'column',  str,  par)
                        text  = Common.readDict(col, 'text',    str,  par, optional=True)
                        sumc  = Common.readDict(col, 'sum',     str,  par, optional=True)
                        avgc  = Common.readDict(col, 'avg',     str,  par, optional=True)
                        span  = Common.readDict(col, 'span',    int,  par, optional=True)

                        if text == '':
                            text        = None
                            col['text'] = text

                        if sumc == '':
                            sumc        = None
                            col['sum'] = sumc

                        if avgc == '':
                            avgc       = None
                            col['avg'] = avgc

                        if cname != '*':
                            mo = ro['query']['mapOut'].get(cname)

                            if not mo:
                                raise Exception('%scolumn [%s] not found in output' % (par, cname))

                            if not span:
                                col['span'] = 1
                                span        = 1

                            if span < 1:
                                raise Exception('%scolumn [%s] has an invalid span [%s]' % (par, cname, str(span)))

                            totspan += span

                            flist.append(mo)
                        else:
                            totspan += visCols
                            flist.append(visCols)

                        if totspan > visCols:
                            raise Exception('%s total span [%d] greater than the number of visible columns [%s]' % (
                                par[:-1], totspan, visCols))

                        if (sumc or avgc) and not gitem.sumCols:
                            raise Exception('%s.column [%s] sum/avg detected, but no sum columns have been defined!' % (
                                par[:-1], cname))

                        if sumc:
                            fnd = False

                            for col in gitem.sumCols:
                                if sumc == col['column']:
                                    fnd = True
                                    break

                            if not fnd:
                                raise Exception('%scolumn [%s, sum:%s] is not a sum column!' % (par[:-1], cname, sumc))

                        if avgc:
                            fnd = False

                            for col in gitem.sumCols:
                                if avgc == col['column']:
                                    fnd = True
                                    break

                            if not fnd:
                                raise Exception('%scolumn [%s, avg:%s] is not a sum column!' % (par[:-1], cname, avgc))

                        cidx += 1

                    setattr(gitem, 'flist', flist)

            except Exception as x:
                self._log.error('Group [%d] not valid [err:%s, trace:%s]' % (gitem.id, str(x), traceback.format_exc()))
                raise Exception('Group [%d] not valid: %s' % (gitem.id, str(x)))


    def _runObject(self, ro):
        """
        Run a report object.

        :param ro: (dict) The run object.
        :return: Number of rows this query returned.
        """
        self._log.info('_runObject - start [genId:%d, caption:%s, queryId:%s, enqId:%s]' % (
            ro['genId'], ro['caption'], ro['query']['id'], ro['enqId']))

        totRows = self._executeQuery(ro)

        if 0 == totRows:
            self._log.info('_runObject - done [genId:%d, caption:%s, queryId:%s, enqId:%s, totRows:%d]' % (
                ro['genId'], ro['caption'], ro['query']['id'], ro['enqId'], totRows))
            return totRows

        ro['input']['startTM'] = datetime.datetime.now()

        self._sortOuput(ro)

        self._groupOutput(ro)

        self._writeOutput(ro)

        ro['input']['endTM'] = datetime.datetime.now()

        self._log.info('_runObject - done [genId:%d, caption:%s, queryId:%s, enqId:%s, totRows:%d]' % (
            ro['genId'], ro['caption'], str(ro['query'].get('id')), ro['enqId'], totRows))

        return totRows


    def _executeQuery(self, ro):
        """
        Execute the report query.

        :param ro: (dict) The run object.
        :return: Row count.
        """
        self._log.debug('_executeQuery - start [genId:%d, queryId:%s]' % (ro['genId'], ro['query']['id']))

        ro['query']['startTM'] = datetime.datetime.now()

        stmnt = None
        dbcon = self._cdao.dbcon

        try:
            stmnt = dbcon.statement(ro['query']['id'])

            stmnt.sql(ro['query']['qry'])

            for mkey, mobj in ro['query']['mapIn'].items():
                stmnt.bindIn(mkey, mobj['value'])

            dbcon.execute(stmnt)

            self._log.debug('_executeQuery - query done, retreiving rows [genId:%d, queryId:%s]' % (
                ro['genId'], ro['query']['id']))

            cols   = []
            rows   = []
            colIdx = 0
            colMap = {}

            if dbcon.fetch(stmnt):

                for col in stmnt.columns:
                    colMap[col]  = colIdx
                    colIdx      += 1

                mlist = []

                for mo, mrec in ro['query']['mapOut'].items():
                    colIdx = colMap.get(mrec['inpout'].queryOutId)

                    if colIdx is None:
                        raise Exception('Output column [%s] not found in query ouput: %s' % (mo, str(stmnt.columns)))

                    mrec['col'] = colIdx
                    mlist.append(mrec)

                mlist = sorted(mlist, key=operator.itemgetter('idx'))

                for mrec in mlist:
                    cx = {
                        'id'     : mrec['mapid'],
                        'type'   : mrec['qryout'].outType if mrec['inpout'].lookupId == '' else tParam.ParamType_Couplet.keyText,  # noqa
                        'hidden' : mrec['inpout'].hidden,
                        'prec'   : mrec['inpout'].outPrecision,
                    }

                    cols.append(cx)

                    if mrec['inpout'].caption != '':
                        cx['caption'] = mrec['inpout'].caption
                    elif mrec['qryout'].caption != '':
                        cx['caption'] = mrec['qryout'].caption
                    else:
                        for cname, cidx in colMap.items():
                            if cidx == colIdx:
                                cx['caption'] = stmnt.columns[cname]
                                break

                while True:
                    row = [None] * len(mlist)

                    for mrec in mlist:
                        val = stmnt.result[mrec['col']]

                        if mrec['outPre'] is not None:
                            val = round(val, mrec['outPre'])

                        if mrec['inpout'].lookupId != '':
                            val = self._lookupCache.lookupVal(mrec['inpout'].lookupId, val, True)

                        row[mrec['idx']] = val

                    rows.append(row)

                    if not dbcon.fetch(stmnt):
                        break

            ro['query']['endTM']   = datetime.datetime.now()
            ro['query']['cols']    = cols
            ro['query']['rows']    = rows
            ro['query']['totRows'] = len(rows)

        except Exception as x:
            del stmnt
            self._log.debug('_executeQuery - except [genId:%d, queryId:%s, err:%s]' % (
                ro['genId'], ro['query']['id'], str(x)))
            raise
        finally:
            dbcon = None

        self._log.debug('_executeQuery - done [genId:%d, queryId:%s, cols:%d, rows:%d]' % (
            ro['genId'], ro['query']['id'], len(cols), len(rows)))

        return len(rows)


    def _sortOuput(self, ro):
        """
        Sort the query row output according to the run object.

        :param ro: (dict) Run object.
        """
        if not ro['input']['sortBy']:
            return

        self._log.debug('_sortOuput - start [genId:%d, queryId:%s]' % (ro['genId'], ro['query']['id']))

        global _SORT_INDEXES_

        _SORT_INDEXES_.clear()

        for sitem in ro['input']['sortBy']:
            scol = sitem['column']
            mo   = ro['query']['mapOut'][scol]
            idx  = mo['idx']

            if sitem['order'].lower() == 'asc':
                _SORT_INDEXES_.append(['a', idx])
            else:
                _SORT_INDEXES_.append(['d', idx])

        ro['query']['rows'].sort(key=functools.cmp_to_key(_genBatch_Sort_Compare))

        self._log.debug('_sortOuput - done [genId:%d, queryId:%s]' % (ro['genId'], ro['query']['id']))


    def _groupOutput(self, ro):
        """
        Group the query output according to the run object.

        :param ro: (dict) Run object.
        """
        if not ro['input']['grps']:
            return

        self._log.debug('_groupOutput - start [genId:%d, queryId:%s]' % (
            ro['genId'], ro['query']['id']))

        for gitem in ro['input']['grps']:
            gobj = None
            ridx = 0
            rows = ro['query']['rows']

            while ridx < len(rows):
                row = rows[ridx]

                if not gobj:
                    gobj = self._grpNewGroup(gitem, row)

                    if gobj:
                        tmpo = self._grpAddHeader(gitem, gobj)

                        if tmpo:
                            rows.insert(ridx, tmpo)
                            ridx += 1
                            tmpo  = None
                    else:
                        ridx += 1

                    continue

                if self._grpEndOfGroup(gitem, gobj, row):
                    tmpo = self._grpAddFooter(gitem, gobj)

                    if tmpo:
                        rows.insert(ridx, tmpo)
                        ridx += 1
                        tmpo  = None

                    gobj = None
                    continue

                self._grpCalcRow(gitem, gobj, row)
                ridx += 1

            if gobj:
                tmpo = self._grpAddFooter(gitem, gobj)

                if tmpo:
                    rows.insert(ridx, tmpo)
                    ridx += 1
                    tmpo  = None


        self._log.debug('_groupOutput - done [genId:%d, queryId:%s]' % (
            ro['genId'], ro['query']['id']))


    def _writeOutput(self, ro):
        """
        Write the run object to the output file.

        :param ro: (dict) The run object.
        :param roIdx: (int) The run object index.
        """
        self._openOutFile(ro)

        if ro['idx'] > 0:
            self._outFH.write(',')

        self._outFH.write('\n{\n')

        self._outFH.write('"version":1.0,\n')
        self._outFH.write('"name":"%s",\n'       % (self._substOutCaption(ro, ro['name'])))
        self._outFH.write('"caption":"%s",\n'    % (self._substOutCaption(ro, ro['caption'])))
        self._outFH.write('"enqId":"%s",\n'      % (ro['enqId']))
        self._outFH.write('"genId":%d,\n'        % (ro['genId']))
        self._outFH.write('"tmStamp":"%s",\n'    % (ro['tmStamp'].strftime(TypeUtil.DATETIME_FMT)))
        self._outFH.write('"totRows":%d,\n'      % (ro['query']['totRows']))
        self._outFH.write('"reqBy":"%s",\n'      % (self._genRec.reqBy))
        self._outFH.write('"reqTmStamp":"%s",\n' % (self._genRec.reqDate.strftime(TypeUtil.DATETIME_FMT)))
        self._outFH.write('"cols":[null')

        cols = ro['query']['cols']
        rdx  = 0

        for col in cols:
            self._outFH.write(',\n  %s' % json.dumps(col))

        self._outFH.write('\n],\n')
        self._outFH.write('"rows":[')

        tmpRow = [None] * (len(cols) + 1)
        rows   = ro['query']['rows']

        for row in rows:

            cdx = 0

            if type(row) == dict:
                # Grouping Row
                tmpRow[0] = {
                    'type' : row['type'],
                    'tag'  : row['gitem'].headTag if row['type'] == 'header' else row['gitem'].footTag,
                    'cnt'  : row['gobj']['cnt'],
                    'grp'  : '',  # row['gobj']['cmp'],
                    'id'   : row['gitem'].id,
                }

                for rval in row['gobj']['row']:
                    cdx += 1
                    tmpRow[cdx] = None

                self._setGroupRowText(
                    row['gitem'],
                    row['gobj'],
                    row['gitem'].header if row['type'] == 'header' else row['gitem'].footer,
                    row['gitem'].hlist  if row['type'] == 'header' else row['gitem'].flist,
                    ro['query']['mapOut'],
                    tmpRow)

            else:
                # Data Row
                tmpRow[0] = None

                for rval in row:
                    cdx += 1
                    tmpRow[cdx] = TypeUtil.forJSON(rval)

            if rdx > 0:
                self._outFH.write(',')

            rdx += 1

            self._outFH.write('\n  %s' % json.dumps(tmpRow))

        self._outFH.write('\n]}')

        ro['query']['in'].clear()
        ro['query']['out'].clear()
        ro['query']['rows'].clear()
        ro['query']['cols'].clear()
        ro['query']['mapOut'].clear()
        ro['query']['mapIn'].clear()


    def _openOutFile(self, ro):
        """
        Open the output file.

        :param ro: (dict) The run object.
        """
        if self._outFH:
            return

        fname = os.path.join(self._genPath, '%d.output' % ro['genId'])

        self._outFH = open(fname, 'wt')

        self._outFH.write('{"genId":%d,\n' % ro['genId'])
        self._outFH.write('"version":1.0,\n')
        self._outFH.write('"name":"%s",\n' % (self._genCaption.strip()))
        self._outFH.write('"caption":"%s",\n' % self._substOutCaption(ro, self._genOutCapt))
        self._outFH.write('"reportId":"%s",\n' % ('' if not self._reportRec else self._reportRec.id))
        self._outFH.write('"output":[')


    def _closeOutFile(self):
        """
        Close the out ouput file.

        :param ex: (Exception) Is there an exception or not?
        """
        if not self._outFH:
            return

        self._outFH.write('\n]}')

        self._outFH.close()
        self._outFH = None



    def _grpNewGroup(self, gitem, row):
        """
        Create a new group if possible.

        :param gitem: (tEnqGroup) Group item object.
        :param row: (list) The row data.
        :return: New group object or None if cannot create a new group from the row.
        """
        if type(row) != list:
            return None

        gobj = {
            'row' : row,
            'cmp' : None,
            'tot' : None,
            'cnt' : 0,
        }

        if gitem.grpCols:
            gobj['cmp'] = []

            for mrec in gitem.mlist:
                gobj['cmp'].append(row[mrec['idx']])

        if gitem.sumCols:
            gobj['tot'] = {}

        return gobj


    def _grpAddHeader(self, gitem, gobj):
        """
        Add a header record for this grouping.

        :param gitem: (tEnqGroup) Group item object.
        :param gobj: (dict) The group object.
        :return: Header object or None if no header.
        """
        if not gitem.header:
            return None

        return {'type' : 'header', 'gitem' : gitem, 'gobj' : gobj}


    def _grpEndOfGroup(self, gitem, gobj, row):
        """
        Check if this is the end of a group.

        :param gitem: (tEnqGroup) Group item object.
        :param gobj: (dict) The group object.
        :param row: (list) The row to check.
        :return: True if at the end of the group.
        """
        if not gitem.grpCols:
            return False

        if type(row) != list:
            return True

        midx = 0

        for mrec in gitem.mlist:
            if gobj['cmp'][midx] != row[mrec['idx']]:
                return True

            midx += 1

        return False


    def _grpAddFooter(self, gitem, gobj):
        """
        Add a footer record for this grouping.

        :param gitem: (tEnqGroup) Group item object.
        :param gobj: (dict) The group object.
        :return: Footer object or None if no footer.
        """
        if not gitem.footer:
            return None

        return {'type' : 'footer', 'gitem' : gitem, 'gobj' : gobj}


    def _grpCalcRow(self, gitem, gobj, row):
        """
        Sum/Avg/Min/Max the total columns.

        :param gitem: (tEnqGroup) Group item object.
        :param gobj: (dict) The group object.
        :param row: (list) The row to calc.
        """
        gobj['cnt'] += 1

        if gobj['tot'] is None:
            return

        if len(gobj['tot']) == 0:
            for sname, sobj in gitem.sdict.items():
                gobj['tot'][sname] = row[sobj['idx']]

            return

        for sname, sobj in gitem.sdict.items():
            gobj['tot'][sname] += row[sobj['idx']]


    def _setGroupRowText(self, gitem, gobj, hf, hflist, mout, tmpRow):
        """
        Sets the group values/text for group row.

        :param gitem: (tEnqGroup) Group item object.
        :param gobj: (dict) The group object.
        :param hf: (list) Header or footer columns.
        :param hflist: (list) Header or footer output map list.
        :param mout: (dict) Query map out columns.
        :param tmpRow: (list) The row to fill.
        """
        idx = 0

        for rec in hf:
            txt = rec.get('text')
            val = None

            if txt:

                start = txt.find('${')

                while start >= 0:
                    end   = txt.find('}', start)

                    if end <= start:
                        raise Exception("Header/Footer text [%s] not valid, missing '}' - [grpId:%d]" % (
                            txt, gitem.id))

                    cname = txt[start + 2 : end]

                    if cname.startswith('sum.'):
                        val = gobj['tot'].get(cname[4:])
                        mo  = gitem.sdict.get(cname[4:])

                        if not val or not mo:
                            txt = '%s<?%s?>%s' % (txt[:start], cname, txt[end + 1:])
                        else:
                            txt = '%s%s%s' % (txt[:start], TypeUtil.forText(
                                val, mo['qryout'].outType, mo['inpout'].outPrecision), txt[end + 1:])

                    elif cname.startswith('avg.'):
                        val = gobj['tot'].get(cname[4:])
                        mo  = gitem.sdict.get(cname[4:])

                        if not val or not mo:
                            txt = '%s<?%s?>%s' % (txt[:start], cname, txt[end + 1:])
                        else:
                            if gobj['cnt'] > 0:
                                val /= gobj['cnt']

                            txt = '%s%s%s' % (txt[:start], TypeUtil.forText(
                                val, mo['qryout'].outType, mo['inpout'].outPrecision), txt[end + 1:])

                    else:
                        mo = mout.get(cname)

                        if not mo:
                            raise Exception('Header/Footer subst text [%s] not found - [grpId:%d]' % (cname, gitem.id))

                        val = gobj['row'][mo['idx']]
                        txt = '%s%s%s' % (txt[:start], TypeUtil.forText(
                            val, mo['qryout'].outType, mo['inpout'].outPrecision, gitem.gdict[cname].get(
                                'null')), txt[end + 1:])

                    start = txt.find('${')

                val = txt

            elif rec.get('sum'):
                val = gobj['tot'].get(rec.get('sum'))

                if not val:
                    val = 0.0

            elif rec.get('avg'):
                val = gobj['tot'].get(rec.get('avg'))

                if not val:
                    val = 0.0

                if gobj['cnt'] > 0:
                    val /= gobj['cnt']


            if hf[idx]['column'] == '*':
                tmpRow[1] = {'value' : val, 'span' : hflist[0]}
            else:
                tmpRow[hflist[idx]['idx'] + 1] = {'value' : val, 'span' : rec['span']}

            idx += 1


    def _substOutCaption(self, ro, caption):
        """
        Substitute input parameters into the caption.

        :param ro: (dict) The run object.
        :param caption: (string) The caption to check substitutions for.
        :return: The new out caption.
        """
        epos = 0
        spos = caption.find('${')

        while spos >= 0:
            epos = caption.find('}', spos)

            if epos > 0:
                varname = caption[spos + 2 : epos]
                mo      = ro['query']['mapIn'].get(varname)

                if mo:
                    caption = '%s%s%s' % (caption[:spos], str(mo['value']), caption[epos + 1:])
                else:
                    for prec in ro['input']['params']:
                        pid   = prec.get('id')

                        if pid == varname:
                            caption = '%s%s%s' % (caption[:spos], str(prec.get('value')), caption[epos + 1:])
                            break


            epos += 2
            spos  = caption.find('${', epos)

        return caption.strip()
