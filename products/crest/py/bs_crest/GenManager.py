# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import json

from mettle.lib.xMettle import xMettle

from bs_crest import TypeUtil

from bs_crest.db.tables.tEnquiryRole import tEnquiryRole
from bs_crest.db.tables.tGen         import tGen
from bs_crest.db.tables.oGenSearch   import oGenSearch
from bs_crest.db.tables.tParam       import tParam


class GenManager:
    """
    Manager object to control generation requests, aborting, status updates etc.
    """

    def __init__(self, pod, siteId, usrId, roleId):
        """
        Constructor.

        :param pod: (Pod) The pod to use.
        :param siteId: (int) The site.
        :param usrId: (string) The user.
        :param roleId: (string) The user's role.
        """
        self._pod      = pod
        self._siteId   = siteId
        self._usrId    = usrId
        self._roleId   = roleId


    def __del__(self):
        """
        Destructor.
        """
        pass


    def __enter__(self):
        """
        On enter event.
        """
        return self


    def __exit__(self, type, value, traceback):
        """
        On exit event.
        """
        pass


    def enquiryRun(self, enquiryId, params):
        """
        Run an enquiry.

        :param enquiryId: (string) The enquiry to run.
        :param params: (bParameter.List) The input parameters.
        :return: (oGenSearch) The generation record.
        """
        self._pod.log.debug('GenManager.enquiryRun - start [enquiryId:%s, params:%d, siteId:%d, usrId:%s, roleId:%s]' % (
            enquiryId, len(params), self._siteId, self._usrId, self._roleId))

        de       = self._pod.dao.dEnquiry(self._pod.dbcon)
        der      = self._pod.dao.dEnquiryRole(self._pod.dbcon)
        deiItems = []

        de.selectOneDeft(enquiryId)

        if not der.trySelectOneDeft(enquiryId, self._roleId, self._siteId):
            raise xMettle('You do not have permission to run enquiry [%s (%s)] - No role found!' % (
                de.rec.caption, de.rec.id))

        if not der.allowRun != tEnquiryRole.AllowRun_Couplet.keyYes:
            raise xMettle('Permission denied to run enquiry [%s (%s)]!' % (de.rec.caption, de.rec.id))

        self._pod.dao.dEnquiryInForRun(self._pod.dbcon).execDeft(enquiryId).fetchAll(deiItems)

        paramJson = self._validateRunParams(params, deiItems)
        dgen      = self._pod.dao.dGen(self._pod.dbcon)

        dgen.insertDeft(
            self._siteId,
            enquiryId,
            '',
            tGen.Status_Couplet.keyPending,
            paramJson,
            self._usrId,
            datetime.datetime.now(),
            datetime.datetime.min,
            datetime.datetime.min,
            0.0,
            0)

        self._pod.log.debug('GenManager.enquiryRun - done [genId:%d, enquiryId:%s, params:%d, siteId:%d, usrId:%s]' % (
            dgen.rec.id, enquiryId, len(params), self._siteId, self._usrId))

        return self._genRec2GenSearchOutRec(dgen.rec)


    def _validateRunParams(self, params, runParams):
        """
        Enusre the parameters passed in match the actual input parameters for an enquiry/report.
        If all are valid, it returns a valid json string of the parameters.

        :param params: (bParameter.List) The input parameters
        :param runParams: (list) The parameters required for this enquiry/report.
        :return: Json dump as string
        """
        dupsCheck = []
        pobj      = []

        for prec in params:
            if prec.id in dupsCheck:
                raise xMettle('Duplicated parameter [%s] detected!' % prec.id)

            dupsCheck.append(prec.id)

            rprec = None

            for rrec in runParams:
                if prec.id == rrec.id:
                    rprec == rrec
                    break

            if not rprec:
                raise xMettle('Parameter [%s] not exepcted' % prec.id)

            if prec.value == '':
                if rprec.defVal != '':
                    continue

                raise xMettle('Parameter [%s] requires a value.' % prec.id)

            if not TypeUtil.isType(prec.value, rprec.paramTypes):
                raise xMettle('Parameter [%s] is not the correct type, value [%s] is not [%s]' % (
                    prec.id, prec.value, tParam.ParamType_Couplet.getValue(rprec.paramTypes)))

            del runParams[rprec]

            pobj.append({'id' : prec.id, 'value' : prec.value})

        for rprec in runParams:
            if rprec.defVal == '':
                raise xMettle('Parameter [%s (%s)] is required, but not given' % (rprec.caption, rprec.id))

        return json.dumps(pobj)


    def _genRec2GenSearchOutRec(self, genRec):
        """
        Converts a gen record to a search out record.

        :param genRec: (tGen) Input rec.
        :return (oGenSearch) Output rec.
        """
        return oGenSearch(
            genRec.id,
            genRec.enquiryId,
            genRec.reportId,
            genRec.status,
            genRec.reqBy,
            genRec.reqDate,
            genRec.dateStart,
            genRec.dateEnd,
            genRec.genTime,
            genRec.totalRows)
