# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os
import os.path
import datetime
import traceback

from bs_lib import Common

from bs_crest.Config      import Config
from bs_crest             import TypeUtil

from bs_crest.db.tables.tGen     import tGen
from bs_crest.db.tables.tGenOut  import tGenOut
from bs_crest.db.tables.tOutType import tOutType


class GenOutput:
    """
    This object is responsible for converting the generated
    ouput json into a usable/readible format.  It provides a
    standard factory that can be overloaded.
    """
    MODULE = '[GenOutput]'


    def __init__(self, pod, dao):
        """
        Constructor.

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) Crest Dao object.
        """
        self._pod         = pod
        self._log         = pod.log
        self._cdao        = dao

        self._siteCode      = ''
        self._startTM       = datetime.datetime.min
        self._genPath       = ''
        self._genOutputPath = ''
        self._genRec        = tGen()
        self._genOutRec     = tGenOut()
        self._outFilename   = ''


    def run(self, genId, outTypeId):
        """
        Main entry point, generates the output for the specified genreation id.

        :param genId: (int) The gen id to create output for.
        :param outTypeId: (string) The output type.
        :return: True if successful, else False.
        """
        self._startTM = datetime.datetime.now()

        self._log.info('run - started [genId:%d, outTypeId:%s]' % (genId, outTypeId))

        otObj = None

        try:
            self._initialize(genId, outTypeId)

            otObj = self._factoryGetOutTypeObject(outTypeId)

            otObj.initialize(self._genRec, self._siteCode)

            if not otObj.active():
                raise Exception('Output type [%s] is not active!' % (outTypeId))

            self._outFilename = os.path.join(self._genPath, '%d.%s' % (genId, otObj.outExt()))

            if os.path.exists(self._outFilename):
                os.remove(self._outFilename)

            depFile = self._runDependancy(otObj)

            otObj.run(self._genOutputPath, self._outFilename, depFile)

            otObj.destroy()

            otObj = None

            self._finalize()

        except Exception as x:
            strace = traceback.format_exc()
            self._log.error('run - failed [genId:%d, outTypeId:%s, error:%s, trace:%s]' % (
                genId, outTypeId, str(x), strace))

            if otObj:
                otObj.destroy()

            if not self._finalize(x):
                raise

            return False

        self._log.info('run - done [genId:%d, outTypeId:%s]' % (genId, outTypeId))
        return True


    def _initialize(self, genId, outTypeId):
        """
        Initialize the batch and ensure all paths, settings, and configs are
        valid for processesing.

        :param genId: (int) The gen pk.
        :param outTypeId: (string) The output type requested.
        """
        conf = Config(self._cdao)
        cfg  = conf.load(['output.path'])

        self._genPath = Common.readDict(cfg, 'output.path', str)

        if not os.path.exists(self._genPath) or not os.path.isdir(self._genPath):
            raise Exception('Generation path not found/valid [%s]' % self._genPath)

        dg  = self._cdao.dao.dGen(self._cdao.dbcon)
        dgo = self._cdao.dao.dGenOut(self._cdao.dbcon)

        dg.selectOneDeft(genId)

        if dg.rec.status != tGen.Status_Couplet.keyComplete:
            raise Exception('Gen item is in unexpected status [genId:%d, status:%s]' % (
                genId, tGen.Status_Couplet.getValue(dg.rec.status)))

        self._genPath = os.path.join(self._genPath, dg.rec.reqDate.strftime(TypeUtil.MONTH_FMT))

        if not os.path.exists(self._genPath) or not os.path.isdir(self._genPath):
            raise Exception('Generation item output path not found/valid [path:%s, genId:%d]' % (
                self._genPath, genId))

        self._genOutputPath = os.path.join(self._genPath, '%d.output' % genId)

        if not os.path.exists(self._genOutputPath) or not os.path.isfile(self._genOutputPath):
            raise Exception('Generation item output file not found/valid [path:%s, genId:%d]' % (
                self._genOutputPath, genId))

        dgo.lockOneDeft(genId, outTypeId, self._pod.stdDBLock())

        if dgo.rec.status != tGenOut.Status_Couplet.keyBusy:
            raise Exception('Gen Output is in unexpected status [genId:%d, outTypeId:%s, status:%s]' % (
                genId, outTypeId, tGenOut.Status_Couplet.getValue(dgo.rec.status)))

        # This is a little naughty, but time constraint look at cleaning it up later.
        siteRes = Common.sqlFetch(self._pod, 'select code from fura.site where id = %d' % dg.rec.siteId, one=True)

        if not siteRes or not siteRes['code']:
            raise Exception('Site [%d] is unexpected!' % dg.rec.siteId)

        self._siteCode  = siteRes['code']
        self._genRec    = dg.rec
        self._genOutRec = dgo.rec


    def _finalize(self, ex=None):
        """
        Finalize the output record, close file handles.

        :param ex: (Exception) Did the output fail with an error?
        :return: (bool) True if db record was update.
        """
        if self._genOutRec.genId < 1:
            return False

        if not ex:
            self._genOutRec.status    = tGenOut.Status_Couplet.keyComplete
            self._genOutRec.dateStart = self._startTM
            self._genOutRec.dateEnd   = datetime.datetime.now()
            self._genOutRec.genTime   = (self._genOutRec.dateEnd - self._genOutRec.dateStart).total_seconds()
            self._genOutRec.fileSize  = os.stat(self._outFilename).st_size
            self._genOutRec.errMsg    = ''
        else:
            self._genOutRec.status    = tGenOut.Status_Couplet.keyFailed
            self._genOutRec.dateStart = self._startTM
            self._genOutRec.dateEnd   = datetime.datetime.now()
            self._genOutRec.genTime   = (self._genOutRec.dateEnd - self._genOutRec.dateStart).total_seconds()
            self._genOutRec.fileSize  = 0
            self._genOutRec.errMsg    = str(ex)

        dgo = self._cdao.dao.dGenOut(self._cdao.dbcon)

        dgo.update(self._genOutRec)


    def _factoryGetOutTypeObject(self, outTypeId):
        """
        Gets the OutType object for the specified outType.  This method can
        be overloaded by systems to inject their own OutType objects.

        :param outTypeId: (string) The outType to load.
        :return: (OutType) A derrived object type OutType
        """
        resObj = None

        if outTypeId == tOutType.Id_Couplet.keyCsv:
            from bs_crest.outtype.OutTypeCSV import OutTypeCSV
            resObj = OutTypeCSV(self._pod, self._cdao)
        elif outTypeId == tOutType.Id_Couplet.keyXml:
            from bs_crest.outtype.OutTypeXML import OutTypeXML
            resObj = OutTypeXML(self._pod, self._cdao)
        elif outTypeId == tOutType.Id_Couplet.keyPdf:
            from bs_crest.outtype.OutTypePDF import OutTypePDF
            resObj = OutTypePDF(self._pod, self._cdao)

        if not resObj:
            raise Exception('OutType [%s] object not expected!' % (outTypeId))

        return resObj


    def _newGenOutputObject(self, pod, dao):
        """
        A virtual method so that the GenOutput object can be overloaded.

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) Crest Dao object.
        """
        return GenOutput(pod, dao)


    def _runDependancy(self, outObj):
        """
        If the outObj has a dependancy of another output type, ensure that the depedant
        file has been generated.  If it has not, run it.

        :param outObj: (OutType) The object to check if it has a dependancy.
        :return: (string|None) The generated dependancy file.
        """
        otdep = outObj.outTypeDependancy()

        if not otdep:
            return None

        self._pod.log.debug('GetOutput._runDependancy [genId:%d, outTypeId:%s, dependancy:%s]' % (
            self._genRec.id, self._genOutRec.outTypeId, otdep))

        dgo = self._cdao.dao.dGenOut(self._cdao.dbcon)

        if dgo.trySelectOneDeft(self._genRec.id, otdep):
            if dgo.rec.status != tGenOut.Status_Couplet.keyComplete:
                dgo.lockOneDeft(self._genRec.id, otdep, self._pod.stdDBLock(miliSeconds=500, retrys=20))
                dgo.rec.status = tGenOut.Status_Couplet.keyBusy
                dgo.update()
        else:
            dgo.rec._copyFrom(self._genOutRec)

            dgo.rec.status_Couplet = tGenOut.Status_Couplet.keyBusy
            dgo.rec.outTypeId      = otdep
            dgo.rec.dateStart      = datetime.datetime.min
            dgo.rec.dateEnd        = datetime.datetime.min
            dgo.rec.genTime        = 0.0
            dgo.rec.fileSize       = 0
            dgo.rec.errMsg         = ''

            dgo.insert()

        if dgo.rec.status != tGenOut.Status_Couplet.keyComplete:
            go = self._newGenOutputObject(self._pod, self._cdao)

            if not go.run(dgo.rec.genId, otdep):
                raise Exception('Error generating dependancy ouput type [%s]' % otdep)

            depFile = go._outFilename
            del go

        else:
            oto     = self._factoryGetOutTypeObject(otdep)
            depFile = os.path.join(self._genPath, '%d.%s' % (dgo.rec.genId, oto.outExt()))

        if not os.path.exists(depFile):
            raise Exception('Dependancy file [%s] not found!' % depFile)

        return depFile
