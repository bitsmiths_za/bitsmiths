# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os
import datetime
import time
import subprocess
import traceback
import fcntl
import json

from dateutil.relativedelta import relativedelta
from bs_lib import Common

from bs_crest.Config      import Config
from bs_crest.InputVar    import InputVar
from bs_crest             import TypeUtil

from bs_crest.db.tables.tGen           import tGen
from bs_crest.db.tables.tGenOut        import tGenOut
from bs_crest.db.tables.tGenTmit       import tGenTmit
from bs_crest.db.tables.tReportSched   import tReportSched


class GenServer:
    """
    This server monitors and schedules report and enquiry
    requests.
    """
    MODULE              = '[GenServer]'
    PROC_INTERVAL_SLEEP = 0.2
    MAX_PIPE_OUT_LEN    = 4096
    SCHEDULE_INTERVAL   = 10.0

    PROC_TYPE_GEN_BATCH    = 'gen-batch'
    PROC_TYPE_GEN_OUTPUT   = 'gen-output'
    PROC_TYPE_GEN_TRANSMIT = 'gen-transmit'

    def __init__(self, pod, dao):
        """
        Constructor.

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) Crest Dao object.
        """
        self._shutdown = False
        self._pod      = pod
        self._log      = pod.log
        self._cdao     = dao

        self._procInterval  = 1.0
        self._maxProcs      = 5
        self._genBatch      = ''
        self._genOutput     = ''
        self._genTransmit   = ''
        self._outPath       = ''
        self._running       = []


    def shutdown(self):
        """
        Instruct the server to shutdown.
        """
        self._shutdown = True


    def run(self):
        """
        Run the server.
        """
        self._shutdown  = False
        scheduleInt     = self.SCHEDULE_INTERVAL

        try:
            self._initialize()

            self._log.info("CrestGensServer - starting [ procInterval:%.2f]" % (self._procInterval))

            while not self._shutdown:

                if scheduleInt <= 0.0:
                    self._scheduleReports()
                    scheduleInt = self.SCHEDULE_INTERVAL
                else:
                    scheduleInt -= 1.0

                self._process()

                if self._shutdown:
                    if not self._running:
                        break

                    self._log.warning(' ... waiting for [%d] child processes to shut down.' % len(self._running))

                pi = self._procInterval

                while pi > 0.0:
                    if self._shutdown and not self._running:
                        break

                    pi -= self.PROC_INTERVAL_SLEEP
                    time.sleep(self.PROC_INTERVAL_SLEEP)

                    if self._running:
                        self._checkRunningProcs()

            self._log.info("CrestGensServer - stopping")

        except Exception as x:
            self._log.error('CrestGensServer exception caught [error:%s, trace:%s]' % (str(x), traceback.format_exc()))
            raise
        finally:
            self._destroy()


    def _initialize(self):
        """
        Initialize the generation server.
        """
        conf = Config(self._cdao)
        cfg  = conf.load([
            'server.interval',
            'server.maxProcs',
            'output.path',
            'batch.genBatch',
            'batch.genOutput',
            'batch.genTransmit',
        ])

        self._procInterval  = Common.readDict(cfg, 'server.interval',   float)
        self._maxProcs      = Common.readDict(cfg, 'server.maxProcs',   int)
        self._outPath       = Common.readDict(cfg, 'output.path',       str)
        self._genBatch      = Common.readDict(cfg, 'batch.genBatch',    str)
        self._genOutput     = Common.readDict(cfg, 'batch.genOutput',   str)
        self._genTransmit   = Common.readDict(cfg, 'batch.genTransmit', str)

        self._log.debug('server.procInterval : %s' % str(self._procInterval))
        self._log.debug('server.maxProcs     : %s' % str(self._maxProcs))
        self._log.debug('output.outPath      : %s' % str(self._outPath))
        self._log.debug('batch.genBatch      : %s' % str(self._genBatch))
        self._log.debug('batch.genOutput     : %s' % str(self._genOutput))
        self._log.debug('batch.genTransmit   : %s' % str(self._genTransmit))

        if self._procInterval < 0.2:
            raise Exception('Interval value invalid [%.2f]' % self._procInterval)

        if self._maxProcs < 1:
            raise Exception('Maximum processes invalid [%d]' % self._maxProcs)

        if not os.path.exists(self._genBatch) or not os.path.isfile(self._genBatch):
            raise Exception('Gen batch path not found/valid [%s]' % self._genBatch)

        if not os.path.exists(self._genOutput) or not os.path.isfile(self._genOutput):
            raise Exception('Gen output path not found/valid [%s]' % self._genOutput)

        if not os.path.exists(self._genTransmit) or not os.path.isfile(self._genTransmit):
            raise Exception('Gen transmit path not found/valid [%s]' % self._genTransmit)

        if not os.path.exists(self._outPath) or not os.path.isdir(self._outPath):
            raise Exception('Output path not found/valid [%s]' % self._outPath)


    def _destroy(self):
        """
        Shuts down the server, does not kill child processes, they will finish on
        their own.
        """
        pass


    def _maxProcsReached(self):
        """
        Check to see if max number of concurret procs have been reached.

        :return: (bool) True if max procs reached.
        """
        if len(self._running) >= self._maxProcs:
            return True

        return False


    def _process(self):
        """
        Main process method.
        """
        self._checkRunningProcs()

        if self._shutdown or self._maxProcsReached():
            return

        self._startPendingTransmits()

        if self._shutdown or self._maxProcsReached():
            return

        self._startPendingOutputs()

        if self._shutdown or self._maxProcsReached():
            return

        self._startPendingBatches()


    def _checkRunningProcs(self):
        """
        Checks to see if the running batches have stopped running.  If they
        have then the respecitve return codes and errors are set.
        """
        self._log.debug('_checkRunningProcs - start [running:%d]' % len(self._running))
        remItems = []

        for ritem in self._running:
            rc = ritem['proc'].poll()

            if ritem['proc'].stdout is not None:
                ritem['stdout'] = self._readProcPipe(ritem['proc'].stdout, ritem['stdout'])

                if rc is not None:
                    ritem['proc'].stdout.close()

            if ritem['proc'].stderr is not None:
                ritem['stderr'] = self._readProcPipe(ritem['proc'].stderr, ritem['stderr'])

                if rc is not None:
                    ritem['proc'].stderr.close()

            if rc is None:
                continue

            ritem['rc'] = rc
            remItems.append(ritem)

        if len(remItems) > 0:
            for ritem in remItems:
                self._markProcessEnded(ritem)
                self._running.remove(ritem)

        self._log.debug('_checkRunningProcs - done [running:%d]' % len(self._running))


    def _markProcessEnded(self, ritem):
        """
        Mark a processes as ended according to its return code and output file generated.

        :param ritem: (dict) The run item to mark ended.
        """
        self._log.debug('_markProcessEnded - [pid:%d, name:%s, rc:%d, type:%s]' % (
            ritem['proc'].pid, ritem['name'], ritem['rc'], ritem['type']))

        try:
            if ritem['type'] == self.PROC_TYPE_GEN_BATCH:
                dg  = self._cdao.dao.dGen(self._cdao.dbcon)
                dgm = self._cdao.dao.dGenMetric(self._cdao.dbcon)
                dg.lockOneDeft(ritem['gen'].id, self._pod.stdDBLock())
                dgm.lockOneDeft(ritem['genMetric'].id, ritem['genMetric'].dateStart, self._pod.stdDBLock())

                self._cdao.dao.dGenOutRegenerate(self._cdao.dbcon).execDeft(dg.rec.id)

                if ritem['rc'] != 0:
                    dg.rec.status  =\
                    dgm.rec.status = tGen.Status_Couplet.keyFailed
                    dgm.rec.errMsg = '[RC:%d, STDERR:%s, STDOUT:%s]' % (ritem['rc'], ritem['stderr'], ritem['stdout'])
                else:
                    self._readGenBatchResult(dg.rec, dgm.rec)

                dg.update()
                dgm.update()
                self._cdao.dbcon.commit()
                return

            if ritem['type'] == self.PROC_TYPE_GEN_OUTPUT:
                if ritem['rc'] != 0:
                    dgo = self._cdao.dao.dGenOut(self._cdao.dbcon)
                    dgo.lockOneWith(ritem['genOut'], self._pod.stdDBLock())

                    if dgo.rec.status == tGenOut.Status_Couplet.keyBusy:
                        dgo.rec.status = tGenOut.Status_Couplet.keyFailed
                        dgo.rec.errMsg = '[RC:%d, STDERR:%s, STDOUT:%s]' % (
                            ritem['rc'], ritem['stderr'], ritem['stdout'])
                        dgo.update()
                        self._cdao.dbcon.commit()
                    else:
                        self._cdao.dbcon.rollback()

                return

            if ritem['type'] == self.PROC_TYPE_GEN_TRANSMIT:
                if ritem['rc'] != 0:
                    dgt = self._cdao.dao.dGenTmit(self._cdao.dbcon)
                    dgt.lockOneWith(ritem['genTmit'], self._pod.stdDBLock())

                    if dgt.rec.status == tGenOut.Status_Couplet.keyBusy:
                        dgt.rec.status = tGenOut.Status_Couplet.keyFailed
                        dgt.rec.errMsg = '[RC:%d, STDERR:%s, STDOUT:%s]' % (
                            ritem['rc'], ritem['stderr'], ritem['stdout'])
                        dgt.update()
                        self._cdao.dbcon.commit()
                    else:
                        self._cdao.dbcon.rollback()

                return

            raise Exception('Run item [%s] not expected [%s]' % (ritem['type'], str(ritem)))

        except Exception as x:
            self._log.error('Exception caught attempting to end processes: [pid:%d, name:%s, rc:%d, type:%s, \
err:%s, trace:%s]' % (ritem['proc'].pid, ritem['name'], ritem['rc'], ritem['type'], str(x), traceback.format_exc()))


    def _readProcPipe(self, filePipe, currOut):
        """
        Reads sub processes pipe, appends any output from the pipe to the current string.

        :param filePipe: (file) File handle read.
        :param currOut: (string) The current string output.
        :return: (string) The new current output string output.
        """
        try:
            while True:
                res = os.read(filePipe.fileno(), self.MAX_PIPE_OUT_LEN)

                if not res:
                    break

                currOut += str(res, 'utf8')

                if len(res) < self.MAX_PIPE_OUT_LEN:
                    break

            if len(currOut) > self.MAX_PIPE_OUT_LEN:
                return currOut[-self.MAX_PIPE_OUT_LEN:]

        except OSError:
            pass

        return currOut


    def _startPendingTransmits(self):
        """
        Start pending generation transmits.
        """
        if self._maxProcsReached():
            return

        availProcs = self._maxProcs - len(self._running)

        self._log.debug('_startPendingTransmits - start [available:%d]' % availProcs)

        self._cdao.dbcon.rollback()

        res     = []
        started = 0

        self._cdao.dao.dGenTmitPending(self._cdao.dbcon).execDeft(availProcs).fetchAll(res)

        for rec in res:
            self._startGenTransmit(rec)
            availProcs -= 1
            started    += 1


        self._log.debug('_startPendingTransmits - done [started:%d, available:%d]' % (
            started, availProcs))


    def _startPendingOutputs(self):
        """
        Start pending generation outputs.
        """
        if self._maxProcsReached():
            return

        availProcs = self._maxProcs - len(self._running)

        self._log.debug('_startPendingOutputs - start [available:%d]' % availProcs)

        self._cdao.dbcon.rollback()

        res     = []
        started = 0

        self._cdao.dao.dGenOutPending(self._cdao.dbcon).execDeft(availProcs).fetchAll(res)

        for rec in res:
            self._startGenOutput(rec)
            availProcs -= 1
            started    += 1

        self._log.debug('_startPendingOutputs - done [started:%d, available:%d]' % (
            started, availProcs))


    def _startPendingBatches(self):
        """
        Start pending generation batches.
        """
        if self._maxProcsReached():
            return

        availProcs = self._maxProcs - len(self._running)

        self._cdao.dbcon.rollback()

        res     = []
        started = 0

        self._cdao.dao.dGenPending(self._cdao.dbcon).execDeft(availProcs).fetchAll(res)

        for rec in res:
            self._startGenBatch(rec)
            availProcs -= 1
            started    += 1

        self._log.debug('_startPendingBatches - done [started:%d, available:%d]' % (started, availProcs))


    def _readGenBatchResult(self, genRec, metRec):
        """
        Reads the output json information from a gen batch.

        :param genRec: (tGen) The gen record whose result to read.
        :param metRec: (tGenMetric) The gen metric record.
        :return: (dict) The result object or None if not found/invalid.
        """
        resPath = os.path.join(self._outPath,
                               genRec.reqDate.strftime(TypeUtil.MONTH_FMT),
                               '%s.result' % genRec.id)

        genRec.dateStart = datetime.datetime.min
        genRec.dateEnd   = datetime.datetime.min
        genRec.genTime   = 0.0
        genRec.totalRows = 0

        if not os.path.exists(resPath):
            return None

        try:
            with open(resPath, 'rt') as fh:
                res = json.loads(fh.read())

                if not res['ok']:
                    genRec.status =\
                    metRec.status = tGen.Status_Couplet.keyFailed
                    metRec.errMsg = '%s [TRACE:%s]' % (res['exception'], res['stackTrace'])
                else:
                    genRec.status =\
                    metRec.status = tGen.Status_Couplet.keyComplete

                genRec.totalRows = res['totalRows']
                genRec.genTime   = res['totTime']
                genRec.caption   = res.get('caption')

                if genRec.caption is None or genRec.caption == '':
                    genRec.caption = 'N/A'

                if res['startTime'] != '':
                    genRec.dateStart = datetime.datetime.strptime(res['startTime'], TypeUtil.DATETIME_FMT)

                if res['endTime'] != '':
                    genRec.dateEnd   = datetime.datetime.strptime(res['endTime'], TypeUtil.DATETIME_FMT)

                metRec.dateEnd    = datetime.datetime.now()
                metRec.genTime    = (metRec.dateEnd - metRec.dateStart).total_seconds()
                metRec.totalRows  = genRec.totalRows
                metRec.qryMetrics = json.dumps(res['queries'])

                return res

        except Exception as x:
            self._log.warning('Output result [%s] could not be read [error:%s, trace:%s]' % (
                resPath, str(x), traceback.format_exc()))
            return None


    def _prepRunProcItem(self, itemType, name):
        """
        Preps a run process item object dictionary.

        :param itemType: (string) The item type.
        :return: (dict) The run item dictionary.
        """
        ritem = {}

        ritem['proc']      = None
        ritem['pid']       = 0
        ritem['rc']        = None
        ritem['stdout']    = ''
        ritem['stderr']    = ''
        ritem['type']      = itemType
        ritem['name']      = name
        ritem['gen']       = None
        ritem['genMetric'] = None
        ritem['genOut']    = None
        ritem['genTmit']   = None

        return ritem


    def _execRunProcItem(self, ritem, cmdStr):
        """
        Execute a run process item.

        :param ritem: (dict) The run object dictionary.
        :param cmdStr: (list) The command line to run.
        """
        self._log.debug(' - running: %s' % ' '.join(cmdStr))

        ritem['proc'] = subprocess.Popen(
            cmdStr,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

        self._running.append(ritem)

        ritem['pid'] = ritem['proc'].pid

        flags = fcntl.fcntl(ritem['proc'].stdout, fcntl.F_GETFL)
        fcntl.fcntl(ritem['proc'].stdout, fcntl.F_SETFL, flags | os.O_NONBLOCK)

        flags = fcntl.fcntl(ritem['proc'].stderr, fcntl.F_GETFL)
        fcntl.fcntl(ritem['proc'].stderr, fcntl.F_SETFL, flags | os.O_NONBLOCK)



    def _startGenTransmit(self, genTmit):
        """
        Start a gen transmit.

        :param genTmit: (tGenTmit) The gen transmit to start.
        """
        self._log.debug('_startGenTransmit - start [%s]' % str(genTmit))

        try:
            dgt  = self._cdao.dao.dGenTmit(self._cdao.dbcon)

            dgt.lockOneDeft(genTmit.genId,
                            genTmit.outTypeId,
                            genTmit.tmitTypeId,
                            genTmit.tmitGroupId,
                            self._pod.stdDBLock())

            if dgt.rec.status != tGenTmit.Status_Couplet.keyPending:
                self._log.warning('GenTmit record [%s] no longer in pending status.' % (str(dgt.rec)))
                return

            dgt.rec.status    = tGenTmit.Status_Couplet.keyBusy
            dgt.rec.dateStart = datetime.datetime.min
            dgt.rec.dateEnd   = datetime.datetime.min
            dgt.rec.genTime   = 0.0
            dgt.rec.errMsg    = ''
            dgt.update()

            ritem            = self._prepRunProcItem(self.PROC_TYPE_GEN_TRANSMIT, 'Transmit (%s, %s)' % (
                dgt.rec.tmitTypeId, dgt.rec.tmitGroupId))
            ritem['genTmit'] = dgt.rec

            self._cdao.dbcon.commit()

            self._execRunProcItem(ritem, [
                'python3',
                self._genTransmit,
                '--gen',
                str(dgt.rec.genId),
                '--outtype',
                dgt.rec.outTypeId,
                '--tmittype',
                dgt.rec.tmitTypeId,
                '--tmitgroup',
                dgt.rec.tmitGroupId,
            ])

        except Exception as x:
            self._log.warning('Exception caught starting output [genId:%d, outTypeId:%s, error:%s, trace:%s]' % (
                dgt.rec.genId, dgt.rec.outTypeId, str(x), traceback.format_exc()))
            return

        self._log.debug('_startGenTransmit - done [%s, item:%s]' % (str(genTmit), str(ritem)))


    def _startGenOutput(self, genOut):
        """
        Start a gen output.

        :param genOut: (tGenOut) The gen output format to start.
        """
        self._log.debug('_startGenOutput - start [%s]' % str(genOut))

        try:
            dgo  = self._cdao.dao.dGenOut(self._cdao.dbcon)
            dgo.lockOneDeft(genOut.genId, genOut.outTypeId, self._pod.stdDBLock())

            if dgo.rec.status != tGenOut.Status_Couplet.keyPending:
                self._log.warning('GenOut record [%s] no longer in pending status.' % (str(dgo.rec)))
                return

            dgo.rec.status    = tGenOut.Status_Couplet.keyBusy
            dgo.rec.dateStart = datetime.datetime.min
            dgo.rec.dateEnd   = datetime.datetime.min
            dgo.rec.genTime   = 0.0
            dgo.rec.fileSize  = 0
            dgo.rec.errMsg    = ''
            dgo.update()

            ritem           = self._prepRunProcItem(self.PROC_TYPE_GEN_OUTPUT, 'OutType (%s)' % dgo.rec.outTypeId)
            ritem['genOut'] = dgo.rec

            self._cdao.dbcon.commit()

            self._execRunProcItem(ritem, [
                'python3',
                self._genOutput,
                '--gen',
                str(dgo.rec.genId),
                '--outtype',
                dgo.rec.outTypeId,
            ])

        except Exception as x:
            self._log.warning('Exception caught starting output [genId:%d, outTypeId:%s, error:%s, trace:%s]' % (
                genOut.genId, genOut.outTypeId, str(x), traceback.format_exc()))
            return

        self._log.debug('_startGenOutput - done [genId:%s, outTypeId:%s, item:%s]' % (
            genOut.genId, genOut.outTypeId, str(ritem)))


    def _startGenBatch(self, genRec):
        """
        Start a gen batch.

        :param genRec: (tGen) The gen record to start.
        """
        self._log.debug('_startGenBatch - start [%s]' % str(genRec))

        try:
            dg  = self._cdao.dao.dGen(self._cdao.dbcon)
            dgm = self._cdao.dao.dGenMetric(self._cdao.dbcon)
            dg.lockOneDeft(genRec.id, self._pod.stdDBLock())

            if dg.rec.status != tGen.Status_Couplet.keyPending:
                raise Exception('Gen record [id:%d, status:%s] no longer in pending status.' % (
                    dg.rec.id, dg.rec.status))

            dg.rec.status = tGen.Status_Couplet.keyBusy
            dg.update()

            dgm.insertDeft(
                dg.rec.id,
                datetime.datetime.now(),
                datetime.datetime.min,
                0.0,
                0,
                tGen.Status_Couplet.keyPending,
                dg.rec.reqBy,
                dg.rec.reqDate,
                '',
                datetime.datetime.min,
                '',
                '',
                0)

            ritem              = self._prepRunProcItem(self.PROC_TYPE_GEN_BATCH, '%s (Enquiry)' % dg.rec.enquiryId if dg.rec.enquiryId != '' else '%s (Report)' % dg.rec.reportId)  # noqa
            ritem['gen']       = dg.rec
            ritem['genMetric'] = dgm.rec

            self._cdao.dbcon.commit()

            self._execRunProcItem(ritem, [
                'python3',
                self._genBatch,
                '--gen',
                str(dg.rec.id),
            ])

        except Exception as x:
            self._log.warning('Exception caught starting batch [genId:%d, error:%s, trace:%s]' % (
                genRec.id, str(x), traceback.format_exc()))
            return

        self._log.debug('_startGenBatch - done [genId:%s, item:%s]' % (genRec.id, str(ritem)))


    def _scheduleReports(self):
        """
        Schedule any pending reports.
        """
        dtnow = datetime.datetime.now()
        slist = []
        cnt   = 0

        self._log.debug('_scheduleReports - start [now:%s]' % (str(dtnow)))

        self._cdao.dbcon.rollback()
        self._cdao.dao.dReportSchedPending(self._cdao.dbcon).execDeft(dtnow).fetchAll(slist)

        if len(slist) > 0:
            drs   = self._cdao.dao.dReportSched(self._cdao.dbcon)
            drst  = self._cdao.dao.dReportSchedTmitForSchedule(self._cdao.dbcon)
            dg    = self._cdao.dao.dGen(self._cdao.dbcon)
            dgo   = self._cdao.dao.dGenOut(self._cdao.dbcon)
            dgt   = self._cdao.dao.dGenTmit(self._cdao.dbcon)
            tlist = []

            for srec in slist:
                drs.lockOneWith(srec, self._pod.stdDBLock())

                if srec.intNext != drs.rec.intNext:
                    self._log.warning('Report schedule interval next changed! [%s, %s!=%s]' % (
                        str(srec), str(srec.intNext), str(drs.rec.intNext)))
                    continue

                if drs.rec.intVal < 1:
                    self._log.warning('Report schedule internval ignored, value is less than 1 [%s], marking disabled' % (
                        str(drs.rec)))
                    drs.rec.status = tReportSched.Status_Couplet.keyDisabled
                    drs.update()
                    self._cdao.dbcon.commit()
                    continue

                prevDate        = drs.rec.intLast
                drs.rec.intLast = drs.rec.intNext

                if prevDate == datetime.datetime.min:
                    prevDate = drs.rec.intNext

                if drs.rec.intType == tReportSched.IntType_Couplet.keyHours:
                    drs.rec.intNext += datetime.timedelta(hours=drs.rec.intVal)
                elif drs.rec.intType == tReportSched.IntType_Couplet.keyDays:
                    drs.rec.intNext += datetime.timedelta(days=drs.rec.intVal)
                elif drs.rec.intType == tReportSched.IntType_Couplet.keyWeeks:
                    drs.rec.intNext += datetime.timedelta(days=drs.rec.intVal * 7)
                elif drs.rec.intType == tReportSched.IntType_Couplet.keyMonths:
                    drs.rec.intNext += relativedelta(months=drs.rec.intVal)
                else:
                    self._log.warning('Report schedule internval type not expected [%s], marking disabled' % (
                        str(drs.rec)))
                    drs.rec.status = tReportSched.Status_Couplet.keyDisabled
                    drs.update()
                    self._cdao.dbcon.commit()
                    continue

                tlist.clear()
                drst.execDeft(drs.rec.reportId, drs.rec.schedId, drs.rec.siteId).fetchAll(tlist)

                pdict = None
                repp  = None

                if drs.rec.param != '':
                    ivar  = InputVar(dg.rec, drs.rec, prevDate)
                    pdict = json.loads(drs.rec.param)
                    repp  = []

                    if not isinstance(pdict, dict):
                        raise Exception('Report parameters expected to be a dictionary type and got [%s]' % (
                            str(type(pdict))))

                    for pkey, pval in pdict.items():
                        if ivar.isVar(pval):
                            repp.append({'id' : pkey, 'value' : TypeUtil.forJSON(ivar.getVarValue(pval, pkey))})
                        else:
                            repp.append({'id' : pkey, 'value' : pval})

                dg.insertDeft(
                    drs.rec.siteId,
                    '',
                    drs.rec.reportId,
                    drs.rec.schedId,
                    tGen.Status_Couplet.keyPending,
                    '' if repp is None else json.dumps(repp),
                    self.MODULE,
                    dtnow,
                    datetime.datetime.min,
                    datetime.datetime.min,
                    0.0,
                    0,
                    '')

                if len(tlist) > 0:
                    lastOut = ''

                    for trec in tlist:
                        if lastOut != trec.outTypeId:
                            dgo.insertDeft(
                                dg.rec.id,
                                trec.outTypeId,
                                tGenOut.Status_Couplet.keyPending,
                                datetime.datetime.min,
                                datetime.datetime.min,
                                0.0,
                                0,
                                '',
                                self.MODULE)

                        dgt.insertDeft(
                            dg.rec.id,
                            trec.outTypeId,
                            trec.tmitTypeId,
                            trec.tmitGroupId,
                            tGenTmit.Status_Couplet.keyPending,
                            datetime.datetime.min,
                            datetime.datetime.min,
                            0.0,
                            '',
                            self.MODULE)

                        lastOut = trec.outTypeId

                drs.update()
                self._cdao.dbcon.commit()
                cnt += 1

        self._log.debug('_scheduleReports - done [cnt:%d]' % (cnt))
