# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os
import os.path
import datetime
import traceback

from bs_lib import Common

from bs_crest.Config import Config
from bs_crest        import TypeUtil

from bs_crest.db.tables.tGen      import tGen
from bs_crest.db.tables.tOutType  import tOutType
from bs_crest.db.tables.tGenTmit  import tGenTmit
from bs_crest.db.tables.tTmitCfg  import tTmitCfg
from bs_crest.db.tables.tTmitType import tTmitType


class GenTransmit:
    """
    This object transmits the output files to the target
    destinations.
    """
    MODULE = '[GenTransmit]'


    def __init__(self, pod, dao):
        """
        Constructor.

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) Crest Dao object.
        """
        self._pod         = pod
        self._log         = pod.log
        self._cdao        = dao

        self._startTM     = datetime.datetime.min
        self._endTM       = datetime.datetime.min
        self._genPath     = ''
        self._genFilePath = ''
        self._genRec      = tGen()
        self._otypeRec    = tOutType()
        self._genTmitRec  = tGenTmit()
        self._tmitCfgRec  = tTmitCfg()


    def run(self, genId, outTypeId, tmitTypeId, tmitGroupId):
        """
        Send the generated outTypeId

        :param genId: (int) The gen id to generate for.
        :param outTypeId: (string) The outtype to send.
        :param tmitTypeId: (string) The transmit type.
        :param tmitGroupId: (string) The transmit group.
        :return: True if successful, else False.
        """
        self._startTM = datetime.datetime.now()

        self._log.info('run - started [genId:%d, outTypeId:%s, tmitTypeId:%s, tmitGroupId:%s]' % (
            genId, outTypeId, tmitTypeId, tmitGroupId))

        tmitObj = None

        try:
            self._initialize(genId, outTypeId, tmitTypeId, tmitGroupId)

            tmitObj = self._factoryGetTransmitObject(tmitTypeId)

            tmitObj.initialize(self._genRec, self._tmitCfgRec, tmitGroupId)

            if self._genRec.caption == '':
                sendName = '%s.%d.%s' % (
                    'Enquiry' if self._genRec.enquiryId != '' else 'Report', self._genRec.id, self._otypeRec.fileExt)
            else:
                sendName = '%s.%s' % (self._genRec.caption, self._otypeRec.fileExt)

            tmitObj.run(self._genFilePath, sendName)

            tmitObj.destroy()

            self._finalize()

        except Exception as x:
            strace = traceback.format_exc()
            self._log.error('run - failed [genId:%d, outTypeId:%s, tmitTypeId:%s, tmitGroupId:%s, error:%s, trace:%s]' % (
                genId, outTypeId, tmitTypeId, tmitGroupId, str(x), strace))

            if tmitObj:
                tmitObj.destroy()

            if not self._finalize(x):
                raise

            return False

        self._log.info('run - done [genId:%d, outTypeId:%s, tmitTypeId:%s, tmitGroupId:%s]' % (
            genId, outTypeId, tmitTypeId, tmitGroupId))

        return True


    def _initialize(self, genId, outTypeId, tmitTypeId, tmitGroupId):
        """
        Initialize the all records and ensure statuses are all valid.

        :param genId: (int) The gen id to generate for.
        :param outTypeId: (string) The outtype to send.
        :param tmitTypeId: (string) The transmit type.
        :param tmitGroupId: (string) The transmit group.
        """
        conf = Config(self._cdao)
        cfg  = conf.load(['output.path'])

        self._genPath = Common.readDict(cfg, 'output.path',    str)

        if not os.path.exists(self._genPath) or not os.path.isdir(self._genPath):
            raise Exception('Generation path not found/valid [%s]' % self._genPath)

        dg   = self._cdao.dao.dGen(self._cdao.dbcon)
        dot  = self._cdao.dao.dOutType(self._cdao.dbcon)
        dtc  = self._cdao.dao.dTmitCfg(self._cdao.dbcon)
        dgt  = self._cdao.dao.dGenTmit(self._cdao.dbcon)

        dgt.lockOneDeft(genId, outTypeId, tmitTypeId, tmitGroupId, self._pod.stdDBLock())

        if dgt.rec.status != tGenTmit.Status_Couplet.keyBusy:
            raise Exception('Gen Transmit is in unexpected status [genId:%d, outTypeId:%s, tmitTypeId:%s, \
tmitGroupId:%s, status:%s]' % (genId, outTypeId, tmitTypeId, tmitGroupId, tGenTmit.Status_Couplet.getValue(dgt.rec.status)))

        dg.selectOneDeft(genId)

        if dg.rec.status != tGen.Status_Couplet.keyComplete:
            raise Exception('Cannot transmit output, gen document not in complete status! [genId:%s, status:%s, \
outTypeId:%s, tmitTypeId:%s, tmitGroupId:%s]' % (genId, dg.rec.status, outTypeId, tmitTypeId, tmitGroupId))

        self._genPath = os.path.join(self._genPath, dg.rec.reqDate.strftime(TypeUtil.MONTH_FMT))

        if not os.path.exists(self._genPath) or not os.path.isdir(self._genPath):
            raise Exception('Generation path not found/valid [%s]' % self._genPath)

        dot.selectOneDeft(outTypeId)
        dtc.selectOneDeft(tmitTypeId, dg.rec.siteId)

        if dtc.rec.status != tTmitCfg.Status_Couplet.keyActive:
            raise Exception('Cannot transmit output, transmit cfg not active! [genId:%s, siteId:%d, outTypeId:%s, \
tmitTypeId:%s]' % (genId, dg.rec.siteId, outTypeId, tmitTypeId))

        self._genFilePath = os.path.join(self._genPath, '%d.%s' % (genId, dot.rec.outExt))

        if not os.path.exists(self._genFilePath) or not os.path.isfile(self._genFilePath):
            raise Exception('Cannot transmit output, file not found [genId:%d, outTypeId:%s, tmitTypeId:%s, \
tmitGroupId:%s, file:%s]' % (genId, outTypeId, tmitTypeId, tmitGroupId, self._genFilePath))

        self._genRec      = dg.rec
        self._otypeRec    = dot.rec
        self._genTmitRec  = dgt.rec
        self._tmitCfgRec  = dtc.rec

        self._tmitCfgRec.cfg = os.path.expandvars(self._tmitCfgRec.cfg)


    def _finalize(self, ex=None):
        """
        Finalize the transmit record.

        :param ex: (Exception) Did the output fail with an error?
        :return: (bool) True if db record was update.
        """
        if self._genTmitRec.genId < 1:
            return False

        if not ex:
            self._genTmitRec.status    = tGenTmit.Status_Couplet.keyComplete
            self._genTmitRec.dateStart = self._startTM
            self._genTmitRec.dateEnd   = datetime.datetime.now()
            self._genTmitRec.genTime   = (self._genTmitRec.dateEnd - self._genTmitRec.dateStart).total_seconds()
            self._genTmitRec.errMsg    = ''
        else:
            self._genTmitRec.status    = tGenTmit.Status_Couplet.keyFailed
            self._genTmitRec.dateStart = self._startTM
            self._genTmitRec.dateEnd   = datetime.datetime.now()
            self._genTmitRec.genTime   = (self._genTmitRec.dateEnd - self._genTmitRec.dateStart).total_seconds()
            self._genTmitRec.errMsg    = str(ex)

        dgt = self._cdao.dao.dGenTmit(self._cdao.dbcon)

        dgt.update(self._genTmitRec)


    def _factoryGetTransmitObject(self, tmitTypeId):
        """
        Gets the transmit object for the specified trasmit type.  This method can
        be overloaded by systems to inject their own Transmit objects.

        :param tmitTypeId: (string) The tramsmit type to load.
        :return: (Transmit) A derrived object type Transmit
        """
        resObj = None

        if tmitTypeId == tTmitType.Id_Couplet.keySmtp:
            from bs_crest.transmit.TransmitSMTP import TransmitSMTP
            resObj = TransmitSMTP(self._pod, self._cdao)

        if not resObj:
            raise Exception('Transmit Type [%s] object not expected!' % (tmitTypeId))

        return resObj
