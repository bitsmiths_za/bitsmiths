# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import os.path


class InputVar:
    """
    An object to handle the setting of input variables, values, and constants.
    """

    def __init__(self, genRec, schedRec=None, prevDate=None):
        """
        Constructor.

        :param genRec: (tGen) The generation record.
        :param schedRec: (tReportSched) Optionally give the report scedule record.
        :param prevDate: (datetime) Optionally give the previous date the report ran.
        """
        self._genRec   = genRec
        self._schedRec = schedRec
        self._prevDate = prevDate

        if not self._prevDate:
            self._prevDate = datetime.datetime.min


    def isVar(self, var):
        """
        Checks if the value is a variable of some kind.

        :param var: (string) The value to check.
        :return: True if is a variable else False.
        """
        if var.startswith('##') or var.startswith('$'):
            return True

        return False


    def getVarValue(self, var, paramId):
        """
        Gets the value for the variable.  Raises an exception if variable is not known.

        :param var: (string) The variable value given.
        :param paramId: (string) The parameter id for the variable, purely for error handling.
        :return: The value for the variable in string form.
        """
        if var.startswith('$s'):
            return os.path.expandvars(var)

        if var.startswith('##'):
            num  = 0
            rix  = var.rfind('##')

            if rix < 1:
                raise Exception('Variable not complete. [var:%s, paramId:%s]' % (var, paramId))

            rem = var[rix + 2:].strip()

            if rem.startswith('+') or rem.startswith('-'):
                if not rem[1:].isdigit():
                    raise Exception('No numeric value found on right side of operator! [var:%s, paramId:%s]' % (var, paramId))

                num = int(rem)

            var = var[:rix + 2]

            if var == '##site##':
                return str(self._genRec.siteId)

            if var == '##today##':
                return str(datetime.date.today() + datetime.timedelta(days=num))

            if var == '##now##':
                return str(datetime.datetime.now() + datetime.timedelta(days=num))

            if var == '##gen.reqby##':
                return self._genRec.reqBy

            if var == '##gen.reqdate##':
                return str(self._genRec.reqDate + datetime.timedelta(days=num))

            if var.startswith('##report.') and not self._schedRec:
                raise Exception('Crest report variable [report] variable when no report schedule has been specified!')

            if var == '##report.date##':
                return str(self._schedRec.intLast.date() + datetime.timedelta(days=num))

            if var == '##report.datetime##':
                return str(self._schedRec.intLast)

            if var == '##report.next-date##':
                return str(self._schedRec.intNext.date() + datetime.timedelta(days=num))

            if var == '##report.next-datetime##':
                return str(self._schedRec.intNext)

            if var == '##report.prev-date##':
                return str(self._prevDate.date() + datetime.timedelta(days=num))

            if var == '##report.prev-datetime##':
                return str(self._prevDate + datetime.timedelta(days=num))

        raise Exception('Crest variable [%s] not expected/found [paramId:%s]' % (var, paramId))
