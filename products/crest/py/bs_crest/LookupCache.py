# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import json

from bs_crest        import TypeUtil

from bs_crest.db.tables.tLookup  import tLookup
from bs_crest.db.tables.tParam   import tParam


class LookupCache:
    """
    The lookup cache object.
    """

    def __init__(self, dao):
        """
        Constructor.

        :param dao: (bs_crest.Dao) Database dao module to use.
        """
        self._dbcon    = dao.dbcon
        self._dao      = dao
        self._cache    = {}


    def cache(self, lookupId, reloadCache=False):
        """
        Cache a lookup.

        :param lookupId: (string) The lookup to cache.
        :param reload: (bool) If true forces a reload.
        :return: The size/count of the lookup cached.
        """
        lc = self._cache.get(lookupId)

        if lc is None or reloadCache:
            lc = self._loadCache(lookupId)
            self._cache[lookupId] = lc

        return len(lc['data'])


    def lookupVal(self, lookupId, val, nullOK=False):
        """
        Looks up the lookup cache value.

        :param lookupId: (string) The lookup to use.
        :param val: (object) The lookup object.
        :param nullOK: (bool) If True and the lookup is not found, returns an empty string.
        :return: The lookup value.
        """
        lc = self._cache.get(lookupId)

        if not lc:
            raise Exception('Lookup [%s] not cached' % lookupId)

        res = lc['data'].get(val)

        if not res:
            if nullOK:
                return ''

            raise Exception('Lookup value not found [lookupId:%s, val:%s]' % (lookupId, str(val)))

        return res


    def _loadCache(self, lookupId):
        """
        Loads a cache.

        :param lookupId: (string) The lookup to load.
        :return: The lookup dictionary.
        """
        dl  = self._dao.dao.dLookup(self._dbcon)
        res = {}

        try:
            dl.selectOneDeft(lookupId)

            res['id']         = dl.rec.id
            res['caption']    = dl.rec.caption
            res['lookupType'] = dl.rec.lookupType
            res['data']       = None

            if dl.rec.lookupSrc == tLookup.LookupSrc_Couplet.keyJSON:
                if dl.rec.src == '':
                    raise Exception('JSON string not defined')

                data = json.loads(dl.rec.src)

                if type(data) != dict:
                    raise Exception('JSON string not a dictionary')

                for key, val in data.items():
                    TypeUtil.isType(key, dl.rec.lookupType)
                    TypeUtil.isType(val, tParam.ParamType_Couplet.keyText)

                res['data'] = data

            elif dl.rec.lookupSrc == tLookup.LookupSrc_Couplet.keyLookup_Items:
                dli  = self._dao.dao.dLookupItemForCache(self._dbcon)
                data = {}

                dli.execDeft(lookupId)

                while dli.fetch():
                    data[TypeUtil.toType(dli.orec.id, dl.rec.lookupType)] = dli.orec.value

                res['data'] = data

            elif dl.rec.lookupSrc == tLookup.LookupSrc_Couplet.keySQL_Query:

                stmnt = None
                data  = {}
                try:
                    stmnt = self._dbcon.statement('lookup')
                    stmnt.sql(dl.rec.src)
                    self._dbcon.execute(stmnt)

                    if not self._dbcon.fetch(stmnt):
                        raise Exception('Lookup query returned no rows.')

                    if len(stmnt.columns) != 2:
                        raise Exception('Lookup query result has [%d] column instead of [2].' % len(stmnt.columns))

                    TypeUtil.isType(stmnt.result[0], dl.rec.lookupType)
                    TypeUtil.isType(stmnt.result[1], tParam.ParamType_Couplet.keyText)

                    while True:
                        data[stmnt.result[0]] = stmnt.result[1]

                        if not self._dbcon.fetch(stmnt):
                            break

                finally:
                    del stmnt

                res['data'] = data

            else:
                raise Exception('Lookup source [%s] not expected' % dl.rec.lookupSrc)

            if len(res['data']) < 1:
                raise Exception('Lookup is empty')

        except Exception as x:
            raise Exception('Lookup [%s] cannot be cached: %s' % (lookupId, str(x)))

        return res
