# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime

DATE_FMT     = '%Y-%m-%d'
DATETIME_FMT = '%Y-%m-%d %H:%M:%S'
MONTH_FMT    = '%Y-%m'

from bs_crest.db.tables.tParam   import tParam


def toType(val, valType, raiseError=False):
    """
    Converts a value to the required type. It either returns the new typed value, or None if the value
    is not compatible with the value type or raises an error.

    :param val: (string) The value to get.
    :param valType: (string) The correct value type of value, see tParam.paramType colum.
    :param raiseError: (bool) If true raises an exception instead of returning None on failure.
    :return: (object) The value converted to the correct type.
    """
    try:
        if type(val) != str:
            if raiseError:
                raise Exception('Value type is not expected [val:%s, type:%s, valType:%s]' % (
                    str(val), str(type(val)), valType))

            return None

        if valType == tParam.ParamType_Couplet.keyText:
            return val

        if valType == tParam.ParamType_Couplet.keyNumber:
            if val == '':
                return 0

            return int(val)

        if valType == tParam.ParamType_Couplet.keyFloat or\
           valType == tParam.ParamType_Couplet.keyPercentile:
            if val == '':
                return 0.0
            return float(val)

        if valType == tParam.ParamType_Couplet.keyDate:
            if val == '':
                return datetime.date.min

            if len(val) > 10:
                return datetime.datetime.strptime(val[:10], DATE_FMT).date()

            return datetime.datetime.strptime(val, DATE_FMT).date()

        if valType == tParam.ParamType_Couplet.keyDateTime:
            if val == '':
                return datetime.datetime.min

            if len(val) > 19:
                return datetime.datetime.strptime(val[:19], DATETIME_FMT)

            return datetime.datetime.strptime(val, DATETIME_FMT)

        if valType == tParam.ParamType_Couplet.keyYesNo:
            if val == '':
                return 'N'

            if val == 'Y' or val == 'N':
                return val

            raise Exception('Yes/No value [%s] not expected' % (val))

        raise Exception('ValueType not expected [val:%s, valType:%s]' % (val, str(valType)))

    except TypeError:
        if raiseError:
            raise

        return None

    except ValueError:
        if raiseError:
            raise

        return None


def isType(val, valType, raiseError=False):
    """
    Checks if an value is the correct type.

    :param val: (object) The value object to check.
    :param valType: (string) The value type the val is suppose to be.
    :param raiseError: (bool) If True and the val type is not the same as valType an error is raised.
    :return: True if val type and valType match else False.
    """
    if type(val) == str and valType == tParam.ParamType_Couplet.keyText:
        return True

    if type(val) == int and valType == tParam.ParamType_Couplet.keyNumber:
        return True

    if type(val) == float and (valType == tParam.ParamType_Couplet.keyFloat or
                               valType == tParam.ParamType_Couplet.keyPercentile):
        return True

    if type(val) == datetime.date and valType == tParam.ParamType_Couplet.keyDate:
        return True

    if type(val) == datetime.datetime and valType == tParam.ParamType_Couplet.keyDateTime:
        return True

    if val in ['Y', 'N'] and valType == tParam.ParamType_Couplet.keyYesNo:
        return True

    if raiseError:
        raise Exception('Value [%s] is not the correct type [%s != %s]' % (
            str(val)[:16], str(type(val)), tParam.ParamType_Couplet.getValue(valType)))

    return False


def forJSON(val):
    """
    Converts an object to a json compatible output.

    :param val: (object) The value object to convert.
    :return: (object) The result object.
    """
    if val is None:
        return ''

    if type(val) == str:
        return val

    if type(val) == int:
        return val

    if type(val) == float:
        return val

    if type(val) == datetime.date:
        if val == datetime.date.min:
            return ''

        return val.strftime(DATE_FMT)

    if type(val) == datetime.datetime:
        if val == datetime.datetime.min:
            return ''

        return val.strftime(DATETIME_FMT)

    raise Exception('Type [%s] not expected' % (str(type(val))))


def forText(val, outType: str, precision: int = None, nullTxt: str = None, sep: str = None) -> str:
    """
    Converts an object to a json compatible output.

    :param val: The value object to convert.
    :param outType: The outtype.
    :param precision: The precision for floats/percentages, None means no formatting.
    :param nullTxt: The text to use for nulls.
    :param sep: The
    :return: The string value.
    """
    if val is None:
        if nullTxt:
            return nullTxt

        return ''

    if type(val) == str:
        if nullTxt and not val:
            return nullTxt

        if sep and val.find(sep) != -1:
            return '"%s"' % val

        return val

    if type(val) == int:
        return str(val)

    if type(val) == float:
        if precision is None:
            precision = 2

        if outType == tParam.ParamType_Couplet.keyPercentile:
            return '%.*f%%' % (precision, val)

        return '%.*f' % (precision, val)

    if type(val) == datetime.date:
        if val == datetime.date.min:
            if nullTxt:
                return nullTxt

            return ''

        return val.strftime(DATE_FMT)

    if type(val) == datetime.datetime:
        if val == datetime.datetime.min:
            if nullTxt:
                return nullTxt

            return ''

        return val.strftime(DATETIME_FMT)

    raise Exception('Type [%s] not expected' % (str(type(val))))
