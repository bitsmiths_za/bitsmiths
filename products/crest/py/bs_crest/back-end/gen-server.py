#!/usr/bin/python3

# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import sys
import os
import os.path

for x in os.getenv('CREST_PYTHON_PATHS').split(':'):
    sys.path.append(os.path.join(os.getenv('HOME'), x))

import logging
import signal
import getopt
import traceback

from bs_lib.Pod              import Pod
from bs_lib.ProviderStd      import ProviderStd

from bs_crest.GenServer      import GenServer
from bs_crest.Dao            import Dao as CrestDao

gGenServer = None


def signal_handler_int(signal, frame):
    """
    Catch interrupt signal. Flag that we must terminate politely
    """
    global gGenServer

    if gGenServer:
        gGenServer._shutdown = True


def signal_handler_kill(signal, frame):
    """
    Catch kill signal. Flag that we must terminate not so politely
    """
    global gGenServer

    if gGenServer:
        gGenServer._shutdown = True


def showUsage():
    """
    Prints the program usage.
    """
    print('usage: gen-server.py [-h --cfgfile CFGFILE')
    print('')
    print('Crest generation server command line interface')
    print('')
    print('optional arguments:')
    print('  -h, --help         Show this help message and exit')
    print('  --cfgfile CFGFILE  Configuration file (default: ${CREST_CONFIG})')
    print('')
    sys.exit(2)


def readArgs():
    """
    Reads the command line arguements for this program.

    :returns: The args object.
    """
    try:
        optlist, gargs = getopt.getopt(sys.argv[1:], 'h', ['help', 'cfgfile='])
    except getopt.GetoptError as err:
        print(err)
        showUsage()

    class Args():
        def __init__(self):
            self.cfgfile  = os.getenv("CREST_CONFIG")

    args = Args()

    for o, a in optlist:
        if o in ("-h", "--help"):
            showUsage()
            sys.exit()
        elif o == '--cfgfile':
            args.cfgfile = a
        else:
            print(' ... unrecognized arguments: %s' % o)
            showUsage()

    return args


def main():
    """
    Main entry point for the app server.
    """
    args   = readArgs()
    prov   = ProviderStd()

    prov.initStdLogging(logConfig=os.getenv('BS_PRODUCTS_LOGGING'))

    cfg    = prov.newConfig({'config-file' : args.cfgfile})
    log    = prov.newLogger(options={'name' : ['crest-gen-server', 'crest', 'server']})

    global gGenServer

    try:
        log.info('Crest Generation Server - starting up')

        dbcon = prov.newDBConnector(None, options={
            'db-type' : 'postgresql',
            'db-conn-string' : cfg['databases']['postgresql']['crest'],
        })

        pod   = Pod()

        pod.initialize(cfg, log, dbcon)

        from bs_crest.db.dao import postgresql as cdao

        gGenServer = GenServer(pod, CrestDao(dbcon, cdao))

        gGenServer.run()

        log.info('Crest Generation Server - shutting down')

    except Exception as x:
        if log:
            log.error("Exception caught in main, gen-server terminating. [err:%s, stack-trace:%s]" % (
                str(x), traceback.format_exc()))

        print("Exception caught in main, appserver terminating. [err:%s, stack-trace:%s]" % (
            str(x), traceback.format_exc()))

        return 1
    finally:
        del gGenServer

        logging.shutdown()

    return 0


signal.signal(signal.SIGINT,  signal_handler_int)
signal.signal(signal.SIGTERM, signal_handler_kill)


if __name__ == '__main__':
    sys.exit(main())
