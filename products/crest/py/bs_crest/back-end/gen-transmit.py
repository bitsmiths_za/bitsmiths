#!/usr/bin/python3

# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import sys
import os
import os.path
import logging
import getopt
import traceback

for x in os.getenv('CREST_PYTHON_PATHS').split(':'):
    sys.path.append(os.path.join(os.getenv('HOME'), x))

from bs_lib.Pod              import Pod
from bs_lib.ProviderStd      import ProviderStd

from bs_crest.GenTransmit    import GenTransmit
from bs_crest.Dao            import Dao as CrestDao


def showUsage():
    """
    Prints the program usage.
    """
    print('usage: gen-transmit.py --gen GENID --outtype OTYPE')
    print('          --tmittype TTYPE --tmitgroup GRPID [-h --cfgfile CFGFILE]')
    print('')
    print('Crest generation output command line interface')
    print('')
    print('required arguments:')
    print('  --gen       GENID  The gen id (primary key crest.gen table)')
    print('  --outtype   OTYPE  The output type id (primark key crest.outtype table)')
    print('  --tmittype  TTYPE  The transmit type id (primark key crest.tmittype table)')
    print('  --tmitgroup GRPID  The transmit group id (see crest.tmitgroup table)')
    print('')
    print('optional arguments:')
    print('  -h, --help         Show this help message and exit')
    print('  --cfgfile CFGFILE  Configuration file (default: ${CREST_CONFIG})')
    print('')
    sys.exit(2)


def readArgs():
    """
    Reads the command line arguements for this program.

    :returns The argparse.parse_args() result.
    """
    try:
        optlist, gargs = getopt.getopt(sys.argv[1:], 'h', ['help', 'gen=', 'outtype=', 'tmittype=', 'tmitgroup=', 'cfgfile='])
    except getopt.GetoptError as err:
        print(err)
        showUsage()

    class Args():
        def __init__(self):
            self.cfgfile  = os.getenv("CREST_CONFIG")
            self.gen      = 0
            self.otype    = ''
            self.ttype    = ''
            self.grpid    = ''

    args = Args()

    for o, a in optlist:
        if o in ("-h", "--help"):
            showUsage()
        elif o == '--gen':
            if not a.isdigit():
                print(' ... gen must be a number')
                showUsage()
                sys.exit(2)
            args.gen = int(a)
        elif o == '--outtype':
            args.otype = a
        elif o == '--tmittype':
            args.ttype = a
        elif o == '--tmitgroup':
            args.grpid = a
        elif o == '--cfgfile':
            args.cfgfile = a
        else:
            print(' ... unrecognized arguments: %s' % o)
            showUsage()

    if args.gen < 1 or len(args.otype) < 1 or len(args.ttype) < 1 or len(args.grpid) < 1:
        showUsage()

    return args


def main():
    """
    Main entry point for the app server.
    """
    args   = readArgs()
    prov   = ProviderStd()

    prov.initStdLogging(logConfig=os.getenv('BS_PRODUCTS_LOGGING'))

    cfg = prov.newConfig({'config-file' : args.cfgfile})
    log = prov.newLogger(options={'name' : ['crest-gen-server', 'crest', 'batch']})

    try:
        log.info('GenOutput starting [gen:%d].' % (args.gen))

        dbcon = prov.newDBConnector(None, options={
            'db-type' : 'postgresql',
            'db-conn-string' : cfg['databases']['postgresql']['crest'],
        })
        pod   = Pod()

        pod.initialize(cfg, log, dbcon)

        from bs_crest.db.dao import postgresql as cdao

        gbt = GenTransmit(pod, CrestDao(dbcon, cdao))

        gbt.run(args.gen, args.otype, args.ttype, args.grpid)

        dbcon.commit()

        log.info('GenOutput [gen:%d] shutting down.' % (args.gen))

    except Exception as x:
        if log:
            log.error("Exception caught [err:%s, stack-trace:%s]" % (
                str(x), traceback.format_exc()))

        print("Exception caught [err:%s, stack-trace:%s]" % (
            str(x), traceback.format_exc()), file=sys.stderr)

        return 1
    finally:
        logging.shutdown()

    return 0


if __name__ == '__main__':
    sys.exit(main())
