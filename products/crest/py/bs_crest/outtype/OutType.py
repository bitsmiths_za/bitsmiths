# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import json

from bs_lib import Common

from bs_crest.db.tables.tOutType import tOutType


class OutType:
    """
    Base out type class.  All out type objects to inherit
    from this base to produce the desired out file.
    """

    def __init__(self, pod, dao):
        """
        Constructor

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) The dao to use.
        """
        self._pod      = pod
        self._cdao     = dao
        self._genId    = 0
        self._genRec   = None
        self._otRec    = None
        self._siteCode = ''
        self._srcObj   = {}

        self._enqRec   = None
        self._repRec   = None
        self._enqCols  = {}


    def outTypeRec(self):
        """
        Gets the outType record.

        :return: (tOutType) The record.
        """
        if not self._otRec:
            dot = self._cdao.dao.dOutType(self._cdao.dbcon)

            dot.selectOneDeft(self.outType())

            self._otRec = dot.rec


        return self._otRec


    def active(self):
        """
        Get if this out type is active.

        :return: (bool) True if active.
        """
        return self.outTypeRec().status == tOutType.Status_Couplet.keyActive


    def outExt(self):
        """
        Gets the output extension.

        :return: (string) the out type local extension.
        """
        return self.outTypeRec().outExt


    def fileExt(self):
        """
        Gets the output file extension.

        :return: (string) the out type proper file extension.
        """
        return self.outTypeRec().fileExt


    def outType(self):
        """
        Abstract method, the output type
        """
        raise Exception('Abstract method OutType.outType not implemented')


    def name(self):
        """
        Abstract method, the name of this output generator.
        """
        raise Exception('Abstract method OutType.name not implemented')


    def outTypeDependancy(self):
        """
        Virtual method, the out type may have a dependancy on another
        out type.

        :return: (string) : The dependancy if it has one.
        """
        return None


    def initialize(self, genRec, siteCode):
        """
        Initialize the out type object.

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) The dao to use.
        """
        self._genId    = genRec.id
        self._genRec   = genRec
        self._siteCode = siteCode


    def destroy(self):
        """
        Destroy the object and free up any internal objects/handles.
        """
        pass


    def run(self, genSrcFile, outFilename, depFileName):
        """
        The main entry point to for generation of a file.

        :param genSrcFile: (string) The generated source json file to read.
        :param outFilename: (string) The full path to the output filename.
        :param depFilename: (string) The full path to the dependancy filename, or None if not applicable.
        """
        self._pod.log.debug('OutType.run - start [genId:%d, genSrcFile:%s, outFilename:%s, depFileName:%s]' % (
            self._genId, genSrcFile, outFilename, str(depFileName)))

        self._loadSrcFile(genSrcFile)

        self._process(outFilename, depFileName)

        self._pod.log.debug('OutType.run - done [genId:%d, genSrcFile:%s, outFilename:%s]' % (
            self._genId, genSrcFile, outFilename))


    def _loadSrcFile(self, genSrcFile):
        """
        Loads the src file as an object.  Ensures that it is valid.

        :param genSrcFile: (string) The generated source json file to read.
        """
        with open(genSrcFile, 'rt') as fh:
            self._srcObj = json.loads(fh.read())

        par = 'SrcObj.'
        ver = Common.readDict(self._srcObj, 'version', float, par)

        if ver > 1.0:
            raise Exception('Source file version [%.2f] is not expected' % ver)

        genId = Common.readDict(self._srcObj, 'genId',    int, par)

        Common.readDict(self._srcObj, 'caption',  str, par)
        Common.readDict(self._srcObj, 'reportId', str, par)

        if genId != self._genId:
            raise Exception('Source file genId [%d] not the same as loaded object [%d]' % (
                genId, self._genId))

        outObjs = Common.readDict(self._srcObj, 'output',  list, par)
        outIdx  = 0

        if len(outObjs) < 1:
            raise Exception('Source file [%s, genId:%d] does not contain any output.' % (
                genSrcFile, genId))

        for oobj in outObjs:
            par     = 'SrcObj.output[%d].' % outIdx
            outIdx += 1

            Common.readDict(oobj, 'caption',      str,  par)
            Common.readDict(oobj, 'enqId',        str,  par)
            Common.readDict(oobj, 'genId',        int,  par)
            Common.readDict(oobj, 'tmStamp',      str,  par)
            Common.readDict(oobj, 'totRows',      int,  par)
            Common.readDict(oobj, 'reqBy',        str,  par)
            Common.readDict(oobj, 'reqTmStamp',   str,  par)
            Common.readDict(oobj, 'cols',         list, par)
            Common.readDict(oobj, 'rows',         list, par)


    def _process(self, outFilename, depFileName):
        """
        Abstract method to generate the output to the target out filename.

        :param outFilename: (string) The destination output file.
        :param depFilename: (string) The dependancy file.
        """
        raise Exception('Abstract method OutType._process not implemented')


    def _getOTFormatDict(self, fmt):
        """
        Reads an outtype format dictionar and returns the
        consolidated/overloaded formats.

        :param fmt: (string) The json string with format options.
        """
        if not fmt:
            return None

        self._pod.log.debug(' - reading json format:%s' % fmt)

        allfmts = json.loads(fmt)
        res     = {}
        optList = [
            '*',
            '*.%s' % self._siteCode,
            self.outExt(),
            '%s.%s' % (self.outExt(), self._siteCode),
        ]

        for opt in optList:
            fmtOpts = allfmts.get(opt)

            if fmtOpts:
                res.update(fmtOpts)

        return res


    def _getPageFormatOptions(self):
        """
        Gets the page layout option formats.

        :return: (dict) The formats.
        """
        if self._enqRec:
            return self._getOTFormatDict(self._enqRec.otFmt)

        if self._repRec:
            return self._getOTFormatDict(self._repRec.otFmt)

        raise Exception('Enquiry/Report record not loaded, internal error!')


    def _getColumnFormatOptions(self, enqId, colid):
        """
        Gets the column layout option formats.

        :param enqId: (string) The enquiry id.
        :param colid: (string) The column id.
        :return: (dict) The formats.
        """
        cols = self._enqCols.get(enqId)

        if not cols:
            raise Exception('Enquiry record [%s] columns not loaded, internal error!' % enqId)

        for ecol in cols:
            if ecol.queryOutId == colid:
                return self._getOTFormatDict(ecol.otFmt)

        raise Exception('Enquiry out column [%s] not found [genId:%d, enquiryId:%s]' % (colid, self._genId, enqId))


    def _readEnqReqRecs(self):
        """
        Read the enquiry or report records.
        """
        if self._genRec.enquiryId:
            de    = self._cdao.dao.dEnquiry(self._cdao.dbcon)
            deo   = self._cdao.dao.dEnquiryOutByEnquiry(self._cdao.dbcon)

            de.selectOneDeft(self._genRec.enquiryId)

            self._enqRec             = de.rec
            self._enqCols[de.rec.id] = []

            deo.execDeft(self._genRec.enquiryId).fetchAll(self._enqCols[de.rec.id])

        elif self._genRec.reportId:
            dr    = self._cdao.dao.dReport(self._cdao.dbcon)
            dei   = self._cdao.dao.dEnquiryForReport(self._cdao.dbcon)
            deo   = self._cdao.dao.dEnquiryOutByEnquiry(self._cdao.dbcon)

            dr.selectOneDeft(self._genRec.reportId)
            dei.execDeft(self._genRec.reportId)

            while dei.fetch():
                self._enqCols[dei.orec.id] = []
                deo.execDeft(dei.orec.id).fetchAll(self._enqCols[dei.orec.id])

            self._repRec = dr.rec

        if len(self._enqCols) < 1:
            raise Exception('Gen record [%d] has no output!' % (self._genRec.id))

        for enqId, cols in self._enqCols.items():
            if len(cols) < 1:
                raise Exception('Enquiry has no output columns [enquiryId:%s, genId:%d] has no output!' % (
                    enqId, self._genRec.id))

            for ec in cols:
                if ec.queryOutId.count(':') == 1:
                    ec.queryOutId = ec.queryOutId.split(':')[1]
