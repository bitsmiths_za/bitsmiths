# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

from bs_crest  import TypeUtil

from bs_crest.outtype.OutType import OutType

from bs_crest.db.tables.tOutType import tOutType


class OutTypeCSV(OutType):
    """
    Generate a standard csv output file.
    """
    SEP = ','

    def __init__(self, pod, dao):
        """
        Constructor

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) The dao to use.
        """
        OutType.__init__(self, pod, dao)
        self._fh = None


    def outType(self):
        """
        Abstract method, the output type, see tGenOut.outType
        """
        return tOutType.Id_Couplet.keyCsv


    def name(self):
        """
        Abstract method, the name of this output generator.
        """
        return 'CSV (Standard)'


    def initialize(self, genRec, siteCode):
        """
        Initialize the out type object.

        :param genRec: (tGen) The generation record.
        :param siteCode: (string) The site code.
        """
        OutType.initialize(self, genRec, siteCode)
        self._readEnqReqRecs()
        self._fh = None


    def destroy(self):
        """
        Destroy the object and free up any internal objects/handles.
        """
        if self._fh:
            self._fh.close()
            self._fh = None

        OutType.destroy(self)


    def _process(self, outFilename, depFilename):
        """
        Abstract method to generate the output to the target out filename.

        :param outFilename: (string) The destination output file.
        :param depFilename: (string) The dependancy file.
        """
        self._pod.log.debug('OutTypeCSV._process - start [%s]' % (outFilename))

        self._fh = open(outFilename, 'wt', 1024 * 16)
        fh       = self._fh
        oidx     = 0

        fmt      = self._getPageFormatOptions()
        exclRows = fmt.get('excl-rows')
        csvSep   = fmt.get('csv-sep')

        if csvSep is None or csvSep == '':
            csvSep = self.SEP

        for oobj in self._srcObj['output']:
            if oidx > 0:
                fh.write('\n\n')

            fh.write('%s%s%s\n'   % ('ENQUIRY:',     csvSep, oobj['caption']))
            fh.write('%s%s%s\n'   % ('TOTAL ROWS:',  csvSep, str(oobj['totRows'])))
            fh.write('%s%s%s\n'   % ('REQ BY:',      csvSep, oobj['reqBy']))
            fh.write('%s%s%s\n\n' % ('REQ DATE:',    csvSep, oobj['reqTmStamp']))

            tlist = []
            cols  = []

            for col in oobj['cols'][1:]:
                if col['hidden'] is True or col['hidden'] == 'Y':
                    cols.append(None)
                else:
                    tlist.append(col['caption'])
                    cols.append(col)

            fh.write('%s\n' % csvSep.join(tlist))

            for row in oobj['rows']:
                cidx = 0
                tidx = 0

                if row[0]:

                    for ritem in row[1:]:
                        if cidx >= len(cols):
                            break

                        if not cols[cidx]:
                            cidx += 1
                            continue

                        if ritem:
                            tlist[tidx] = TypeUtil.forText(ritem.get('value'), col['prec'], sep=csvSep)
                        else:
                            tlist[tidx] = ''

                        tidx  += 1
                        cidx  += 1

                    while tidx < len(tlist):
                        tlist[tidx]  = ''
                        tidx        += 1

                else:
                    if exclRows:
                        oidx += 1
                        continue

                    for ritem in row[1:]:
                        if cols[cidx]:
                            tlist[tidx] = TypeUtil.forText(ritem, col['prec'], sep=csvSep)
                            tidx += 1

                        cidx += 1

                fh.write('%s\n' % csvSep.join(tlist))

            oidx += 1

        fh = None

        self._pod.log.debug('OutTypeCSV._process - done [%s]' % (outFilename))
