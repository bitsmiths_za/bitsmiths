# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os
import os.path
import subprocess

from bs_crest.Config  import Config

from bs_crest.outtype.OutType import OutType


class OutTypeFOP(OutType):
    """
    The base class for all fop generation.  The pdf, png, rtf types will
    inherit this object but just oveload the final cli args to fop.
    """
    def __init__(self, pod, dao):
        """
        Constructor

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) The dao to use.
        """
        OutType.__init__(self, pod, dao)

        self._templPath  = ''
        self._configPath = ''
        self._xslPath    = ''


    def _fopFileExt(self):
        """
        Abstract method to get the fop file extension cli param.

        :return: The file extension cli.
        """
        raise Exception('Abstract method OutTypeFop._fopFileExt not implemented')


    def initialize(self, genRec, siteCode):
        """
        Initialize the out type object.

        :param genRec: (tGen) The generation record.
        :param siteCode: (string) The site code.
        """
        OutType.initialize(self, genRec, siteCode)

        conf    = Config(self._cdao)
        cfg     = conf.load([
            'fop.template-path',
            'fop.%s' % (self.outExt()),
            'fop.config'])

        self._templPath = cfg['fop.template-path']

        if not os.path.exists(self._templPath) or not os.path.isdir(self._templPath):
            raise Exception('FOP template path [%s] not found/valid' % self._templPath)

        custConfig = 'fop.config.%s' % (self._siteCode)
        custXsl    = 'fop.%s.%s' % (self.outExt(), self._siteCode)
        cfg        = conf.load([custConfig, custXsl], ignoreMissing=True)

        if cfg.get(custConfig):
            self._configPath = os.path.join(self._templPath, cfg.get(custConfig))
        elif cfg['fop.config'] != '':
            self._configPath = os.path.join(self._templPath, cfg['fop.config'])

        if cfg.get(custXsl):
            self._xslPath = os.path.join(self._templPath, cfg.get(custXsl))
        else:
            self._xslPath = os.path.join(self._templPath, cfg['fop.%s' % (self.outExt())])

        if self._configPath != '' and (not os.path.exists(self._configPath) or not os.path.isfile(self._configPath)):
            raise Exception('FOP config file [%s] not found/valid' % self._configPath)

        if not os.path.exists(self._xslPath) or not os.path.isfile(self._xslPath):
            raise Exception('FOP template xsl file [%s] not found/valid' % self._xslPath)


    def destroy(self):
        """
        Destroy the object and free up any internal objects/handles.
        """
        OutType.destroy(self)


    def _fopCLI(self, outFilename, depFilename):
        """
        Virtual method to build up the fop command line, the default should
        work for most scenarious.

        :param outFilename: (string) The destination output file.
        :param depFilename: (string) The dependancy file.
        """
        cmd = ['fop']

        if self._configPath != '':
            cmd.append('-c')
            cmd.append(self._configPath)

        cmd.append('-xml')
        cmd.append(depFilename)
        cmd.append('-xsl')
        cmd.append(self._xslPath)
        cmd.append('-%s' % self._fopFileExt())
        cmd.append(outFilename)

        return cmd


    def _process(self, outFilename, depFilename):
        """
        Abstract method to generate the output to the target out filename.

        :param outFilename: (string) The destination output file.
        :param depFilename: (string) The dependancy file.
        """
        self._pod.log.debug('OutTypeFOP._process - start [%s, depFilename:%s]' % (
            outFilename, str(depFilename)))

        cmd = self._fopCLI(outFilename, depFilename)

        self._pod.log.debug(' - FOP CLI: %s' % ' '.join(cmd))

        proc = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

        if proc.returncode != 0:
            raise Exception('Failed to generate: [rc:%d, cmd:%s, stderr:%s]' % (
                proc.returncode, ' '.join(cmd), str(proc.stderr)))

        if not os.path.exists(outFilename):
            raise Exception('Fop succeeded, but output file not found [outFile:%s, cmd:%s]' % (outFilename, ' '.join(cmd)))

        self._pod.log.debug('OutTypeFOP._process - done [%s]' % (outFilename))
