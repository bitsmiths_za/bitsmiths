# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os
import os.path

from xml.sax.saxutils import escape as xmlEscape

from bs_crest  import TypeUtil

from bs_crest.outtype.OutType import OutType

from bs_crest.db.tables.tOutType import tOutType


class OutTypeXML(OutType):
    """
    Generate a standard xml output file.
    """
    SEP = ','

    def __init__(self, pod, dao):
        """
        Constructor

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) The dao to use.
        """
        OutType.__init__(self, pod, dao)
        self._fh = None


    def outType(self):
        """
        Abstract method, the output type
        """
        return tOutType.Id_Couplet.keyXml


    def name(self):
        """
        Abstract method, the name of this output generator.
        """
        return 'XML (Standard)'


    def initialize(self, genRec, siteCode):
        """
        Initialize the out type object.

        :param genRec: (tGen) The generation record.
        :param siteCode: (string) The site code.
        """
        OutType.initialize(self, genRec, siteCode)
        self._readEnqReqRecs()
        self._fh = None


    def destroy(self):
        """
        Destroy the object and free up any internal objects/handles.
        """
        if self._fh:
            self._fh.close()
            self._fh = None

        OutType.destroy(self)


    def _process(self, outFilename, depFilename):
        """
        Abstract method to generate the output to the target out filename.

        :param outFilename: (string) The destination output file.
        :param depFilename: (string) The dependancy file.
        """
        self._pod.log.debug('OutTypeXML._process - start [%s]' % (outFilename))

        self._fh = open(outFilename, 'wt', 1024 * 16)
        fh       = self._fh
        oidx     = 0

        fh.write('<crest><env><home>%s</home><user>%s</user></env>' % (
            str(os.environ.get('LOCAL_PATH', os.environ.get('HOME'))), str(os.environ.get('USER'))))

        fh.write('<output genId="%d" caption="%s" reqBy="%s" reqDate="%s" cnt="%d" reportId="%s"' % (
            self._genId,
            xmlEscape(self._srcObj['caption']),
            xmlEscape(self._srcObj['output'][0]['reqBy']),
            xmlEscape(self._srcObj['output'][0]['reqTmStamp']),
            len(self._srcObj['output']),
            xmlEscape(self._srcObj['reportId'])
        ))

        fmt = self._getPageFormatOptions()

        if fmt:
            for fkey, fval in fmt.items():
                if not fkey.startswith('fop.'):
                    continue

                fh.write(' %s="%s"' % (fkey[4:], xmlEscape(fval)))

        fh.write('>')

        for oobj in self._srcObj['output']:
            fh.write('<item name="%s" enqId="%s" totrows="%s" outIdx="%d">' % (
                xmlEscape(oobj['caption']),
                xmlEscape(oobj['enqId']),
                str(oobj['totRows']),
                oidx))

            cols    = []
            cidx    = 0
            viscols = 0

            for col in oobj['cols'][1:]:
                if col['hidden'] is True or col['hidden'] == 'Y':
                    cols.append(None)
                else:
                    cols.append(col)
                    viscols += 1

            fh.write('<columns cnt="%d">' % viscols)

            cidx = 0

            for col in cols:
                if not col:
                    continue

                fh.write('<column idx="%d" type="%s" id="%s"' % (cidx, xmlEscape(col['type']), xmlEscape(col['id'])))

                fmt = self._getColumnFormatOptions(oobj['enqId'], col['id'])

                if fmt:
                    for fkey, fval in fmt.items():
                        fh.write(' %s="%s"' % (fkey, xmlEscape(fval)))

                fh.write('>%s</column>' % (xmlEscape(col['caption'])))
                cidx += 1

            fh.write('</columns>')
            fh.write('<rows cnt="%d">' % len(oobj['rows']))

            for row in oobj['rows']:
                cidx = 0
                tidx = 0

                if row[0]:
                    fh.write('<row type="%s" tag="%s" cnt="%d">' % (row[0]['type'], row[0]['tag'], row[0]['cnt']))

                    span = 1

                    for ritem in row[1:]:
                        span -= 1

                        if cidx >= len(cols) or tidx >= viscols:
                            break

                        if not cols[cidx]:
                            cidx += 1
                            span += 1
                            continue

                        if span > 0:
                            cidx += 1
                            tidx += 1
                            continue

                        if ritem:
                            span = ritem.get('span')

                            if not span:
                                span = 1

                            fh.write('<col idx="%d"' % (tidx))

                            if span > 1:
                                fh.write(' span="%d"' % (span))

                            fh.write(">%s</col>" % xmlEscape(TypeUtil.forText(ritem.get('value'), cols[cidx]['prec'])))
                        else:
                            fh.write('<col idx="%d"/>' % (tidx))

                        tidx  += 1
                        cidx  += 1

                    while tidx < viscols:
                        fh.write('<col idx="%d"/>' % (tidx))
                        tidx += 1

                else:
                    fh.write('<row type="row">')

                    for ritem in row[1:]:
                        if cols[cidx]:
                            fh.write('<col idx="%d">%s</col>' % (tidx, xmlEscape(TypeUtil.forText(ritem, cols[cidx]['prec']))))
                            tidx += 1

                        cidx += 1

                fh.write('</row>')

            fh.write('</rows>')

            fh.write('</item>')
            oidx += 1

        fh.write('</output></crest>')
        fh = None

        self._pod.log.debug('OutTypeXML._process - done [%s]' % (outFilename))
