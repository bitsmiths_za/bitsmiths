#
# Copyright (C) 2017 Bitsmiths (Pty) Ltd et al.  All rights reserved.
#
import json


class Transmit:
    """
    Base transmit class.  All transmit objects to inherit
    from this base to send out files.
    """

    def __init__(self, pod, dao):
        """
        Constructor

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) The dao to use.
        """
        self._pod      = pod
        self._cdao     = dao

        self._genRec      = None
        self._cfgRec      = None
        self._genFilePath = ''
        self._ttRec       = None
        self._tmitTargs   = []


    def tmitTypeRec(self):
        """
        Gets the transmit type record.

        @return tTmitType : The record.
        """
        if not self._ttRec:
            dtt = self._cdao.dao.dTmitType(self._cdao.dbcon)

            dtt.selectOneDeft(self.transmitTypeId())

            self._ttRec = dtt.rec

        return self._ttRec


    def transmitTypeId(self):
        """
        Abstract method, the transmit type
        """
        raise Exception('Abstract method Transmit.transmitTypeId not implemented')


    def initialize(self, genRec, cfgRec, tmitGroupId):
        """
        Initialize the out type object.

        :param filePath: (string) The full file path to send.
        :param genRec: (tGen) The gen record.
        :param cfgRec: (tTmitCfg) The transmit config.
        :param tmitGroupId: (string) The transmission group.
        """
        self._genRec      = genRec
        self._cfgRec      = cfgRec

        tgi = self._cdao.dao.dTmitGroupItem(self._cdao.dbcon)

        tgi.selectOneDeft(tmitGroupId, genRec.siteId, cfgRec.tmitTypeId)

        if len(tgi.rec.destAddr) == 0:
            raise Exception('No destination addresses specified [genId:%d, tmitGroupId:%s, siteId:%d, tmitTypeId:%s]' % (
                genRec.id, tmitGroupId, genRec.siteId, cfgRec.tmitTypeId))

        self._tmitTargs = json.loads(tgi.rec.destAddr)

        if type(self._tmitTargs) == str:
            self._tmitTargs = [self._tmitTargs]
        elif type(self._tmitTargs) != list:
            raise Exception('Destination addresses is not valid [genId:%d, tmitGroupId:%s, siteId:%d, tmitTypeId:%s]' % (
                genRec.id, tmitGroupId, genRec.siteId, cfgRec.tmitTypeId))

        if len(self._tmitTargs) == 0:
            raise Exception('Destination addresses is empty [genId:%d, tmitGroupId:%s, siteId:%d, tmitTypeId:%s]' % (
                genRec.id, tmitGroupId, genRec.siteId, cfgRec.tmitTypeId))

        idx = 0

        for addr in self._tmitTargs:
            if not self._validateTargAddr(self._tmitTargs):
                raise Exception('Destination addresses [idx:%d, addr:%s] is not valid [genId:%d, tmitGroupId:%s, \
siteId:%d, tmitTypeId:%s]' % (idx, addr, genRec.id, tmitGroupId, genRec.siteId, cfgRec.tmitTypeId))


    def destroy(self):
        """
        Destroy the object and free up any internal objects/handles.
        """
        pass


    def run(self, filePath, sendName):
        """
        The main entry point to send a file.

        :param filePath: (string) The generated out type file path.
        :param sendName: (string) The name of the file to be sent.
        """
        self._pod.log.debug('Transmit.run - start [genId:%d, filePath:%s, sendName:%s, transmitType:%s]' % (
            self._genRec.id, filePath, sendName, self.transmitTypeId()))

        self._readConfig()

        self._process(filePath, sendName)

        self._pod.log.debug('Transmit.run - done [genId:%d, filePath:%s, sendName:%s, transmitType:%s]' % (
            self._genRec.id, filePath, sendName, self.transmitTypeId()))


    def _readConfig(self):
        """
        Abstract method to read the transmit config.
        """
        raise Exception('Abstract method Transmit._readConfig not implemented')


    def _process(self, filePath, sendName):
        """
        Abstract method to transmits the outfile.

        :param filePath: (string) The generated out type file to transmit.
        :param sendName: (string) The filename of the transmitted file.
        """
        raise Exception('Abstract method Transmit._process not implemented')


    def _validateTargAddr(self, addr):
        """
        Virtual method to validate a target address.

        :param idx: (int) The address index.
        :param addr: (string) The address.
        :return: True if the address is valid else False.
        """
        if not addr:
            return False

        return True
