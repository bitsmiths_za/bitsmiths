#
# Copyright (C) 2017 Bitsmiths (Pty) Ltd et al.  All rights reserved.
#
import json

import email.encoders
import email.mime.base
import email.mime.multipart
import email.mime.text
import email.mime.image
import mimetypes

import smtplib

from bs_lib import Common

from bs_crest.db.tables.tTmitType import tTmitType

from bs_crest.transmit.Transmit import Transmit


class TransmitSMTP(Transmit):
    """
    Standard SMTP email transmitter.
    """
    TIMEOUT = 60   # 1 min

    def __init__(self, pod, dao):
        """
        Constructor

        :param pod: (Pod) The pod to use.
        :param dao: (Dao) The dao to use.
        """
        Transmit.__init__(self, pod, dao)

        self._smtp     = None
        self._settings = {}


    def __del__(self):
        self._logoff()


    def transmitTypeId(self):
        return tTmitType.Id_Couplet.keySmtp


    def destroy(self):
        self._logoff()
        Transmit.destroy(self)


    def _readConfig(self):
        """
        Implemented abstract method to read the transmit config.
        """
        self._settings = json.loads(self._cfgRec.cfg)

        Common.readDict(self._settings, 'host',        str)
        Common.readDict(self._settings, 'port',        int)
        Common.readDict(self._settings, 'username',    str)
        Common.readDict(self._settings, 'passwd',      str)
        Common.readDict(self._settings, 'from',        str)
        Common.readDict(self._settings, 'ssl',         bool)
        Common.readDict(self._settings, 'tls',         bool)


    def _process(self, filePath, sendName):
        """
        Implemented abstract method to transmits the outfile.

        :param filePath: (string) The generated out type file to transmit.
        :param sendName: (string) The filename of the transmitted file.
        """
        self._login()

        self._send(filePath, sendName)

        self._logoff()


    def _logoff(self):
        """
        Log off of the smtp server.
        """
        if self._smtp:
            try:
                self._smtp.quit()
            except smtplib.SMTPServerDisconnected:
                pass
            finally:
                self._smtp = None


    def _login(self):
        """
        Log into the smtp server.
        """
        self._logoff()

        if self._settings['ssl']:
            self._smtp = smtplib.SMTP_SSL(self._settings['host'], self._settings['port'], timeout=self.TIMEOUT)
        else:
            self._smtp = smtplib.SMTP(self._settings['host'], self._settings['port'], timeout=self.TIMEOUT)

        if self._settings['tls']:
            self._smtp.starttls()

        # ensure we can login
        if len(self._settings['username']) < 1:
            raise Exception('Cannot log into smtp server without a user name!')

        self._smtp.login(self._settings['username'], self._settings['passwd'])


    def _send(self, filePath, sendName):
        """
        Send email.
        """
        try:
            emsg = email.mime.multipart.MIMEMultipart('related')

            if self._genRec.caption == '':
                emsg['Subject'] = '%s [%d]' % ('Enquiry' if self._genRec.enquiryId != '' else 'Report', self._genRec.id)
            else:
                emsg['Subject'] = self._genRec.caption

            emsg['From']    = self._settings['from']
            emsg['To']      = ';'.join(self._tmitTargs)
            emsg['Date']    = email.utils.formatdate(localtime=True)
            emsg.preamble   = 'This is a multi-part message in MIME format.'

            msgAlt = email.mime.multipart.MIMEMultipart('alternative')
            emsg.attach(msgAlt)

            msg = 'Automated Report - %s\n\nHave a nice day.' % emsg['Subject']

            msgAlt.attach(email.mime.text.MIMEText(msg))

            ctype, encoding = mimetypes.guess_type(sendName)

            if ctype is None:
                ctype = 'application/octet-stream'

            maintype, subtype = ctype.split('/', 1)

            part = email.mime.base.MIMEBase(maintype, subtype)

            with open(filePath, 'rb') as fh:
                part.set_payload(fh.read())

            email.encoders.encode_base64(part)

            part.add_header('Content-Disposition', 'attachment', filename=sendName)
            emsg.attach(part)

            self._smtp.sendmail(self._settings['from'], self._tmitTargs, emsg.as_string())

        except Exception:
            raise
