--****************************************************************************--
--                          This file is part of:                             --
--                               BITSMITHS                                    --
--                          https://bitsmiths.co.za                           --
--****************************************************************************--
-- Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       --
--  * https://bitbucket.org/bitsmiths_za/bitsmiths                            --
--                                                                            --
-- Permission is hereby granted, free of charge, to any person obtaining a    --
-- copy of this software and associated documentation files (the "Software"), --
-- to deal in the Software without restriction, including without limitation  --
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,   --
-- and/or sell copies of the Software, and to permit persons to whom the      --
-- Software is furnished to do so, subject to the following conditions:       --
--                                                                            --
-- The above copyright notice and this permission notice shall be included in --
-- all copies or substantial portions of the Software.                        --
--                                                                            --
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR --
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   --
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    --
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER --
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    --
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        --
-- DEALINGS IN THE SOFTWARE.                                                  --
--****************************************************************************--

-- CREATE AUDIT TABLES

insert into audit.tcfg select 'crest.Config',      'id',              'audId|tmStamp', null, null where not exists (select id from audit.tcfg where id = 'crest.Config');
insert into audit.tcfg select 'crest.Enquiry',     'id',              'audId|tmStamp', null, null where not exists (select id from audit.tcfg where id = 'crest.Enquiry');
insert into audit.tcfg select 'crest.EnquiryGrp',  'id|enquiryId',    'audId|tmStamp', 'crest.Enquiry', 'enquiryId' where not exists (select id from audit.tcfg where id = 'crest.EnquiryGrp');
insert into audit.tcfg select 'crest.EnquiryIn',   'id|enquiryId',    'audId|tmStamp', 'crest.Enquiry', 'enquiryId' where not exists (select id from audit.tcfg where id = 'crest.EnquiryIn');
insert into audit.tcfg select 'crest.EnquiryOut',  'id|enquiryId',    'audId|tmStamp', 'crest.Enquiry', 'enquiryId' where not exists (select id from audit.tcfg where id = 'crest.EnquiryOut');
insert into audit.tcfg select 'crest.Lookup',      'id',              'audId|tmStamp', null, null where not exists (select id from audit.tcfg where id = 'crest.Lookup');
insert into audit.tcfg select 'crest.LookupItem',  'id|lookupId',     'audId|tmStamp', 'crest.Lookup',  'crest.LookupItem' where not exists (select id from audit.tcfg where id = 'crest.LookupItem');
insert into audit.tcfg select 'crest.Param',       'id',              'audId|tmStamp', null, null where not exists (select id from audit.tcfg where id = 'crest.Param');
insert into audit.tcfg select 'crest.Query',       'id',              'audId|tmStamp', null, null where not exists (select id from audit.tcfg where id = 'crest.Query');
insert into audit.tcfg select 'crest.QueryIn',     'id',              'audId|tmStamp', 'crest.Query', 'crest.queryId' where not exists (select id from audit.tcfg where id = 'crest.QueryIn');
insert into audit.tcfg select 'crest.QueryOut',    'id',              'audId|tmStamp', 'crest.Query', 'crest.queryId' where not exists (select id from audit.tcfg where id = 'crest.QueryOut');


-- CREST ADMIN : SPECIAL ADMIN ROLES

insert into fura.funcgrp select 'CREST-ADMIN', 'Crest Administration', '[crest]', current_timestamp where not exists (select id from fura.funcgrp where id = 'CREST-ADMIN');
insert into fura.funcgrp select 'CREST',       'Crest',                '[crest]', current_timestamp where not exists (select id from fura.funcgrp where id = 'CREST');

insert into fura.func select 'crest.audit-trail.read',    'CREST-ADMIN', 'Read audit data',   'R', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.audit-trail.read');

insert into fura.func select 'crest.cfg-maint.create',    'CREST-ADMIN', 'Create a config',   'C', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.cfg-maint.create');
insert into fura.func select 'crest.cfg-maint.read',      'CREST-ADMIN', 'Read a config',     'R', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.cfg-maint.read');
insert into fura.func select 'crest.cfg-maint.update',    'CREST-ADMIN', 'Update a config',   'U', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.cfg-maint.update');
insert into fura.func select 'crest.cfg-maint.delete',    'CREST-ADMIN', 'Delete a config',   'D', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.cfg-maint.delete');
insert into fura.func select 'crest.cfg-maint.menu',      'CREST-ADMIN', 'Delete a config',   'N', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.cfg-maint.menu');

insert into fura.func select 'crest.query-maint.create',  'CREST-ADMIN', 'Create a query',    'C', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.query-maint.create');
insert into fura.func select 'crest.query-maint.read',    'CREST-ADMIN', 'Read a query',      'R', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.query-maint.read');
insert into fura.func select 'crest.query-maint.update',  'CREST-ADMIN', 'Update a query',    'U', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.query-maint.update');
insert into fura.func select 'crest.query-maint.delete',  'CREST-ADMIN', 'Delete a query',    'D', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.query-maint.delete');
insert into fura.func select 'crest.query-maint.menu',    'CREST-ADMIN', 'Access to queries', 'N', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.query-maint.menu');

insert into fura.func select 'crest.enquiry-maint.create',  'CREST-ADMIN', 'Create an enquiry',    'C', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.enquiry-maint.create');
insert into fura.func select 'crest.enquiry-maint.read',    'CREST-ADMIN', 'Read an enquiry',      'R', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.enquiry-maint.read');
insert into fura.func select 'crest.enquiry-maint.update',  'CREST-ADMIN', 'Update an enquiry',    'U', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.enquiry-maint.update');
insert into fura.func select 'crest.enquiry-maint.delete',  'CREST-ADMIN', 'Delete an enquiry',    'D', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.enquiry-maint.delete');
insert into fura.func select 'crest.enquiry-maint.menu',    'CREST-ADMIN', 'Access to enquries',   'N', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.enquiry-maint.menu');

insert into fura.func select 'crest.lookup-maint.create',  'CREST-ADMIN', 'Create a lookup',    'C', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.lookup-maint.create');
insert into fura.func select 'crest.lookup-maint.read',    'CREST-ADMIN', 'Read a lookup',      'R', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.lookup-maint.read');
insert into fura.func select 'crest.lookup-maint.update',  'CREST-ADMIN', 'Update a lookup',    'U', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.lookup-maint.update');
insert into fura.func select 'crest.lookup-maint.delete',  'CREST-ADMIN', 'Delete a lookup',    'D', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.lookup-maint.delete');
insert into fura.func select 'crest.lookup-maint.menu',    'CREST-ADMIN', 'Access to lookups',  'N', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.lookup-maint.menu');

insert into fura.func select 'crest.param-maint.create',  'CREST-ADMIN', 'Create a parameter',    'C', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.param-maint.create');
insert into fura.func select 'crest.param-maint.read',    'CREST-ADMIN', 'Read a parameter',      'R', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.param-maint.read');
insert into fura.func select 'crest.param-maint.update',  'CREST-ADMIN', 'Update a parameter',    'U', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.param-maint.update');
insert into fura.func select 'crest.param-maint.delete',  'CREST-ADMIN', 'Delete a parameter',    'D', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.param-maint.delete');
insert into fura.func select 'crest.param-maint.menu',    'CREST-ADMIN', 'Access to parameters',  'N', 'Y', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.param-maint.menu');

-- GENERAL CREST ROLES AND FUNCTIONS

insert into fura.func select 'crest.enquiry.view',   'CREST', 'Access to Enquiries',        'N', 'N', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.enquiry.view');
insert into fura.func select 'crest.enquiry.hist',   'CREST', 'Access to Enquiry History',  'N', 'N', 5, '[crest]', current_timestamp where not exists (select id from fura.func where id = 'crest.enquiry.hist');


-- CONFIG VALUES

insert into crest.config select 'dbcon.readuser',  'user=crestro password=dev dbname=ut_crest hostaddr=127.0.0.1',  'BACK-END', '[crest]', current_timestamp;
insert into crest.config select 'dbcon.maxconn',   '5',                                                             'BACK-END', '[crest]', current_timestamp;

insert into crest.config values('server.interval',      '1.0',                      'BACK-END', '[crest]', current_timestamp);
insert into crest.config values('server.maxProcs',      '5',                        'BACK-END', '[crest]', current_timestamp);
insert into crest.config values('output.path',          '$HOME/crest/out',          'BACK-END', '[crest]', current_timestamp);
insert into crest.config values('batch.genBatch',       '$HOME/bin/genbatch.py',    'BACK-END', '[crest]', current_timestamp);
insert into crest.config values('batch.genOutput',      '$HOME/bin/genoutput.py',   'BACK-END', '[crest]', current_timestamp);
insert into crest.config values('batch.genTransmit',    '$HOME/bin/gentransmit.py', 'BACK-END', '[crest]', current_timestamp);

insert into crest.config values('fop.template-path',   '$HOME/crest/fop',     'FOP-TEMPLATES', '[crest]', current_timestamp);
insert into crest.config values('fop.pdf',             'std-pdf.xsl',         'FOP-TEMPLATES', '[crest]', current_timestamp);
insert into crest.config values('fop.config',          'config.xml',          'FOP-TEMPLATES', '[crest]', current_timestamp);


-- OUT TYPES

insert into crest.outtype values('output', 'Reserved for Generation', 'R', 'output', 'xxx', '[crest]', current_timestamp);
insert into crest.outtype values('result', 'Reserved for Generation', 'R', 'result', 'xxx', '[crest]', current_timestamp);

insert into crest.outtype values('html', 'Standard HTML', 'D', 'html', 'html', '[crest]', current_timestamp);
insert into crest.outtype values('pdf',  'Standard PDF',  'A', 'pdf',  'pdf',  '[crest]', current_timestamp);
insert into crest.outtype values('csv',  'Standard CSV',  'A', 'csv',  'csv',  '[crest]', current_timestamp);
insert into crest.outtype values('png',  'Standard PNG',  'D', 'png',  'png',  '[crest]', current_timestamp);
insert into crest.outtype values('json', 'Standard JSON', 'D', 'json', 'json', '[crest]', current_timestamp);
insert into crest.outtype values('xml',  'Standard XML',  'A', 'xml',  'xml',  '[crest]', current_timestamp);

-- Transmit Types

insert into crest.tmittype values ('smtp', 'SMTP Email Server', '{"host":"string", "username":"string", "ssl":"boolean", "tls":"boolean", "port":"number", "passwd": "string", "from":"string"}', '[crest]', current_timestamp);
