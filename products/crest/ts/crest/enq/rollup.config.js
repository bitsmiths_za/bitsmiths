export default {
  name: 'bs-crest.enq',
  input: 'dist/index.js',
  sourcemap: true,
  output: {
    file: 'dist/bundles/enq.umd.js',
    format: 'umd'
  },
  globals: {
    '@angular/core': 'ng.core',
    '@angular/common': 'ng.common',
    '@angular/forms': 'ng.forms',
    '@angular/router': 'ng.router',
    '@angular/material': 'ng.material',
    '@covalent/core': 'covalent.core',
    '@mettle/lib': 'mettle.lib',
    '@mettle/io': 'mettle.io',
    '@mettle/braze': 'mettle.braze',
    '@mettle/db': 'mettle.db',
    '@bs-fura/lib': 'bs-fura.lib',
    '@bs-fura/std': 'bs-fura.std'
  },
  external: [
    '@angular/core',
    '@angular/common',
    '@angular/forms',
    '@angular/router',
    '@angular/material',
    '@covalent/core',
    '@mettle/lib',
    '@mettle/io',
    '@mettle/braze',
    '@mettle/db',
    '@bs-fura/lib',
    '@bs-fura/std'
  ]
}
