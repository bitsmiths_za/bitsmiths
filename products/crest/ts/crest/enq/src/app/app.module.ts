/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { NgModule }                from '@angular/core';
import { Router, RouterModule }    from '@angular/router';
import { HttpModule }              from '@angular/http';
import { BrowserModule, Title }    from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  FuraBrazeService,
  FuraAuthService,
}  from '@bs-fura/lib';

import { AppComponent } from './app.component';

import { SharedModule } from './shared/shared.module';

import { routedComponents, AppRoutingModule } from './app-routing.module';

import { environment } from '../environments/environment';


//import { CrestBrazeService } from '@bs-crest/lib';

export let furaBrazeServiceFactory = (router: Router): FuraBrazeService => {
  return new FuraBrazeService(router, environment.fura_url, environment.fura_cors, environment.fura_cred, environment.fura_login_route);
};

/*export let crestBrazeServiceFactory = (router: Router) => {
  return new CrestBrazeService(router, environment.fura_url, environment.fura_cors, environment.fura_cred});
};*/

@NgModule({
  declarations: [
    AppComponent,
    routedComponents
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    RouterModule,
    SharedModule,
  ],
  providers: [
    FuraAuthService,
    {
      provide    : FuraBrazeService,
      useFactory : furaBrazeServiceFactory,
      deps       : [Router]
    },
    /*{
      provide   : CrestBrazeService,
      useFactor : crestBrazeServiceFactory,
      deps      : [Router]
    }*/
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
