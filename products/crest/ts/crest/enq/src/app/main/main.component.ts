/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Component, OnInit, OnDestroy, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, Subscription } from 'rxjs';

import { TdDialogService, TdLoadingService, TdMediaService } from '@covalent/core';

import { FuraAuthService } from '@bs-fura/lib';

@Component({
  selector    :  'app-main',
  templateUrl :  './main.component.html',
  styleUrls   : ['./main.component.scss'],
})
export class MainComponent implements OnInit, OnDestroy, AfterViewInit {

  dispName  : string = "";
  dispEmail : string = "";

  authChangeSub : Subscription | null = null;

  constructor(
    private _loadingService    : TdLoadingService,
    private _dialogService     : TdDialogService,
    private _changeDetectorRef : ChangeDetectorRef,
    public  router             : Router,
    public  media              : TdMediaService,
    public  authService        : FuraAuthService
    )
  {
  }

  ngOnInit(): void {
    this.authChangeSub = this.authService.authChange$.subscribe( (isAuthenticated: boolean) => {
      this._buildMenu();
    });
  }

  ngOnDestroy(): void {
    if (this.authChangeSub != null)
      this.authChangeSub.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.media.broadcast();
    this._changeDetectorRef.detectChanges();
  }

  logout(): void {
    this._loadingService.register('logoutLoading');

    this.authService.logout()
      .then( () => {
        this._loadingService.resolve('logoutLoading');
        this.router.navigate(['/auth/login']);
      })
      .catch( (msg: string) => {
        this._loadingService.resolve('logoutLoading');
        this._dialogService.openAlert({message: 'Error logging out: ' + msg});
      });
  }

  hasAccess(func: string): boolean {
    if (func == '')
      return true;

    return this.authService.hasAccess([func]);
  }

  _buildMenu()
  {
  }
}
