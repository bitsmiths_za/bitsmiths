export const environment: {
  production       : boolean,
  fura_url         : string,
  fura_cors        : RequestMode,
  fura_cred        : RequestCredentials
  fura_login_route : string,
} = {
  production: true,
  fura_url         : '/braze',
  fura_cors        : 'no-cors',
  fura_cred        : 'include',
  fura_login_route : '/auth/login',
};
