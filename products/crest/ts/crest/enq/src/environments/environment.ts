// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment: {
  production       : boolean,
  fura_url         : string,
  fura_cors        : RequestMode,
  fura_cred        : RequestCredentials,
  fura_login_route : string,
} = {
  production: false,
  fura_url         : 'http://localhost:8769/braze',
  fura_cors        : 'cors',
  fura_cred        : 'include',
  fura_login_route : '/auth/login',
};
