export default {
  name: 'bs-crest.lib',
  input: 'dist/index.js',
  sourcemap: true,
  output: {
    file: 'dist/bundles/lib.umd.js',
    format: 'umd'
  },
  globals: {
    '@angular/core': 'ng.core',
    '@angular/router': 'ng.router',
    '@mettle/lib': 'mettle.lib',
    '@mettle/io': 'mettle.io',
    '@mettle/braze': 'mettle.braze',
    '@mettle/db': 'mettle.db'
  },
  external: [
    '@angular/core',
    '@angular/router',
    '@mettle/lib',
    '@mettle/io',
    '@mettle/braze',
    '@mettle/db'
  ]
}
