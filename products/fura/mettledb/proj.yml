# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #
file-type : Mettle.DB
version   : 2.1
name      : BS.Fura

table-list:
  - ada.yml
  - aga.yml
  - config.yml
  - eatok.yml
  - func.yml
  - funcgrp.yml
  - role.yml
  - rolefuncrel.yml
  - site.yml
  - sitecfg.yml
  - token.yml
  - tokenhist.yml
  - tokensentry.yml
  - ula.yml
  - usr.yml
  - usrauth.yml
  - usrotp.yml
  - usrtype.yml

language-generators:
  - target    : Python3
    enabled   : True
    dest-path : ../python3/bs_fura/db
    options   :
      class.dao       : 'd|'
      class.qryout    : 'o|'
      class.qryin     : 'i|'
      class.table     : 't|'
      class.couplet   : '|_Couplet'
      file.dao        :
      file.qryout     : 'o|'
      file.qryin      : 'i|'
      file.table      :
      namespace       : bs_fura.db
      db.schema       : fura
      data.model      : dataclass
      gen.clear       : True
      gen.serializers : True
      gen.pk_structs  : True
      gen.pk_compare  : True
      gen.davs        : True
      async           :
        enabled   : True
        dest-path :
        ns.dao    : '|_async'
      standard        :
        enabled   : True
        ns.dao    :
      casing          :
        class  : pascal
        method : snake
        member : snake
        file   : snake

  - target    : TypeScript
    enabled   : True
    dest-path : ../ts/fura/projects/bs-fura-lib/src/db
    options   :
      class.dao     : 'd|'
      class.qryout  : 'o|'
      class.qryin   : 'i|'
      class.table   : 't|'
      class.couplet : '|_Couplet'
      file.dao      :
      file.qryout   : 'o|'
      file.qryin    : 'i|'
      file.table    :
      namespace     : bs_fura.db
      db.schema     : fura
      casing        :
        class  : pascal
        method : camel
        member : camel
        file   : snake

  - target    : Sql
    enabled   : True
    dest-path : sqldef
    options   :
      db.schema   : fura
      sp.path     : stored-procs

database-generators:
  - target   : postgresql
    enabled  : True
    dao-mode : inline
    generate :
      Python3 : { enabled: True }
      Sql     : { enabled: True }
