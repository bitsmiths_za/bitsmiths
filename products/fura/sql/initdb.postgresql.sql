--****************************************************************************--
--                          This file is part of:                             --
--                               BITSMITHS                                    --
--                          https://bitsmiths.co.za                           --
--****************************************************************************--
-- Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       --
--  * https://bitbucket.org/bitsmiths_za/bitsmiths                            --
--                                                                            --
-- Permission is hereby granted, free of charge, to any person obtaining a    --
-- copy of this software and associated documentation files (the "Software"), --
-- to deal in the Software without restriction, including without limitation  --
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,   --
-- and/or sell copies of the Software, and to permit persons to whom the      --
-- Software is furnished to do so, subject to the following conditions:       --
--                                                                            --
-- The above copyright notice and this permission notice shall be included in --
-- all copies or substantial portions of the Software.                        --
--                                                                            --
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR --
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   --
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    --
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER --
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    --
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        --
-- DEALINGS IN THE SOFTWARE.                                                  --
--****************************************************************************--

-- CREATE AUDIT TABLES

insert into audit.cfg select 'fura.config',        'id',                       'modified_by|tm_stamp', null, null, null where not exists (select id from audit.cfg where id = 'fura.config');
insert into audit.cfg select 'fura.func',          'id',                       'modified_by|tm_stamp', null, null, null where not exists (select id from audit.cfg where id = 'fura.func');
insert into audit.cfg select 'fura.site',          'id',                       'modified_by|tm_stamp', null, null, null where not exists (select id from audit.cfg where id = 'fura.site');
insert into audit.cfg select 'fura.sitecfg',       'site_id|id',               'modified_by|tm_stamp', null, null, null where not exists (select id from audit.cfg where id = 'fura.sitecfg');
insert into audit.cfg select 'fura.role',          'site_id|id',               'modified_by|tm_stamp', null, null, null where not exists (select id from audit.cfg where id = 'fura.role');
insert into audit.cfg select 'fura.rolefuncrel',   'site_id|role_id|func_id',  'modified_by|tm_stamp', null, null, null where not exists (select id from audit.cfg where id = 'fura.rolefuncrel');
insert into audit.cfg select 'fura.usr',           'site_id|id',               'modified_by|tm_stamp', null, null, null where not exists (select id from audit.cfg where id = 'fura.usr');
insert into audit.cfg select 'fura.usrtype',       'id',                       'modified_by|tm_stamp', null, null, null where not exists (select id from audit.cfg where id = 'fura.usrtype');


-- FURA ADMIN : SPECIAL ADMIN ROLES

insert into fura.funcgrp values('FURA-ADMIN', 'Fura Administration', '[fura]', current_timestamp);
insert into fura.funcgrp values('FURA',       'Fura',                '[fura]', current_timestamp);

insert into fura.func values('fura.cfg.create',    'FURA-ADMIN', 'Create a config',   'C', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.cfg.read',      'FURA-ADMIN', 'Read a config',     'R', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.cfg.update',    'FURA-ADMIN', 'Update a config',   'U', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.cfg.delete',    'FURA-ADMIN', 'Delete a config',   'D', 'Y', 5, '[fura]', current_timestamp);

insert into fura.func values('fura.site.create',   'FURA-ADMIN', 'Create a site',   'C', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.site.read',     'FURA-ADMIN', 'Read a site',     'R', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.site.update',   'FURA-ADMIN', 'Update a site',   'U', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.site.delete',   'FURA-ADMIN', 'Delete a site',   'D', 'Y', 5, '[fura]', current_timestamp);

insert into fura.func values('fura.usrtype.create',   'FURA-ADMIN', 'Create a user type',   'C', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usrtype.read',     'FURA-ADMIN', 'Read a user type',     'R', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usrtype.update',   'FURA-ADMIN', 'Update a user type',   'U', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usrtype.delete',   'FURA-ADMIN', 'Delete a user type',   'D', 'Y', 5, '[fura]', current_timestamp);

insert into fura.site values(1, 'FURA', 'Fura Administration', 'A', 'ZA', 'ZAR', '27', '[fura]', current_timestamp);

select setval('fura.site_id_seq', 1);

insert into fura.config values ('audit.denied.level',  'Audit Denied Level',              '100', '[fura]', current_timestamp);
insert into fura.config values ('audit.denied.age',    'Audit Denied Max Age (days)',     '90',  '[fura]', current_timestamp);
insert into fura.config values ('audit.granted.level', 'Audit Granted Level',             '100', '[fura]', current_timestamp);
insert into fura.config values ('audit.granted.age',   'Audit Granted Max Age (days)',    '90',  '[fura]', current_timestamp);
insert into fura.config values ('audit.usrlogin.age',  'Audit User Login Max Age (days)', '90',  '[fura]', current_timestamp);

insert into fura.config values ('paths.salt',        'Salt directory', 'env/fura/salt',  '[fura]', current_timestamp);
insert into fura.config values ('token.webkey',      'Web token key',  '{"k":"ADkGzwL3kKBljqhH-7OR7ZQeSLMx5rmihwuPVWfgjeM","kty":"oct"}', '[fura]', current_timestamp);
insert into fura.config values ('otp.sms.length',    'Length of SMS One Time Pins.', '6', '[fura]', current_timestamp);
insert into fura.config values ('otp.sms.content',   'Content of SMS OTP, [alpha, numeric, or alphanumeric]', 'numeric', '[fura]', current_timestamp);
insert into fura.config values ('otp.sms.timeout',   'Time-out for SMS OTPs in minutes',   '5',  '[fura]', current_timestamp);
insert into fura.config values ('otp.email.length',  'Length of e-mail OTP.', '6', '[fura]', current_timestamp);
insert into fura.config values ('otp.email.content', 'Content of e-mail OTP, [alpha, numeric, or alphanumeric]', 'alphanumeric', '[fura]', current_timestamp);
insert into fura.config values ('otp.email.timeout', 'Time-out for auth reset emails in minutes', '10', '[fura]', current_timestamp);
insert into fura.config values ('loco.trigger.type', 'The Loco trigger interface to use.', 'local_db', '[fura]', current_timestamp);
insert into fura.config values ('loco.trigger.cfg',  'The Loco trigger config.', '{"dao" : "postgresql"}', '[fura]', current_timestamp);

insert into fura.config values ('authreset.urlmask.email', 'The URL mask to email the user.  Use $site, $usr, $tok, and $authtype for replacement.', 'http://fura_example.co.za/resetUserAuth?site=$site&usr=$usr&otp=$otp&authtype=$authtype&otpmethod=$otpmethod', '[fura]', current_timestamp);
insert into fura.config values ('authreset.urlmask.sms',   'The URL mask to sms the user.  Use $site, $usr, $otp, and $authtype for replacement.',   'http://fura_example.co.za/resetUserAuth?site=$site&usr=$usr&otp=$otp&authtype=$authtype&otpmethod=$otpmethod', '[fura]', current_timestamp);
insert into fura.config values ('authreset.support-email', 'The support email address to use for auth resets.', 'support@bitsmiths.co.za', '[FURA]', current_timestamp);

insert into fura.config values ('loco.authreset',          'Reset account loco type to use.',        'fura.reset_passwd',  '[fura]', current_timestamp);
insert into fura.config values ('loco.usrreg',             'New registration loco type to use.',     'fura.registration',  '[fura]', current_timestamp);
insert into fura.config values ('loco.login_attempt',      'Failed login attempt loco type to use.', 'fura.login_attempt', '[fura]', current_timestamp);
insert into fura.config values ('loco.confirm_addr',       'Confirm address loco type to use.',      'fura.confirm_addr',  '[fura]', current_timestamp);

insert into fura.config values ('auth-policy.P.descr', 'Auth password policy description.', 'Minimum ten characters, at least one uppercase letter, one lowercase letter, one number and one special character.', '[fura]', current_timestamp);
insert into fura.config values ('auth-policy.P.regex', 'Auth passowrd policy regex.',       '^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{10,}$', '[fura]', current_timestamp);

insert into fura.sitecfg values (1, 'audit.denied.level',  'Audit Denied Level',              '100', '[fura]', current_timestamp);
insert into fura.sitecfg values (1, 'audit.denied.age',    'Audit Denied Max Age (days)',     '90',  '[fura]', current_timestamp);
insert into fura.sitecfg values (1, 'audit.granted.level', 'Audit Granted Level',             '100', '[fura]', current_timestamp);
insert into fura.sitecfg values (1, 'audit.granted.age',   'Audit Granted Max Age (days)',    '90',  '[fura]', current_timestamp);
insert into fura.sitecfg values (1, 'audit.usrlogin.age',  'Audit User Login Max Age (days)', '90',  '[fura]', current_timestamp);

insert into fura.role values (1, 'admin', 'Fura super user admin', 'S', 1, 60, '[fura]', current_timestamp);

insert into fura.usrtype values ('sys',    'System User', '[fura]', current_timestamp);
insert into fura.usrtype values ('client', 'Client User', '[fura]', current_timestamp);

insert into fura.usr values (
  1, 'admin', 'admin', 'A', 'Admin', 'Admin', null, current_date, null,
  'nicolas@bitsmiths.co.za', 'steven@bitsmiths.co.za', null, null, '[fura]', current_timestamp, 'sys',
  true, true, false, false, true, true, true);


-- GENERAL FURA ROLES AND FUNCTIONS

insert into fura.func values('fura.role.create',   'FURA', 'Create a role',        'C', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.role.read',     'FURA', 'Read a role',          'R', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.role.update',   'FURA', 'Update a role',        'U', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.role.delete',   'FURA', 'Delete a role',        'D', 'Y', 5, '[fura]', current_timestamp);

insert into fura.func values('fura.rfr.add',       'FURA', 'Assign function to role',   'C', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.rfr.read',      'FURA', 'Read a role function',      'R', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.rfr.rem',       'FURA', 'Remove function from role', 'D', 'Y', 5, '[fura]', current_timestamp);

insert into fura.func values('fura.func.read',     'FURA', 'Read a function',        'R', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.funcgrp.read',  'FURA', 'Read a function group',  'R', 'Y', 5, '[fura]', current_timestamp);

insert into fura.func values('fura.service.create',  'FURA', 'Create a service matrix',    'C', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.service.read',    'FURA', 'Read a service matrix',      'R', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.service.update',  'FURA', 'Update a service matrix',    'U', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.service.delete',  'FURA', 'Delete a service matrix',    'D', 'Y', 5, '[fura]', current_timestamp);


insert into fura.func values('fura.usr.create',      'FURA', 'Create a user',     'C', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.read',        'FURA', 'Read a user',       'R', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.update',      'FURA', 'Update a user',     'U', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.delete',      'FURA', 'Delete a user',     'D', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.activate',    'FURA', 'Activate a user',   'A', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.susp',        'FURA', 'Suspend a user',    'A', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.disable',     'FURA', 'Disable a user',    'A', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.reset-auth',  'FURA', 'Request a reset link (email) for a user.', 'O', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.reset-token', 'FURA', 'Request a token auth is reset (regenerated).', 'O', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.read-token',  'FURA', 'Read an auth token.', 'O', 'Y', 5, '[fura]', current_timestamp);
insert into fura.func values('fura.usr.setrole',     'FURA', 'Set a users role.', 'U', 'Y', 5, '[fura]', current_timestamp);


-- INSERT STANDARD LOCO TEMPLATES

insert into loco.notype  select 'fura.reset_passwd',       'A', 'Fura reset password',             'Fura', 10, null,       1,  5, '[fura]', current_timestamp where not exists (select id from loco.notype where id = 'fura.reset_passwd');
insert into loco.notype  select 'fura.registration',       'A', 'Fura new registration',           'Fura', 10, null,       1, 60, '[fura]', current_timestamp where not exists (select id from loco.notype where id = 'fura.registration');
insert into loco.notype  select 'fura.confirm_addr',       'A', 'Fura confirm email/sms address',  'Fura', 10, null,       1, 60, '[fura]', current_timestamp where not exists (select id from loco.notype where id = 'fura.confirm_addr');
insert into loco.notype  select 'fura.login_attempt',      'A', 'Fura failed login attempt',       'Fura', 10, '["sms"]',  1, 60, '[fura]', current_timestamp where not exists (select id from loco.notype where id = 'fura.login_attempt');

insert into loco.notempl select 'fura.reset_passwd',   'sms',   'A', null, '{}', null, '[fura]', current_timestamp where not exists (select notype_id from loco.notempl where corrtype_id = 'sms'   and notype_id = 'fura.reset_passwd');
insert into loco.notempl select 'fura.reset_passwd',   'email', 'A', null, '{}', null, '[fura]', current_timestamp where not exists (select notype_id from loco.notempl where corrtype_id = 'email' and notype_id = 'fura.reset_passwd');
insert into loco.notempl select 'fura.confirm_addr',   'sms',   'A', null, '{}', null, '[fura]', current_timestamp where not exists (select notype_id from loco.notempl where corrtype_id = 'sms'   and notype_id = 'fura.confirm_addr');
insert into loco.notempl select 'fura.confirm_addr',   'email', 'A', null, '{}', null, '[fura]', current_timestamp where not exists (select notype_id from loco.notempl where corrtype_id = 'email' and notype_id = 'fura.confirm_addr');
insert into loco.notempl select 'fura.login_attempt',  'sms',   'A', null, '{}', null, '[fura]', current_timestamp where not exists (select notype_id from loco.notempl where corrtype_id = 'sms'   and notype_id = 'fura.login_attempt');
insert into loco.notempl select 'fura.login_attempt',  'email', 'A', null, '{}', null, '[fura]', current_timestamp where not exists (select notype_id from loco.notempl where corrtype_id = 'email' and notype_id = 'fura.login_attempt');
insert into loco.notempl select 'fura.registration',   'sms',   'A', null, '{}', null, '[fura]', current_timestamp where not exists (select notype_id from loco.notempl where corrtype_id = 'sms'   and notype_id = 'fura.registration');
insert into loco.notempl select 'fura.registration',   'email', 'A', null, '{}', null, '[fura]', current_timestamp where not exists (select notype_id from loco.notempl where corrtype_id = 'email' and notype_id = 'fura.registration');

update loco.notempl set templ = '{}', modified_by = '[orv]', tm_stamp = current_timestamp where notype_id like 'fura.%';

-- RESET PASSWORD

update loco.notempl
set templ = '{"msg": "You are about to reset your ${site_descr}, OTP: ${otp}"}'
where corrtype_id = 'sms' and notype_id = 'fura.reset_passwd';

update loco.notempl
set templ = '{"subject": "${site_descr} Password Reset", "body-text": "${site_descr}\n\nReset Password OTP: ${otp}\n\nDidn`t request a new password? If you need any help don`t hesitate to contact us at: ${support_email}", "body-html": "<!DOCTYPE html><html><head><title>Password Reset</title><style type=''text/css''> body { font-size: 0.87em; color: #ffffff; text-align: center; height: 100%;} h1  {font-size: 2.1em;} a {color: inherit;} img { display:block; width:100%; margin:-8% 0;}.img-row { overflow:hidden; margin:0;}.wrapper-div { margin: auto; max-width:600px;background-color: #373535ff;}.body-text {padding-top: 7%;}.body-text, .new-password-div {font-size: 1.2em;}.new-password-div { margin: auto; background-color: #90c84bff; padding: 2%; max-width: 35%; margin-top: 5%; font-weight: bold; color: #000000;}.footer-div { padding-top: 0.2%; padding-bottom: 2%; max-height: 7.857em; margin-top: 10%; background-color: #373535ff;}.footer-div, .body-text { padding-left: 10%; padding-right: 10%;}</style></head><body><div class=''wrapper-div''><div class=''row''><div class=''body-text''><h1>${site_descr}</h1><p><h3>Reset Password OTP : </h3><h2>${otp}</h2></p><p>Or to reset to your account in one easy step click the button below.</p></div><a style=''text-decoration:none'' href=''${httplink}''><div class=''new-password-div''>Reset My Password</div></a></div><div class=''row''><div class=''footer-div''><p>Didn`t request a new password?  If you need any help don`t hesitate to <a href=''mailto:${support_email}?subject=Mail from Our Site''>contact us.</a></p></div></div></div></body></html>" }'
where corrtype_id = 'email' and notype_id = 'fura.reset_passwd';

-- FAILED LOGIN ATTEMPT

update loco.notempl
set templ = '{"msg": "Failed login attempt for ${site_descr} detected on ${datetime}, remaining attempts: ${remaining}"}'
where corrtype_id = 'sms' and notype_id = 'fura.login_attempt';

update loco.notempl
set templ = '{"subject": "Failed Login Attemp", "body-text": "${site_descr}\n\nFailed login attempt detected on ${datetime}, remaining attempts: ${remaining}\n\nDidn`t try an login? If you need any help don`t hesitate to contact us at: ${support_email}", "body-html": "<!DOCTYPE html><html><head><title>Failed Login Attempt</title><style type=''text/css''> body { font-size: 0.87em; color: #ffffff; text-align: center; height: 100%;} h1  {font-size: 2.1em;} a {color: inherit;} img { display:block; width:100%; margin:-8% 0;}.img-row { overflow:hidden; margin:0;}.wrapper-div { margin: auto; max-width:600px;background-color: #373535ff;}.body-text {padding-top: 7%; font-size: 1.0em}.body-text, .body-text { padding-left: 10%; padding-right: 10%;}</style></head><body><div class=''wrapper-div''><div class=''row''><div class=''body-text''><h1>${site_descr}</h1><p><h3>Failed Login Attempt!</h3></p><p>Failed login attempt detected on ${datetime}, remaining attempts: ${remaining}</p><p>Didn`t attempt to login?  If you need any help don`t hesitate to <a href=''mailto:${support_email}?subject=Mail from Our Site''>contact us.</a></p><br></div></div></div></div></body></html>" }'
where corrtype_id = 'email' and notype_id = 'fura.login_attempt';

-- CONFIRM ADDRESS

update loco.notempl
set templ = '{"msg": "You are about to confirm your ${site_descr} cell number, OTP: ${otp}"}'
where corrtype_id = 'sms' and notype_id = 'fura.confirm_addr';

update loco.notempl
set templ = '{"subject": "Confirm You E-mail Address", "body-text": "${site_descr}\n\nConfirm OTP: ${otp}\n", "body-html": "<!DOCTYPE html><html><head><title>Confirm E-mail Address</title><style type=''text/css''> body { font-size: 0.87em; color: #ffffff; text-align: center; height: 100%;} h1 {font-size: 2.1em;} a {color: inherit;} img { display:block; width:100%; margin:-8% 0;}.img-row { overflow:hidden; margin:0;}.wrapper-div { margin: auto; max-width:600px;background-color: #373535ff;}.body-text {padding-top: 7%;}.body-text, .body-text { padding-left: 10%; padding-right: 10%;}</style></head><body><div class=''wrapper-div''><div class=''img-row''><img src=''mail-hero.png''></div><div class=''row''><div class=''body-text''><h1>${site_descr}</h1><p><h3>Confirm Your E-mail Address OTP : </h3><h2>${otp}</h2></p><p>If you need any help don`t hesitate to <a href=''mailto:${support_email}?subject=Mail from Our Site''>contact us.</a></p><br></div></div></div></div></div></body></html>" }'
where corrtype_id = 'email' and notype_id = 'fura.confirm_addr';

-- USER REGISTRATION

update loco.notempl
set templ = '{"msg": "Your are about to complete your account registration with ${site_descr}, OTP: ${otp}"}'
where corrtype_id = 'sms' and notype_id = 'fura.registration';

update loco.notempl
set templ = '{"subject": "${site_descr} Account Registration", "body-text": "${site_descr}\n\nAccount Registration OTP: ${otp}\n\nDidn`t register with us. No problem you can safely ignore this email or don`t hesitate to contact us at: ${support_email}", "body-html": "<!DOCTYPE html><html><head><title>Account Registration</title><style type=''text/css''> body { font-size: 0.87em; color: #ffffff; text-align: center; height: 100%;} h1  {font-size: 2.1em;} a {color: inherit;} img { display:block; width:100%; margin:-8% 0;}.img-row { overflow:hidden; margin:0;}.wrapper-div { margin: auto; max-width:600px;background-color: #373535ff;}.body-text {padding-top: 7%;}.body-text, .new-password-div {font-size: 1.2em;}.new-password-div { margin: auto; background-color: #90c84bff; padding: 2%; max-width: 35%; margin-top: 5%; font-weight: bold; color: #000000;}.footer-div { padding-top: 0.2%; padding-bottom: 2%; max-height: 7.857em; margin-top: 10%; background-color: #373535ff;}.footer-div, .body-text { padding-left: 10%; padding-right: 10%;}</style></head><body><div class=''wrapper-div''><div class=''row''><div class=''body-text''><h1>${site_descr}</h1><p><h3>Account Registration OTP : </h3><h2>${otp}</h2></p><p>Or to register to your account in one easy step click the button below.</p></div><a style=''text-decoration:none'' href=''${httplink}''><div class=''new-password-div''>Register My Account</div></a></div><div class=''row''><div class=''footer-div''><p>Didn`t register an account with us?  No problem, you can ignore this email or don`t hesitate to <a href=''mailto:${support_email}?subject=Mail from Our Site''>contact us.</a></p></div></div></div></body></html>" }'
where corrtype_id = 'email' and notype_id = 'fura.registration';
