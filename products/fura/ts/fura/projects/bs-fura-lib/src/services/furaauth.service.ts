/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Injectable }      from '@angular/core';
import { Subject ,  BehaviorSubject ,  ReplaySubject } from 'rxjs';

import { StringList }    from '@bitsmiths/mettle-braze';
import { xMettle }       from '@bitsmiths/mettle-lib';

import {
  bUserLogin,
  AuthType_Couplet,
  ResetMethod_Couplet,
 } from '../braze/fura_structs';

import { tRole }               from '../db/tables/role';
import { RoleStatus_Couplet }  from '../db/tables/role';
import { tUsr }                from '../db/tables/usr';

import {
  FuraBrazeService,
  FuraErrorCodes,
} from './furabraze.service';


@Injectable()
export class FuraAuthService
{
  currentUser   : tUsr|null                = null;
  userRole      : tRole|null               = null;
  userFuncs     : {[key: string]: boolean} = {};

  displayName$  : Subject<string>  = new BehaviorSubject<string>("");
  displayEmail$ : Subject<string>  = new BehaviorSubject<string>("");
  authChange$   : Subject<boolean> = new ReplaySubject<boolean>();

  constructor(private _furaService: FuraBrazeService)
  {
    let storedUsr   : string|null = sessionStorage.getItem("fura.authservice.currentUser");
    let storedRole  : string|null = sessionStorage.getItem("fura.authservice.currentRole");
    let storedFuncs : string|null = sessionStorage.getItem("fura.authservice.currentFuncs");

    if (storedUsr)
    {
      this.currentUser = new tUsr();
      this.currentUser._init((<tUsr>JSON.parse(storedUsr)));

      if (this.currentUser.dateActivate !== null) {
        this.currentUser.dateActivate = new Date(this.currentUser.dateActivate);
      }

      if (this.currentUser.dateExpire !== null) {
        this.currentUser.dateExpire = new Date(this.currentUser.dateExpire);
      }

      if (this.currentUser.tmStamp !== null) {
        this.currentUser.tmStamp = new Date(this.currentUser.tmStamp);
      }
    }

    if (storedRole)
    {
      this.userRole = new tRole();
      this.userRole._init((<tRole>JSON.parse(storedRole)));
    }

    if (storedFuncs)
      this.userFuncs = JSON.parse(storedFuncs);

    this._userChangePublish();
  }

  _userChangePublish()
  {
    if (this.currentUser == null)
    {
      this.displayName$.next("");
      this.displayEmail$.next("");

      sessionStorage.removeItem("fura.authservice.currentUser");
      sessionStorage.removeItem("fura.authservice.currentRole");
      sessionStorage.removeItem("fura.authservice.currentFuncs");
    }
    else
    {
      this.displayName$.next(this.currentUser.nameFirst + " " + this.currentUser.nameLast);
      this.displayEmail$.next(this.currentUser.email1);

      sessionStorage.setItem("fura.authservice.currentUser",  JSON.stringify(this.currentUser));
      sessionStorage.setItem("fura.authservice.currentRole",  JSON.stringify(this.userRole));
      sessionStorage.setItem("fura.authservice.currentFuncs", JSON.stringify(this.userFuncs));
    }

    this.authChange$.next(this.isAuthenticated());
  }

  _resetUser()
  {
    this.currentUser = null;
    this.userFuncs   = {};
    this._userChangePublish();
  }

  usrId(): string
  {
    return this.currentUser == null ? '' : this.currentUser.id;
  }

  isAuthenticated(): boolean
  {
    return this.currentUser != null;
  }

  hasAccess(funcs: string[]): boolean
  {
    if (funcs.length === 0)
      return true;

    if (this.userRole == null)
      return false;

    if (this.userRole.status === RoleStatus_Couplet.keySuper_User)
      return true;

    for (let func of funcs) {
      if (func === '' || func in this.userFuncs)
        return true;
    }

    return false;
  }

  login(site: string, username: string, password: string): Promise<tUsr|null>
  {
    let prom = new Promise<tUsr|null>( (resolve, reject) =>
    {
      let ul = new bUserLogin()._initDeft(site, username, AuthType_Couplet.keyPassword, password, '', '');

      this._furaService.client.userLogin(ul)
        .then( (errorCode: number) =>
        {
          if (errorCode === FuraErrorCodes.NoError) {

            let promises = new Array<any>();

            promises.push(this._furaService.client.userRec());
            promises.push(this._furaService.client.userFuncs('*'));
            promises.push(this._furaService.client.userRole());

            Promise.all(promises)
            .then( ( proms: Array<any> ) =>
            {
              this.currentUser = proms[0];

              for (let x of proms[1]) {
                this.userFuncs[x] = true;
              }

              this.userRole = proms[2];

              this._userChangePublish();
              resolve(this.currentUser);
            })
            .catch( (err: xMettle) =>
            {
              this._resetUser();
              reject(err.getMsg());
            });
          }
          else
          {
            this._resetUser();
            reject(this._furaService.getFuraErrorMessage(errorCode));
          }
        })
        .catch( (err: xMettle) =>
        {
          this._resetUser();
          reject(err.getMsg());
        });
    });

    return prom;
  }

  logout(): Promise<void>
  {
    let prom = new Promise<void>( (resolve, reject) =>
    {
      if (this.currentUser == null)
      {
        resolve();
        return;
      }

      this._furaService.client.userLogout()
        .then( (errorCode: number) =>
        {
          if (errorCode === FuraErrorCodes.NoError)
          {
            this._resetUser();
            resolve();
          }
          else
          {
            if (this._furaService.isFuraTokenError(errorCode))
            {
              this._resetUser();
              resolve();
            }
            else
            {
              reject(this._furaService.getFuraErrorMessage(errorCode));
            }
          }
        })
        .catch( (err: xMettle) =>
        {
          if (this._furaService.isTokenException(err))
          {
            this._resetUser();
            resolve();
          }
          else
          {
            reject(err.getMsg());
          }
        });
    });

    return prom;
  }

  forgotPassword(site: string, username: string): Promise<void>
  {
    let prom = new Promise<void>( (resolve, reject) =>
    {
      this._furaService.client.userAuthReset(site, username, AuthType_Couplet.keyPassword, ResetMethod_Couplet.keyEmail)
        .then( () =>
        {
          resolve();
        })
        .catch( (err: xMettle) =>
        {
          reject('Error resetting your password: ' + err.getMsg());
        });

    });

    return prom;
  }
}
