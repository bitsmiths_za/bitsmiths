/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Injectable }           from '@angular/core';
import { Inject }               from '@angular/core';
import { Router }               from '@angular/router';

import { FetchClient }          from '@bitsmiths/mettle-braze';
import { xMettle }              from '@bitsmiths/mettle-lib';
import { xMettleECode }         from '@bitsmiths/mettle-lib';

import { FuraClientMarshaler }  from '../braze/fura_client_marshaler';

export enum FuraErrorCodes
{
  NoError          = 0,
  UnexpectedError  = 2,

  UserNotFound     = 100,
  UserDisabled     = 101,
  UserDeleted      = 102,
  UserSuspended    = 103,
  UserExpired      = 104,
  UserHasNoRole    = 105,
  USER_MAX_ERROR   = 199,

  SiteNotFound     = 200,
  SiteDisabled     = 201,
  SITE_MAX_ERROR   = 299,

  RoleNotFound     = 300,
  RoleDisabled     = 301,
  ROLE_MAX_ERROR   = 399,

  FuncNotfound     = 400,
  FUNC_MAX_ERROR   = 499,

  AccessDenied     = 500,
  AccReqEaTok      = 501,
  ACCESS_MAX_ERROR = 599,

  EaTokNotFound    = 600,
  EaTokExpired     = 601,
  EaTokMaxUses     = 602,
  EaTokWrongFunc   = 603,
  EaTokWrongUser   = 604,
  EAKTOK_MAX_ERROR = 699,

  AuthInvalid      = 700,
  AuthEmpty        = 701,
  AuthNotFound     = 702,
  AuthWrongType    = 703,
  AuthMaxFailures  = 704,
  AuthNotLoggedIn  = 705,
  AuthOTPNotFound  = 720,
  AuthOTPUsed      = 721,
  AuthOTPExpired   = 722,
  AuthOTPInvlalid  = 723,
  AuthOTPTarget    = 724,
  AuthOTPTMaxFails = 725,
  AuthOTPTMethod   = 726,
  AUTH_MAX_ERROR   = 799,

  TokenInvalid     = 800,
  TokenTooMany     = 801,
  TOKEN_MAX_ERROR  = 899,
}


@Injectable()
export class FuraBrazeService
{
  fetchClient : FetchClient;
  client      : FuraClientMarshaler;

  constructor(
                              private _router        : Router,
    @Inject('furaUrl')        private furaUrl        : string,
    @Inject('furaCors')       private furaCors       : string,
    @Inject('furaCred')       private furaCred       : string,
    @Inject('furaLoginRoute') private furaLoginRoute : string,
    @Inject('furaHeaders')    private furaHeaders    : any = undefined,
    @Inject('sigUrl')         private sigUrl         : boolean = false )
  {
    this.fetchClient = new FetchClient(
      furaUrl,
      <RequestMode> furaCors,
      <RequestCredentials> furaCred,
      furaHeaders,
      sigUrl);

    this.client = new FuraClientMarshaler(this.fetchClient);
  }

  isFuraTokenError(errorCode: number): boolean
  {
    if (errorCode === xMettleECode.CredExpired  ||
        errorCode === xMettleECode.CredInvalid  ||
        errorCode === xMettleECode.CredDenied   ||
        errorCode === xMettleECode.TokenExpired ||
        errorCode === xMettleECode.TokenInvalid)
      return true;

    return false;
  }

  isTokenException(err: xMettle): boolean
  {
    let errCode: number = err.getErrorCode();

    if (errCode === xMettleECode.CredExpired  ||
        errCode === xMettleECode.CredInvalid  ||
        errCode === xMettleECode.CredDenied   ||
        errCode === xMettleECode.TokenExpired ||
        errCode === xMettleECode.TokenInvalid)
      return true;

    return false;
  }

  handleError(err : xMettle) : string
  {
    if (err instanceof Error)
      return err.name + ": " + err.message;

    if (this.isTokenException(err))
    {
      this._router.navigate([this.furaLoginRoute], {
        queryParamsHandling : "merge",
        queryParams         : {"next": this._router.url}
      });
    }

    let msg = this.getFuraErrorMessage(err.getErrorCode());

    if (msg) {
      return msg;
    }

    return this._getErrorMessage(err.getErrorCode(), err);
  }

  _getErrorMessage(errCode: number, err: xMettle): string
  {
    if (errCode === xMettleECode.CredExpired)
      return 'Your credentials have expired. Please sign in again.';

    if (errCode === xMettleECode.CredInvalid)
      return 'Your credentials are invalid. Please sign in again.';

    if (errCode === xMettleECode.CredDenied)
      return 'Your credentials are denied. Please sign in again.';

    if (errCode === xMettleECode.TokenExpired)
      return 'Your access token has expired. Please sign in again.';

    if (errCode === xMettleECode.TokenInvalid)
      return 'Your access token is invalid. Please sign in again.';

    return err.getMsg();
  }

  getFuraErrorMessage(errorCode: number, defaultError: boolean = false): string
  {
    let msg = null;

    switch (errorCode)
    {
      case FuraErrorCodes.NoError          : msg = "No Error"; break;
      case FuraErrorCodes.UnexpectedError  : msg = "An unexpected error occured while logging you in. Please contact your system administrator."; break;
      case FuraErrorCodes.UserNotFound     : msg = "The specified user does not exist in the system. Please contact your system administrator."; break;
      case FuraErrorCodes.UserDisabled     : msg = "The specified user is disabled. Please contact your system administrator."; break;
      case FuraErrorCodes.UserDeleted      : msg = "The specified user has been deleted. Please contact your system administrator."; break;
      case FuraErrorCodes.UserSuspended    : msg = "The specified user is suspended. Please contact your system administrator."; break;
      case FuraErrorCodes.UserExpired      : msg = "The specified user has expired. Please contact your system administrator."; break;
      case FuraErrorCodes.UserHasNoRole    : msg = "The specified user has no roles defined. Please contact your system administrator."; break;
      case FuraErrorCodes.SiteNotFound     : msg = "The specified site could not be found. Please contact your system administrator."; break;
      case FuraErrorCodes.SiteDisabled     : msg = "The specified site is disabled. Please contact your system administrator."; break;
      case FuraErrorCodes.RoleNotFound     : msg = "The specified role could not be found. Please contact your system administrator."; break;
      case FuraErrorCodes.RoleDisabled     : msg = "The specified role is disabled. Please contact your system administrator."; break;
      case FuraErrorCodes.FuncNotfound     : msg = "The specified function could not be found in the system. Please contact your system administrator."; break;
      case FuraErrorCodes.AccessDenied     : msg = "Access Denied. Please contact your system administrator."; break;
      case FuraErrorCodes.AccReqEaTok      : msg = "Access Requires additional authorisation. Please contact your system administrator."; break;
      case FuraErrorCodes.EaTokNotFound    : msg = "Additional autorisation token not found. Please contact your system administrator."; break;
      case FuraErrorCodes.EaTokExpired     : msg = "Additional autorisation token has expired.."; break;
      case FuraErrorCodes.EaTokMaxUses     : msg = "Additional autorisation token has been used too many times."; break;
      case FuraErrorCodes.EaTokWrongFunc   : msg = "Additional autorisation token is for a different function."; break;
      case FuraErrorCodes.EaTokWrongUser   : msg = "Additional autorisation token is for an incorrect user."; break;
      case FuraErrorCodes.AuthInvalid      : msg = "Authorisation is invalid. Please contact your system administrator."; break;
      case FuraErrorCodes.AuthEmpty        : msg = "Authorisation is empty. Please contact your system administrator."; break;
      case FuraErrorCodes.AuthNotFound     : msg = "Authorisation is not found. Please contact your system administrator."; break;
      case FuraErrorCodes.AuthWrongType    : msg = "Authorisation is of the wrong type. Please contact your system administrator."; break;
      case FuraErrorCodes.AuthMaxFailures  : msg = "Authorisation has exceeded maximum failures. Please contact your system administrator."; break;
      case FuraErrorCodes.AuthNotLoggedIn  : msg = "You are not logged in. Please sign in to continue."; break;
      case FuraErrorCodes.AuthOTPNotFound  : msg = "OTP not found."; break;
      case FuraErrorCodes.AuthOTPUsed      : msg = "OTP already used."; break;
      case FuraErrorCodes.AuthOTPExpired   : msg = "OTP has expired, please request a new OTP."; break;
      case FuraErrorCodes.AuthOTPInvlalid  : msg = "OTP Invalid, please try again."; break;
      case FuraErrorCodes.AuthOTPTarget    : msg = "OTP is designated for a different purpose."; break;
      case FuraErrorCodes.AuthOTPTMaxFails : msg = "Maximum OTP failures have been exceeded, please request a new OTP."; break;
      case FuraErrorCodes.AuthOTPTMethod   : msg = "OTP method mismatch."; break;
      case FuraErrorCodes.TokenInvalid     : msg = "Your token is invalid. Please sign in."; break;
      case FuraErrorCodes.TokenTooMany     : msg = "Your token has been used too many times. Please sign in."; break;

      default :
        if (defaultError) {
          msg = "An unknown error occured while logging in. Please contact your system administrator.";
        }
        break;
    }

    return msg;
  }
}
