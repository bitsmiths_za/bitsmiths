/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import {
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';

import {
  Router,
  ActivatedRoute
}  from '@angular/router';

import { NgForm }           from '@angular/forms';

import {
  TdDialogService,
  TdLoadingService,
} from '@covalent/core';

import { xMettle }          from '@bitsmiths/mettle-lib';
import { FuraAuthService }  from '@bitsmiths/fura-lib';
import { tUsr }             from '@bitsmiths/fura-lib';

@Component({
  // moduleId    :  module.id,
  selector    :  'fura-login',
  template : `
<td-layout-nav logo="assets:logo">
  <button mat-icon-button td-menu-button tdLayoutToggle>
    <mat-icon>menu</mat-icon>
  </button>

  <div td-toolbar-content layout="row" layout-align="center center" flex>
    <span>Sign In</span>
    <span flex></span>
  </div>

  <div class="mat-content" layout-padding flex>
    <div layout-gt-xs="row" layout-align-gt-xs="center start" class="margin">
      <div flex-gt-xs="50">

        <mat-card tdMediaToggle="gt-xs" [mediaClasses]="['push-top-lg']">
          <mat-card-title><mat-icon class="mat-icon-logo" svgIcon="assets:logo"></mat-icon> Sign In</mat-card-title>
          <mat-card-subtitle>Sign in with your username</mat-card-subtitle>
          <mat-divider></mat-divider>

          <mat-card-content>
            <form #loginForm="ngForm" (submit)="login()">

              <div layout="row" class="push-top">
                <mat-form-field flex>
                  <input
                    matInput
                    #userElement
                    #userControl = "ngModel"
                    type         = "text"
                    maxlength    = "128"
                    name         = "username"
                    [(ngModel)]  = "username"
                    required
                  >
                  <mat-placeholder><mat-icon>person</mat-icon>Username</mat-placeholder>
                  <mat-hint align="start">
                    <span [hidden]="!userControl.errors?.required || userControl.pristine" class="tc-red-600">Required</span>
                  </mat-hint>
                  <mat-hint align="end">{{userElement.value.length}} / {{userElement.maxLength}}</mat-hint>
                </mat-form-field>
              </div>

              <div layout="row" class="push-top">
                <mat-form-field flex>
                  <input
                    matInput
                    #passElement
                    #passControl= "ngModel"
                    type        = "password"
                    name        = "password"
                    [(ngModel)] = "password"
                    maxlength   = "64"
                    required
                  >
                  <mat-placeholder><mat-icon>lock</mat-icon>Password</mat-placeholder>
                  <mat-hint align="start">
                    <span [hidden]="!passControl.errors?.required || passControl.pristine" class="tc-red-600">Required</span>
                  </mat-hint>
                  <mat-hint align="end">Something hard to guess</mat-hint>
                </mat-form-field>
              </div>

              <div layout="row" class="push-top">
                <mat-form-field flex="50">
                  <input
                    matInput
                    #siteElement
                    #siteControl = "ngModel"
                    type         = "text"
                    maxlength    = "32"
                    name         = "site"
                    [(ngModel)]  = "site"
                    [disabled]   = "fixedSite"
                    required>
                  <mat-placeholder><mat-icon>place</mat-icon>Site</mat-placeholder>
                  <mat-hint align="start">
                    <span [hidden]="!siteControl.errors?.required || siteControl.pristine" class="tc-red-600">Required</span>
                  </mat-hint>
                  <mat-hint align="end">{{siteElement.value.length}} / {{siteElement.maxLength}}</mat-hint>
                </mat-form-field>
              </div>
              <button type="submit" style="display:none">hidden submit</button>
            </form>
          </mat-card-content>

          <mat-divider></mat-divider>
          <mat-card-actions layout="row">
            <button
              flex
              mat-raised-button
              color     = "accent"
              [disabled]= "!loginForm.form.valid"
              (click)   = "login()"
            >
              Sign In
            </button>

            <button
              flex
              mat-raised-button
              *ngIf      = "!fixedSite"
              color      = "accent"
              [disabled] = "!siteControl.valid || !userControl.valid"
              (click)    = "forgotPassword()"
            >
              Forgot Password
            </button>

            <button
              flex
              mat-raised-button
              *ngIf      = "fixedSite"
              color      = "accent"
              [disabled] = "!userControl.valid"
              (click)    = "forgotPassword()"
            >
              Forgot Password
            </button>

          </mat-card-actions>
        </mat-card>
      </div>
    </div>
  </div>
</td-layout-nav>
`,
  styles   : [
`:host /deep/ {
    md-divider {
        display: block;
        border-top-style: solid;
        border-top-width: 1px;
    }
}`
  ],
})

export class LoginComponent {

  @ViewChild('loginForm') loginForm: NgForm;

  username  : string  = "";
  password  : string  = "";
  site      : string  = "";
  next      : string  = "";
  fixedSite : boolean = false;

  constructor(
    private _authService    : FuraAuthService,
    private _router         : Router,
    private _route          : ActivatedRoute,
    private _loadingService : TdLoadingService,
    private _dialogService  : TdDialogService)
  {
    let storedVal: string|null = localStorage.getItem("fura.logincomponent.site");

    if (storedVal != null)
      this.site = storedVal;

    this._route.queryParams.subscribe((params: {next: string, siteid: string}) =>
    {
      this.next = params.next;

      if (params.siteid !== undefined)
      {
        this.fixedSite = true;
        this.site      = params.siteid;
      }
    });
  }

  login(): void
  {
    if (!this.loginForm.form.valid)
      return;

    this._loadingService.register();

    localStorage.setItem("fura.logincomponent.site", this.site);

    this._authService.login(this.site, this.username, this.password)
      .then( (usr: tUsr) =>
      {
        this._loadingService.resolve();

        console.log("login success:" + this.next)

        if (this.next === undefined || this.next === "")
          this._router.navigate(['/']);
        else
          this._router.navigateByUrl(this.next);
      })
      .catch( (err: string) =>
      {
        this._loadingService.resolve();
        this._dialogService.openAlert({message: 'Error logging in: ' + err});
      });
  }

  forgotPassword()
  {
    this._loadingService.register();
    this._authService.forgotPassword(this.site, this.username)
      .then( () =>
      {
        this._loadingService.resolve();
        this._dialogService.openAlert({message: 'Password reset email has been sent.'});
      })
      .catch( (err: string) =>
      {
        this._loadingService.resolve();
        this._dialogService.openAlert({message: 'Error resetting password: ' + err});
      });
  }
}
