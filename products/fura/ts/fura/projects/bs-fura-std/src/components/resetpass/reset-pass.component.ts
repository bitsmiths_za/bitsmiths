/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import {
  Component,
  OnInit,
} from '@angular/core';

import {
  Router,
  ActivatedRoute,
} from '@angular/router';

import { FormsModule } from '@angular/forms';

import {
  TdDialogService,
  TdLoadingService,
} from '@covalent/core';

import { xMettle }          from '@bitsmiths/mettle-lib';
import { FuraBrazeService } from '@bitsmiths/fura-lib';

import {
  bUserLogin,
  AuthType_Couplet,
  ResetMethod_Couplet,
} from '@bitsmiths/fura-lib';

@Component({
  selector    :  'fura-resetpass',
  template    :  `
<div layout="column" layout-fill>
  <mat-toolbar color="primary" class="mat-whiteframe-z1">
    <span>Reset Password</span>
  </mat-toolbar>

  <div class="mat-content" layout-padding flex>
    <div layout-gt-xs="row" layout-align-gt-xs="center start" class="margin">
      <div flex-gt-xs="50">
        <mat-card tdMediaToggle="gt-xs" [mediaClasses]="['push-top-lg']">

          <mat-card-title><mat-icon class="mat-icon-logo" svgIcon="assets:logo"></mat-icon>Reset Password</mat-card-title>
          <mat-card-subtitle>Reset your password</mat-card-subtitle>
          <mat-divider></mat-divider>
          <mat-card-content>
            <form #resetPasswordForm="ngForm">

              <div layout="row" class="push-top">
                <mat-form-field flex>
                  <input
                    matInput
                    #passElement
                    #passControl = "ngModel"
                    placeholder  = "Password"
                    type         = "password"
                    name         = "password"
                    [(ngModel)]  = "password"
                    required
                  >
                  <span matPrefix><mat-icon>lock</mat-icon></span>
                  <mat-hint align="start">
                    <span [hidden]="!passControl.errors?.required || passControl.pristine" class="tc-red-600">Required</span>
                  </mat-hint>
                  <mat-hint align="end">Something hard to guess</mat-hint>
                </mat-form-field>
              </div>

              <div layout="row" class="push-top">
                <mat-form-field flex>
                  <input
                    matInput
                    #confirmPassElement
                    #confirmPassControl = "ngModel"
                    [fieldMatches]      = "passControl"
                    placeholder         = "Confirm Password"
                    type                = "password"
                    name                = "passwordConfirm"
                    [(ngModel)]         = "passwordConfirm"
                    required
                  >
                  <span matPrefix><mat-icon>lock</mat-icon></span>
                  <mat-hint align="start">
                    <span [hidden]="!confirmPassControl.errors?.required || confirmPassControl.pristine" class="tc-red-600">Required</span>
                    <span [hidden]="!confirmPassControl.errors?.fieldMatches || confirmPassControl.pristine" class="tc-red-600">Passwords do not match</span>
                  </mat-hint>
                  <mat-hint align="end">Confirm you password</mat-hint>
                </mat-form-field>
              </div>
            </form>
          </mat-card-content>
          <mat-divider></mat-divider>
          <mat-card-actions layout="row">
            <button
              flex
              mat-raised-button
              color      = "accent"
              [disabled] = "!resetPasswordForm.form.valid"
              (click)    = "resetPassword()"
            >Reset Password
            </button>
          </mat-card-actions>
        </mat-card>

      </div>
    </div>
  </div>
</div>
`,
  styles   : [``]
})
export class ResetPassComponent implements OnInit
{
  site            : string;
  user            : string;
  password        : string;
  passwordConfirm : string;
  token           : string;

  constructor(
    private _furaService    : FuraBrazeService,
    private _route          : ActivatedRoute,
    private _router         : Router,
    private _loadingService : TdLoadingService,
    private _dialogService  : TdDialogService)
  {
  }

  ngOnInit(): void
  {
    this._route
      .queryParams
      .subscribe(params =>
      {
        this.site  = params["site"];
        this.user  = params["usr"];
        this.token = params["tok"];
      });
  }

  resetPassword(): void
  {
    this._loadingService.register();

    let ul = new bUserLogin()._initDeft(this.site, this.user, AuthType_Couplet.key_Password, this.password, '', '');

    this._furaService.client.userSetNewAuth(ul, this.token, ResetMethod_Couplet.key_Email)
      .then( () =>
      {
        this._router.navigate(['../login']);
        this._loadingService.resolve();
      })
      .catch( (err: xMettle) =>
      {
        this._loadingService.resolve();
        this._dialogService.openAlert({message: 'Error resetting password: ' + this._furaService.handleError(err)});
      });
  }
}
