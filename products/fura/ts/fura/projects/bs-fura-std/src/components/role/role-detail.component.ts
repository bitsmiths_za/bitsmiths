/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';

import { TdDialogService, TdLoadingService } from '@covalent/core';

import { List }    from '@bitsmiths/mettle-braze';
import { xMettle } from '@bitsmiths/mettle-lib';

import { TextService } from '@bs-lib/lib';

import { tRole, tRolestatus_Couplet } from '@bitsmiths/fura-lib';

import { FuraAuthService }  from '@bitsmiths/fura-lib';
import { FuraBrazeService } from '@bitsmiths/fura-lib';

//import { SNACKBAR_DURATION } from '../../../config/sys.config';

@Component({
  selector    :  'fura-role-detail',
  template    :  `
<td-layout-nav logo="assets:logo">
  <button mat-icon-button td-menu-button tdLayoutToggle>
    <mat-icon>menu</mat-icon>
  </button>

  <div td-toolbar-content layout="row" layout-align="center center" flex>
    <span>Role</span>
    <span flex></span>
  </div>

  <td-layout-card-over cardTitle="Role Detail" cardSubtitle="{{subTitle}}">
    <ng-template tdLoading="fura.role-detail">
      <mat-card-content class="push-bottom-none">
        <form #userForm="ngForm">

          <div layout="row">
          <!-- Role ID -->
            <mat-form-field flex>
              <input matInput
                #idElement
                #idControl = "ngModel"
                type       = "text"
                placeholder= "Role ID"
                [(ngModel)]= "rec.id"
                [disabled] = "readOnly() || action!=='add'"
                name       = "roleId"
                maxlength  = "128"
                required
              >
              <mat-error align="start">
                <span [hidden]="idControl.pristine">
                  <span [hidden]="!idControl.hasError('required')">Required</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{idElement.value.length}} / {{idElement.maxLength}}</mat-hint>
            </mat-form-field>
          </div>

          <!-- Description -->
          <div layout="row" class="push-top">
            <mat-form-field flex>
              <input matInput
                #descrElement
                #descrControl = "ngModel"
                type          = "text"
                placeholder   = "Description"
                [(ngModel)]   = "rec.descr"
                name          = "descr"
                [disabled]    = "readOnly()"
                maxlength     = "256"
                required
              >
              <mat-error align="start">
                <span [hidden]="descrControl.pristine">
                  <span [hidden]="!descrControl.hasError('required')">Required</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{descrElement.value.length}} / {{descrElement.maxLength}}</mat-hint>
            </mat-form-field>
          </div>

          <!-- Status -->
          <div layout="row" class="push-top">
            <mat-form-field flex="50">
              <mat-select
                #statusElement
                #statusControl = "ngModel"
                placeholder    = "Status"
                [(ngModel)]    = "rec.status"
                name           = "status"
                [disabled]    = "readOnly()"
                required
              >
                <mat-error align="start">
                  <span [hidden]="statusControl.pristine">
                    <span [hidden]="!statusControl.hasError('required')">Required</span>
                  </span>
                </mat-error>
                <mat-option *ngFor="let rstat of roleStatuses" [value]="rstat">{{getRoleStatus(rstat)}}</mat-option>
              </mat-select>
            </mat-form-field>
          </div>

          <!-- Level -->
          <div layout="row" class="push-top">
            <mat-form-field flex = "33">
              <input matInput
                #levelElement
                #levelControl = "ngModel"
                type          = "number"
                placeholder   = "Level"
                [(ngModel)]   = "rec.level"
                name          = "level"
                [disabled]    = "readOnly()"
                min           = "1"
                max           = "20"
                required
              >
              <mat-error align="start">
                <span [hidden]="levelControl.pristine">
                  <span [hidden]="!levelControl.hasError('required')">Required</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{levelElement.min}}...{{levelElement.max}}</mat-hint>
            </mat-form-field>
          </div>

          <!-- Sess Timeout -->
          <div layout="row" class="push-top">
            <mat-form-field flex = "33">
              <input matInput
                #stolElement
                #stoControl   = "ngModel"
                type          = "number"
                placeholder   = "Session Timeout"
                [(ngModel)]   = "rec.sessTimeout"
                name          = "sessTimeout"
                [disabled]    = "readOnly()"
                min           = "0"
                max           = "120"
                required
              >
              <mat-error align="start">
                <span [hidden]="stoControl.pristine">
                  <span [hidden]="!stoControl.hasError('required')">Required</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{stolElement.min}}...{{stolElement.max}} (Minutes) - Note, zero disables timeout.</mat-hint>
            </mat-form-field>
          </div>

          <!-- Audit ID -->
          <div layout="row" class="push-top" *ngIf="action!=='add'" >
            <div flex="66"></div>
            <mat-form-field flex>
              <input matInput
                type          = "text"
                name          = "audId"
                [(ngModel)]   = "rec.audId"
                [disabled]    = "true">
              <mat-hint align="start">Last Modified By</mat-hint>
            </mat-form-field>
          </div>

          <!-- Timestamp -->
          <div layout="row" class="push-top" *ngIf="action!=='add'" >
            <div flex="66"></div>
            <mat-form-field flex>
              <input matInput
                type          = "text"
                name          = "audTmStamp"
                [ngModel]     = "textService.formatDateTime(rec.tmStamp)"
                [disabled]    = "true">
              <mat-hint align="start">Last Modified Timestamp</mat-hint>
            </mat-form-field>
          </div>


        </form>
      </mat-card-content>
      <mat-divider></mat-divider>
      <mat-card-actions>
        <button mat-button [disabled]="!userForm.form.valid || !canSave()" color="primary" (click)="save()">SAVE</button>
        <button mat-button (click)="goBack()">CANCEL</button>
      </mat-card-actions>
    </ng-template>
  </td-layout-card-over>
</td-layout-nav>
`,
  styles   : [``],
})
export class RoleDetailComponent implements OnInit
{
  action       : string;
  id           : string;
  rec          : tRole    = new tRole();
  roleStatuses : string[] = tRolestatus_Couplet.getKeys();
  subTitle     : string   = "";

  constructor(
    private _furaService     : FuraBrazeService,
    private _router          : Router,
    private _route           : ActivatedRoute,
    private _snackBarService : MatSnackBar,
    private _loadingService  : TdLoadingService,
    private _dialogService   : TdDialogService,
    public  textService      : TextService,
    public  authService      : FuraAuthService)
  {
  }

  goBack(): void
  {
    this._router.navigate(['..'], {relativeTo: this._route});
  }

  ngOnInit(): void
  {
    this._route.url.subscribe((url: any) =>
    {
      if (url.length == 2)
        this.action = url[1] == "edit" ? "edit" : "view";
      else if (url.length == 1)
        this.action = url[0] == "add" ? "add" : "view";

      if (this.action == "add")
        this.subTitle = "Fill form to create new role";
      else if (this.action == "edit")
        this.subTitle = "Update role";
      else
        this.subTitle = "Read only";
    });

    this._route.params.subscribe((params: {id: string}) =>
    {
      this.id = params.id;

      if (this.id != null)
        this.loadViewEdit();
    });
  }

  loadViewEdit(): void
  {
    this._loadingService.register('fura.role-detail');

    this._furaService.client.roleRead(this.id)
      .then( (roles: List<tRole>) =>
      {
        if (roles.length === 0)
        {
          this._dialogService.openAlert({message: 'Role [' + this.id + '] not found!'});
          this._loadingService.resolve('fura.role-detail');
          return;
        }

        this.rec = (<tRole>roles[0]);
        this._loadingService.resolve('fura.role-detail');
      })
      .catch( (err: xMettle) =>
      {
        this._loadingService.resolve('fura.role-detail');
        this._dialogService.openAlert({message: 'Error loading role: ' + this._furaService.handleError(err)});
      });
  }

  getRoleStatus(status: string): string
  {
    return tRolestatus_Couplet.getValue(status);
  }

  canSave(): boolean
  {
    if (this.action === 'add')
    {
      return this.authService.hasAccess(['fura.role.create']);
    }
    else if (this.action == 'edit')
    {
      return this.authService.hasAccess(['fura.role.update']);
    }

    return false;
  }

  readOnly(): boolean
  {
    return this.action !== 'add' && this.action !== 'edit';
  }

  save(): void
  {
    this._loadingService.register('fura.role-detail');

    if (this.action === 'add')
    {
      this.rec.audId = "NEW";

      this._furaService.client.roleCreate(this.rec)
        .then( (rec: tRole) =>
        {
          this.rec = rec;
          this._snackBarService.open('Role Saved', 'OK', {'duration': 2 })// SNACKBAR_DURATION});
          this._loadingService.resolve('fura.role-detail');
          this.goBack();
        })
        .catch( (err: xMettle) =>
        {
          this._loadingService.resolve('fura.role-detail');
          this._dialogService.openAlert({message: 'Error saving role: ' + this._furaService.handleError(err)});
        });

      return;
    }

    this._furaService.client.roleUpdate(this.rec)
      .then( (rec: tRole) =>
      {
        this.rec = rec;
        this._snackBarService.open('Role Saved', 'OK', {'duration': 2 })// SNACKBAR_DURATION});
        this._loadingService.resolve('fura.role-detail');
      })
      .catch( (err: xMettle) =>
      {
        this._loadingService.resolve('fura.role-detail');
        this._dialogService.openAlert({message: 'Error saving role: ' + this._furaService.handleError(err)});
      });
  }
}
