/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import {
  Component,
  AfterViewInit,
  OnInit,
  ChangeDetectorRef,
} from '@angular/core';

import { Title }       from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material';

import {
  TdLoadingService,
  TdDialogService,
  TdMediaService,
  TdDataTableService,
  TdDataTableSortingOrder,
  ITdDataTableSortChangeEvent,
  ITdDataTableColumn,
  IPageChangeEvent,
} from '@covalent/core';

import { List }    from '@bitsmiths/mettle-braze';
import { xMettle } from '@bitsmiths/mettle-lib';

import { FuraAuthService }  from '@bitsmiths/fura-lib';
import { FuraBrazeService } from '@bitsmiths/fura-lib';

import {
  tRole,
  tRolestatus_Couplet
} from '@bitsmiths/fura-lib';

import * as moment from 'moment'

@Component({
  selector    :  'fura-role-enquiry',
  template    :  `
<td-layout-nav logo="assets:logo">
  <button mat-icon-button td-menu-button tdLayoutToggle>
    <mat-icon>menu</mat-icon>
  </button>

  <div td-toolbar-content layout="row" layout-align="center center" flex>
    <span>Role Management</span>
    <span flex></span>
  </div>

  <td-layout-manage-list #manageList
                        [opened]="media.registerQuery('gt-sm') | async"
                        [mode]="(media.registerQuery('gt-sm') | async) ? 'side' : 'push'"
                        [sidenavWidth]="(media.registerQuery('gt-xs') | async) ? '257px' : '100%'">

    <mat-toolbar td-sidenav-content>
      <span>Roles</span>
    </mat-toolbar>

    <mat-nav-list td-sidenav-content (click)="!media.query('gt-sm') && manageList.close()">
      <ng-template let-index="index" let-item ngFor [ngForOf]="navs">
        <a mat-list-item [ngClass]="{'active':item.statusFilter == searchStatus}" (click)="statusFilter(item.statusFilter, item.label);">
          <mat-icon mat-list-icon>{{ item.icon }}</mat-icon>
          {{ item.label }}
        </a>
      </ng-template>
    </mat-nav-list>

    <mat-toolbar>
      <div td-toolbar-content layout="row" layout-align="start center" flex>
        <button mat-icon-button tdLayoutManageListOpen [hideWhenOpened]="true">
          <mat-icon>arrow_back</mat-icon>
        </button>
        <span>{{ headerTitle }}</span>
      </div>
    </mat-toolbar>

    <mat-card class="push-bottom-xxl" tdMediaToggle="gt-xs" [mediaClasses]="['push']">
      <div layout="row" layout-align="start center" class="pad-left-sm pad-right-sm">
        <span class="push-left-sm">
          <button
            mat-icon-button
            (click)    = "load()"
            matTooltip = "Refresh"
          >
            <mat-icon>refresh</mat-icon>
          </button>

          <button
            mat-icon-button
            [routerLink] = "['add']"
            [disabled]   = "!authService.hasAccess(['fura.role.create'])"
            matTooltip   = "Add"
          >
            <mat-icon>add</mat-icon>
          </button>

          <button
            mat-icon-button
            [routerLink]        = "[selectedRoleId(), 'view']"
            [disabled]          = "selectedRoleId()=='' || !authService.hasAccess(['fura.role.read'])"
            matTooltip          = "View"
          >
            <mat-icon>remove_red_eye</mat-icon>
          </button>
          <button
            mat-icon-button
            [routerLink] = "[selectedRoleId(), 'edit']"
            [disabled]   = "selectedRoleId()=='' || !authService.hasAccess(['fura.role.read', 'fura.role.update'])"
            matTooltip   = "Edit"
          >
            <mat-icon>edit</mat-icon>
          </button>

          <button
            mat-icon-button
            [routerLink] = "[selectedRoleId(), 'func']"
            [disabled]   = "selectedRoleId()=='' || !notSuperUser() || !authService.hasAccess(['fura.rfr.menu'])"
            matTooltip   = "Functions">
            <mat-icon>vpn_key</mat-icon>
          </button>

          <button
            mat-icon-button
            (click)    = "delete(selectedRoleId())"
            [disabled] = "selectedRoleId()=='' || !authService.hasAccess(['fura.role.delete'])"
            matTooltip = "Delete"
          >
            <mat-icon>delete</mat-icon>
          </button>
        </span>

        <span *ngIf="searchBox.searchVisible" class="push-left-sm">
          <span *ngIf="selectedRows.length" class="mat-body-1">0 item(s) selected</span>
        </span>
        <td-search-box #searchBox backIcon="arrow_back" class="push-right-sm" placeholder="Search here" (searchDebounce)="search($event)" flex>
        </td-search-box>

      </div>

      <ng-template tdLoading="fura.role-enquiry"></ng-template>

      <mat-divider></mat-divider>

      <td-data-table
        #dataTable
        [data]       = "filteredData"
        [columns]    = "columns"
        [selectable] = "true"
        [multiple]   = "false"
        [sortable]   = "true"
        [sortBy]     = "sortBy"
        [sortOrder]  = "sortOrder"
        (sortChange) = "sort($event)"
        [(ngModel)]  = "selectedRows"
        >
        <ng-template tdDataTableTemplate="type" let-value="value" let-row="row" let-column="column">
        </ng-template>
      </td-data-table>

      <div class="mat-padding" *ngIf="!dataTable.hasData" layout="row" layout-align="center center">
        <h3>No results to display.</h3>
      </div>

      <td-paging-bar
        #pagingBar
        [pageSize]  = "pageSize"
        [total]     = "filteredTotal"
        (change)    = "page($event)">
        <span  hide-xs>Row per page:</span>
        <mat-select [style.width.px]="50" [(ngModel)]="pageSize">
          <mat-option *ngFor="let size of [16, 32, 64, 128]" [value]="size">
            {{size}}
          </mat-option>
        </mat-select>
        {{pagingBar.range}} <span hide-xs>of {{pagingBar.total}}</span>
      </td-paging-bar>
    </mat-card>

  </td-layout-manage-list>
</td-layout-nav>
`,
  styles   : [``],
})

export class RoleEnquiryComponent implements AfterViewInit, OnInit
{
 columns: ITdDataTableColumn[] = [
    { name: 'id',          label: 'ID',       numeric: false, width:150},
    { name: 'descr',       label: 'Description' },
    { name: 'status',      label: 'Status',   numeric: false, width:150, format: v=>tRolestatus_Couplet.getValue(v), filter:false },
    { name: 'level',       label: 'Level',    numeric: true,  width:100, filter:false },
    { name: 'sessTimeout', label: 'STO',      numeric: true,  width:100, tooltip: 'Session Timeout (Minutes)', filter:false },
    { name: 'audId',       label: 'Mod By',   numeric: false, width:200, filter:false },
    { name: 'tmStamp',     label: 'Mod Time', hidden: true },
  ];

  navs: any[] = [
    {id: 0, label: 'All Roles',        icon: 'start',              statusFilter: ''},
    {id: 1, label: 'Super User Roles', icon: 'verified_user',      statusFilter: 'S'},
    {id: 2, label: 'Active Roles',     icon: 'account_circle',     statusFilter: 'A'},
    {id: 3, label: 'Disabled Roles',   icon: 'highlight_off',      statusFilter: 'D'},
  ];

  roles         : List<tRole>             = new List<tRole>(tRole);
  filteredData  : List<tRole>             = this.roles;
  filteredTotal : number                  = this.roles.length;
  headerTitle   : string                  = 'All Roles';

  searchTerm    : string = '';
  searchStatus  : string = '';
  fromRow       : number = 1;
  currentPage   : number = 1;
  pageSize      : number = 16;
  selectedRows  : List<tRole>             = new List<tRole>(tRole);
  sortBy        : string                  = 'descr';
  sortOrder     : TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;

  constructor(
    private _furaService       : FuraBrazeService,
    private _titleService      : Title,
    private _loadingService    : TdLoadingService,
    private _dialogService     : TdDialogService,
    private _snackBarService   : MatSnackBar,
    private _changeDetectorRef : ChangeDetectorRef,
    private _dataTableService  : TdDataTableService,
    public   authService       : FuraAuthService,
    public   media             : TdMediaService)
  {
  }

  ngOnInit(): void {
    this._titleService.setTitle('Roles');
    this.load();
  }

  ngAfterViewInit(): void {
    this.media.broadcast();
    this._changeDetectorRef.detectChanges();
  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void {
    this.sortBy    = sortEvent.name;
    this.sortOrder = sortEvent.order;
    this.filter();
  }

  search(searchTerm: string): void {
    this.searchTerm = searchTerm;
    this.filter();
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow     = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize    = pagingEvent.pageSize;
    this.filter();
  }

  load(): void {
    this._loadingService.register('fura.role-enquiry');
    this.roles.length        = 0;
    this.filteredData.length = 0;
    this.selectedRows.length = 0;

    this._furaService.client.roleRead("*")
      .then((roles: List<tRole>) => {
        this.roles = roles;
        this.filter();
        this._loadingService.resolve('fura.role-enquiry');
      })
      .catch((err: xMettle) => {
        this.roles.length = 0;
        this._loadingService.resolve('fura.role-enquiry');
        this._dialogService.openAlert({message: 'Error loading roles: ' + this._furaService.handleError(err)});
      });
  }

  filter(): void {
    let newData         : any      = this.roles;
    let excludedColumns : string[] = this.columns

    .filter((column: ITdDataTableColumn) => {
      return ((column.filter === undefined && column.hidden === true) ||
              (column.filter !== undefined && column.filter === false));
    }).map((column: ITdDataTableColumn) => {
      return column.name;
    });

    newData            = this._dataTableService.filterData(newData, this.searchTerm, true, excludedColumns);

    if (this.searchStatus != '') {
      let filtColumns : string[] = this.columns

      .filter((column: ITdDataTableColumn) => {
        return column.name != 'status';
      }).map((column: ITdDataTableColumn) => {
      return column.name;
      });

      newData = this._dataTableService.filterData(newData, this.searchStatus, false, filtColumns);
    }

    this.filteredTotal = newData.length;
    newData            = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
    newData            = this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
    this.filteredData  = newData;
  }

  selectedRoleId(): string {
    if (this.selectedRows == null || this.selectedRows.length == 0)
      return '';

    return (<tRole>this.selectedRows[0]).id;
  }

  statusFilter(status: string, label: string) : void {
    this.searchStatus = status;
    this.headerTitle  = label;
    this.filter();
  }

  _delete(id: string): void {
    this._loadingService.register('fura.role-enquiry');

    this._furaService.client.roleDelete(id)
      .then(() => {
        this._loadingService.resolve('fura.role-enquiry');
        this.load();
      })
      .catch((err: xMettle) => {
        this._loadingService.resolve('fura.role-enquiry');
        this._dialogService.openAlert({message: 'Error deleting role: ' + this._furaService.handleError(err)});
      });
    }

  delete(id: string): void {
    this._dialogService
       .openConfirm({message: 'Are you sure you want to delete this role?'})
       .afterClosed().toPromise().then((confirm: boolean) => {
         if (confirm) {
           this._delete(id);
         }
       });
  }

  notSuperUser(): boolean {
    return this.selectedRows.length != 0 && (<tRole>this.selectedRows[0]).status != tRolestatus_Couplet.key_Super_User;
  }
}
