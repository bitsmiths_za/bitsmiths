/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import {
  Component,
  OnInit,
  ChangeDetectorRef,
 }                         from '@angular/core';

import {
  ActivatedRoute,
  Router,
} from '@angular/router';

import { MatSnackBar }      from '@angular/material';

import {
  TdDialogService,
  TdLoadingService,
  TdMediaService,
  CovalentExpansionPanelModule
} from '@covalent/core';

import {
  TdDataTableService,
  TdDataTableSortingOrder,
  ITdDataTableSortChangeEvent,
  ITdDataTableColumn,
  ITdDataTableRowClickEvent,
} from '@covalent/core';

//import { SNACKBAR_DURATION } from '../../../config/sys.config';

import { List, StringList }  from '@bitsmiths/mettle-braze';
import { xMettle }           from '@bitsmiths/mettle-lib';

import {
  tRole,
  tFunc,
  tFuncaction_Couplet,
  tFuncoride_Couplet,
  tFuncGrp,
  bRoleFuncRel
} from '@bitsmiths/fura-lib';

import { FuraBrazeService }  from '@bitsmiths/fura-lib';

@Component({
  selector    :  'fura-role-function',
  template    :  `
<td-layout-nav logo="assets:logo">
  <button mat-icon-button td-menu-button tdLayoutToggle>
    <mat-icon>menu</mat-icon>
  </button>

  <div td-toolbar-content layout="row" layout-align="center center" flex>
    <span>Role Functions</span>
    <span flex></span>
  </div>

  <td-layout-card-over cardTitle="Role Function Maintenance" cardSubtitle="{{roleRec.descr}}" cardWidth="80">

    <div *tdLoading="'fura.role-function'; type:'circular'; mode:'indeterminate'; strategy:'overlay'; color:'primary'">

      <mat-card-content class="push-bottom-none">
        <form #roleFuncForm="ngForm">

            <ng-template ngFor let-fgrp [ngForOf]="cacheFuncGrps">

              <td-expansion-panel
                label      ="{{fgrp.descr}}"
                sublabel   ="{{funcAssignedCount(fgrp.id)}} / {{funcCount(fgrp.id)}}"
                [disabled] ="false">

                <td-data-table
                  [data]       = "rfRels[fgrp.id]"
                  [columns]    = "columns"
                  [sortable]   = "false"
                  [clickable]  = "true"
                  (rowClick)   = "rowClicked($event)"
                  >
                  <ng-template tdDataTableTemplate="on" let-value="value" let-row="row" let-column="column">
                    <div layout="row">
                      <mat-icon
                        [ngStyle] = "{'color':getAccessColor(row[column])}"
                        >
                        {{getAccesIcon(row[column])}}
                      </mat-icon>
                    </div>
                  </ng-template>
                </td-data-table>

              </td-expansion-panel>

            </ng-template>

        </form>
      </mat-card-content>

      <mat-divider></mat-divider>

      <mat-card-actions>
        <button mat-button [disabled]="!roleFuncForm.form.valid" color="primary" (click)="save()">SAVE</button>
        <button mat-button (click)="goBack()">CANCEL</button>
      </mat-card-actions>

    </div>

  </td-layout-card-over>
</td-layout-nav>
`,
  styles   : [``],
})
export class RoleFunctionComponent implements OnInit {

 columns: ITdDataTableColumn[] = [
    { name: 'on',      label: 'Access',      width:50 },
    { name: 'descr',   label: 'Description'},
    { name: 'funcId',  label: 'ID',          width:200 },
    { name: 'action',  label: 'Action',      width:150, format: v=>tFuncaction_Couplet.getValue(v) },
    { name: 'oride',   label: 'Override',    width:100, format: v=>tFuncoride_Couplet.getValue(v) },
    { name: 'alvl',    label: 'Audit Level', numeric:true, width:100},
  ];

  roleId         : string;
  roleRec        : tRole                   = new tRole();
  rfRels         : {[key: string]: any[] } = {};
  cacheFuncGrps  : List<tFuncGrp>          = new List<tFuncGrp>(tFuncGrp);
  cacheFuncs     : List<tFunc>             = new List<tFunc>(tFunc);
  funcActions    : string[]                = tFuncaction_Couplet.getKeys();
  funcOverrides  : string[]                = tFuncoride_Couplet.getKeys();

  constructor(
    private _furaService       : FuraBrazeService,
    private _router            : Router,
    private _route             : ActivatedRoute,
    private _snackBarService   : MatSnackBar,
    private _changeDetectorRef : ChangeDetectorRef,
    private _loadingService    : TdLoadingService,
    private _dialogService     : TdDialogService,
    public  media              : TdMediaService)
  {
  }

  goBack(): void
  {
    this._router.navigate(['..'], {relativeTo: this._route});
  }

  ngAfterViewInit(): void
  {
    this.media.broadcast();
    this._changeDetectorRef.detectChanges();
  }

  ngOnInit(): void
  {
    this._route.params.subscribe((params: {id: string}) =>
    {
      this.roleId = params.id;
      this.load();
    });
  }

  load(): void
  {
    this._loadingService.register('fura.role-function');

    this.rfRels = {};

    this.cacheFuncGrps._clear();
    this.cacheFuncs._clear();

    let promises = [];

    promises.push(this._furaService.client.roleRead(this.roleId));
    promises.push(this._furaService.client.funcGrpRead('*'));
    promises.push(this._furaService.client.funcRead('*', '*'));
    promises.push(this._furaService.client.roleFuncRead(this.roleId, '*'));

    Promise.all(promises)
      .then( (proms) =>
      {
        let roles = (<List<tRole>>proms[0]);
        let fgrps = (<List<tFuncGrp>>proms[1]);
        let funcs = (<List<tFunc>>proms[2]);
        let rfrs  = (<List<bRoleFuncRel>>proms[3]);

        if (roles.length === 0)
        {
          this._dialogService.openAlert({message: 'Role [' + this.roleId + '] not found!'});
          return;
        }
        else
          this.roleRec = (<tRole>roles[0]);

        this.cacheFuncGrps = fgrps;
        this.cacheFuncs    = funcs;

        for (let xgrp of this.cacheFuncGrps) {
          this.rfRels[(<tFuncGrp>xgrp).id] = [];

          for (let xfunc of this.cacheFuncs) {
            if ((<tFuncGrp>xgrp).id == (<tFunc>xfunc).funcGrpId)
              this.rfRels[(<tFuncGrp>xgrp).id].push({
                grpId  : (<tFunc>xfunc).funcGrpId,
                funcId : (<tFunc>xfunc).id,
                descr  : (<tFunc>xfunc).descr,
                on     : false,
                action : (<tFunc>xfunc).action,
                oride  : (<tFunc>xfunc).oride,
                alvl   : (<tFunc>xfunc).auditLvl,
                mod    : false,
              })
          }
        }

        for (let xrfr of rfrs) {
          let obj = this.rfRels[(<bRoleFuncRel>xrfr).fgrpId];

          if (obj === undefined)
            continue;

          for (let ff of obj) {
            if (ff.funcId == (<bRoleFuncRel>xrfr).funcId)
              ff.on = true;
          }
        }

        this._loadingService.resolve('fura.role-function');
      })
      .catch( (err: xMettle) =>
      {
        this._loadingService.resolve('fura.role-function');
        this._dialogService.openAlert({message: 'Error loading role function data: ' + this._furaService.handleError(err)});
      });
  }

  save(): void
  {
    this._loadingService.register('fura.role-function');

    let addRoles : StringList = new StringList();
    let remRoles : StringList = new StringList();
    let ok       : boolean    = true;

    for (let xgrp of this.cacheFuncGrps)
    {
      let rflist = this.rfRels[(<tFuncGrp>xgrp).id];

      for (let xrec of rflist)
        if (xrec.mod === true)
        {
          if (xrec.on)
            addRoles.push(xrec.funcId);
          else
            remRoles.push(xrec.funcId);
        }
    }

    let promises = []

    if (remRoles.length > 0)
      promises.push(this._furaService.client.roleFuncRem(this.roleId, remRoles));

    if (addRoles.length > 0)
      promises.push(this._furaService.client.roleFuncAdd(this.roleId, addRoles));

    Promise.all(promises)
      .then( (proms) =>
      {
        this._snackBarService.open('Role Functions Saved', 'OK', {'duration': 2 }); //SNACKBAR_DURATION});
        this._loadingService.resolve('fura.role-function');
        this.load();
      })
      .catch ( (err: xMettle ) =>
      {
        this._loadingService.resolve('fura.role-function');
        this._dialogService.openAlert({message: 'Error saving role functions: ' + this._furaService.handleError(err)});
      });
  }

  funcCount(funcGrpId: string): number
  {
    let cnt = 0;

    for (let xrec of this.cacheFuncs)
    {
      if ( (<tFunc>xrec).funcGrpId === funcGrpId)
        cnt += 1;
    }

    return cnt;
  }

  funcAssignedCount(funcGrpId: string): number
  {
    let obj = this.rfRels[funcGrpId];
    let cnt = 0;

    if (obj === undefined)
      return cnt;

    for (let xrec of obj)
      if (xrec.grpId === funcGrpId && xrec.on === true)
        cnt += 1;

    return cnt;
  }

  getAccesIcon(onoff: boolean): string
  {
    if (onoff)
      return "check_circle";

    return "remove_circle";
  }

  getAccessColor(onoff: boolean): string
  {
    if (onoff)
      return "green";

    return "red";
  }

  rowClicked(rowEvent: ITdDataTableRowClickEvent): void
  {
    rowEvent.row.mod = !rowEvent.row.mod;
    rowEvent.row.on  = !rowEvent.row.on;
  }
}
