/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';

import {
  TdDialogService,
  TdLoadingService
} from '@covalent/core';

import { List }    from '@bitsmiths/mettle-braze';
import { xMettle } from '@bitsmiths/mettle-lib';

import { TextService } from '@bs-lib/lib';

import {
  tRole,
  tUsr,
  tUsrstatus_Couplet,
} from '@bitsmiths/fura-lib';

import { FuraAuthService }  from '@bitsmiths/fura-lib';
import { FuraBrazeService } from '@bitsmiths/fura-lib';

//import { SNACKBAR_DURATION } from '../../../config/sys.config';

@Component({
  selector    :  'fura-user-detail',
  template    :  `
<td-layout-nav logo="assets:logo">
  <button mat-icon-button td-menu-button tdLayoutToggle>
    <mat-icon>menu</mat-icon>
  </button>

  <div td-toolbar-content layout="row" layout-align="center center" flex>
    <span>User</span>
    <span flex></span>
  </div>

  <td-layout-card-over cardTitle="User Detail" cardSubtitle="{{subTitle}}">
    <ng-template tdLoading="fura.user-enquiry">
      <mat-card-content class="push-bottom-none">
        <form #userForm="ngForm">

          <div layout="row" class="push-top">
            <mat-form-field flex>
              <input matInput
                      #idElement
                      #idControl  = "ngModel"
                      type        = "text"
                      placeholder = "Username"
                      [(ngModel)] = "user.id"
                      [disabled]  = "readOnly() || action!=='add'"
                      name        = "id"
                      maxlength   = "128"
                      required>
              <mat-error align="start">
                <span [hidden]="idControl.pristine">
                  <span [hidden]="!idControl.hasError('required')">Required</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{idElement.value.length}} / {{idElement.maxLength}}</mat-hint>
            </mat-form-field>
          </div>

          <div layout="row" class="push-top">
            <mat-form-field flex="45">
              <input matInput
                      #titleElement
                      #titleControl = "ngModel"
                      type          = "text"
                      placeholder   = "Title"
                      [(ngModel)]   = "user.title"
                      [disabled]    = "readOnly()"
                      name          = "title"
                      maxlength     = "5">
              <mat-hint align="end">{{titleElement.value.length}} / {{titleElement.maxLength}}</mat-hint>
            </mat-form-field>
          </div>

          <div layout="row" class="push-top">
            <mat-form-field flex>
              <input matInput
                      #nameFirstElement
                      #nameFirstControl = "ngModel"
                      type              = "text"
                      placeholder       = "First Name"
                      [(ngModel)]       = "user.nameFirst"
                      [disabled]        = "readOnly()"
                      name              = "nameFirst"
                      maxlength         = "128"
                      required>
              <mat-error align="start">
                <span [hidden]="nameFirstControl.pristine">
                  <span [hidden]="!nameFirstControl.hasError('required')">Required</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{nameFirstElement.value.length}} / {{nameFirstElement.maxLength}}</mat-hint>
            </mat-form-field>
          </div>

          <div layout="row" class="push-top">
            <mat-form-field flex>
              <input matInput
                      #nameLastElement
                      #nameLastControl = "ngModel"
                      type             = "text"
                      placeholder      = "Last Name"
                      [(ngModel)]      = "user.nameLast"
                      [disabled]       = "readOnly()"
                      name             = "nameLast"
                      maxlength        = "128"
                      required>
              <mat-error align="start">
                <span [hidden]="nameLastControl.pristine">
                  <span [hidden]="!nameLastControl.hasError('required')">Required</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{nameLastElement.value.length}} / {{nameLastElement.maxLength}}</mat-hint>
            </mat-form-field>
          </div>

          <div layout="row" class="push-top">
            <mat-form-field flex="45">
              <input matInput
                      #emailElement
                      #emailControl = "ngModel"
                      type          = "text"
                      placeholder   = "Email"
                      [(ngModel)]   = "user.email1"
                      [disabled]    = "readOnly()"
                      name          = "email"
                      maxlength     = "256"
                      pattern       = "^[a-zA-Z0-9]+(\\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{2,15})$"
                      required>
              <mat-error align="start">
                <span [hidden]="emailControl.pristine">
                  <span [hidden]="!emailControl.hasError('required')">Required</span>
                  <span [hidden]="!emailControl.hasError('pattern')">Invalid email address</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{emailElement.value.length}} / {{emailElement.maxLength}}</mat-hint>
            </mat-form-field>

            <div flex="10"></div>

            <mat-form-field flex="45">
              <input matInput
                      #email2Element
                      #email2Control = "ngModel"
                      type           = "text"
                      placeholder    = "Backup Email"
                      [(ngModel)]    = "user.email2"
                      [disabled]     = "readOnly()"
                      name           = "email2"
                      maxlength      = "256"
                      pattern        = "^[a-zA-Z0-9]+(\\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{2,15})$">
              <mat-error align="start">
                <span [hidden]="email2Control.pristine">
                  <span [hidden]="!email2Control.hasError('pattern')">Invalid email address</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{email2Element.value.length}} / {{email2Element.maxLength}}</mat-hint>
            </mat-form-field>
          </div>

          <div layout="row" class="push-top">
            <mat-form-field flex="45">
              <input matInput
                      #cellElement
                      #cellControl = "ngModel"
                      type         = "text"
                      placeholder  = "Cell Number"
                      [(ngModel)]  = "user.cellNo1"
                      [disabled]   = "readOnly()"
                      name         = "cell"
                      maxlength    = "32"
                      pattern      = "^((?:\\+27|27)|0)(\\d{2})(\\d{7})$"
                      required>
              <mat-error align="start">
                <span [hidden]="cellControl.pristine">
                  <span [hidden]="!cellControl.hasError('required')">Required</span>
                  <span [hidden]="!cellControl.hasError('pattern')">Invalid cell number</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{cellElement.value.length}} / {{cellElement.maxLength}}</mat-hint>
            </mat-form-field>

            <div flex="10"></div>

            <mat-form-field flex="45">
              <input matInput
                      #cell2Element
                      #cell2Control = "ngModel"
                      type          = "text"
                      placeholder   = "Backup Cell Number"
                      [(ngModel)]   = "user.cellNo2"
                      [disabled]    = "readOnly()"
                      name          = "cell2"
                      maxlength     = "32"
                      pattern       = "^((?:\\+27|27)|0)(\\d{2})(\\d{7})$">
              <mat-error align="start">
                <span [hidden]="cell2Control.pristine">
                  <span [hidden]="!cell2Control.hasError('pattern')">Invalid cell number</span>
                </span>
              </mat-error>
              <mat-hint align="end">{{cell2Element.value.length}} / {{cell2Element.maxLength}}</mat-hint>
            </mat-form-field>
          </div>

          <div layout="row" class="push-top">
            <mat-form-field flex="45">
              <mat-select
                #statusElement
                #statusControl = "ngModel"
                placeholder    = "Status"
                [(ngModel)]    = "user.status"
                [disabled]     = "readOnly()"
                name           = "statusSelect"
                required
              >
                <mat-option *ngFor="let status of statuses" [value]="status">{{ getStatusDescr(status) }}</mat-option>
              </mat-select>
              <mat-error align="start">
                <span [hidden]="statusControl.pristine">
                  <span [hidden]="!statusControl.hasError('required')">Required</span>
                </span>
              </mat-error>
            </mat-form-field>

            <div flex="10"></div>

            <mat-form-field flex="45">
              <input
                matInput
                #dateActivateControl = "ngModel"
                [matDatepicker]      = "dateActivateElement"
                placeholder          = "Activate Date"
                [(ngModel)]          = "user.dateActivate"
                [disabled]           = "readOnly()"
                name                 = "dateActivate"
                required
              >
              <mat-datepicker-toggle matSuffix [for]="dateActivateElement"></mat-datepicker-toggle>
              <mat-error align="start">
                <span [hidden]="dateActivateControl.pristine">
                  <span [hidden]="!dateActivateControl.hasError('required')">Required</span>
                </span>
              </mat-error>
            </mat-form-field>
            <mat-datepicker touchUi="true" #dateActivateElement></mat-datepicker>
          </div>

          <div layout="row" class="push-top">
            <mat-form-field flex="45">
              <mat-select
                #roleElement
                #roleControl = "ngModel"
                placeholder  = "role"
                [(ngModel)]  = "user.roleId"
                name         = "roleSelect"
                [disabled]   = "readOnly()"
                required
              >
                <mat-option *ngFor="let role of roles" [value]="role.id">{{ role.descr }}</mat-option>
              </mat-select>
              <mat-error align="start">
                <span [hidden]="roleControl.pristine">
                  <span [hidden]="!roleControl.hasError('required')">Required</span>
                </span>
              </mat-error>
            </mat-form-field>

            <div flex="10"></div>

            <mat-form-field flex="45">
              <input
                matInput
                #dateActivateControl  = "ngModel"
                [matDatepicker]       = "dateExpireElement"
                placeholder           = "Expire Date"
                [(ngModel)]           = "user.dateExpire"
                name                  = "dateExpire"
                [disabled]            = "readOnly()"
              >
              <mat-datepicker-toggle matSuffix [for]="dateExpireElement"></mat-datepicker-toggle>
            </mat-form-field>
            <mat-datepicker touchUi="true" #dateExpireElement></mat-datepicker>
          </div>

          <!-- Audit ID -->
          <div layout="row" class="push-top" *ngIf="action!=='add'">
            <div flex="66"></div>
            <mat-form-field flex>
              <input matInput
                type          = "text"
                name          = "audId"
                [(ngModel)]   = "user.audId"
                required      = "false"
                [disabled]    = "true">
              <mat-hint align="start">Last Modified By</mat-hint>
            </mat-form-field>
          </div>

          <!-- Timestamp -->
          <div layout="row" class="push-top" *ngIf="action!=='add'" >
            <div flex="66"></div>
            <mat-form-field flex>
              <input matInput
                type          = "text"
                name          = "audTmStamp"
                [ngModel]     = "textService.formatDateTime(user.tmStamp)"
                required      = "false"
                [disabled]    = "true">
              <mat-hint align="start">Last Modified Timestamp</mat-hint>
            </mat-form-field>
          </div>

        </form>
      </mat-card-content>
      <mat-divider></mat-divider>
      <mat-card-actions>
        <button
          mat-button
          [disabled] = "!userForm.form.valid || !canSave()"
          color      = "primary"
          *ngIf      = "!readOnly()"
          (click)    = "save()"
        >
          SAVE
        </button>

        <button
          mat-button
          (click) = "goBack()"
          *ngIf   = "!readOnly()"
        >
          CANCEL
        </button>

        <button
          mat-button
          (click) = "goBack()"
          *ngIf   = "readOnly()"
          color   = "primary"
        >
          BACK
        </button>

      </mat-card-actions>
    </ng-template>
  </td-layout-card-over>
</td-layout-nav>
`,
  styles   : [``],
})
export class UserDetailComponent implements OnInit {

  statuses : string[]    = tUsrstatus_Couplet.getKeys();
  action   : string|null = null;
  roles    : List<tRole> = new List<tRole>(tRole);
  user     : tUsr        = new tUsr();
  id       : string      = "";
  subTitle : string      = "";

  constructor(
    private _furaService     : FuraBrazeService,
    private _router          : Router,
    private _route           : ActivatedRoute,
    private _snackBarService : MatSnackBar,
    private _loadingService  : TdLoadingService,
    private _dialogService   : TdDialogService,
    public  textService      : TextService,
    public  authService      : FuraAuthService )
  {
    this.user.dateActivate = new Date();
    this.user.status       = tUsrstatus_Couplet.key_Active;
  }

  goBack(): void
  {
    this._router.navigate(['..'], {relativeTo: this._route});
  }

  ngOnInit(): void
  {
    this._route.url.subscribe((url: any) =>
    {
      if (url.length == 2)
        this.action = url[1] == "edit" ? "edit" : "view";
      else if (url.length == 1)
        this.action = url[0] == "add" ? "add" : "view";

      if (this.action == "add")
        this.subTitle = "Fill form to create new user";
      else if (this.action == "edit")
        this.subTitle = "Update user";
      else
        this.subTitle = "Read only";
    });

    this._route.params.subscribe((params: {id: string}) =>
    {
      this.id = params.id;

      if (this.id != null)
        this.loadViewEdit();
      else
        this.loadAdd();
    });
  }

  getStatusDescr(status: string): string
  {
    return tUsrstatus_Couplet.getValue(status);
  }

  loadAdd(): void
  {
    this._loadingService.register('fura.user-detail');

    this._furaService.client.roleRead("")
      .then( (roles: List<tRole>) =>
      {
        this.roles = roles;
        this._loadingService.resolve('fura.user-detail');
      })
      .catch( (err: xMettle) =>
      {
        this._loadingService.resolve('fura.user-detail');
        this._dialogService.openAlert({message: 'Error loading user: ' + this._furaService.handleError(err)});
      });
  }

  loadViewEdit()
  {
    this._loadingService.register('fura.user-detail');

    let promises = []

    promises.push(this._furaService.client.roleRead(""));
    promises.push(this._furaService.client.userRead(this.id, "sys"));

    Promise.all(promises)
      .then( (proms) =>
      {
        this.roles = proms[0];

        if (proms[1].length > 0)
        {
          this.user = <tUsr> proms[1][0];
        }
        else
        {
          this._dialogService.openAlert({message: 'User ' + this.id + ' could not be found'});
        }

        this._loadingService.resolve('fura.user-detail');
      })
      .catch( (err: xMettle) =>
      {
        this._loadingService.resolve('fura.user-detail');
        this._dialogService.openAlert({message: 'Error loading user: ' + this._furaService.handleError(err)});
      });
  }

  canSave(): boolean
  {
    if (this.action === 'add')
      return this.authService.hasAccess(['fura.usr.create']);

    if (this.action === 'edit')
      return this.authService.hasAccess(['fura.usr.update']);

    return false;
  }

  readOnly(): boolean
  {
    return this.action !== 'add' && this.action !== 'edit';
  }

  save()
  {
    this._loadingService.register('fura.user-detail');

    if (this.action === 'add')
    {
      this.user.usrTypeId = "sys";
      this._furaService.client.userCreate(this.user)
        .then( (rec: tUsr) =>
        {
          this._snackBarService.open('User Saved', 'OK', {'duration': 2 });//SNACKBAR_DURATION});
          this._loadingService.resolve('fura.user-detail');
          this.goBack();
        })
        .catch( (err: xMettle) =>
        {
          this._loadingService.resolve('fura.user-detail');
          this._dialogService.openAlert({message: 'Error saving user: ' + this._furaService.handleError(err)});
        });
    }
    else if (this.action === 'edit')
    {
      this._furaService.client.userUpdate(this.user)
        .then( (rec: tUsr) =>
        {
          this._snackBarService.open('User Saved', 'OK', {'duration': 2 }); //SNACKBAR_DURATION});
          this._loadingService.resolve('fura.user-detail');
          this.goBack();
        })
        .catch( (err: xMettle) =>
        {
          this._loadingService.resolve('fura.user-detail');
          this._dialogService.openAlert({message: 'Error saving user: ' + this._furaService.handleError(err)});
        });
    }

  }
}
