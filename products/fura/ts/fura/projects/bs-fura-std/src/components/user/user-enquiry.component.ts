/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import {
  Component,
  AfterViewInit,
  OnInit,
  ChangeDetectorRef,
} from '@angular/core';

import { Title }       from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material';

import {
  TdLoadingService,
  TdDialogService,
  TdMediaService,
  TdDataTableService,
  TdDataTableSortingOrder,
  ITdDataTableSortChangeEvent,
  ITdDataTableColumn,
  IPageChangeEvent,
} from '@covalent/core';

import { List }    from '@bitsmiths/mettle-braze';
import { xMettle } from '@bitsmiths/mettle-lib';

import { TextService } from '@bs-lib/lib';

import { FuraAuthService }  from '@bitsmiths/fura-lib';
import { FuraBrazeService } from '@bitsmiths/fura-lib';

import {
  tUsr,
  tUsrstatus_Couplet,
} from '@bitsmiths/fura-lib';


@Component({
  selector    :  'fura-user-enquiry',
  template    :  `
<td-layout-nav logo="assets:logo">
  <button mat-icon-button td-menu-button tdLayoutToggle>
    <mat-icon>menu</mat-icon>
  </button>

  <div td-toolbar-content layout="row" layout-align="center center" flex>
    <span>User Management</span>
    <span flex></span>
  </div>

  <td-layout-manage-list
    #manageList
    [opened]       = "media.registerQuery('gt-sm') | async"
    [mode]         = "(media.registerQuery('gt-sm') | async) ? 'side' : 'push'"
    [sidenavWidth] = "(media.registerQuery('gt-xs') | async) ? '257px' : '100%'">

    <mat-toolbar td-sidenav-content>
      <span>User Enquiry</span>
    </mat-toolbar>

    <mat-nav-list td-sidenav-content (click)="!media.query('gt-sm') && manageList.close()">
      <ng-template let-index="index" let-item ngFor [ngForOf]="navs">
        <a mat-list-item [ngClass]="{'active':item.statusFilter == searchStatus}" (click)="statusFilter(item.statusFilter, item.label);">
          <mat-icon mat-list-icon>{{ item.icon }}</mat-icon>
          {{ item.label }}
        </a>
      </ng-template>
    </mat-nav-list>

    <mat-toolbar>
      <div td-toolbar-content layout="row" layout-align="start center" flex>
        <button mat-icon-button tdLayoutManageListOpen [hideWhenOpened]="true">
          <mat-icon>arrow_back</mat-icon>
        </button>
        <span>{{ headerTitle }}</span>
      </div>
    </mat-toolbar>

    <mat-card class="push-bottom-xxl" tdMediaToggle="gt-xs" [mediaClasses]="['push']">
      <div layout="row" layout-align="start center" class="pad-left-sm pad-right-sm">
        <span class="push-left-sm">
          <button
            mat-icon-button
            (click)    = "load()"
            matTooltip = "Refresh">
            <mat-icon>refresh</mat-icon>
          </button>
          <button
            mat-icon-button
            [routerLink] = "['add']"
            [disabled]   = "!authService.hasAccess(['fura.usr.create'])"
            matTooltip   = "Add"
          >
            <mat-icon>add</mat-icon>
          </button>
          <button
            mat-icon-button
            [routerLink]        = "[selectedUserId(), 'view']"
            queryParamsHandling = "merge"
            [disabled]          = "selectedUserId()=='' || !authService.hasAccess(['fura.usr.read'])"
            matTooltip          = "View"
          >
            <mat-icon>remove_red_eye</mat-icon>
          </button>
          <button
            mat-icon-button
            [routerLink] = "[selectedUserId(), 'edit']"
            [disabled]   = "selectedUserId()=='' || !authService.hasAccess(['fura.usr.update'])"
            matTooltip   = "Edit"
          >
            <mat-icon>edit</mat-icon>
          </button>
          <button
            mat-icon-button
            (click)    = "delete(selectedUserId())"
            [disabled] = "selectedUserId()=='' || !authService.hasAccess(['fura.usr.delete'])"
            matTooltip = "Delete"
          >
            <mat-icon>delete</mat-icon>
          </button>
        </span>

        <span *ngIf="searchBox.searchVisible" class="push-left-sm">
          <span *ngIf="selectedRows.length" class="mat-body-1">0 item(s) selected</span>
        </span>
        <td-search-box #searchBox backIcon="arrow_back" class="push-right-sm" placeholder="Search here" (searchDebounce)="search($event)" flex>
        </td-search-box>

      </div>

      <mat-divider></mat-divider>

      <ng-template tdLoading="fura.user-enquiry">

        <td-data-table
          #dataTable
          [data]       = "filteredData"
          [columns]    = "columns"
          [selectable] = "true"
          [multiple]   = "false"
          [sortable]   = "true"
          [sortBy]     = "sortBy"
          [sortOrder]  = "sortOrder"
          (sortChange) = "sort($event)"
          [(ngModel)]  = "selectedRows">
          <ng-template tdDataTableTemplate="type" let-value="value" let-row="row" let-column="column">
            <div layout="row">
              <span flex>{{row[column]}}</span>
            </div>
          </ng-template>
        </td-data-table>

        <div class="mat-padding" *ngIf="!dataTable.hasData" layout="row" layout-align="center center">
          <h3>No results to display.</h3>
        </div>

      </ng-template>

      <td-paging-bar
        #pagingBar
        [pageSize]  = "pageSize"
        [total]     = "filteredTotal"
        (change)    = "page($event)">
        <span hide-xs>Row per page:</span>
        <mat-select [style.width.px]="50" [(ngModel)]="pageSize">
          <mat-option *ngFor="let size of [16, 32, 64, 128]" [value]="size">
            {{size}}
          </mat-option>
        </mat-select>
        {{pagingBar.range}} <span hide-xs>of {{pagingBar.total}}</span>
      </td-paging-bar>

    </mat-card>

  </td-layout-manage-list>
</td-layout-nav>
`,
  styles      : [``],
})
export class UserEnquiryComponent implements AfterViewInit, OnInit
{
  navs: any[] = [
    {id: 0, label: 'All Users',       icon: 'people',                      statusFilter: ''},
    {id: 1, label: 'Active Users',    icon: 'person',                      statusFilter: tUsrstatus_Couplet.key_Active},
    {id: 2, label: 'Disabled Users',  icon: 'sentiment_very_dissatisfied', statusFilter: tUsrstatus_Couplet.key_Disabled},
    {id: 3, label: 'Suspended Users', icon: 'sentiment_neutral',           statusFilter: tUsrstatus_Couplet.key_Suspended},
    {id: 4, label: 'Deleted Users',   icon: 'delete',                      statusFilter: tUsrstatus_Couplet.key_Deleted},
    {id: 5, label: 'Expired Users',   icon: 'person_outline',              statusFilter: tUsrstatus_Couplet.key_Expired},
  ];

 columns: ITdDataTableColumn[] = [
    { name: 'siteId',       label: 'Site ID', hidden: true },
    { name: 'id',           label: 'ID' },
    { name: 'nameFirst',    label: 'First Name' },
    { name: 'nameLast',     label: 'Last Name' },
    { name: 'status',       label: 'Status',    format: v=>tUsrstatus_Couplet.getValue(v) },
    { name: 'dateActivate', label: 'Activated', format: v=> this.textService.formatDate(v) },
    { name: 'dateExpire',   label: 'Expired',   format: v=> this.textService.formatDate(v), hidden: true },
    { name: 'email1',       label: 'Email',         hidden: true },
    { name: 'email2',       label: 'Backup Email',  hidden: true },
    { name: 'cellNo1',      label: 'Cell No',       hidden: true },
    { name: 'cellNo2',      label: 'Backup CellNo', hidden: true },
    { name: 'audId',        label: 'Mod By',        hidden: true },
    { name: 'tmStamp',      label: 'Mod Time',      hidden: true },
  ];

  userList      : List<tUsr>  = new List<tUsr>(tUsr);
  filteredData  : List<tUsr>  = this.userList;
  filteredTotal : number      = this.userList.length;
  headerTitle   : string      = 'All Users';

  searchTerm    : string = '';
  searchStatus  : string = '';
  fromRow       : number = 1;
  currentPage   : number = 1;
  pageSize      : number = 16;
  selectedRows  : List<tUsr>              = new List<tUsr>(tUsr);
  sortBy        : string                  = 'id';
  sortOrder     : TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;

  constructor(
    private _furaService       : FuraBrazeService,
    private _titleService      : Title,
    private _loadingService    : TdLoadingService,
    private _dialogService     : TdDialogService,
    private _snackBarService   : MatSnackBar,
    private _changeDetectorRef : ChangeDetectorRef,
    private _dataTableService  : TdDataTableService,
    public  textService        : TextService,
    public  authService        : FuraAuthService,
    public  media              : TdMediaService)
  {
  }

  ngOnInit(): void
  {
    this._titleService.setTitle('Users');
    this.load();
  }

  ngAfterViewInit(): void
  {
    this.media.broadcast();
    this._changeDetectorRef.detectChanges();
  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void
  {
    this.sortBy    = sortEvent.name;
    this.sortOrder = sortEvent.order;
    this.filter();
  }

  search(searchTerm: string): void
  {
    this.searchTerm = searchTerm;
    this.filter();
  }

  page(pagingEvent: IPageChangeEvent): void
  {
    this.fromRow     = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize    = pagingEvent.pageSize;
    this.filter();
  }

  load()
  {
    this._loadingService.register('fura.user-enquiry');

    this.userList.length      = 0;
    this.filteredData.length  = 0;

    this._furaService.client.userRead("", "sys")
      .then((userList: List<tUsr>) =>
      {
        this.userList = userList;
        this.filter();
        this._loadingService.resolve('fura.user-enquiry');
      })
      .catch((err: xMettle) =>
      {
        this._loadingService.resolve('fura.user-enquiry');
        this._dialogService.openAlert({message: 'Error loading users: ' + this._furaService.handleError(err)});
      });
  }

  filter(): void
  {
    let newData         : any      = this.userList;
    let excludedColumns : string[] = this.columns
      .filter((column: ITdDataTableColumn) =>
      {
        return ((column.filter === undefined && column.hidden === true) ||
                (column.filter !== undefined && column.filter === false));
      }).map((column: ITdDataTableColumn) =>
      {
        return column.name;
      });

    newData = this._dataTableService.filterData(newData, this.searchTerm, true, excludedColumns);

    if (this.searchStatus != '')
    {
      let filtColumns : string[] = this.columns
        .filter((column: ITdDataTableColumn) =>
        {
          return column.name != 'status';
        }).map((column: ITdDataTableColumn) =>
        {
          return column.name;
        });

      newData = this._dataTableService.filterData(newData, this.searchStatus, false, filtColumns);
    }

    this.filteredTotal = newData.length;
    newData            = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
    newData            = this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
    this.filteredData  = newData;
  }

  selectedUserId(): string {
    if (this.selectedRows == null || this.selectedRows.length == 0)
      return '';

    return (<tUsr>this.selectedRows[0]).id;
  }

  statusFilter(status: string, label: string) : void
  {
    this.searchStatus = status;
    this.headerTitle  = label;
    this.filter();
  }

  delete(id: string)
  {
    this._dialogService
      .openConfirm({message: 'Are you sure you want to delete this user?'})
      .afterClosed()
      .subscribe((confirm: boolean) =>
      {
        if (confirm)
          this._delete(id);
      });
  }

  _delete(id: string)
  {
    this._loadingService.register('fura.user-enquiry');

    this._furaService.client.userDelete(id)
      .then(() =>
      {
        this._loadingService.resolve('fura.user-enquiry');
        this.load();
      })
      .catch((err: xMettle) =>
      {
        this._loadingService.resolve('fura.user-enquiry');
        this._dialogService.openAlert({message: 'Error deleting user: ' + this._furaService.handleError(err)});
      });
  }
}
