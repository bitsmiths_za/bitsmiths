/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';
import { RouterModule } from '@angular/router';

import {
 MatSnackBarModule,
 MatIconModule,
 MatListModule,
 MatTooltipModule,
 MatCardModule,
 MatButtonModule,
 MatToolbarModule,
 MatInputModule,
 MatSelectModule,
 MatSlideToggleModule,
 MatMenuModule,
 MatDatepickerModule,
 MatNativeDateModule,
 MatSliderModule,
} from '@angular/material';

import {
  CovalentLoadingModule,
  CovalentDialogsModule,
  CovalentMediaModule,
  CovalentLayoutModule,
  CovalentSearchModule,
  CovalentCommonModule,
  CovalentDataTableModule,
  CovalentPagingModule,
  CovalentExpansionPanelModule,
} from '@covalent/core';

import {
  FuraAuthService,
  FuraBrazeService,
  FuraErrorCodes,
} from '@bitsmiths/fura-lib';

import { FieldMatchesValidatorDirective } from './directives/fieldmatches.directive';

import { UserEnquiryComponent }      from './components/user/user-enquiry.component';
import { UserDetailComponent }       from './components/user/user-detail.component';
import { LoginComponent }            from './components/login/login.component';
import { ResetPassComponent }        from './components/resetpass/reset-pass.component';
import { RoleEnquiryComponent }      from './components/role/role-enquiry.component';
import { RoleDetailComponent }       from './components/role/role-detail.component';
import { RoleFunctionComponent }     from './components/role/role-function.component';

import { AuthGuardService } from './services/auth-guard.service';

import {
  routedComponents,
  FuraStdRoutingModule,
} from './fura-std.routes';

// export {
//   LoginComponent,
//   ResetPassComponent,
// };

@NgModule({
  declarations: [
    FieldMatchesValidatorDirective,
    routedComponents
  ], // directives, components, and pipes owned by this NgModule
  imports: [
    // angular modules
    CommonModule,
    FormsModule,
    RouterModule,

    //material modules
    MatSnackBarModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSliderModule,

    // covalent modules
    CovalentLoadingModule,
    CovalentDialogsModule,
    CovalentMediaModule,
    CovalentLayoutModule,
    CovalentSearchModule,
    CovalentCommonModule,
    CovalentDataTableModule,
    CovalentPagingModule,
    CovalentExpansionPanelModule,

    // extra
    FuraStdRoutingModule,
  ], // modules needed to run this module
  providers: [
    AuthGuardService,
  ],
  exports: [
    RouterModule,
  ]
})
export class FuraStdModule {}
