/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import {
  NgModule,
  ModuleWithProviders,
} from '@angular/core';

import {
  RouterModule,
  Routes
} from '@angular/router';

import { LoginComponent }         from './components/login/login.component';
import { ResetPassComponent }     from './components/resetpass/reset-pass.component';

import { UserEnquiryComponent }   from './components/user/user-enquiry.component';
import { UserDetailComponent }    from './components/user/user-detail.component';
import { RoleEnquiryComponent }   from './components/role/role-enquiry.component';
import { RoleDetailComponent }    from './components/role/role-detail.component';
import { RoleFunctionComponent }  from './components/role/role-function.component';

import { AuthGuardService }       from './services/auth-guard.service';

const routes: Routes = [
  { path : 'login',     component: LoginComponent },
  { path : 'resetpass', component: ResetPassComponent },
  { path : 'user',
    children : [
      { path: '',              component: UserEnquiryComponent, canActivate: [AuthGuardService] },
      { path: 'add',           component: UserDetailComponent },
      { path: ':id/edit',      component: UserDetailComponent },
      { path: ':id/view',      component: UserDetailComponent },
      { path: '**',            redirectTo: '' },
    ],
  },
  { path : 'role',
    children : [
      { path: '',            component: RoleEnquiryComponent, canActivate: [AuthGuardService] },
      { path: 'add',         component: RoleDetailComponent },
      { path: ':id/edit',    component: RoleDetailComponent },
      { path: ':id/view',    component: RoleDetailComponent },
      { path: ':id/func',    component: RoleFunctionComponent, canActivate: [AuthGuardService] },
      { path: '**',          redirectTo: '' },
    ],
  },
];

//export const furaStdRoutes: ModuleWithProviders = RouterModule.forChild(routes);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FuraStdRoutingModule { }

export const routedComponents = [
  LoginComponent,
  ResetPassComponent,
  UserEnquiryComponent,
  UserDetailComponent,
  RoleEnquiryComponent,
  RoleDetailComponent,
  RoleFunctionComponent,
];
