/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_LIB_IPROVIDER_H_
#define __BS_LIB_IPROVIDER_H_

namespace Mettle
{

namespace Braze
{
   class IServerMarshaler;
   class ITransport;
   class Server;
}

namespace DB
{
   class IConnect;
}

namespace Lib
{
	 class Logger;
}

namespace Util
{
	 class CmdLineArgs;
   class XmlConfig;
}

}

namespace BS {

namespace Lib {

class IContentManager;
class Pod;

class IProvider
{
public:
   virtual ~IProvider() noexcept;

   virtual Mettle::Braze::Server     *newBrazeServer(Mettle::Lib::Logger              *logger,
                                                     Mettle::Braze::IServerMarshaler  *marshaler,
                                                     Mettle::Braze::ITransport        *trans) = 0;

   virtual Mettle::Braze::ITransport *newBrazeTransport(Mettle::Util::XmlConfig *cfg) = 0;

   virtual Mettle::Util::CmdLineArgs *newCmdLineArgs(const int argc, const char **argv) = 0;

   virtual Mettle::Util::XmlConfig   *newConfig(Mettle::Util::CmdLineArgs *cargs) = 0;

   virtual Mettle::DB::IConnect      *newDBConnector() = 0;

   virtual Mettle::Lib::Logger       *newLogger(const char *logfile, const int logLevel, const bool logshow) = 0;

   virtual Pod                       *newPod(Mettle::DB::IConnect *dbCon, Mettle::Lib::Logger *logger, const bool autoCommit) = 0;

   virtual BS::Lib::IContentManager  *newContentManager(Mettle::Util::XmlConfig *cfg, BS::Lib::Pod *p) = 0;

protected:

   IProvider() noexcept;
};

}}

#endif
