/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_LIB_POD_H_
#define __BS_LIB_POD_H_

namespace Mettle
{
   namespace Lib
   {
      class Logger;
   }

   namespace DB
   {
      class IConnect;
   }
}

namespace BS { namespace Lib {

/** \class Pod pod.h bs/lib/pod.h
 * \brief A containter object to store commonly used objects.
 *
 * The purpose of this object is purely provide a convenient way in which to
 * store commonly used objects.
 *
 * Note the Pod object does not take responsibility for the objects assigned to it.  It is
 * still up to calling code to clean up these objects.
 */
class Pod
{
public:

   /** \brief Constructor that takes a logger and database connection object.
    * \param dbConnect an IConnect object.
    * \param logger a Logger object.
    * \param autoCommit if true, then automatic commits are allowed.  It is up to the implemented code to respect this flag or not.
    */
   Pod(Mettle::DB::IConnect *dbConnect,
       Mettle::Lib::Logger  *logger,
       bool                  autoCommit) noexcept;

   /** \brief Destructor, empty.
    */
   virtual ~Pod() noexcept;

   /** \brief Public access the database connection object.
    */
   Mettle::DB::IConnect *_dbCon;

   /** \brief Public access the logger object.
    */
   Mettle::Lib::Logger  *_log;

   /** \brief A flag to control automatic database commits by the 'PodCommit' class.
    */
   bool _allowAutoCommit;
};

}}

#endif
