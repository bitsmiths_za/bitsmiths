/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_LIB_PODCOMMIT_H_
#define __BS_LIB_PODCOMMIT_H_

namespace BS { namespace Lib {

class Pod;

/** \class PodCommit podcommit.h bs/lib/podcommit.h
 * \brief A helper funcitont to automate database rollbacks.
 *
 * When this object falls out of scope (is deleted) it automatically applies a database
 * rollback if a commit (or rollback) was not manually performed.
 *
 * Note that this object respects the pod's '_allowAutoCommit' membmer.  If this memeber is
 * set to false, then this object effectively does nothing.
 */
class PodCommit
{
public:

   /** \brief Constructor.
    * \param pod the object containing the database connection object to perfrom the commit and rollbacks on.
    */
   PodCommit(Pod *pod);

   /** \brief Destructor which automatically performs a rollback if a commit action was not performed.
    */
   virtual ~PodCommit();

   /** \brief Performs a database commit with the pod assigned by the constructor.
    */
   void commit();

   /** \brief Performs a database rollback with the pod assigned by the constructor.
    */
   void rollback();

protected:

   Pod  *_pod;
   bool  _done;
};

}}

#endif
