/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Injectable } from '@angular/core';

import { FormatProviderService } from './format-provider.service';

import moment from 'moment';

@Injectable()
export class TextService {

  constructor(private _formatProvider: FormatProviderService) {
  }

  _isDateNull(dv: Date) : boolean {
    if (!dv)
      return true;

    return false;
  }

  getNullDate(): Date {
    return undefined;
  }

  formatDate(dv: Date, nullStr: string = '-') : string {
    if (this._isDateNull(dv))
      return nullStr;

    return moment(dv).format(this._formatProvider.getDateFormat());
  }

  formatDateStr(dv: string, nullStr: string = '-') : string {
    if (!dv || dv === "0001-01-01T00:00:00+0000" || dv === "0001-01-01")
      return nullStr;

    return this.formatDate(new Date(dv), nullStr);
  }

  formatDateTime(dv: Date, nullStr: string = '-') : string {
    if (this._isDateNull(dv))
      return '';

    return moment(dv).format(this._formatProvider.getDateTimeFormat());
  }

  formatDateTimeStr(dv: string, nullStr: string = '-') : string {
    if (dv === undefined || dv === null || dv === "" || dv === "0001-01-01T00:00:00+0000" || dv === "0001-01-01")
      return nullStr;

    return this.formatDateTime(new Date(dv), nullStr);
  }

  dateScrapTZ(dt: Date): Date {
    return new Date(dt.valueOf() - dt.getTimezoneOffset() * 60000);
  }

  humanFileSize(size: number) : string {
    let i   : number = Math.floor(Math.log(size) / Math.log(1024));
    let sz  : any    = ['B', 'kB', 'MB', 'GB', 'TB']
    let val : string = (size / Math.pow(1024, i)).toFixed(2);

    if (i < 0 || i > 4) {
      return size.toString();
    }

    return val + ' ' + sz[i].toString();
  }

}
