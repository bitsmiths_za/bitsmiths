# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

file-type : Mettle.DB.Table
version   : 2.1
name      : NoTempl

standard-procs:
  select_one : true
  lock_one   : true
  select_all : false
  update     : true
  insert     : true
  delete_one : false
  delete_all : false

dicts:
  status:
    A : Active
    D : Disabled

# NoTempl
# The active notification template table holds all the current information for a given
# message (notification) template.
columns:
  - { name: notype_id,      type: string, max: 128 }
  - { name: corrtype_id,    type: string, max: 16 }
  - { name: status,         type: char, in: status }
  - { name: lastcfg_id,     type: int64, null: true } # last config that effected this record.

  - { name: templ,          type: json }  # the template
  - { name: res_list,       type: json, null: true }  # A dict of resources required to add to template.

  - { name: modified_by,  type: string, max: 128 }
  - { name: tm_stamp,     type: timestamp }

primary-key: [ notype_id, corrtype_id ]

foreign-keys:
  - { name: fk_notype,   table: NoType,     columns: [ notype_id ] }
  - { name: fk_corrtype, table: CorrType,   columns: [ corrtype_id ] }
  - { name: fk_cfg,      table: NoTemplCfg, columns: [ lastcfg_id ] }

procs:
  - name   : ByNoType
    input  :
      - { name: notype_id, type: this }
      - { name: criteria,  type: dynamic }
    output :
      - { type: 'table:this' }
    sql    :
      - db    : std
        query : |
                select
                  t.*
                from
                  loco.notempl t
                where
                  t.notype_id := t.notype_id
                  [criteria]
                order by
                  corrtype_id
