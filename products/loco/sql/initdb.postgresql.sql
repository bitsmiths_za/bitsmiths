--****************************************************************************--
--                          This file is part of:                             --
--                               BITSMITHS                                    --
--                          https://bitsmiths.co.za                           --
--****************************************************************************--
-- Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       --
--  * https://bitbucket.org/bitsmiths_za/bitsmiths                            --
--                                                                            --
-- Permission is hereby granted, free of charge, to any person obtaining a    --
-- copy of this software and associated documentation files (the "Software"), --
-- to deal in the Software without restriction, including without limitation  --
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,   --
-- and/or sell copies of the Software, and to permit persons to whom the      --
-- Software is furnished to do so, subject to the following conditions:       --
--                                                                            --
-- The above copyright notice and this permission notice shall be included in --
-- all copies or substantial portions of the Software.                        --
--                                                                            --
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR --
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   --
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    --
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER --
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    --
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        --
-- DEALINGS IN THE SOFTWARE.                                                  --
--****************************************************************************--

-- CREATE AUDIT TABLES

insert into audit.cfg select 'loco.config',       'id',              'modified_by|tm_stamp', null, null where not exists (select id from audit.cfg where id = 'loco.config');

-- CONFIG VALUES

insert into loco.config values('msg.archive-age.sent',        'Max days before a sent message is archived',       '10',    'Archive', '[loco]', current_timestamp);
insert into loco.config values('msg.archive-age.pending',     'Max days before a pending message is archived',    '365',   'Archive', '[loco]', current_timestamp);
insert into loco.config values('msg.archive-age.failed',      'Max days before a failed message is archived',     '365',   'Archive', '[loco]', current_timestamp);
insert into loco.config values('msg.archive-age.retry',       'Max days before a retry message is archived',      '365',   'Archive', '[loco]', current_timestamp);
insert into loco.config values('msg.archive-age.cancelled',   'Max days before a cancelled message is archived',  '10',    'Archive', '[loco]', current_timestamp);

insert into loco.config values('server.proc-interval',     'Server process interval (seconds)',          '1.0',      'Server', '[loco]', current_timestamp);
insert into loco.config values('server.prov-interval',     'Server prover refresh interval (seconds)',   '3600.0',   'Server', '[loco]', current_timestamp);

-- CORRESPONDENCE TYPES

insert into loco.corrtype select 'email', 'E-Mail', 'A', '[{"id": "subject", "required": false, "descr": "Email Subject"}, {"id": "body-html", "required": false, "descr": "HTML Email Body"}, {"id":"body-text", "required": true, "descr": "Plain Text Email Body"}]', '[loco]', current_timestamp;
insert into loco.corrtype select 'sms',   'SMS',    'A', '[{"id":"msg", "required": true, "descr": "The sms message"}]', '[loco]', current_timestamp;

-- PROVIDERS

insert into loco.corrprov select 'logfile', 'email',  'Record Email to a log file',  'A', '[{"id": "path", "type": "string", "descr": "Path to the output log file."}]', '{"path": "$LOCAL_PATH/log/loco-email.log"}', null, '[loco]', current_timestamp;
insert into loco.corrprov select 'logfile', 'sms',    'Record SMS to a log file',    'A', '[{"id": "path", "type": "string", "descr": "Path to the output log file."}]', '{"path": "$LOCAL_PATH/log/loco-sms.log"}',   null, '[loco]', current_timestamp;
insert into loco.corrprov select 'awssns',  'sms',    'Send SMS via AWS Simple Notification Service',  'D', '[{"id": "aws_access_key_id", "type": "string", "descr": "AWS access key."}, {"id": "aws_secret_access_key", "type": "string", "descr": "AWS secret access key."}, {"id": "aws_region", "type": "string", "descr": "AWS region."}]', '{"aws_access_key_id": "$AWS_ACCESS_KEY_ID", "aws_secret_access_key": "$AWS_SECRET_ACCESS_KEY", "aws_region": "$AWS_REGION"}', null, '[loco]', current_timestamp;
insert into loco.corrprov select 'smtp',    'email',  'Send Email via SMTP',  'D', '[{"id": "host", "type": "string", "descr": "Host name"}, {"id": "port", "type": "int", "descr": "Port number"}, {"id": "username", "type": "string", "descr": "Account user name"}, {"id": "passwd", "type": "string", "descr": "Account password"}, {"id": "from", "type": "string", "descr": "From email address"}, {"id": "ssl", "type": "boolean", "descr": "SSL Enabled"}, {"id": "tls", "type": "boolean", "descr": "TLS Enabled"}, {"id": "timeout", "type": "float", "descr": "Timeout"}]', '{"host": "$SMTP_HOST", "port": "$SMTP_PORT", "username": "$SMTP_USER" , "passwd": "$SMTP_PASSWD", "from": "loco@bitsmiths.co.za", "ssl": true, "tls": true, "timeout": 30.0}', null, '[loco]', current_timestamp;
insert into loco.corrprov select 'bulksms', 'sms',    'Send SMS via Bulk SMS',  'D', '[{"id": "url", "type": "string", "descr": "Bulk SMS Endpoint URL."}, {"id": "username", "type": "string", "descr": "Bulk SMS user name."}, {"id": "passwd", "type": "string", "descr": "Bulkd SMS user password."}]', '{"url": "https://api.bulksms.com/v1/messages", "token": "$BULK_SMS_TOKEN", "token-secret": "$BULK_SMS_TOKEN_SECRET"}', null, '[loco]', current_timestamp;


-- TEST/SAMPLE NOTICES
insert into loco.notype  select 'loco.test-msg', 'A',     'Bitsmiths LOCO Test message', 'Tests', 20, null, 0, 5, '[loco]', current_timestamp;

insert into loco.notempl select 'loco.test-msg', 'email', 'A', null, '{"body-text": "Hello\nThis is a standard test plain text email from ${usrid}\nHave a nice day", "body-html": "<html><body><h3>Hello</h3><p>This is a standard HTML email from <b>${usrid}</b></p><p>Have a nice day</p></body></html>", "subject": "Loco Test Message"}', null, '[loco]', current_timestamp;
insert into loco.notempl select 'loco.test-msg', 'sms',   'A', null, '{"msg": "Hello\nThis is a standard test sms from ${usrid}\nHave a nice day"}', null, '[loco]', current_timestamp;
