/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "bs/monitor/api.h"

#include "bs/monitor/db/dao/batchDAO.h"
#include "bs/monitor/db/dao/batchinstDAO.h"
#include "bs/monitor/db/dao/batchitemDAO.h"
#include "bs/monitor/db/dao/jobDAO.h"
#include "bs/monitor/db/dao/jobinstDAO.h"
#include "bs/monitor/db/dao/jobinstcancelDAO.h"
#include "bs/monitor/db/dao/jobmetricDAO.h"

#include "bs/lib/pod.h"
#include "bs/lib/podcommit.h"

#include "mettle/db/iconnect.h"
#include "mettle/db/dblock.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/logger.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/string.h"
#include "mettle/lib/stringbuilder.h"
#include "mettle/lib/xmettle.h"

using namespace Mettle::Lib;
using namespace BS::Monitor::DB;
using namespace BS::Monitor::Braze;

namespace BS { namespace Monitor {

#define SOURCE          "BS.Monitor.Api"
#define POD_COMMIT(p)   BS::Lib::PodCommit _mcPodCommit(p)
#define COMMIT          _mcPodCommit.commit()

Api::Api(BS::Lib::Pod *pod)
{
   _pod  = pod;
}

Api::~Api()
{
}

tBatch *Api::batchLoad(const int32_t   id,
                       const String   &name)
{
   _pod->_log->info("BatchLoad - Start(id:%d/name:%s)", id, name.c_str());

   POD_COMMIT(_pod);
   tBatch::Safe res(0);

   if (id > 0)
   {
      dBatch batch(_pod->_dbCon);

      batch.selectOne(id);

      res.remember(new tBatch(batch.rec));
   }
   else if (!name.isEmpty())
   {
      dBatchByName batchQry(_pod->_dbCon);

      if (!batchQry.exec(name).fetch())
         throw xMettle(__FILE__, __LINE__, SOURCE, "Batch (name:%s) not found.", name.c_str());

      res.remember(new tBatch(batchQry.orec));
   }
   else
      throw xMettle(__FILE__, __LINE__, SOURCE, "BatchLoad requires a valid arguement");

   _pod->_log->info("BatchLoad - Done(id:%d/name:%s)", res.obj->id, res.obj->name.c_str());

   return res.forget();
}

int32_t Api::batchSave(const char     dbAction,
                       const tBatch  &rec)
{
   _pod->_log->info("BatchSave - Start(action:%c/id:%d/name:%s)", dbAction, rec.id, rec.name.c_str());

   POD_COMMIT(_pod);
   dBatch batch(_pod->_dbCon);

   batch.rec = rec;

   if (dbAction == Action_Couplet::keyDelete)
   {
      batch.deleteOne();
   }
   else
   {
      if (dbAction == Action_Couplet::keyInsert)
         batch.insert();
      else if (dbAction == Action_Couplet::keyUpdate)
         batch.update();

      checkForRecursion(&batch.rec);
   }

   COMMIT;

   _pod->_log->info("BatchSave - Done(id:%d)", batch.rec.id);

   return batch.rec.id;
}

tBatch::List *Api::batchSearch(const bBatchQuery &criteria)
{
   _pod->_log->info("BatchSearch - Start");

   POD_COMMIT(_pod);
   tBatch::List::Safe  res(new tBatch::List());
   dBatchSearch        qry(_pod->_dbCon);
   StringBuilder       crit;
   String              jot;

   if (criteria.batchId > 0)
      crit.addFormat(" AND b.id = %d", criteria.batchId);

   if (criteria.status != 0)
      crit.addFormat(" AND b.status = '%c'", cleanSQLChar(criteria.status));

   if (!criteria.name.isEmpty())
      crit.addFormat(" AND b.name = '%s'", cleanSQL(criteria.name, jot));
   else if (!criteria.nameLike.isEmpty())
      crit.addFormat(" AND b.name ILIKE '%%%s%%'", cleanSQL(criteria.nameLike, jot));

   if (!criteria.groupBatch.isEmpty())
      crit.addFormat(" AND b.groupId = '%s'", cleanSQL(criteria.groupBatch, jot));
   else if (!criteria.groupBatchike.isEmpty())
      crit.addFormat(" AND b.groupId ILIKE '%%%s%%'", cleanSQL(criteria.groupBatchike, jot));

   qry.irec.criteria.eat(crit);

   qry.exec().fetchAll(*res.obj);

   _pod->_log->info("BatchSearch - Done(cnt:%u)", res.obj->count());

   return res.forget();
}

tBatchInst::List *Api::batchInstSearch(const bBatchInstQuery &criteria)
{
   _pod->_log->info("BatchInstSearch - Start");

   POD_COMMIT(_pod);
   tBatchInst::List::Safe  res(new tBatchInst::List());
   dBatchInstSearch        qry(_pod->_dbCon);
   StringBuilder           crit;

   qry.irec.dateFrom = criteria.dateFrom;
   qry.irec.dateTo   = criteria.dateTo;

   if (criteria.maxRecords > 0)
      qry.irec.limitCrit.format(" LIMIT %d", criteria.maxRecords);

   if (criteria.batchId > 0)
      crit.addFormat(" AND bi.batchId = %d", criteria.batchId);

   if (criteria.status.count() > 0)
   {
      crit.add(" AND bi.status IN ('");

      _FOREACH(char, ch, criteria.status)
      {
         if (ch.idx)
            crit.add("','");

         crit.add(cleanSQLChar(*ch.obj));
      }

      crit.add("')");
   }

   qry.irec.criteria.eat(crit);

   qry.exec().fetchAll(*res.obj);

   _pod->_log->info("BatchInstSearch - Done(cnt:%u)", res.obj->count());

   return res.forget();
}

tBatchItem::List *Api::batchItemsLoad(const int32_t batchId)
{
   _pod->_log->info("BatchItemsLoad - Start(batchId:%d)", batchId);

   POD_COMMIT(_pod);
   tBatchItem::List::Safe res(new tBatchItem::List());

   dBatchItemByBatchId(_pod->_dbCon).exec(batchId).fetchAll(*res.obj);

   _pod->_log->info("BatchItemsLoad - Done(batchId:%d/cnt:%u)", batchId, res.obj->count());

   return res.forget();
}

void Api::batchItemsSave(const int32_t           batchId,
                         const tBatchItem::List &items)
{
   _pod->_log->info("BatchItemsSave - Start(batchId:%d/cnt:%u)", batchId, items.count());

   POD_COMMIT(_pod);
   dBatchItemDeleteByBatchId(_pod->_dbCon).exec(batchId);

   dBatchItem bi(_pod->_dbCon);

   _FOREACH (tBatchItem, item, items)
   {
      bi.rec = *item.obj;

      bi.rec.batchId = batchId;

      bi.insert();
   }

   COMMIT;

   _pod->_log->info("BatchItemsSave - Done(batchId:%d/cnt:%u)", batchId, items.count());
}

tJob *Api::jobLoad(const int32_t   id,
                   const String   &name)
{
   _pod->_log->info("JobLoad - Start(id:%d/name:%s)", id, name.c_str());

   POD_COMMIT(_pod);
   tJob::Safe res(0);

   if (id > 0)
   {
      dJob job(_pod->_dbCon);

      job.selectOne(id);

      res.remember(new tJob(job.rec));
   }
   else if (!name.isEmpty())
   {
      dJobByName jobQry(_pod->_dbCon);

      if (!jobQry.exec(name).fetch())
         throw xMettle(__FILE__, __LINE__, SOURCE, "Job (name:%s) not found.", name.c_str());

      res.remember(new tJob(jobQry.orec));
   }
   else
      throw xMettle(__FILE__, __LINE__, SOURCE, "JobLoad requires a valid arguement");

   _pod->_log->info("JobLoad - Done(id:%d/name:%s)", res.obj->id, res.obj->name.c_str());

   return res.forget();
}

int32_t Api::jobSave(const char  dbAction,
                     const tJob &rec)
{
   _pod->_log->info("JobSave - Start(action:%c/id:%d/name:%s)", dbAction, rec.id, rec.name.c_str());

   POD_COMMIT(_pod);
   dJob job(_pod->_dbCon);

   job.rec = rec;

   if (dbAction == Action_Couplet::keyInsert)
      job.insert();
   else if (dbAction == Action_Couplet::keyUpdate)
      job.update();
   else if (dbAction == Action_Couplet::keyDelete)
      job.deleteOne();

   COMMIT;

   _pod->_log->info("JobSave - Done(id:%d)", job.rec.id);

   return job.rec.id;
}

tJob::List *Api::jobSearch(const bJobQuery &criteria)
{
   _pod->_log->info("JobSearch - Start");

   POD_COMMIT(_pod);
   tJob::List::Safe  res(new tJob::List());
   dJobSearch        qry(_pod->_dbCon);
   StringBuilder     crit;
   String            jot;

   if (criteria.jobId > 0)
      crit.addFormat(" AND j.id = %d", criteria.jobId);

   if (criteria.batchId > 0)
      crit.addFormat(" AND j.batchId = %d", criteria.batchId);

   if (!criteria.name.isEmpty())
      crit.addFormat(" AND j.name = '%s'", cleanSQL(criteria.name, jot));
   else if (!criteria.nameLike.isEmpty())
      crit.addFormat(" AND j.name ILIKE '%%%s%%'", cleanSQL(criteria.nameLike, jot));

   if (!criteria.groupJob.isEmpty())
      crit.addFormat(" AND j.groupId = '%s'", cleanSQL(criteria.groupJob, jot));
   else if (!criteria.groupJobLike.isEmpty())
      crit.addFormat(" AND j.groupId ILIKE '%%%s%%'", cleanSQL(criteria.groupJobLike, jot));

   qry.irec.criteria.eat(crit);

   qry.exec().fetchAll(*res.obj);

   _pod->_log->info("JobSearch - Done(cnt:%u)", res.obj->count());

   return res.forget();
}

void Api::jobInstCancel(const int32_t  jobInstId,
                        const String  &userName,
                        const String  &reason)
{
   _pod->_log->info("JobInstCancel - Start(id:%d/user:%s/reason:%s)", jobInstId, userName.c_str(), reason.c_str());

   POD_COMMIT(_pod);
   dJobInst jobInst(_pod->_dbCon);

   if (!jobInst.trySelectOne(jobInstId))
   {
      _pod->_log->info("JobInstCancel - Faled(id:%d/No such job instance exists)", jobInstId);
      return;
   }

   if (jobInst.rec.status != tJobInst::Status_Couplet::keyRunning)
   {
      _pod->_log->info("JobInstCancel - Ignored(id:%d/The job instance is no longer running:%c)", jobInstId, jobInst.rec.status);
      return;
   }

   dJobInstCancel(_pod->_dbCon).insert(jobInstId, userName, reason);

   COMMIT;

   _pod->_log->info("JobInstCancel - Done(id:%d)", jobInstId);
}

bJobInstData *Api::jobInstLoad(const int32_t  id)
{
   _pod->_log->info("JobInstLoad - Start(jobInstId:%d)", id);

   POD_COMMIT(_pod);
   bJobInstData::Safe res(new bJobInstData());

   dJobInst            jobInst(_pod->_dbCon);
   dJob                job(_pod->_dbCon);
   dJobMetricByJobInst metrics(_pod->_dbCon);

   jobInst.selectOne(id);
   job.selectOne(jobInst.rec.jobId);

   res.obj->jobInst = jobInst.rec;
   res.obj->job     = job.rec;

   metrics.exec(id).fetchAll(res.obj->jobmetrics);

   if (!jobInst.rec.batchInstId_NULL)
   {
      dBatch              batch(_pod->_dbCon);
      dBatchInst          batchInst(_pod->_dbCon);

      batchInst.selectOne(jobInst.rec.batchInstId);
      batch.selectOne(batchInst.rec.batchId);

      res.obj->batch     = batch.rec;
      res.obj->batchInst = batchInst.rec;
   }

   _pod->_log->info("JobInstLoad - Done(jobInstId:%d)", id);

   return res.forget();
}

bool Api::jobInstRun(const int32_t  jobInstId,
                     const String  &modifiedBy,
                     const bool     rerunIfFailed,
                     const bool     rerunIfCompleted)
{
   _pod->_log->info("JobInstRun - Start(jobInstId:%d/modifiedBy:%s/rerunIfFailed:%d/rerunIfCompleted:%d)", jobInstId, modifiedBy.c_str(), rerunIfFailed, rerunIfCompleted);

   POD_COMMIT(_pod);
   bool     res = false;
   dJobInst jobInst(_pod->_dbCon);

   jobInst.selectOne(jobInstId);

   if (jobInst.rec.status == tJobInst::Status_Couplet::keyNone    ||
       jobInst.rec.status == tJobInst::Status_Couplet::keyPending ||
       (rerunIfFailed &&
        (jobInst.rec.status == tJobInst::Status_Couplet::keyFailed ||
         jobInst.rec.status == tJobInst::Status_Couplet::keyCancel)
       ) ||
       (rerunIfCompleted &&
        (jobInst.rec.status == tJobInst::Status_Couplet::keyCompleted ||
         jobInst.rec.status == tJobInst::Status_Couplet::keyForcedOK)
       ))
   {
      _pod->_log->info(" - Marking job instance(%d) as pending from (%s)", jobInstId, tJobInst::Status_Couplet::getValue(jobInst.rec.status));

      jobInst.rec.modifiedBy = modifiedBy;
      jobInst.rec.status     = tJobInst::Status_Couplet::keyPending;
      res                    = true;

      jobInstSave(Action_Couplet::keyUpdate, jobInst.rec);
   }

   _pod->_log->info("JobInstRun - Done(jobInstId:%d/res:%d)", jobInstId, res);
}

int32_t Api::jobInstSave(const char      dbAction,
                         const tJobInst &rec)
{
   _pod->_log->info("JobInstSave - Start(action:%c/jobInstId:%d/jobId:%d)", dbAction, rec.id, rec.jobId);

   POD_COMMIT(_pod);
   dJobInst jobInst(_pod->_dbCon);

   if (dbAction == Action_Couplet::keyInsert)
   {
      jobInst.rec = rec;

      jobInst.rec.batchInstId      = 0;
      jobInst.rec.batchInstId_NULL = true;

      jobInst.insert();
   }
   else if (dbAction != Action_Couplet::keyNone)
   {
      Mettle::DB::DBLock   dbLock(100, 5);

      jobInst.lockOne(rec.id, &dbLock);

      if (rec.tmStamp != jobInst.rec.tmStamp)
         throw xMettle(__FILE__, __LINE__, SOURCE, "The record (JobInst:%d) has been modified by another user or process.  Please reload and try again.", rec.id);

      jobInst.rec = rec;

      if (dbAction == Action_Couplet::keyUpdate)
         jobInst.update();
      else if (dbAction == Action_Couplet::keyDelete)
         jobInst.deleteOne();
   }

   COMMIT;

   _pod->_log->info("JobInstSave - Done(jobInstId:%d)", jobInst.rec.id);

   return jobInst.rec.id;
}

oJobInstSearch::List *Api::jobInstSearch(const bJobInstQuery &criteria)
{
   _pod->_log->info("JobInstSearch - Start");

   POD_COMMIT(_pod);
   oJobInstSearch::List::Safe  res(new oJobInstSearch::List());
   dJobInstSearch              qry(_pod->_dbCon);
   StringBuilder               crit;
   String                      jot;

   qry.irec.dateFrom = criteria.dateFrom;
   qry.irec.dateTo   = criteria.dateTo;

   if (criteria.maxRecords > 0)
      qry.irec.limitCrit.format(" LIMIT %d", criteria.maxRecords);

   if (criteria.jobId > 0)
      crit.addFormat(" AND j.id = %d", criteria.jobId);

   if (!criteria.jobName.isEmpty())
      crit.addFormat(" AND j.name = '%s'", cleanSQL(criteria.jobName, jot));
   else if (!criteria.jobNameLike.isEmpty())
      crit.addFormat(" AND j.name ILIKE '%%%s%%'", cleanSQL(criteria.jobNameLike, jot));

   if (criteria.jobInstId > 0)
      crit.addFormat(" AND i.id = %d", criteria.jobInstId);

   if (criteria.jobInstStatus.count() > 0)
   {
      crit.add(" AND i.status IN ('");

      _FOREACH(char, ch, criteria.jobInstStatus)
      {
         if (ch.idx)
            crit.add("','");

         crit.add(cleanSQLChar(*ch.obj));
      }

      crit.add("')");
   }

   if (!criteria.groupJob.isEmpty())
      crit.addFormat(" AND i.GroupJob = '%s'", cleanSQL(criteria.groupJob, jot));
   else if (!criteria.groupJobLike.isEmpty())
      crit.addFormat(" AND i.GroupJob ILIKE '%%%s%%'", cleanSQL(criteria.groupJobLike, jot));

   if (!criteria.groupBatch.isEmpty())
      crit.addFormat(" AND i.GroupBatch = '%s'", cleanSQL(criteria.groupBatch, jot));
   else if (!criteria.groupBatchLike.isEmpty())
      crit.addFormat(" AND i.GroupBatch ILIKE '%%%s%%'", cleanSQL(criteria.groupBatchLike, jot));

   if (criteria.batchInstId > 0)
      crit.addFormat(" AND i.batchInstId = %d", criteria.batchInstId);

   qry.irec.criteria.eat(crit);

   qry.exec().fetchAll(*res.obj);

   _pod->_log->info("JobInstSearch - Done(cnt:%u)", res.obj->count());

   return res.forget();
}

const char *Api::cleanSQL(const String &source,
                                String &jotter)

{
   if (source.length() > 2048)
      throw xMettle(__FILE__, __LINE__, SOURCE, "Api::cleanSQL() - input source to large (%d bytes)", source.length());

   jotter = source;

   if (jotter.isEmpty())
      return jotter.c_str();

   for (char *p = (char *) jotter.c_str(); *p; ++p)
      if (*p == '%' || *p == '\n' || *p == '\r' || *p == '\f' || *p == '\'')
         *p = ' ';

   jotter.replace("'", "''");

   return jotter.c_str();
}

const char Api::cleanSQLChar(const char ch)
{
   if (ch < 32 || ch >= 127 || ch == '\'')
      return '?';

   return ch;
}

void Api::checkForRecursion(const tBatch *batch)
{
   dBatch parent(_pod->_dbCon);

   parent.rec = *batch;

   while (parent.rec.parentId != 0)
   {
      if (parent.rec.parentId == batch->id)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Ifinite batch recursion detected in batch (id:%d/name:%s)", batch->id, batch->name.c_str());

      parent.selectOne(parent.rec.parentId);
   }
}

#undef SOURCE
#undef POD_COMMIT
#undef COMMIT

}}
