/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BITSMITHS_MONITOR_API_H_
#define __BITSMITHS_MONITOR_API_H_

#include "bs/monitor/braze/monitorstruct.h"

namespace BS {

namespace Lib
{
  class Pod;
}

namespace Monitor {

/** \class Api api.h bs/monitor/api.h
 * \brief The main API into interacting the bithsmiths monitor library.
 *
 *  It is possible to directly bypass the 'API' and hit the database code
 *  directly but this is discouraged unless there is a very specific reason for
 *  doing so.
 */
class Api
{
public:
   /** \brief Constructor.
    *  \param pod the Admin object requires an initialized Pod object to operate.
    *
    *  Note that the Admin object will respect the 'Auto Commit' property of the pod, if
    *  'Auto Commit' is turned off, then it is up to the calling code to perform commits and rollbacks
    *  when methods succeed or throw exceptions.
    */
   Api(BS::Lib::Pod *pod);

   /** \brief Destructor.
    */
   virtual ~Api();

   /** \brief Loads a batch record by the primary key or by name.
    *  \param id the batch, if 0 then a search on name is done instead.
    *  \param name the batch name to load, if this empty and if  \p id is 0 an exception is thrown.
    *  \returns the batch table record.
    */
   BS::Monitor::DB::tBatch *batchLoad(const int32_t                   id,
                                      const Mettle::Lib::String      &name);

   /** \brief Saves a Batch
    *  \param dbAction the action to perform, see Braze.Monitor.DBAction Couplet
    *  \param rec the batch record to be saved.
    *  \returns the PK of the batch.
    *  \warning May throw Foreign Key exceptions if delete occurs on active batches that have instance history.
    */
   int32_t batchSave(const char                     dbAction,
                     const BS::Monitor::DB::tBatch &rec);

   /** \brief Searches for batches based on the query criteria
    *
    *  The \p criteria arguement contains serveral members used for searching.  Any
    *  members that are left blank are simply ignored.  Anything that is filled in will
    *  form part of the search.
    *
    *  \param criteria the search critiera to be used.
    *  \returns a list of batches that matched the search criteria.
    */
   BS::Monitor::DB::tBatch::List *batchSearch(const BS::Monitor::Braze::bBatchQuery &criteria);

   /** \brief Searches for batch instances based on the query criteria
    *
    *  The \p criteria arguement contains serveral members used for searching.  Any
    *  members that are left blank are simply ignored.  Anything that is filled in will
    *  form part of the search.
    *
    *  \param criteria the search critiera to be used.
    *  \returns a list of batch instances that matched the search criteria.
    */
   BS::Monitor::DB::tBatchInst::List *batchInstSearch(/* i  */ const BS::Monitor::Braze::bBatchInstQuery &criteria);

   /** \brief Loads the list of Batch Items for the specified batch.
    *
    *  \param batchId the Batch in question.
    *  \returns the list of Batch Items associated with the Batch.
    */
   BS::Monitor::DB::tBatchItem::List *batchItemsLoad(const int32_t batchId);

   /** \brief Saves a new list of Batch Items for the specified batch.
    *
    *  This method first removes all the existing Batch Items for the specified
    *  batch and then replaces them with the new list of Items.
    *
    *  \param batchId the Batch in question.
    *  \param items the list of batch items to be inserted.
    */
   void batchItemsSave(const int32_t                            batchId,
                       const BS::Monitor::DB::tBatchItem::List &items);


   /** \brief Loads a job record by the primary key or by name.
    *  \param id the job, if 0 then a search on name is done instead.
    *  \param name the job name to load, if this empty and if the \p is 0 an exception is thrown.
    *  \returns the job table record.
    */
   BS::Monitor::DB::tJob *jobLoad(const int32_t                   id,
                                  const Mettle::Lib::String      &name);

   /** \brief Saves a Job
    *  \param dbAction the action to perform, see Braze.Monitor.DBAction Couplet
    *  \param rec the job record to be saved.
    *  \returns the PK of the job.
    *  \warning May throw Foreign Key exceptions if delete occurs on active jobs that have instance history.
    */
   int32_t jobSave(const char                   dbAction,
                   const BS::Monitor::DB::tJob &rec);

   /** \brief Searches for jobs based on the query criteria
    *
    *  The \p criteria arguement contains serveral members used for searching.  Any
    *  members that are left blank are simply ignored.  Anything that is filled in will
    *  form part of the search.
    *
    *  \param criteria the search critiera to be used.
    *  \returns a list of jobs that matched the search criteria.
    */
   BS::Monitor::DB::tJob::List *jobSearch(const BS::Monitor::Braze::bJobQuery &criteria);

   /** \brief Instructs the monitor manager to cancel this running job instance.
    *
    *  Note that if the job instance is no longer running, this call is ignored.
    *  Mutliple calls to this method have not effect.
    *  Please wait up to 1 second for the monitor manager to cancel the job.
    *
    *  \param jobInstId the id of the job instance to be cancelled/aborted.
    *  \param userName the user who is cancelling the job.
    *  \param reason the reason the user is cancelling the job, this param can be left blank.
    */
   void jobInstCancel(const int32_t               jobInstId,
                      const Mettle::Lib::String  &userName,
                      const Mettle::Lib::String  &reason);

   /** \brief Loads a job instance record by the primary key with all its dependent table records.
    *  \param id the job instance id, exception is thrown if there record is not found.
    *  \returns the job instance data record.
    */
   BS::Monitor::Braze::bJobInstData *jobInstLoad(const int32_t  id);

   /** \brief Runs a job instance, optionally re-run a job if it has completed or failed.
    *  \param jobInstId the job instance id, exception is thrown if there record is not found.
    *  \param modifiedBy the user who is reruning the job.
    *  \param rerunIfFailed if true the job will be marked for run if it has failed.
    *  \param rerunIfCompleted if true the job will be marked for run if it has already completed.
    *  \returns true if the job was marked for rerun.
    */
   bool jobInstRun(const int32_t               jobInstId,
                   const Mettle::Lib::String  &modifiedBy,
                   const bool                  rerunIfFailed,
                   const bool                  rerunIfCompleted);

   /** \brief Saves a Job
    *  \param dbAction the action to perform, see Braze.Monitor.DBAction Couplet
    *  \param rec the job instance record to be saved.
    *  \returns the PK of the job instance.
    *  \warning May throw Foreign Key exceptions if delete occurs on records with foreign keys.
    */
   int32_t jobInstSave(const char                       dbAction,
                       const BS::Monitor::DB::tJobInst &rec);

   /** \brief Searches for jobs instances based on the query criteria
    *
    *  The \p criteria arguement contains serveral members used for searching.  Any
    *  members that are left blank are simply ignored.  Anything that is filled in will
    *  form part of the search.
    *
    *  \param criteria the search critiera to be used.
    *  \returns a list of jobs instance search results matched from the input criteria.
    */
   BS::Monitor::DB::oJobInstSearch::List *jobInstSearch(const BS::Monitor::Braze::bJobInstQuery &criteria);


protected:

   BS::Lib::Pod *_pod;

   const char *cleanSQL(const Mettle::Lib::String &source,
                              Mettle::Lib::String &jotter);

   const char cleanSQLChar(const char ch);

   void checkForRecursion(const BS::Monitor::DB::tBatch *batch);

private:

};

}}

#endif
