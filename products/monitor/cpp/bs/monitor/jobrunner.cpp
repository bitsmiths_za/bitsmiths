/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "bs/monitor/jobrunner.h"

#include <stdlib.h>

#include "mettle/lib/common.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/filemanager.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/logger.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/processmanager.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include "mettle/db/dblock.h"
#include "mettle/db/iconnect.h"

#include "bs/monitor/db/dao/batchDAO.h"
#include "bs/monitor/db/dao/batchinstDAO.h"
#include "bs/monitor/db/dao/jobDAO.h"
#include "bs/monitor/db/dao/jobinstDAO.h"
#include "bs/monitor/db/dao/jobinstcancelDAO.h"
#include "bs/monitor/db/dao/jobmetricDAO.h"

#include "bs/lib/pod.h"

using namespace Mettle::Lib;
using namespace BS::Monitor::DB;

namespace BS { namespace Monitor {

#define SOURCE                         "[BS:JobRunner]"
#define NICE_DATETIME_FORMAT           "%Y-%m-%d %H:%M:%S"


JobRunner::JobRunner(BS::Lib::Pod *pod) noexcept
{
   _pod     = pod;
   _job     = 0;
   _jobInst = 0;
}


JobRunner::~JobRunner() noexcept
{
   _DELETE(_jobInst)
   _DELETE(_job)
}


int JobRunner::run(const int32_t jobInstId,
                   const char    *prePath)
{
   int rc = 1;

   try
   {
      _pod->_log->debug("JobRunner::run - Start(job:%d, prepath:%s)", jobInstId, prePath ? prePath : String::empty());

      if (!lockJobInst(jobInstId))
         return 1;

      if (!validateJobInst())
         return 1;

      if (!validateJobPath(prePath))
         return 1;

      rc = runJob();
   }
   _SPOT_STD_EXCEPTIONS(_pod->_log, return 1)

   _pod->_log->debug("JobRunner::run - Done(job:%d, prepath:%s, rc:%d)", jobInstId, prePath ? prePath : String::empty(), rc);

   return rc;
}


bool JobRunner::lockJobInst(const int32_t jobInstId)
{
   _pod->_log->debug("JobRunner::lockJobInst - Start(job:%d)", jobInstId);

   Mettle::DB::DBLock dbLock(100, 10);
   dJobInst           jobInst(_pod->_dbCon);
   dJobInstCancel     jobInstCancel(_pod->_dbCon);

   jobInst.lockOne(jobInstId, &dbLock);

   if (jobInst.rec.status != tJobInst::Status_Couplet::keyWaiting)
   {
      _pod->_log->error("JobInst (%d) found in unexpected state (%c) - Marking job as failed and aborting", jobInstId, jobInst.rec.status);
      commitStatus(tJobInst::Status_Couplet::keyFailed, false);
      return false;
   }

   if (jobInstCancel.trySelectOne(jobInst.rec.id))
   {
      jobInstCancel.lockOne(jobInst.rec.id, &dbLock);
      jobInstCancel.deleteOne();
      _pod->_dbCon->commit();

      jobInst.lockOne(jobInstId, &dbLock);

      if (jobInst.rec.status != tJobInst::Status_Couplet::keyWaiting)
      {
         _pod->_log->error("JobInst (%d) found in unexpected state (%c) - Marking job as failed and aborting", jobInstId, jobInst.rec.status);
         commitStatus(tJobInst::Status_Couplet::keyFailed, false);
         return false;
      }
   }

   _DELETE(_jobInst)

   _jobInst = new tJobInst(jobInst.rec);

   _pod->_log->debug("JobRunner::lockJobInst - Done(job:%d)", jobInstId);

   return true;
}


bool JobRunner::validateJobInst()
{
   _pod->_log->debug("JobRunner::validateJobInst - Start(job:%d)", _jobInst->id);

   DateTime now(DateTime::now());

   if (_jobInst->processDate > now)
   {
      char j1[64];
      char j2[64];

      commitMetricAndStatus(String().format("MonJobInst (%d) Process DateTime (%s) is greater that the current DateTime(%s)", _jobInst->id, _jobInst->processDate.format(j1, NICE_DATETIME_FORMAT), now.format(j2, NICE_DATETIME_FORMAT)),
                            tJobMetric::Type_Couplet::keyError,
                            tJobInst::Status_Couplet::keyFailed,
                            false);
      return false;
   }

   if (_jobInst->parentId > 0)
   {
      dJobInst parent(_pod->_dbCon);

      if (!parent.trySelectOne(_jobInst->parentId))
      {
         commitMetricAndStatus(String().format("Parent MonJobInst (%d) is missing", _jobInst->parentId),
                               tJobMetric::Type_Couplet::keyError,
                               tJobInst::Status_Couplet::keyFailed,
                               false);
         return false;
      }

      if (parent.rec.status != tJobInst::Status_Couplet::keyCompleted &&
          parent.rec.status != tJobInst::Status_Couplet::keyForcedOK)
      {
         commitMetricAndStatus(String().format("Parent MonJobInst (%d) should be in completed status - Not status (%s)", _jobInst->parentId, tJobInst::Status_Couplet::getValue(parent.rec.status)),
                               tJobMetric::Type_Couplet::keyError,
                               tJobInst::Status_Couplet::keyFailed,
                               false);
      }
   }

   _pod->_log->debug("JobRunner::validateJobInst - Done(job:%d)", _jobInst->id);

   return true;
}


bool JobRunner::validateJobPath(const char *prePath)
{
   _pod->_log->debug("JobRunner::validateJobPath - Start(job:%d, prepath:%s)", _jobInst->id, prePath ? prePath : String::empty());

   dJob job(_pod->_dbCon);

   if (!job.trySelectOne(_jobInst->jobId))
   {
      commitMetricAndStatus(String().format("Monitor Job (%d) is missing", _jobInst->jobId),
                            tJobMetric::Type_Couplet::keyError,
                            tJobInst::Status_Couplet::keyFailed,
                            false);
      return false;
   }

   if (prePath)
   {
      _jobPath = prePath;
      _jobPath.makeDirectory();
   }
   else
   {
      char *tmp = getenv("HOME");

      if (tmp)
      {
         _jobPath = tmp;
         _jobPath.makeDirectory();
      }
   }

   if (job.rec.programArgs.startsWith("$"))
      job.rec.programArgs = _jobPath + job.rec.programArgs.splice(1);

   if (job.rec.programPath.startsWith("#"))
   {
      _jobPath = job.rec.programPath.splice(1);
   }
   else
   {
      _jobPath += job.rec.programPath;
   }

   if (!FileManager::pathExists(_jobPath.c_str()))
   {
      commitMetricAndStatus(String().format("Job (%d) program path (%s) is missing", job.rec.id, _jobPath.c_str()),
                            tJobMetric::Type_Couplet::keyError,
                            tJobInst::Status_Couplet::keyFailed,
                            false);

      return false;
   }

   _DELETE(_job)

   _job = new tJob(job.rec);

   _pod->_log->debug("JobRunner::validateJobPath - Done(job:%d, jobpath:%s)", _jobInst->id, _jobPath.c_str());

   return true;
}


void JobRunner::commitStatus(const char newStatus, const bool relock)
{
   if (!_jobInst)
      return;

   _pod->_log->debug("JobRunner::commitStatus - Start(job:%d, status:%c, relock:%d)", _jobInst->id, newStatus, relock);

   _jobInst->status     = newStatus;
   _jobInst->modifiedBy = SOURCE;

   dJobInst(_pod->_dbCon).update(_jobInst);

   _pod->_dbCon->commit();

   if (newStatus == tJobInst::Status_Couplet::keyFailed)
      notifyJobInstFailed();
   else if (newStatus == tJobInst::Status_Couplet::keyCompleted)
      notifyJobInstSuccess();

   if (relock)
   {
      Mettle::DB::DBLock dbLock(0, 1);
      dJobInst           jobInst(_pod->_dbCon);

      jobInst.lockOne(_jobInst->id, &dbLock);

      if (jobInst.rec.status != _jobInst->status)
         throw xMettle(__FILE__, __LINE__, SOURCE, "JobInst (%d) - was modified by an outside source unexpectedly! - Expected Status (%c) != Actual Status (%c)", jobInst.rec.id, jobInst.rec.status, newStatus);

      *_jobInst = jobInst.rec;
   }

   _pod->_log->debug("JobRunner::commitStatus - Done(job:%d, status:%c, relock:%d)", _jobInst->id, newStatus, relock);
}


void JobRunner::commitMetricAndStatus(const String &msg, const char metricType, const char newStatus, const bool relock)
{
   if (metricType == tJobMetric::Type_Couplet::keyError)
      _pod->_log->error(" - Job Status Change (status:%s, msg:%s)", tJobInst::Status_Couplet::getValue(newStatus), msg.c_str());
   else
      _pod->_log->debug(" - Job Status Change (status:%s, msg:%s)", tJobInst::Status_Couplet::getValue(newStatus), msg.c_str());

   dJobMetric(_pod->_dbCon).insert(_jobInst->id, metricType, msg);

   commitStatus(newStatus, relock);
}


void JobRunner::notifyJobInstFailed()
{
}


void JobRunner::notifyJobInstSuccess()
{
}


int32_t JobRunner::runJob()
{
   Mettle::Lib::ProcessManager              procManager;
   Mettle::Lib::ProcessManager::ChildProc   item;
   Mettle::Lib::ProcessManager::ChildProc  *spawn;
   String                                   args;
   dJobInstCancel                           cancel(_pod->_dbCon);
   unsigned int                             cancelCheck;

   _pod->_log->info("JobRunner::RunJob - Start(job:%d)", _jobInst->id);

   try
   {
      addJobArgs(args);

      commitMetricAndStatus("Started",
                            tJobMetric::Type_Couplet::keyStart,
                            tJobInst::Status_Couplet::keyRunning,
                            true);

      _pod->_log->info(" - running [job:%d, path:%s, args:%s]", _jobInst->id, _jobPath.c_str(), args.c_str());

      spawn = procManager.spawnChild(_jobPath.c_str(), args.c_str());

      for (;;)
      {
         procManager.houseKeeping();

         if (procManager.fetchChildExit(item))
            break;

         Common::sleepMili(100);

         if (++cancelCheck % 10 == 0) // check to cancel job every second
         {
            if (cancel.trySelectOne(_jobInst->id))
            {
               char jotter[32];

               _pod->_log->info(" - Job (inst:%d) cancel requested! (user:%s, time:%s, reason:%s)",
                  cancel.rec.modifiedBy.c_str(),
                  cancel.rec.tmStamp.format(jotter, NICE_DATETIME_FORMAT),
                  cancel.rec.reason.c_str());

               dJobMetric(_pod->_dbCon).insert(
                  _jobInst->id,
                  tJobMetric::Type_Couplet::keyCancelled,
                  String().format("user:%s, reason:%s", cancel.rec.modifiedBy.c_str(), cancel.rec.reason.c_str()));

               procManager.terminate(spawn, 2);

               Common::sleepMili(100);

               while (!procManager.fetchChildExit(item))
               {
                  procManager.terminate(spawn, 1);
                  Common::sleepMili(50);
               }

               break;
            }
         }
      }

      if (item.rc != 0)
      {
         commitMetricAndStatus(String().format("Failed with return code (%d)", item.rc),
                               tJobMetric::Type_Couplet::keyCompleted,
                               tJobInst::Status_Couplet::keyFailed,
                               false);
      }
      else
      {
         commitMetricAndStatus("OK",
                               tJobMetric::Type_Couplet::keyCompleted,
                               tJobInst::Status_Couplet::keyCompleted,
                               false);
      }
   }
   _SPOT_STD_EXCEPTIONS(_pod->_log, throw)

   _pod->_log->info("JobRunner::RunJob - Done(job:%d, rc:%d)", _jobInst->id, item.rc);

   return item.rc;
}


void JobRunner::addJobArgs(String &args)
{
   args = _job->programArgs;

   if (_jobInst->extraArgs.length() > 1)
   {
     args += ' ';
     args += _jobInst->extraArgs;
   }

   args += " --job ";
   args += _jobInst->id;

   args.strip();

   if (_jobInst->batchInstId > 0)
   {
      dBatchInst bi(_pod->_dbCon);
      String     rdate;

      bi.selectOne(_jobInst->batchInstId);

      rdate.format(" --rundate %d", bi.rec.runDate.date.toInt32());

      args += rdate;
   }
}


#undef SOURCE
#undef NICE_DATETIME_FORMAT

}}
