/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_MONITOR_JOBRUNNER_H_
#define __BS_MONITOR_JOBRUNNER_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/string.h"

namespace BS {

namespace Lib
{
   class Pod;
}

namespace Monitor {

namespace DB
{
   class tJob;
   class tJobInst;
}

class JobRunner
{
public:

   JobRunner(BS::Lib::Pod  *pod) noexcept;

   virtual ~JobRunner() noexcept;

   int run(const int32_t  jobInstId,
           const char    *prePath = 0);

protected:

   BS::Lib::Pod                  *_pod;
   BS::Monitor::DB::tJob         *_job;
   BS::Monitor::DB::tJobInst     *_jobInst;
   Mettle::Lib::String            _jobPath;

   virtual bool lockJobInst(const int32_t jobInstId);

   virtual bool validateJobInst();

   virtual bool validateJobPath(const char *prePath);

   virtual int runJob();

   virtual void commitStatus(const char newStatus, const bool relock);

   virtual void commitMetricAndStatus(const Mettle::Lib::String &msg, const char metricType, const char newStatus, const bool relock);

   virtual void notifyJobInstFailed();

   virtual void notifyJobInstSuccess();

   virtual void addJobArgs(Mettle::Lib::String &args);
};

}}

#endif
