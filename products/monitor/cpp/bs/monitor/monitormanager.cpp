/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "bs/monitor/monitormanager.h"

#include "mettle/lib/common.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/filemanager.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/logger.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include "bs/monitor/db/dao/batchDAO.h"
#include "bs/monitor/db/dao/batchinstDAO.h"
#include "bs/monitor/db/dao/batchitemDAO.h"
#include "bs/monitor/db/dao/jobDAO.h"
#include "bs/monitor/db/dao/jobinstDAO.h"
#include "bs/monitor/db/dao/jobmetricDAO.h"

#include "mettle/db/dblock.h"
#include "mettle/db/iconnect.h"

#include "bs/lib/pod.h"
#include "bs/lib/podcommit.h"

using namespace Mettle::Lib;
using namespace BS::Monitor::DB;

namespace BS { namespace Monitor {

#define SOURCE                         "[BS:Monitor]"
#define NICE_DATETIME_FORMAT           "%Y-%m-%d %H:%M:%S"
#define POD_COMMIT(p)   BS::Lib::PodCommit _mcPodCommit(p)
#define COMMIT          _mcPodCommit.commit()

MonitorManager::MonitorManager(      BS::Lib::Pod   *pod,
                               const String         &jobRunnerPath,
                               const unsigned int    maxConcurrentJobs,
                               const unsigned int    batchCheckInterval)
{
   _pod                 = pod;
   _jobRunnerPath       = jobRunnerPath;
   _maxConcurrentJobs   = maxConcurrentJobs;
   _batchCheckInterval  = batchCheckInterval;
   _batchInterval       = 0;
}


MonitorManager::~MonitorManager() throw()
{
}


void MonitorManager::initialize()
{
   _pod->_log->debug("MonitorManager::run - Start()");

   if (!FileManager::pathExists(_jobRunnerPath.c_str()))
      throw xMettle(__FILE__, __LINE__, SOURCE, "JobRunnerPath is not found (%s)", _jobRunnerPath.c_str());

   if (_batchCheckInterval < 1)
   {
      _batchCheckInterval = 60;
      _pod->_log->info(" - Defaulting Batch Interval to 60 seconds as zero is not a valid value.");
   }
   else if (_batchCheckInterval > 3600)
   {
      _batchCheckInterval = 3600;
      _pod->_log->info(" - Defaulting Batch Interval to 1 hour as the current value is greater than 1 hour.");
   }

   _batchInterval = _batchCheckInterval;

   failRunningJobsInUnexpectedState(tJobInst::Status_Couplet::keyWaiting);
   failRunningJobsInUnexpectedState(tJobInst::Status_Couplet::keyRunning);
   failRunningJobsInUnexpectedState(tJobInst::Status_Couplet::keyCancel);

   _pod->_log->debug("MonitorManager::run - Done()");
}


void MonitorManager::run()
{
   if (++_batchInterval > _batchCheckInterval)
      _batchInterval = 0;

   _pod->_log->debug("MonitorManager::run - Start(interval:%d)", _batchInterval);

   try
   {
      if (_batchInterval == 0)
      {
         completeFinishedBatches();

         scheduleBatches();

         createScheduleItems();
      }

      checkForCompletedProcs();

      startPendingProcs();
   }
   _SPOT_STD_EXCEPTIONS(_pod->_log, throw)

   _pod->_log->debug("MonitorManager::run - Done(interval:%d)", _batchInterval);
}


void MonitorManager::failRunningJobsInUnexpectedState(const char status)
{
   _pod->_log->info("MonitorManager::failRunningJobsInUnexpectedState - Start(status:%c)", status);

   tJobInst::List  runList;
   int             cnt;

   dJobInstByStatus(_pod->_dbCon).exec(status).fetchAll(runList);

   _FOREACH(tJobInst, rec, runList)
   {
      try
      {
         Mettle::DB::DBLock dbLock(0, 1);
         dJobInst           inst(_pod->_dbCon);

         inst.lockOne(rec.obj->id, &dbLock);

         if (inst.rec.status != status)
         {
            _pod->_log->info("  - [%d] JobInst (%d) has changed status with status '%c: %s'",
               rec.idx, rec.obj->id, inst.rec.status, tJobInst::Status_Couplet::getValue(inst.rec.status));
            _pod->_dbCon->rollback();
            continue;
         }

         inst.rec.status     = tJobInst::Status_Couplet::keyFailed;
         inst.rec.modifiedBy = SOURCE;

         inst.update();
         _pod->_dbCon->commit();

         _pod->_log->info("  - [%d] JobInst (%d) is not running and has been marked as 'Failed'", rec.idx, rec.obj->id);
      }
      catch (xMettle &x)
      {
         if (x.errorCode != xMettle::DBLockNoWaitFailed)
         {
            _pod->_log->spot(__FILE__, __LINE__, x);
            throw;
         }

         _pod->_dbCon->rollback();
         _pod->_log->info("  - [%d] MonJobInst (%d) is still running (locked)", rec.idx, rec.obj->id);
      }
   }

   _pod->_log->info("MonitorManager::failRunningJobsInUnexpectedState - Done(cnt:%u)", runList.count());
}


void MonitorManager::completeFinishedBatches()
{
   _pod->_log->debug("MonitorManager::completeFinishedBatches - Start()");

   POD_COMMIT(_pod);
   Mettle::DB::DBLock                    dbLock(100, 1);
   dBatchInstAllChildrenCompleted        qry(_pod->_dbCon);
   oBatchInstAllChildrenCompleted::List  list;
   dBatchInst                            batchInst(_pod->_dbCon);
   dBatch                                batch(_pod->_dbCon);

   qry.exec().fetchAll(list);

   _FOREACH(oBatchInstAllChildrenCompleted, rec, list)
   {
      batchInst.lockOne(rec.obj->id, &dbLock);
      batch.lockOne(batchInst.rec.batchId, &dbLock);

      batchInst.rec.status  = tBatchInst::Status_Couplet::keyCompleted;
      batchInst.rec.endDate = DateTime::now();

      batchInst.update();

      incrementRundate(&batch.rec);

      batch.update();
   }

   if (list.count() > 0)
      COMMIT;

   _pod->_log->debug("MonitorManager::completeFinishedBatches - Done(cnt:%d)", list.count());
}


void MonitorManager::scheduleBatches()
{
   DateTime now = DateTime::now();
   char     jot[32];

   _pod->_log->debug("MonitorManager::scheduleBatches - Start(%s)", now.format(jot, NICE_DATETIME_FORMAT));

   POD_COMMIT(_pod);
   dBatchForScheduling      qry(_pod->_dbCon);
   dBatchInstNotCompleted   notCompQry(_pod->_dbCon);
   unsigned int             cnt = 0;

   qry.exec(now);

   while (qry.fetch())
   {
      notCompQry.exec(qry.orec.id, now);

      if (notCompQry.fetch())
      {
         _pod->_log->info(" - Cannot start (batch:%d, name:%s) because batch instance (id:%d, startdate:%s) is in status '%c: %s'",
            qry.orec.id,
            qry.orec.name.c_str(),
            notCompQry.orec.id,
            notCompQry.orec.endDate.format(jot, NICE_DATETIME_FORMAT),
            notCompQry.orec.status,
            tBatchInst::Status_Couplet::getValue(notCompQry.orec.status));

         continue;
      }

      insertNewBatchInst(&qry.orec);

      cnt++;
   }

   if (cnt)
      COMMIT;

   _pod->_log->debug("MonitorManager::scheduleBatches - Done(cnt:%u)", cnt);
}


void MonitorManager::insertNewBatchInst(tBatch *batch)
{
   dBatchInst batchInst(_pod->_dbCon);
   DateTime   null;
   char       jot[32];

   batchInst.insert(0,
                    batch->id,
                    tBatchInst::Status_Couplet::keyPending,
                    batch->runDate,
                    null,
                    null,
                    true,
                    false);

   insertChildBatchInst(batch, &batchInst.rec);

   _pod->_log->info("MonitorManager::insertNewBatchInst - Done(batch:%d, batchInst:%d, rundate:%s)", batch->id, batchInst.rec.id, batch->runDate.format(jot, NICE_DATETIME_FORMAT));
}


void MonitorManager::insertChildBatchInst(const tBatch     *batch,
                                   const tBatchInst *batchInst)
{
   if (batch->cycle == tBatch::Cycle_Couplet::keyMinutely ||
       batch->cycle == tBatch::Cycle_Couplet::keyHourly)
      return;

   _pod->_log->debug("MonitorManager::insertNewBatchInst - Start(batch:%d, batchInst:%d)", batch->id, batchInst->id);

   unsigned int   cnt = 0;
   dBatchByParent qry(_pod->_dbCon);
   dBatchInst     childInst(_pod->_dbCon);
   DateTime       null;

   qry.exec(batch->id);

   while (qry.fetch())
   {
      childInst.insert(batchInst->id,
                       batch->id,
                       tBatchInst::Status_Couplet::keyPending,
                       batch->runDate,
                       null,
                       null,
                       false);

      insertChildBatchInst(&qry.orec, &childInst.rec);

      cnt++;
   }

   _pod->_log->debug("MonitorManager::insertNewBatchInst - Done(batch:%d, batchInst:%d, cnt:%u)", batch->id, batchInst->id, cnt++);
}


void MonitorManager::createScheduleItems()
{
   _pod->_log->debug("MonitorManager::createScheduleItems - Start()");

   tBatchInst::List  instList;
   dBatchInst        inst(_pod->_dbCon);
   dBatch            batch(_pod->_dbCon);
   unsigned int      cnt = 0;
   DateTime          now(DateTime::now());

   dBatchInstForProcessing(_pod->_dbCon).exec(now).fetchAll(instList);

   _FOREACH(tBatchInst, inst, instList)
   {
      if (!inst.obj->parentId_NULL)
      {
         dBatchInst parent(_pod->_dbCon);

         parent.selectOne(inst.obj->parentId);

         if (parent.rec.status != tBatchInst::Status_Couplet::keyCompleted)
            continue;
      }

      insertJobInstances(inst.obj);
      cnt++;
   }

   _pod->_dbCon->rollback();

   _pod->_log->debug("MonitorManager::createScheduleItems - Done(cnt:%u)", cnt);
}


void MonitorManager::insertJobInstances(const tBatchInst *inst)
{
   _pod->_log->debug("MonitorManager::insertJobInstances - Start(batchInst:%d)", inst->id);

   Mettle::DB::DBLock     dbLock(100, 5);
   dBatchItemForInserting qry(_pod->_dbCon);
   dBatchInst             batchInst(_pod->_dbCon);
   dJobInst               jobInst(_pod->_dbCon);
   int32_t                prevJobInstId = 0;
   unsigned int           cnt = 0;

   batchInst.lockOne(inst->id, &dbLock);

   if (inst->status != batchInst.rec.status &&
       inst->status != tBatchInst::Status_Couplet::keyPending)
   {
      _pod->_dbCon->rollback();
      return;
   }

   qry.exec(inst->batchId);

   while (qry.fetch())
   {
      jobInst.insert(prevJobInstId,
                     qry.orec.jobId,
                     inst->id,
                     inst->runDate,
                     qry.orec.priority,
                     tJobInst::Status_Couplet::keyPending,
                     qry.orec.extraArgs,
                     qry.orec.groupJob,
                     qry.orec.groupBatch,
                     SOURCE,
                     prevJobInstId == 0);

      prevJobInstId = jobInst.rec.id;

      cnt++;
   }

   if (prevJobInstId == 0)
   {
      batchInst.rec.status = tBatchInst::Status_Couplet::keyCompleted;
   }
   else
   {
      batchInst.rec.startDate = DateTime::now();
      batchInst.rec.status    = tBatchInst::Status_Couplet::keyBusy;
   }

   batchInst.update();

   _pod->_dbCon->commit();

   _pod->_log->debug("MonitorManager::insertJobInstances - Done(batchInst:%d, cnt:%u)", inst->id, cnt);
}


void MonitorManager::checkForCompletedProcs()
{
   _pod->_log->debug("MonitorManager::checkForCompletedProcs - Start()");

   _procManager.houseKeeping();

   ProcessManager::ChildProc item;
   unsigned int              cnt = 0;

   while (_procManager.fetchChildExit(item))
   {
      cnt++;

      if (item.rc == 0)
      {
         _pod->_log->info(" - Job (pid:%d, name:%s) completed ok", item.pid, item.name.c_str());
         continue;
      }

      _pod->_log->error(" - Job (pid:%d, name:%s) failed! - Exception will be thrown...", item.pid, item.name.c_str());

      throw xMettle(__FILE__, __LINE__, SOURCE, "Job Child Process (%d) Failed!  No child procs should ever fail, terminating queue!", item.pid);
   }

   _pod->_log->debug("MonitorManager::checkForCompletedProcs - Done(cnt:%u)", cnt);
}


void MonitorManager::startPendingProcs()
{
   unsigned int numChildProcs = _procManager.numChildren();

   _pod->_log->debug("MonitorManager::startPendingProcs - Start(numprocs:%u, maxjobs:%u)", numChildProcs, _maxConcurrentJobs);

   if (_maxConcurrentJobs > 0 && numChildProcs >= _maxConcurrentJobs)
   {
      _pod->_log->debug("MonitorManager::startPendingProcs - Done(maxjobs limit reached)");
      return;
   }

   Mettle::DB::DBLock    dbLock(100, 1);
   String                prevGroup;
   tJobInst::List        runList;
   dJobInst              jobInst(_pod->_dbCon);
   unsigned int          cnt = 0;

   dJobInstReadyToRun(_pod->_dbCon).exec().fetchAll(runList);

   _FOREACH(tJobInst, rec, runList)
   {
      if (prevGroup == rec.obj->groupJob)
         continue;

      if (rec.obj->status != tJobInst::Status_Couplet::keyPending)
         continue;

      prevGroup = rec.obj->groupJob;

      jobInst.lockOne(rec.obj->id, &dbLock);

      if (jobInst.rec.status != tJobInst::Status_Couplet::keyPending)
      {
         _pod->_dbCon->rollback();
         continue;
      }

      jobInst.rec.status     = tJobInst::Status_Couplet::keyWaiting;
      jobInst.rec.modifiedBy = SOURCE;

      jobInst.update();

      _pod->_dbCon->commit();

      ProcessManager::ChildProc *proc;

      proc = _procManager.spawnChild(_jobRunnerPath.c_str(),
                                     String().format("--job %d", jobInst.rec.id),
                                     0,
                                     false,
                                     true);

      _pod->_log->info(" - Job (pid:%d, name:%s) started", proc->pid, proc->name.c_str());

      cnt++;

      if (_maxConcurrentJobs > 0 && ++numChildProcs >= _maxConcurrentJobs)
      {
         _pod->_log->debug(" - Max child process limit reached.");
         break;
      }
   }

   _pod->_dbCon->rollback();

   _pod->_log->debug("MonitorManager::startPendingProcs - Done(numprocs:%u, maxjobs:%u)", numChildProcs, _maxConcurrentJobs);
}


void MonitorManager::incrementRundate(BS::Monitor::DB::tBatch *batch)
{
   DateTime next = batch->runDate;
   char     jot[32];

   if (batch->runInterval < 1)
   {
      _pod->_log->error("MonitorManager::incrementRundate- Not Done(!) Invalid Interval (%c) detected in batch.  Disabling this batch!", batch->runInterval);

      batch->status     = tBatch::Status_Couplet::keyDisabled;
      batch->modifiedBy = SOURCE;

      return;
   }

   if (batch->cycle == tBatch::Cycle_Couplet::keyMinutely)
   {
      next.add((unsigned int) batch->runInterval, Time::Minutes);
   }
   else if (batch->cycle == tBatch::Cycle_Couplet::keyHourly)
   {
      next.add((unsigned int) batch->runInterval, Time::Hours);
   }
   else if (batch->cycle == tBatch::Cycle_Couplet::keyDaily)
   {
      next.add((unsigned int) batch->runInterval, Date::Days);
   }
   else if (batch->cycle == tBatch::Cycle_Couplet::keyWeekly)
   {
      next.add((unsigned int) batch->runInterval * 7, Date::Days);
   }
   else if (batch->cycle == tBatch::Cycle_Couplet::keyMonthly)
   {
      next.add((unsigned int) batch->runInterval, Date::Months);
   }
   else
   {
      _pod->_log->error("MonitorManager::incrementRundate- Not Done(!) Invalid Cycle (%c) detected in batch.  Disabling this batch!", batch->cycle);

      batch->status     = tBatch::Status_Couplet::keyDisabled;
      batch->modifiedBy = SOURCE;

      return;
   }

   Time rtime(batch->runTime.c_str(), false);

   if (!rtime.isNull())
   {
      if (batch->cycle == tBatch::Cycle_Couplet::keyMinutely)
      {
         next.time.setSecond(rtime.second());
      }
      else if (batch->cycle == tBatch::Cycle_Couplet::keyHourly)
      {
         next.time.setSecond(rtime.second());
         next.time.setMinute(rtime.minute());
      }
      else if (batch->cycle == tBatch::Cycle_Couplet::keyDaily)
      {
         next.time.setSecond(rtime.second());
         next.time.setMinute(rtime.minute());
         next.time.setHour(rtime.hour());
      }
   }

   batch->runDate = next;
}

#undef SOURCE
#undef NICE_DATETIME_FORMAT
#undef POD_COMMIT
#undef COMMIT

}}
