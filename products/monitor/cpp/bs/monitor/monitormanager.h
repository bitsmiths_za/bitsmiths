/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_MONITOR_MONITORMANAGER_H_
#define __BS_MONITOR_MONITORMANAGER_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/string.h"
#include "mettle/lib/processmanager.h"

namespace BS {

namespace Lib
{
   class Pod;
}

namespace Monitor {

namespace DB
{
   class tBatch;
   class tBatchInst;
}

class MonitorManager
{
public:

   MonitorManager(      BS::Lib::Pod        *pod,
                  const Mettle::Lib::String &jobRunnerPath,
                  const unsigned int         maxConcurrentJobs,
                  const unsigned int         batchCheckInterval);

   virtual ~MonitorManager() noexcept;

   virtual void initialize();

   virtual void run();

protected:

   BS::Lib::Pod                  *_pod;
   Mettle::Lib::ProcessManager    _procManager;
   Mettle::Lib::String            _jobRunnerPath;
   unsigned int                   _maxConcurrentJobs;
   unsigned int                   _batchCheckInterval;
   unsigned int                   _batchInterval;


   virtual void failRunningJobsInUnexpectedState(const char status);

   virtual void completeFinishedBatches();

   virtual void scheduleBatches();

   virtual void insertNewBatchInst(BS::Monitor::DB::tBatch *batch);

   virtual void insertChildBatchInst(const BS::Monitor::DB::tBatch     *batch,
                                     const BS::Monitor::DB::tBatchInst *batchInst);

   virtual void insertJobInstances(const BS::Monitor::DB::tBatchInst *inst);

   virtual void createScheduleItems();

   virtual void checkForCompletedProcs();

   virtual void startPendingProcs();

   virtual void incrementRundate(BS::Monitor::DB::tBatch *batch);

};

}}

#endif
