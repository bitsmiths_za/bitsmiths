/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "bs/monitor/server.h"

#include "bs/monitor/api.h"
#include "bs/monitor/monitormanager.h"

#include "mettle/lib/common.h"
#include "mettle/lib/logger.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/string.h"

#include "mettle/braze/stdtypelists.h"

#define MODULE   "BS.Monitor.Server"

using namespace Mettle::Lib;
using namespace Mettle::IO;
using namespace Mettle::Braze;

using namespace BS::Monitor::DB;
using namespace BS::Monitor::Braze;

namespace BS { namespace Monitor {

Server::Server(BS::Lib::Pod              *pod,
               Mettle::Braze::ITransport *transPort)
{
   _transPort    = transPort;
   _pod          = pod;
   _api          = 0;
   _monMan       = 0;
   _monManErrors = 0;
}

Server::~Server()
{
   _destruct();
}

void Server::_destruct()
{
   _DELETE(_api)
   _DELETE(_monMan)
}

void Server::_slackTimeHandler()
{
   if (!_monMan)
      return;

   bool exceptionCaught = false;

   try
   {
      _monMan->run();
   }
   _SPOT_STD_EXCEPTIONS(_pod->_log, exceptionCaught = true)

   if (exceptionCaught)
      _monManErrors++;
   else
      _monManErrors = 0;
}

void Server::_construct()
{
   _destruct();

   _api          = _initApi();
   _monMan       = _initMonitorManager();
   _monManErrors = 0;

   if (_monMan)
      _monMan->initialize();
}

Api *Server::_initApi()
{
   return new Api(_pod);
}

MonitorManager *Server::_initMonitorManager()
{
   return 0;
}

bool Server::_shutdownServer()
{
   if (_monManErrors < 10)
      return false;

   _pod->_log->error("_ShutdownServer: [%u] concurrent monitor manager error have occurred, shutting down server!", _monManErrors);

   return true;
}

bool Server::ping()
{
   String addr;

   _pod->_log->info("Ping (%s)", _transPort->clientAddress(&addr)->c_str());

   return true;
}

tBatch *Server::batchLoad(/* i  */ const int32_t                   id,
                          /* i  */ const Mettle::Lib::String      &name)
{
   return _api->batchLoad(id, name);
}

int32_t Server::batchSave(/* i  */ const char     dbAction,
                          /* i  */ const tBatch  &rec)
{
   return _api->batchSave(dbAction, rec);
}

tBatch::List *Server::batchSearch(/* i  */ const bBatchQuery &criteria)
{
   return _api->batchSearch(criteria);
}

tBatchInst::List *Server::batchInstSearch(/* i  */ const bBatchInstQuery &criteria)
{
   return _api->batchInstSearch(criteria);
}

tBatchItem::List *Server::batchItemsLoad(/* i  */ const int32_t                   batchId)
{
   return _api->batchItemsLoad(batchId);
}

void Server::batchItemsSave(/* i  */ const int32_t           batchId,
                            /* i  */ const tBatchItem::List &items)
{
   _api->batchItemsSave(batchId, items);
}

tJob::List *Server::jobSearch(/* i  */ const bJobQuery          &criteria)
{
   return _api->jobSearch(criteria);
}

tJob *Server::jobLoad(/* i  */ const int32_t      id,
                      /* i  */ const String      &name)
{
   return _api->jobLoad(id, name);
}

int32_t Server::jobSave(/* i  */ const char     dbAction,
                        /* i  */ const tJob    &rec)
{
   return _api->jobSave(dbAction, rec);
}

void Server::jobInstCancel(/* i  */ const int32_t      jobInstId,
                           /* i  */ const String      &userName,
                           /* i  */ const String      &reason)
{
   return _api->jobInstCancel(jobInstId, userName, reason);
}

bJobInstData *Server::jobInstLoad(/* i  */ const int32_t  id)
{
   return _api->jobInstLoad(id);
}

bool Server::jobInstRun(/* i  */ const int32_t  jobInstId,
                        /* i  */ const String  &modifiedBy,
                        /* i  */ const bool     rerunIfFailed,
                        /* i  */ const bool     rerunIfCompleted)
{
   return _api->jobInstRun(jobInstId, modifiedBy, rerunIfFailed, rerunIfCompleted);
}

int32_t Server::jobInstSave(/* i  */ const char      dbAction,
                            /* i  */ const tJobInst &rec)
{
   return _api->jobInstSave(dbAction, rec);
}

oJobInstSearch::List *Server::jobInstSearch(/* i  */ const bJobInstQuery &criteria)
{
   return _api->jobInstSearch(criteria);
}


#undef MODULE

}}
