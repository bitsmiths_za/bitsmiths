/******************************************************************************/
/*                          This file is part of:                             */
/*                               BITSMITHS                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/bitsmiths                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __BS_MONITOR_SERVER_H_
#define __BS_MONITOR_SERVER_H_

#include "bs/lib/iprovider.h"
#include "bs/lib/pod.h"

#include "bs/monitor/braze/simonitor.h"

#include "mettle/braze/itransport.h"

namespace BS { namespace Monitor {

class Api;
class MonitorManager;

class Server : public BS::Monitor::Braze::siMonitor
{
public:

   Server(BS::Lib::Pod               *pod,
          Mettle::Braze::ITransport  *transPort);

   ~Server();

   void _construct();

   void _destruct();

   bool _shutdownServer();

   void _slackTimeHandler();



   bool ping();

   BS::Monitor::DB::tBatch *batchLoad(/* i  */ const int32_t                   id,
                                      /* i  */ const Mettle::Lib::String      &name);

   int32_t batchSave(/* i  */ const char                      dbAction,
                     /* i  */ const BS::Monitor::DB::tBatch  &rec);

   BS::Monitor::DB::tBatch::List *batchSearch(/* i  */ const BS::Monitor::Braze::bBatchQuery &criteria);

   BS::Monitor::DB::tBatchInst::List *batchInstSearch(/* i  */ const BS::Monitor::Braze::bBatchInstQuery &criteria);

   BS::Monitor::DB::tBatchItem::List *batchItemsLoad(/* i  */ const int32_t batchId);

   void batchItemsSave(/* i  */ const int32_t                   batchId,
                       /* i  */ const BS::Monitor::DB::tBatchItem::List &items);

   BS::Monitor::DB::tJob::List *jobSearch(/* i  */ const BS::Monitor::Braze::bJobQuery &criteria);

   BS::Monitor::DB::tJob *jobLoad(/* i  */ const int32_t                   id,
                                  /* i  */ const Mettle::Lib::String      &name);

   int32_t jobSave(/* i  */ const char                      dbAction,
                   /* i  */ const BS::Monitor::DB::tJob    &rec);

   void jobInstCancel(/* i  */ const int32_t                   jobInstId,
                      /* i  */ const Mettle::Lib::String      &userName,
                      /* i  */ const Mettle::Lib::String      &reason);

   BS::Monitor::Braze::bJobInstData *jobInstLoad(/* i  */ const int32_t  id);

   bool jobInstRun(/* i  */ const int32_t                   jobInstId,
                   /* i  */ const Mettle::Lib::String      &modifiedBy,
                   /* i  */ const bool                      rerunIfFailed,
                   /* i  */ const bool                      rerunIfCompleted);

   int32_t jobInstSave(/* i  */ const char                      dbAction,
                       /* i  */ const BS::Monitor::DB::tJobInst &rec);

   BS::Monitor::DB::oJobInstSearch::List *jobInstSearch(/* i  */ const BS::Monitor::Braze::bJobInstQuery &criteria);

protected:

   virtual Api *_initApi();

   virtual MonitorManager *_initMonitorManager();

   Mettle::Braze::ITransport   *_transPort;
   BS::Lib::Pod                *_pod;
   Api                         *_api;
   MonitorManager              *_monMan;
   unsigned int                 _monManErrors;
};

}}

#endif
