# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #
file-type : Mettle.DB.Table
version   : 2.1
name      : BatchItem

standard-procs:
  select_one : true
  lock_one   : false
  select_all : false
  update     : true
  insert     : true
  delete_one : true
  delete_all : false

columns:
  - { name: id,            type: int64 }
  - { name: batch_id,      type: int64 }
  - { name: job_id,        type: int64 }
  - { name: extra_args,    type: string, max: 128, null: True }
  - { name: stamp_by,      type: string, max: 128 }
  - { name: stamp_tm,      type: timestamp }

primary-key: [ id, batch_id ]

foreign-keys:
  - { name: fk_batch, table: Batch, columns: [ batch_id ] }
  - { name: fk_job,   table: Job,   columns: [ job_id ] }

indexes:
  - { name: ix_batch, columns: [ batch_id ] }
  - { name: ix_job,   columns: [ job_id ] }

procs:
  - name   : ForInserting
    input  :
      - { name: batchId, type: this.batch_id }
    output :
      - { name: jobId,       type: this.job_id }
      - { name: extraArgs,   type: this.extra_args }
      - { name: groupJob,    type: 'column:Job.group_id' }
      - { name: groupBatch,  type: 'column:Job.name' }
      - { name: priority,    type: 'column:Job.priority' }
    sql    :
      - db    : std
        query : |
                select
                  i.job_id,
                  i.extra_args,
                  j.group_id,
                  substring(j.name, 1, 32),
                  j.priority
                from
                  monitor.batchitem i
                    join
                  monitor.job j
                    on j.id = i.job_id
                where
                  i.batch_id = :batchId
                order by
                  i.id

  - name   : ByBatchId
    input  :
      - { name: batchId,  type: this.batch_id }
      - { name: criteria, type: dynamic }
    output :
      - { type: 'table:this' }
    sql    :
      - db    : std
        query : |
                select
                  i.*
                from
                  monitor.batchitem i
                where
                  i.batch_id = :batchId
                  [criteria]
                order by
                  i.id

  - name   : DeleteByBatchId
    input  :
      - { name: batchId, type: this.batch_id }
    sql    :
      - db    : std
        query : |
                delete from
                  monitor.batchitem
                where
                  batch_id = :batchId


  - name   : SwopId
    input  :
      - { name: batchId, type: this.batch_id }
      - { name: currId,  type: int64 }
      - { name: newId,   type: int64 }
    sql    :
      - db    : std
        query : |
                update
                  monitor.batchitem
                set
                  id = :newId
                where
                  batch_id = :batchId and
                  id       = :currId
