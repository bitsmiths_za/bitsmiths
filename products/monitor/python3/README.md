# BITSMITHS MONITOR #

The monitor is a light weight orchestration package where a system requires batch jobs to be run on a recurring basis.  The monitor
uses a relational database to track which jobs are to be scheduled, running, and have completed.


The main features are:
- Create batch schedules on a minute, hour, day, week, month, or yearly cycle.
- Batch schedules can have dependencies on one another.
- Limit the amount of concurrent running jobs.
- Overload the main classes to customize the package for your needs.
- Supports ad-hoc running of batch jobs.
- Stores a full run history of all your batch jobs.


This product has a dependency on:

- Mettle (`bitsmiths-mettle`)
- Bitsmiths Library (`bitsmiths-lib`)


**Note** that we package the generated Mettle code in this package, which means that the Mettle version required in this module is important.

## Tables & Setup ##

*Monitor* is designed to use a relational database with a schema/namespace of `monitor` in your database. You will need to create this schema manually.
It requires several other relational database tables.

The package provides the SQL code to create these tables. There are two ways to access the table creation SQL.

1. You can run `bs-monitor -g postgresql` at the command line, this will print all the SQL to `stdout`.
2. You can import the bs_monitor module and get the SQL as shown below:

```python

import bs_monitor

print(bs_monitor.get_table_sql('postgresql'))

```

**Note!** Currently only *postgresql* SQL is added to the package. If you want us to support another database let
us know and we will put it into the development pipeline.


## Change History ##

### 2.1.10 ###

| Type | Description |
| ---- | ----------- |
| New  | Implemented the dataclass feature from the latest mettle (2.1.13) version. |


### 2.1.9 ###

| Type | Description |
| ---- | ----------- |
| Bug  | Applied code generation bug fix from lastest mettle library. |

### 2.1.8 ###

| Type | Description |
| ---- | ----------- |
| New  | Improved TUI shortcuts and fixed some minor TUI issues. |

### 2.1.7 ###

| Type | Description |
| ---- | ----------- |
| New  | Added shortcuts to all the TUI buttons. |
| Bug  | Fixed TUI crash when using the button to Rerun a Job Instance. |

### 2.1.6 ###

| Type | Description |
| ---- | ----------- |
| New  | Added `--tui` option the command line interface. This will bring up a text user interface for the monitor. You will need `pip install asciimatics` as it is optional. |


### 2.1.5 ###

| Type | Description |
| ---- | ----------- |
| Bug  | Fixed a monitor manager crash when handling database lock timeouts, introduced by the mettle refactor. |


### 2.1.4 ###

| Type | Description |
| ---- | ----------- |
| Bug  | Fixed a crash when using monthly or yearly monitor intervals. |


### 2.1.3 ###

| Type | Description |
| ---- | ----------- |
| Bug  | Default job and runtime arguement generation was still using old C++ style, has been fixed. |


### 2.1.2 ###

| Type | Description |
| ---- | ----------- |
| Bug  | Fixed a bug where schedule batch jobs would not set or wait for their parent job. |


### 2.1.1 ###

| Type | Description |
| ---- | ----------- |
| Bug  | Fixed a typo bug in the job-runner, where any job that had a parent would cause it to raise an exception. |
| Bug  | Fixed a bug where the monitor-cli spawn job command would add its arguments to the spawned job. |


### 2.1.0 ###

| Type | Description |
| ---- | ----------- |
| New  | The python equivalent of the monitor is finally released. |
