UNAME   = $(OS)

ifeq ($(UNAME), Windows_NT)
	MAKE        = make.exe
	SUP-SQL    = psql.exe -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U postgres -v ON_ERROR_STOP=1 -At
	SUP-DB-SQL = psql.exe -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U postgres -d $(TARGET_DB_NAME) -v ON_ERROR_STOP=1 -At
	SQL        = psql.exe -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U $(TARGET_DB_USER) -v ON_ERROR_STOP=1 -At
	DEL_CMD     = del
else
	MAKE        = make
	SUP-SQL    = psql -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U postgres -v ON_ERROR_STOP=1 -At
	SUP-DB-SQL = psql -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U postgres -d $(TARGET_DB_NAME) -v ON_ERROR_STOP=1 -At
	SQL        = psql -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U $(TARGET_DB_USER) -v ON_ERROR_STOP=1 -At
	DEL_CMD     = rm -f
endif

TARGET_DB_NAME = bs
TARGET_DB_USER = bs
TARGET_DB_PASS = dev
TARGET_DB_HOST = 127.0.0.1
TARGET_DB_PORT = 5432

SQL_CMD_ARG   = "-c"
SQL_FILE_ARG  = "-f"
DB_TYPE       = postgresql
TARG_SCHEMA   =
SCHEMAS       = audit loco fura monitor crest

AUDIT_PATH  = ../../products/audit/mettledb/sqldef
AUDIT_INDEX = $(patsubst %.index,%.run_index, $(sort $(wildcard $(AUDIT_PATH)/$(DB_TYPE)/*.index)))
AUDIT_TABLE = $(patsubst %.table,%.run_table, $(sort $(wildcard $(AUDIT_PATH)/$(DB_TYPE)/*.table)))
AUDIT_CONST = $(patsubst %.constraint,%.run_const, $(sort $(wildcard $(AUDIT_PATH)/$(DB_TYPE)/*.constraint)))

LOCO_PATH  = ../../products/loco/mettledb/sqldef
LOCO_INDEX = $(patsubst %.index,%.run_index, $(sort $(wildcard $(LOCO_PATH)/$(DB_TYPE)/*.index)))
LOCO_TABLE = $(patsubst %.table,%.run_table, $(sort $(wildcard $(LOCO_PATH)/$(DB_TYPE)/*.table)))
LOCO_CONST = $(patsubst %.constraint,%.run_const, $(sort $(wildcard $(LOCO_PATH)/$(DB_TYPE)/*.constraint)))
LOCO_SQL   = $(patsubst %.sql,%.run_sql, $(sort $(wildcard ../../products/loco/sql/initdb.$(DB_TYPE).sql)))

FURA_PATH  = ../../products/fura/mettledb/sqldef
FURA_INDEX = $(patsubst %.index,%.run_index, $(sort $(wildcard $(FURA_PATH)/$(DB_TYPE)/*.index)))
FURA_TABLE = $(patsubst %.table,%.run_table, $(sort $(wildcard $(FURA_PATH)/$(DB_TYPE)/*.table)))
FURA_CONST = $(patsubst %.constraint,%.run_const, $(sort $(wildcard $(FURA_PATH)/$(DB_TYPE)/*.constraint)))
FURA_SQL   = $(patsubst %.sql,%.run_sql, $(sort $(wildcard ../../products/fura/sql/initdb.$(DB_TYPE).sql)))
FURA_TEST  = $(patsubst %.sql,%.run_sql, $(sort $(wildcard ../_sql/fura-test.$(DB_TYPE).sql)))

MONITOR_PATH  = ../../products/monitor/mettledb/sqldef
MONITOR_INDEX = $(patsubst %.index,%.run_index, $(sort $(wildcard $(MONITOR_PATH)/$(DB_TYPE)/*.index)))
MONITOR_TABLE = $(patsubst %.table,%.run_table, $(sort $(wildcard $(MONITOR_PATH)/$(DB_TYPE)/*.table)))
MONITOR_CONST = $(patsubst %.constraint,%.run_const, $(sort $(wildcard $(MONITOR_PATH)/$(DB_TYPE)/*.constraint)))
MONITOR_SQL   = $(patsubst %.sql,%.run_sql, $(sort $(wildcard ../../products/monitor/sql/initdb.$(DB_TYPE).sql)))
MONITOR_TEST  = $(patsubst %.sql,%.run_sql, $(sort $(wildcard ../_sql/monitor-test.$(DB_TYPE).sql)))


.PHONY: help
help:
	@echo
	@echo "  Settings [host:$(TARGET_DB_HOST)/$(TARGET_DB_PORT), db:$(TARGET_DB_NAME) user:$(TARGET_DB_USER)]"
	@echo
	@echo "  Usage: localdb.postgres.makefile [target]"
	@echo "  ----------------------------------------"
	@echo "  rebuild         : rebuild all bitsmiths databases"
	@echo "  rebuild-fura    : rebuild the fura schema"
	@echo "  rebuild-monitor : rebuild the monitor schema"
	@echo
	@echo "  -- Utility --"
	@echo
	@echo "  create-user     : create the user if it is missing"
	@echo "  create-db       : create the database if it is missing"
	@echo "  create-schemas  : create required schemas"
	@echo "  drop-user       : drop the user, won't work if has dependencies"
	@echo "  drop-db         : drops the db "
	@echo
	@echo "  help            : this help"
	@echo


.PHONY: drop-user
drop-user:role_count = $(shell $(SUP-SQL) -c "SELECT 1 FROM pg_roles WHERE rolname='$(TARGET_DB_USER)'")
drop-user:
	$(if $(filter-out $(role_count), 1),\
		@echo "  role [$(TARGET_DB_USER)] does not exist"\
	,\
		@echo "  dropping role [$(TARGET_DB_USER)]" ;\
		$(SUP-SQL) -c "DROP OWNED BY \"$(TARGET_DB_USER)\"" ;\
		$(SUP-SQL) -c "DROP USER \"$(TARGET_DB_USER)\"" \
	)


.PHONY: create-user
create-user:role_count = $(shell $(SUP-SQL) -c "SELECT 1 FROM pg_roles WHERE rolname='$(TARGET_DB_USER)'")
create-user:
	$(if $(filter-out $(role_count), 1),\
		@echo "  creating role [$(TARGET_DB_USER)]" ;\
		$(SUP-SQL) -c "CREATE USER \"$(TARGET_DB_USER)\" WITH PASSWORD '$(TARGET_DB_PASS)'" \
	,\
		@echo "  role [$(TARGET_DB_USER)] already exists"\
    )


.PHONY: create-schemas
create-schemas:
	for sname in $(SCHEMAS); do\
		$(SQL) $(SQL_CMD_ARG) "CREATE SCHEMA $$sname;" $(SQL_DEST_ARG);\
	done


.PHONY: drop-db
drop-db:db_count = $(shell $(SUP-SQL) -c "SELECT 1 FROM pg_database WHERE datname='$(TARGET_DB_NAME)'")
drop-db:
	$(if $(filter-out $(db_count), 1),\
		@echo "  database [$(TARGET_DB_NAME)] does not exist" \
	,\
		@echo "  dropping database [$(TARGET_DB_NAME)]" ;\
		$(SUP-SQL) -c "DROP DATABASE \"$(TARGET_DB_NAME)\"" \
    )


.PHONY: create-db
create-db:db_count = $(shell $(SUP-SQL) -c "SELECT 1 FROM pg_database WHERE datname='$(TARGET_DB_NAME)'")
create-db: create-user
	$(if $(filter-out $(db_count), 1),\
		@echo "  creating database [$(TARGET_DB_NAME)]" ;\
		$(SUP-SQL) -c "CREATE DATABASE \"$(TARGET_DB_NAME)\" ENCODING \"UTF-8\" OWNER \"$(TARGET_DB_USER)\"" ;\
		$(SUP-SQL) -c "GRANT ALL PRIVILEGES ON DATABASE \"$(TARGET_DB_NAME)\" TO \"$(TARGET_DB_USER)\"" ;\
		$(SUP-DB-SQL) -c "ALTER SCHEMA public OWNER TO \"${TARGET_DB_USER}\"" ;\
		$(SUP-DB-SQL) -c "CREATE EXTENSION IF NOT EXISTS pgcrypto" ;\
	,\
		@echo "  database [$(TARGET_DB_NAME)] already exists"\
    )


.PHONY: build-audit-tables
build-audit-tables: $(AUDIT_TABLE) $(AUDIT_CONST) $(AUDIT_INDEX)
	$(DEL_CMD) *.log

.PHONY: build-loco-tables
build-loco-tables: $(LOCO_TABLE) $(LOCO_CONST) $(LOCO_INDEX) $(LOCO_SQL)
	$(DEL_CMD) *.log

.PHONY: build-fura-tables
build-fura-tables: $(FURA_TABLE) $(FURA_CONST) $(FURA_INDEX) $(FURA_SQL) $(FURA_TEST)
	$(DEL_CMD) *.log

.PHONY: build-monitor-tables
build-monitor-tables: $(MONITOR_TABLE) $(MONITOR_CONST) $(MONITOR_INDEX) $(MONITOR_SQL) $(MONITOR_TEST)
	$(DEL_CMD) *.log

.PHONY: recreate-schema
recreate-schema:
	$(SQL) $(SQL_CMD_ARG) "DROP SCHEMA $(TARG_SCHEMA) CASCADE;" $(SQL_DEST_ARG);\
	$(SQL) $(SQL_CMD_ARG) "CREATE SCHEMA $(TARG_SCHEMA);" $(SQL_DEST_ARG);


.PHONY: rebuild
rebuild: \
   drop-db \
   create-db \
   create-schemas \
   build-audit-tables \
   build-loco-tables \
   build-fura-tables \
   build-monitor-tables \

.PHONY: rebuild-fura
rebuild-fura:TARG_SCHEMA=fura
rebuild-fura: \
   recreate-schema \
   build-fura-tables \

.PHONY: rebuild-monitor
rebuild-monitor:TARG_SCHEMA=monitor
rebuild-monitor: \
   recreate-schema \
   build-monitor-tables \


%.run_index: %.index
	$(SQL) $(SQL_FILE_ARG) $< $(SQL_DEST_ARG)

%.run_table: %.table
	$(SQL) $(SQL_FILE_ARG) $< $(SQL_DEST_ARG)

%.run_const: %.constraint
	$(SQL) $(SQL_FILE_ARG) $< $(SQL_DEST_ARG)

%.run_sql: %.sql
	$(SQL) $(SQL_FILE_ARG) $< $(SQL_DEST_ARG)
