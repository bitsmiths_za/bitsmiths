delete from fura.usrauth;

insert into
  fura.usrauth
select
  u.site_id,
  u.id,
  'P',
  'TODO',
  0,
  null,
  null,
  null,
  null,
  null,
  '[ut]',
  current_timestamp
from
  fura.usr u
where
  u.site_id = 1 and
  u.id  = 'admin';

update
  fura.usrauth u
set
  auth_data = crypt(u.site_id::varchar || u.usr_id || 'dev', gen_salt('bf', 12))
where
  u.auth_type = 'P' and
  u.site_id = 1 and
  u.usr_id = 'admin';


insert into fura.funcgrp select 'BS-TEST', 'BS Tests Func Group', '[ut]', current_timestamp;
insert into fura.funcgrp select 'BS-XYZ', 'BS xyz Group', '[ut]', current_timestamp;

insert into fura.func select 'bstest.create', 'BS-TEST', 'BS Test Create', 'C', 'Y', 5, '[ut]', current_timestamp;
insert into fura.func select 'bstest.read',   'BS-TEST', 'BS Test Read',   'C', 'Y', 6, '[ut]', current_timestamp;
insert into fura.func select 'bstest.update', 'BS-TEST', 'BS Test Update', 'C', 'Y', 7, '[ut]', current_timestamp;
insert into fura.func select 'bstest.delete', 'BS-TEST', 'BS Test Delete', 'C', 'Y', 4, '[ut]', current_timestamp;

insert into fura.func select 'bstest.nav',   'BS-XYZ', 'BS Authorise',   'A', 'Y', 2, '[ut]', current_timestamp;
insert into fura.func select 'bstest.menu',  'BS-XYZ', 'BS Menu Access', 'N', 'Y', 5, '[ut]', current_timestamp;
