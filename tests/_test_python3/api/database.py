import logging

from databases      import Database

__dbcon  = None
DB_URL   = 'postgresql://bs:dev@127.0.0.1/bs'


def print_safe_db_url() -> str:
    at_idx = DB_URL.find('@')

    if at_idx != -1:
        col_idx = DB_URL.rfind(':', 0, at_idx) + 1

        return '%s********%s' % (DB_URL[:col_idx], DB_URL[at_idx:])

    return DB_URL


async def connect_async():
    global __dbcon

    pool_min = 4
    pool_max = 10

    logging.getLogger().info('Connecting to database: %s [pool: %d - %d] (async)' % (
        print_safe_db_url(), pool_min, pool_max))

    if not __dbcon:
        __dbcon = Database(DB_URL,
                           min_size = pool_min,
                           max_size = pool_max)


    if not __dbcon.is_connected:
        await __dbcon.connect()

        db_logger = logging.getLogger('databases')

        if db_logger:
            db_logger.propagate = False


async def disconnect_async():
    global __dbcon

    if __dbcon and __dbcon.is_connected:
        await __dbcon.disconnect()
        __dbcon = None

        logging.getLogger().info('Disconnected from database (async)')
