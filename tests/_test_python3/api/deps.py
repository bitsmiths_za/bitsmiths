# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import logging
import fastapi

from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials

from databases.core import Connection

from mettle.db.encode_io_databases.aconnect_postgres import AConnectPostgres

from bs_lib      import PodAsync

from . import cache
from . import database

from .test_transport_server import TestTransportServer
from .test_server           import TestServer
from .test_server_async     import TestServerAsync


class JWTBearer(HTTPBearer):
    def __init__(self, auto_error: bool = False):
        HTTPBearer.__init__(self, auto_error=auto_error)

    async def __call__(self, request: fastapi.Request):

        if 'BEARER' in request.headers:
            credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)

            if credentials:
                if not credentials.scheme == "Bearer":
                    return None

                return { 'bearer': True, 'tok': credentials.credentials }

        tok = request.headers.get('BRAZE-TOK') or 'BRZTOK'

        return { 'bearer' : False, 'tok': tok }



async def get_pod() -> PodAsync:
    dbcon = None

    async with Connection(database.__dbcon._backend) as conn:
        dbcon = AConnectPostgres(conn)
        pod   = PodAsync(None, dbcon, logging.getLogger())

        yield pod
        del pod
        await dbcon.rollback()



async def get_test_server_async(pod: PodAsync = fastapi.Depends(get_pod)) -> TestServerAsync:
    """
    Gets a newly async server with new async pod and db connector.
    """
    ts = TestTransportServer()
    ts.construct(None)

    server = TestServerAsync(ts, pod)

    server.construct()

    return server


def get_test_server() -> TestServer:
    """
    Gets a normal normal non async server
    """
    if cache.sync_server:
        return cache.sync_server

    ts = TestTransportServer()
    ts.construct(None)

    server = TestServer(ts, cache.pod)

    server.construct()

    cache.sync_server = server

    return cache.sync_server
