#!/usr/bin/env python

# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import fastapi
import logging
import os
import typing

from fastapi.responses         import PlainTextResponse
from starlette.middleware.cors import CORSMiddleware

from bs_lib import Pod

from _test_python3.api       import deps

from . import cache
from . import database


SYS_ID = '[bs-servers ut]'

logging.basicConfig(format='%(levelname)s - %(message)s', level=logging.DEBUG)

logging.info('%s Service Starting' % SYS_ID)


app = fastapi.FastAPI(
    title       = 'Bitsmitshs Test Service',
    openapi_url = "/openapi.json",
)

app.add_middleware(
    CORSMiddleware,
    allow_credentials = True,
    allow_origins     = ["*"],
    allow_methods     = ["*"],
    allow_headers     = ["*"])


@app.on_event("startup")
async def startup():
    await database.connect_async()

    from mettle.db.psycopg2.connect import Connect as Psycopg2Connect

    cache.dbcon = Psycopg2Connect()

    cache.dbcon.connect(database.DB_URL)

    cache.pod = Pod(None, cache.dbcon, logging.getLogger(), SYS_ID)

    logging.info('%s Ready for action' % SYS_ID)


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect_async()

    if cache.dbcon:
        cache.dbcon.disconnect()
        cache.dbcon = None




@app.get("/ping", response_class=PlainTextResponse)
async def ping():
    """
    Always returns 'OK' - used for health checkers.
    """
    return 'OK'


@app.get("/")
async def root():
    """
    Returns the service name and the running environment name.
    """
    return {
        'service'  : SYS_ID,
        'version'  : os.getenv('BITSMITHS_VERSION'),
        'codename' : os.getenv('BITSMITHS_VER_CODENAME'),
    }


def _set_braze_cookie(resp: fastapi.Response, name: str, value: str):
    """
    Set the authentication cookie.

    :param resp: The response object on which to set the cookie
    :param name: The name of the cookie
    :param value: The value of the cookie
    """
    resp.set_cookie(
        key      = name,
        value    = value,
        secure   = False,
        httponly = False,
        samesite = 'none')



@app.post("/braze/{server}/{rpc}", response_model = typing.List)
def braze_call(
    req: fastapi.Request,
    resp: fastapi.Response,
    server: str,
    rpc: str,
    braze_data: list = fastapi.Body(..., embed=False),
    test_server = fastapi.Depends(deps.get_test_server),
) -> typing.List:
    """
    B2B Braze API
    """
    # logging.debug('%s /braze/%s/%s - start %r' % (SYS_ID, server, rpc, braze_data))
    logging.debug('%s /braze/%s/%s - start' % (SYS_ID, server, rpc))

    tok_id = req.headers.get('BRAZE-TOK') or 'BRZTOK'
    res    = []

    try:
        brz_tok   = req.cookies.get(tok_id)
        impl_data = None

        test_server.token_set(brz_tok)

        test_server.get_transport().set_req(req)
        test_server.get_transport().set_stream_list(braze_data)

        rc    = test_server.service_rpc(impl_data)
        res   = test_server.get_transport().get_stream_list()

        if rc != 0:
            logging.warning('%s error out: %s' % (SYS_ID, str(res)))
            # raise Exception(str(res)) - In production we should check for visibile exceptions and only throw those
            #  non visible exceptions. But for testing we are just going to return all exceptions as error messages.

        marsh = test_server.get_marshaler()

        if marsh:
            new_tok = marsh._server_impl()._has_new_rpc_token()

            if new_tok:
                test_server.token_set(new_tok)

    finally:
        _set_braze_cookie(resp, tok_id, test_server.token_get())
        test_server.get_transport().set_req(None)


    logging.debug('%s /braze/%s/%s - done' % (SYS_ID, server, rpc))

    return res


@app.post("/braze_async/{server}/{rpc}", response_model = typing.List)
async def braze_call_async(
    req: fastapi.Request,
    resp: fastapi.Response,
    server: str,
    rpc: str,
    braze_data: list = fastapi.Body(..., embed=False),
    test_server = fastapi.Depends(deps.get_test_server_async),
    access_token: dict = fastapi.Depends(deps.JWTBearer())
) -> typing.List:
    """
    B2B Braze Async API
    """
    logging.debug('%s /braze_async/%s/%s - start' % (SYS_ID, server, rpc))

    res    = []

    try:
        brz_tok   = None
        impl_data = {}

        if access_token:
            if access_token.get('bearer') is True:
                brz_tok = access_token.get('tok')
            else:
                brz_tok = req.cookies.get(access_token['tok'])

        test_server.token_set(brz_tok)

        test_server.get_transport().set_req(req)
        test_server.get_transport().set_stream_list(braze_data)

        rc  = await test_server.service_rpc_async(impl_data)
        res = test_server.get_transport().get_stream_list()

        if rc != 0:
            logging.warning('%s error out: %s' % (SYS_ID, str(res)))
            # raise Exception(str(res)) - In production we should check for visibile exceptions and only throw those
            #  non visible exceptions. But for testing we are just going to return all exceptions as error messages.

        marsh = test_server.get_marshaler()

        if marsh:
            new_tok = marsh._server_impl()._has_new_rpc_token()

            if new_tok:
                test_server.token_set(new_tok)

    finally:
        if access_token and access_token.get('bearer') is False and access_token.get('tok'):
            _set_braze_cookie(resp, access_token['tok'], test_server.token_get())

        test_server.get_transport().set_req(None)

    logging.debug('%s /braze_async/%s/%s - done' % (SYS_ID, server, rpc))

    return res
