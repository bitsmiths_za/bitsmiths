# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

from mettle.lib   import xMettle

from mettle.braze import ITransport
from mettle.braze import ServerMultiMarsh

from bs_lib         import Pod

from bs_fura.access_async import AccessAsync
from bs_fura              import ErrCode
from bs_fura.xfura        import xFura


class TestServerAsync(ServerMultiMarsh):
    """
    Test Braze Async server..

    To be used by a Fast API web framework.
    """
    MODULE         = '[TestServerAsync]'
    AUTH_LOGGED_IN = '_LOGGED_IN_'


    def __init__(self, trans: ITransport, pod: Pod):
        """
        Constructor.

        :param trans: (mettle.braze.ITransport) The transport object this server will use.
        :param Pod: The pod to use.
        """
        self._tok        = None
        self._fura_si    = None
        self._fura_sm    = None
        self._monitor_si = None
        self._monitor_sm = None
        self._pod        = pod

        sm_list = []

        from bs_fura.fura_server_impl_async  import FuraServerImplAsync
        from bs_fura.braze                   import FuraAsyncServerMarshaler
        self._fura_si = FuraServerImplAsync(pod, 'postgresql')
        self._fura_sm = FuraAsyncServerMarshaler(self._fura_si)

        from bs_monitor.monitor_server_impl_async  import MonitorServerImplAsync
        from bs_monitor.braze                      import MonitorAsyncServerMarshaler
        self._monitor_si = MonitorServerImplAsync(pod, 'postgresql')
        self._monitor_sm = MonitorAsyncServerMarshaler(self._monitor_si)

        sm_list.append(self._fura_sm)
        sm_list.append(self._monitor_sm)

        ServerMultiMarsh.__init__(self, sm_list, trans)


    def __del__(self):
        """
        Destructor.
        """
        pass


    def construct(self) -> None:
        """
        Overload.
        """
        ServerMultiMarsh.construct(self)


    def token_set(self, tok) -> None:
        self._tok = tok


    def token_get(self) -> str:
        return '' if self._tok is None else self._tok


    async def auth_async(self, rpc_name, rpc_auth, rpc_args) -> dict:
        """
        Overload base method.

        This call should raise an exception if the authentication fails.

        :param rpc_name: (string) The rpc call name.
        :param rpc_auth: (string) The rpc authentication method, sort of a factory string.
        :param rpc_args: (dict) The rpc args in a dictionary.
        :return: (dict) A auth data object for the server impl to possibly use.
        """
        self._pod.log.debug('%s.auth - start [rpc:%s, auth:%s]' % (self.MODULE, rpc_name, rpc_auth))

        if not rpc_auth:
            raise Exception('Authorize has no method! [rpc:%s]' % str(rpc_name))

        if not self._tok:
            raise xMettle("Token has not been set! [rpc:%s, rpc_auth:%s]!" % (
                str(rpc_name), str(rpc_auth)), self.MODULE, xMettle.eCode.TokenInvalid)

        wt    = await self._fura_si.get_fura_token()
        claim = wt.read_usr_token(self._tok)

        self._pod.log.debug('%s.auth [%s] - CLAIM: %s' % (self.MODULE, self.get_marshaler()._signature(), str(claim)))

        await self._pod.dbcon.rollback()

        try:
            async with AccessAsync(self._fura_si.get_dao(), claim['site'], claim['usr'], rpc_name) as acc:
                self._tok     = wt.refresh_token(5, claim, self._tok)
                claim['role'] = acc._role

                if rpc_auth == self.AUTH_LOGGED_IN:
                    return claim

                if self.get_marshaler()._signature() == 'Monitor':
                    # In production you can use whatever authentication method you want here instead.
                    return claim

                await acc.required(rpc_auth, rpc_args.get('eaTok'))

            self._pod.log.debug('%s.auth - done [success, usrid:%s, rpc:%s, auth:%s]' % (
                self.MODULE, str(claim.get('usr')), rpc_name, rpc_auth))

        except xFura as xf:
            self._pod.log.debug('%s.auth - done [failed:%s, usrid:%s, rpc:%s, auth:%s]' % (
                self.MODULE, str(xf), str(claim.get('usr')), rpc_name, rpc_auth))

            ec = xf.get_error_code()

            if ec == ErrCode.AccessDenied:
                raise xMettle(
                    "Access denied [ec:%d, reason:%s]!" % (ec.value, ec.name),
                    self.MODULE,
                    xMettle.eCode.CredDenied)

            raise

        except Exception as x:
            self._pod.log.debug('%s.auth - done [failed:%s, usrid:%s, rpc:%s, auth:%s]' % (
                self.MODULE, str(x), str(claim.get('usr')), rpc_name, rpc_auth))
            raise
        finally:
            await self._pod.dbcon.commit()

        return claim
