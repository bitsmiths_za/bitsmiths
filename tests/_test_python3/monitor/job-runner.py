import getopt
import logging
import sys

import mettle.db

from bs_lib import Pod

from bs_monitor.job_runner import JobRunner


DB_CONN_STR = 'postgresql://bs:dev@127.0.0.1/bs'
g_dbcon     = None


def get_db_connect() -> mettle.db.IConnect:
    global g_dbcon

    if not g_dbcon:
        from mettle.db.psycopg2.connect import Connect as Psycopg2Connect

        g_dbcon = Psycopg2Connect()

        g_dbcon.connect(DB_CONN_STR)

    return g_dbcon


def show_usage() -> None:
    print()
    print(f' Usage: {sys.argv[0]} --job JID')
    print()


def main() -> int:

    try:
        optlist, gargs = getopt.getopt(sys.argv[1:], '?h', ['help', 'job='])
    except getopt.GetoptError as err:
        print(err)
        show_usage()
        return 2

    jobinst_id = 0

    for a, o in optlist:
        if a != '--job':
            show_usage()
            return 2

        if not o.isdigit():
            print(' Arguement JID is not a number\n')
            show_usage()
            return 2

        jobinst_id = int(o)


    jr = JobRunner(Pod(None, get_db_connect(), logging.getLogger()), 'postgresql')

    jr.run(jobinst_id)

    return 0


if __name__ == '__main__':
    sys.exit(main())
