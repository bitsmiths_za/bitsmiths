#!/usr/bin/env python

import sys

import logging

import mettle.db
from bs_lib import Pod

from bs_monitor.monitor_cli import MonitorCli, MonitorCliArgs

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()


class TestMonitorCli(MonitorCli):
    def _init_db_connection(self) -> mettle.db.IConnect:
        from mettle.db.psycopg2.connect import Connect as Psycopg2Connect

        conn = Psycopg2Connect()
        conn.connect('postgresql://bs:dev@127.0.0.1/bs')

        return conn


def main() -> int:
    pod = Pod({}, None, logger)

    mcli = TestMonitorCli(pod, 'postgresql', MonitorCliArgs())

    import pdb
    pdb.set_trace()

    return mcli.run(sys.argv[1:])


if __name__ == '__main__':
    sys.exit(main())
