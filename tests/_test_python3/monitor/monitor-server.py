#!/usr/bin/env python

import logging
import os.path
import signal
import sys
import time
import traceback

from mettle.db  import IConnect

from bs_monitor.monitor_manager import MonitorManager

from bs_lib import Pod


g_shutdown   = False
g_monman     = None

SLEEP_TIME          = 1.0
PROC_INTERVAL_SLEEP = 0.2
SERVER_ID           = '[Monitor Server]'
PYTHON_EXE_PATH     = os.path.expandvars('$LOCAL_PATH/bs-venv/bin/python')
TEST_BATCH_PATH     = os.path.expandvars('$LOCAL_PATH/tests/_test_python3/monitor')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()


def signal_handler_int(signal, frame) -> None:
    global g_shutdown

    g_shutdown = True


def signal_handler_kill(signal, frame) -> None:
    global g_shutdown

    g_shutdown = True


def sleep_interrupt(duration: float) -> None:
    global g_shutdown

    pi = duration

    while pi > 0.0:
        if g_shutdown:
            break

        pi -= PROC_INTERVAL_SLEEP

        time.sleep(PROC_INTERVAL_SLEEP)


def run(pod: Pod) -> int:
    global g_shutdown

    if g_shutdown:
        return 0

    try:
        while not g_shutdown:
            g_monman.run()

            sleep_interrupt(SLEEP_TIME)

        logger.info(f'{SERVER_ID}: shutdown received...')

    except Exception as x:
        logger.error(f'{SERVER_ID}:Exception caught in run, [err:{x}, stack-trace:{traceback.format_exc()}]')
        return 1

    finally:
        logger.info(f'{SERVER_ID}: shutting down...')

    return 0


def new_db_connector() -> IConnect:
    from mettle.db.psycopg2.connect import Connect as Psycopg2Connect

    conn = Psycopg2Connect()
    conn.connect('postgresql://bs:dev@127.0.0.1/bs')

    return conn


def main() -> int:
    global g_monman

    logger.info(f'{SERVER_ID}: starting...')

    dbcon = new_db_connector()

    logger.info(f'{SERVER_ID}: connected to database...')


    pod = Pod({}, dbcon, logger, 'SERVER_ID')

    g_monman = MonitorManager(pod,
                              f'{PYTHON_EXE_PATH} {os.path.join(TEST_BATCH_PATH, "job-runner.py")}',
                              1,
                              1,
                              'postgresql')

    g_monman.initialize()

    logger.info(f'{SERVER_ID}: initialized...')

    return run(pod)


signal.signal(signal.SIGINT,  signal_handler_int)
signal.signal(signal.SIGTERM, signal_handler_kill)


if __name__ == '__main__':
    sys.exit(main())
