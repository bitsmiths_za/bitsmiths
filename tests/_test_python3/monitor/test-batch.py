#!/usr/bin/env python

import signal
import sys
import time


PROC_INTERVAL_SLEEP = 0.1

g_shutdown = False


def signal_handler_int(signal, frame) -> None:
    global g_shutdown

    g_shutdown = True

    print(' ! Shutdown signal received !')


def signal_handler_kill(signal, frame) -> None:
    global g_shutdown

    g_shutdown = True

    print(' ! Kill signal received !')


def sleep_interrupt(duration: float) -> None:
    global g_shutdown

    pi = duration

    while pi > 0.0:
        if g_shutdown:
            break

        pi -= PROC_INTERVAL_SLEEP

        time.sleep(PROC_INTERVAL_SLEEP)


def main() -> int:
    print(f'Test Batch - start [args: { sys.argv[1:] }]')

    for arg in sys.argv[1:]:
        if arg.startswith('sleep-raise'):
            sleep_sec = 1
            arg_sec = arg[11:]

            if arg_sec:
                sleep_sec = float(arg_sec)

            print(' - sleep-raise', sleep_sec, 'requested')
            sleep_interrupt(sleep_sec)
            raise Exception(f'Sleep Raise {arg_sec} Requested')

        elif arg.startswith('sleep'):
            sleep_sec = 1
            arg_sec = arg[5:]

            if arg_sec:
                sleep_sec = float(arg_sec)

            print(' - sleep', sleep_sec, 'requested')
            sleep_interrupt(sleep_sec)

        elif arg == 'fail':
            print(' - fail requested')
            return 1

        elif arg == 'crash':
            print(' - crash requested')
            raise Exception('CRASHED!')

        elif arg == 'forever':
            print(' - forver')

            while g_shutdown is False:
                sleep_interrupt(1.0)
                print('  - and ever')

        else:
            print(' - unexpected arg [', arg , ']')

    print('Test Batch - done')

    return 0


signal.signal(signal.SIGINT,  signal_handler_int)
signal.signal(signal.SIGTERM, signal_handler_kill)


if __name__ == '__main__':
    sys.exit(main())
