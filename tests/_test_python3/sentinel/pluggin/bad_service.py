from bs_lib.sentinel_thread import SentinelThread


class BadService(SentinelThread):

    def __init__(self):
        SentinelThread.__init__(self, 'bad-service')

    def process(self):
        print(' thread ->', self.name, '- processing')
        if self._shutdown:
            return

        raise Exception('Bad Service!')


class BadPodService(SentinelThread):

    def __init__(self):
        SentinelThread.__init__(self, 'bad-pod-service')

    def pod_required(self) -> bool:
        return True

    def process(self):
        print(' thread ->', self.name, '- processing')
        if self._shutdown:
            return

        raise Exception('Bad Pod Service!')


class BadDelayedService(SentinelThread):

    def __init__(self):
        SentinelThread.__init__(self, 'bad-delayed-service')
        self.delay_cnt = 4

    def process(self):
        print(' thread ->', self.name, '- processing:', self.delay_cnt)

        self.delay_cnt -= 1

        if not self.delay_cnt:
            raise Exception('Bad Delay Service!')
