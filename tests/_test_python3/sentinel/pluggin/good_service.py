import time

from bs_lib.sentinel_thread import SentinelThread


class GoodService(SentinelThread):

    def __init__(self):
        SentinelThread.__init__(self, 'good-service')

    def process(self):
        print(' thread ->', self.name, '- processing')
        if self._shutdown:
            return


class GoodPodService(SentinelThread):

    def __init__(self):
        SentinelThread.__init__(self, 'good-pod-service')

    def pod_required(self) -> bool:
        return True

    def process(self):
        print(' thread ->', self.name, '- processing')
        if self._shutdown:
            return


class StubbornService(SentinelThread):

    def __init__(self):
        SentinelThread.__init__(self, 'stubborn-service')

    def process(self):
        print(' thread ->', self.name, '- processing')

        while True:
            if self._shutdown:
                return

            time.sleep(1.0)
