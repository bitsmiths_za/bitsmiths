#!/usr/bin/env python

import sys
import getopt
import time
import traceback

import braze


def stop_service(sent: braze.SentinelClientMarshaler, process: str, pluggin: str) -> None:
    print(f'Stopping Services: [process: {process}, pluggin: {pluggin}]')
    sent.service_stop(process, pluggin)


def start_service(sent: braze.SentinelClientMarshaler, process: str, pluggin: str) -> None:
    print(f'Starting Services: [process: {process}, pluggin: {pluggin}]')
    sent.service_start(process, pluggin)


def check_service(sent: braze.SentinelClientMarshaler, process: str, pluggin: str) -> None:
    print(f'Checking Services: [process: {process}, pluggin: {pluggin}]')

    res  = sent.service_status(process, pluggin)
    mask = ' - %-7s %-32s [status:%s, running:%d/%d, port:%s]'

    for x in res:
        if x.serv_type == braze.ServiceType_Couplet.key_server:
            print(mask % (
                braze.ServiceType_Couplet.get_value(x.serv_type),
                x.name,
                braze.ServiceStatus_Couplet.get_value(x.serv_status),
                x.run_active,
                x.run_cnt,
                x.port))

    for x in res:
        if x.serv_type == braze.ServiceType_Couplet.key_pluggin:
            print(mask % (
                braze.ServiceType_Couplet.get_value(x.serv_type),
                x.name,
                braze.ServiceStatus_Couplet.get_value(x.serv_status),
                x.run_active,
                x.run_cnt))


def show_usage() -> None:
    print('Sentinel CLI')
    print()
    print(f'usage: {sys.argv[0]} [-h -p PROC -g PLUG]')
    print()
    print('optional arguments:')
    print('  -c, --check        Check the services (all or targeted)')
    print('  -s, --start        Start the services (all or targeted)')
    print('  -t, --stop         Stop the services (all or targeted)')
    print('  -b, --bounce       Stops and starts services (all or targeted)')
    print('  -p, --proc PROC    Target a specific server process (default: *)')
    print('  -g  --plug PLUG    Target a specfici pluggin thread (default: *)')
    print('  -h, --help         Show this help message and exit')
    print()
    sys.exit(2)


def read_args() -> object:
    try:
        optlist, gargs = getopt.getopt(sys.argv[1:],
                                       'hcstbp:g:',
                                       ['help', 'check', 'start', 'stop', 'bounce', 'proc=', 'plug='])

    except getopt.GetoptError as err:
        print(err)
        show_usage()

    class Args():
        def __init__(self):
            self.check   = False
            self.start   = False
            self.stop    = False
            self.bounce  = False
            self.proc    = '*'
            self.plug    = '*'

    args = Args()

    for o, a in optlist:
        if o in ("-h", "--help"):
            show_usage()
            sys.exit()
        elif o == '-c' or o == '--check':
            args.check = True
        elif o == '-s' or o == '--start':
            args.start = True
        elif o == '-t' or o == '--stop':
            args.stop = True
        elif o == '-b' or o == '--bounce':
            args.bounce = True
        elif o == '-c' or o == '--check':
            args.check = True
        elif o == '-p' or o == '--proc':
            args.proc = a
        elif o == '-g' or o == '--plug':
            args.plug = a
        else:
            print(' ... unrecognized arguments: %s' % o)
            show_usage()

    if not args.check and not args.start and not args.stop and not args.bounce:
        args.check = True

    return args


def main():
    args      = read_args()
    sleep_cnt = 3

    try:
        from mettle.braze.tcp   import TransportClient
        from mettle.braze.tcp   import TransportClientSettings
        from mettle.braze       import Client

        trans  = TransportClient()
        sett   = TransportClientSettings('127.0.0.1', '41041', 5.0)
        trans.construct(sett)

        client = Client(trans)
        sent   = braze.SentinelClientMarshaler(client)

        if args.bounce:
            stop_service(sent, args.proc, args.plug)

            for x in range(sleep_cnt):
                print('  ...sleeping [%d/%d]' % (x + 1, sleep_cnt))
                time.sleep(1.0)

            start_service(sent, args.proc, args.plug)

            for x in range(sleep_cnt):
                print('  ...sleeping [%d/%d]' % (x + 1, sleep_cnt))
                time.sleep(1.0)
        else:
            if args.stop:
                stop_service(sent, args.proc, args.plug)
                time.sleep(1.0)

            elif args.start:
                start_service(sent, args.proc, args.plug)
                time.sleep(1.0)

        if args.check:
            check_service(sent, args.proc, args.plug)

    except Exception as x:
        print("Exception caught in main, sentinel_cli terminating. [err:%s, stack-trace:%s]" % (str(x), traceback.format_exc()))
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main())
