#!/usr/bin/env python

import getopt
import logging
import os
import os.path
import signal
import sys
import traceback


g_sent_man      = None
g_server_impl   = None

HOME_PATH = os.path.join(os.getenv('LOCAL_PATH'), 'tests', '_test_python3', 'sentinel')
SERVER_ID = '[Sentinel Server]'

sys.path.append(HOME_PATH)

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()


import braze


class SentinelServerImpl(braze.SentinelServerInterface):
    """
    The sentinel server implementation.
    """
    PROC_INTERVAL = 0.2

    def __init__(self, sent_man):
        self._sent_man = sent_man
        self._log      = sent_man._log


    def _shutdown_server(self):
        return self._sent_man.shutting_down()


    def _slacktime_handler(self):
        self._sent_man.monitor_services()


    def _construct(self):
        self._sent_man.start_services()


    def _destruct(self):
        self._sent_man.shutdown_services()
        self._sent_man.monitor_services(True)


    def ping(self):
        return True


    def sys_version(self):
        return self._sent_man._version


    def service_start(self, server, pluggin):
        if server == '*' and pluggin == '*':
            self._sent_man.start_services()
            return

        if server:
            if server == '*':
                self._sent_man.start_services(pluggins=False, processes=True)
            else:
                self._sent_man.start_processes(server)

        if pluggin:
            if pluggin == '*':
                self._sent_man.start_services(pluggins=True, processes=False)
            else:
                self._sent_man.start_pluggin(pluggin)


    def service_status(self, server, pluggin):
        """
        :param server: Server to check
        :param pluggin: Pluggin to check
        :return: (bService.List) List of service statuses
        """
        res = braze.bService.List()

        if server == '' and pluggin == '':
            return res

        for pname, pobj in self._sent_man._processes.items():
            if server != '*' and server != pname:
                continue

            x = braze.bService(
                id           = pobj['name'],
                name         = pobj['name'],
                serv_type    = '',
                serv_status  = '',
                path         = pobj['ppath'],
                args         = pobj['pargs'],
                run_cnt      = pobj['run-count'],
                run_active   = 0,
                port         = '')

            x.serv_type = braze.ServiceType_Couplet.key_server

            if x.args is None:
                x.args = ''

            if pobj['port'] is not None:
                x.port = str(pobj['port'])

            for p in pobj['procs']:
                if p is None:
                    continue

                if p.is_alive():
                    x.run_active  += 1
                    x.serv_status  = braze.ServiceStatus_Couplet.key_running

            if x.run_active < 1:
                x.serv_status = braze.ServiceStatus_Couplet.key_stopped

            res.append(x)

            if server != '*':
                break

        for pname, pobj in self._sent_man._pluggins.items():
            if pluggin != '*' and pluggin != pname:
                continue

            x = braze.bService(
                id          = pobj['name'],
                name        = pobj['name'],
                serv_type   = '',
                serv_status = '',
                path        = pobj['mpath'],
                args        = '',
                run_cnt     = pobj['run-count'],
                run_active  = 0,
                port        = '')

            x.serv_type = braze.ServiceType_Couplet.key_pluggin

            for p in pobj['threads']:
                if p is None:
                    continue

                if p.is_alive():
                    x.run_active  += 1
                    x.serv_status  = braze.ServiceStatus_Couplet.key_running

            if x.run_active < 1:
                x.serv_status  = braze.ServiceStatus_Couplet.key_stopped

            res.append(x)

            if pluggin != '*':
                break

        return res


    def service_stop(self, server, pluggin):
        """
        :param server: Server to stop
        :param pluggin: Pluggin to stop
        """
        if server == '*' and pluggin == '*':
            self._sent_man.shutdown_services()
            return

        if server:
            if server == '*':
                self._sent_man.shutdown_services(pluggins=False, processes=True)
            else:
                self._sent_man.shutdown_process(server)

        if pluggin:
            if pluggin == '*':
                self._sent_man.shutdown_services(pluggins=True, processes=False)
            else:
                self._sent_man.shutdown_pluggin(pluggin)





def signal_handler_int(signal, frame):
    """
    Catch interrupt signal. Flag that we must terminate politely.
    """
    global g_sent_man

    if g_sent_man is None or g_sent_man._term:
        return

    # import pdb
    # pdb.set_trace()

    g_sent_man.shutdown()


def signal_handler_kill(signal, frame):
    """
    Catch kill signal. Flag that we must terminate not so politely.
    """
    global g_sent_man

    if g_sent_man is None or g_sent_man._kill:
        return

    g_sent_man.kill()


def show_usage():
    """
    Prints the program usage.
    """
    print(f'usage: {sys.argv[0]} [-h]')
    print('')
    print('Sentinel server command line interface')
    print('')
    print('optional arguments:')
    print('  -h, --help         Show this help message and exit')
    print('')


def read_args():
    """
    Reads the command line arguements for this program.

    :return: The args object.
    """
    try:
        optlist, gargs = getopt.getopt(sys.argv[1:], 'h', ['help'])
    except getopt.GetoptError as err:
        print(err)
        show_usage()
        sys.exit(2)

    class Args:
        def __init__(self):
            pass

    args = Args()

    for o, a in optlist:
        if o in ("-h", "--help"):
            show_usage()
            sys.exit()
        else:
            print(' ... unrecognized arguments: %s' % o)
            show_usage()
            sys.exit(2)

    return args


def main():
    """
    Main entry point for the sentinel service.
    """
    read_args()

    global g_sent_man
    global g_server_impl

    from bs_lib.sentinel_manager import SentinelManager

    from mettle.braze.tcp  import TransportServer
    from mettle.braze.tcp  import TransportServerSettings
    from mettle.braze      import Server

    trans  = None
    marsh  = None
    server = None

    cfg = {
        'port'      : 41041,
        'home_path' : HOME_PATH,
        'timeout'   : 5.0,
    }

    try:
        logger.info(f'{SERVER_ID} started.')

        g_sent_man    = SentinelManager(cfg, logger, None)
        g_server_impl = SentinelServerImpl(g_sent_man)
        trans         = TransportServer()
        marsh         = braze.SentinelServerMarshaler(g_server_impl)
        server        = Server(marsh, trans, 'sentinel')

        trans.construct(TransportServerSettings(
            str(cfg['port']),
            5.0,
            retrys=5,
            managed=False))

        g_sent_man.initialize(os.path.join(HOME_PATH, 'sentinel.yml'))

        server.run(0.2)

        logger.info(f'{SERVER_ID} shutting down.')

    except Exception as x:
        logger.error("Exception caught in main, sentinel terminating. [err:%s, stack-trace:%s]" % (
            str(x), traceback.format_exc()))
        return 1

    finally:
        if g_sent_man:
            g_sent_man.shutdown_services()
            g_sent_man.monitor_services(True)
            g_sent_man.destroy()

        del server
        del marsh
        del trans
        del g_server_impl
        del g_sent_man

        logging.shutdown()

    return 0


signal.signal(signal.SIGINT,  signal_handler_int)
signal.signal(signal.SIGTERM, signal_handler_kill)

if __name__ == '__main__':
    sys.exit(main())
