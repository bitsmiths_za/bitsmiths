# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import asyncio
import os
import pytest
import sys

from mettle.db import IConnect, IAConnect

from bs_lib import IProvider, PodAsync


g_encode_io_database = None


sys.path.append(os.path.join(os.path.dirname(__file__), '_test_python3'))
sys.path.append(os.path.join(os.path.dirname(__file__), 'helpers'))


def pytest_addoption(parser):
    parser.addoption("--db_target", action="store", default='postgresql')
    parser.addoption("--db_driver", action="store", default='psycopg2')
    parser.addoption("--server_async", action="store", default='false')



@pytest.fixture(scope='session')
def event_loop():
    """
    A module-scoped event loop.
    """
    return asyncio.get_event_loop()


@pytest.fixture(scope='session')
def server_async(request) -> str:
    res = request.config.option.server_async

    if res.lower() == 'true':
        return True

    return False


@pytest.fixture(scope='session')
def configurations(server_async) -> dict:
    braze_ep = 'braze'

    if server_async:
        braze_ep += '_async'

    return {
        'server'    : f'http://127.0.0.1:3881/{braze_ep}',
    }


@pytest.fixture(scope='session')
def db_supported_targets() -> tuple:
    return ('postgresql')


@pytest.fixture(scope='session')
def db_supported_drivers() -> tuple:
    return (
        'mettledb_postgresql',
        'psycopg2',
        'encode_io_postgresql',
    )


@pytest.fixture(scope='session')
def db_connection_strings() -> dict:
    return {
        'postgresql' : {
            '*' : 'postgresql://bs:dev@127.0.0.1/bs',
        },
    }


@pytest.fixture(scope='session')
def db_target(request, db_supported_targets) -> str:
    res = request.config.option.db_target

    if res not in db_supported_targets:
        raise Exception(f'Database target [{res}] not supported. Supported targets are: {db_supported_targets}')

    return res


@pytest.fixture(scope='session')
def db_driver(request, db_supported_drivers) -> str:
    res = request.config.option.db_driver

    if res not in db_supported_drivers:
        raise Exception(f'Database driver [{res}] not supported. Supported drivers are: {db_supported_drivers}')

    return res


@pytest.fixture(scope='session')
def db_url(db_connection_strings, db_target, db_driver) -> str:
    conn_str = db_connection_strings[db_target].get(db_driver)

    if not conn_str:
        conn_str = db_connection_strings[db_target].get('*')

    assert conn_str is not None

    return conn_str


@pytest.fixture(scope='session')
def http_braze_client(configurations):
    from mettle.braze                            import Client
    from mettle.braze.http                       import TransportClientSettings
    from _test_python3.api.test_transport_client import TestTransportClient

    trans = TestTransportClient()
    trans.construct(TransportClientSettings(configurations['server'], True))

    client = Client(trans)

    return client



@pytest.fixture(scope='session')
def provider_std() -> IProvider:
    from bs_lib.provider_std import ProviderStd

    return ProviderStd()



@pytest.fixture(scope='session')
async def _init_encoded_io_database(db_connection_strings) -> bool:
    from databases import Database

    global g_encode_io_database

    if not g_encode_io_database:
        g_encode_io_database = Database(db_connection_strings['postgresql']['*'], min_size=1, max_size=4)

    if not g_encode_io_database.is_connected:
        await g_encode_io_database.connect()
        assert g_encode_io_database.is_connected

    return True


@pytest.fixture(scope='session')
async def async_pg_db_conn(db_connection_strings, _init_encoded_io_database) -> IAConnect:
    from databases.core import Connection

    global g_encode_io_database

    from mettle.db.encode_io_databases.aconnect_postgres import AConnectPostgres

    async with Connection(g_encode_io_database._backend) as en_con:
        conn = AConnectPostgres(en_con)
        yield conn


@pytest.fixture(scope='session')
def pg_db_conn(db_connection_strings) -> IConnect:
    from mettle.db.psycopg2.connect import Connect as Psycopg2Connect

    dbcon = Psycopg2Connect()
    dbcon.connect(db_connection_strings['postgresql']['*'])

    return dbcon


@pytest.fixture(scope='session')
def bs_pod(provider_std, pg_db_conn) -> IProvider:
    pod = provider_std.new_pod(None, provider_std.new_logger(), pg_db_conn)
    pod.usr_id = '[bs_pytest]'
    return pod


@pytest.fixture(scope='session')
async def bs_apod(provider_std, async_pg_db_conn) -> PodAsync:
    pod = PodAsync(None, async_pg_db_conn, provider_std.new_logger())
    pod.usr_id = '[bs_pytest]'
    yield pod
