import random
import string

from mettle.db import IConnect

from bs_lib.provider_std import ProviderStd


class MultiTest:
    """
    This is a helper class designed to help test multiple versions of the same objects in a row.
    """
    def __init__(self, sync_objs: list, async_objs: list):
        self.sync_objs = sync_objs
        self.async_objs = async_objs

    def all(self) -> list:
        return self.sync_objs + self.async_objs

    def all_sync(self) -> list:
        return self.sync_objs

    def all_async(self) -> list:
        return self.async_objs

    async def call(self, sa_obj, meth_name: str, args: list = None):
        fobj = getattr(sa_obj, meth_name)

        print(sa_obj.__class__, ' -> ', meth_name, args)

        if args is None:
            args = []

        if sa_obj in self.sync_objs:
            return fobj(*args)
        elif sa_obj in self.async_objs:
            return await fobj(*args)

        raise Exception('Not found!')

    def is_async(self, sa_obj) -> bool:
        return sa_obj in self.async_objs


class ProviderPytest(ProviderStd):
    def new_db_connector(self, cfg: dict = None, options: dict = None) -> IConnect:
        from mettle.db.psycopg2.connect import Connect as Psycopg2Connect

        dbcon = Psycopg2Connect()
        dbcon.connect(cfg['db_url'])

        return dbcon


def random_name(nlen: int = 16) -> str:
    return ''.join(random.SystemRandom().choice(string.ascii_lowercase) for _ in range(nlen))
