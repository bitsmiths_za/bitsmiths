#!/bin/bash

# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

set -m
set -e

export TEST_PATH="$LOCAL_PATH/tests"
export LOG_PATH="$LOCAL_PATH/tests/log"

declare -a _arg_skip_steps
declare -a _arg_run_steps
_arg_verbose=0
_arg_list=


declare -a _test_steps=(\
  build_database\
  run_pytest\
)

declare -a _test_steps_descr=(\
  "Build the database for the rest of the tests"\
  "Run pytest for the bitsmiths libraries"\
)

# start_braze_service\
# "Start the braze test (FastAPI) service"\


function vcho()
{
  if [[ $_arg_verbose -eq 1 ]]; then
    echo "${@}"
  fi
}

function _kill_child_processes()
{
  set +e

  vcho "kill start"

  while kill %% 2>/dev/null; do sleep 0; done

  vcho "kill done"

  set +m
}

function run_pytest()
{
  cd "$LOCAL_PATH"

  if [ ! -d test-results ]; then
    mkdir test-results
  fi

  if [[ $_arg_verbose -eq 1 ]]; then
    local vbflag="--capture=no"
  fi

  coverage run -m pytest --junitxml=test-results/results.xml ${vbflag}
  coverage html

}

function _rebuild_db()
{
  local targ_db="${1}"
  local targ_schema="${2}"
  local make_cmd="rebuild"

  if [ "$targ_db" = "" ]; then
    targ_db="postgresql"
  fi

  if [ "$targ_schema" != "" ]; then
    make_cmd="rebuild-${targ_schema}"
  fi

  vcho " Rebuilding [database: $targ_db, schema: $targ_schema]"

  cd "${TEST_PATH}/_makefiles"

  if [ $_arg_verbose -eq 1 ]; then
    make -f "localdb.${targ_db}.makefile" $make_cmd
  else
    make -f "localdb.${targ_db}.makefile" $make_cmd > "${LOG_PATH}/create_db_${targ_db}.out" 2>&1
  fi
}

function start_braze_service()
{
  cd "$LOCAL_PATH/tests"

  local out_file="${LOG_PATH}/_run-braze-fastpi-service.out"

  ./_run-braze-fastpi-service.sh > "$out_file" 2>&1 &
  sleep 2

  set +e

  local running=$(jobs | grep Running)

  if [ $_arg_verbose -eq 1 ] || [ ! "$running" ]; then
    echo "  --- Braze Service Output -- [Start]"
    cat "$out_file"
    echo "  --- Braze Service Output -- [End]"
  fi

  set -e

  if [ ! "$running" ]; then
    echo "TestBraze service not running after starting up!"
    return 1
  fi
}

function build_database()
{
  _rebuild_db "" ""
}

function _list_steps()
{
  local idx=0

  echo ""

  for step in ${_test_steps[@]}; do
    printf "  %-32.32s : %s" "$step" "${_test_steps_descr[${idx}]}"
    echo ""
    idx=$((idx+1))
  done

  echo ""
}

function _usage_and_exit()
{
  echo ""
  echo " Run Bitsmiths Tests"
  echo ""
  echo "Usage: $0 [option] [ -s skip | -r run | -v | -] [arg]..."
  echo "----------------------------------------------------------------------------"
  echo " -s, --skip SKIP : Skip a step in the test *"
  echo " -r, --run RUN   : Run only a specfic step in the test *"
  echo " -l, --list      : List all the steps instead of running the test"
  echo " -v, --verbose   : Turn on verbose messages"
  echo " -h, -?, --help  : This help"
  echo ""
  echo "  * These arguments can be use multiple times"
  echo ""

  exit 2
}

function main()
{
  if [ "$_arg_list" ]; then
    _list_steps
    exit 2
  fi

  if [ "$_arg_skip_steps" ]; then vcho " - NOTE! Skipping steps: ${_arg_skip_steps[@]}"; fi
  if [ "$_arg_run_steps" ];  then vcho " - NOTE! Running steps: ${_arg_run_steps[@]}"; fi
  if [ "$_arg_verbose" ];    then vcho " - NOTE! Verbose ON"; fi

  vcho ""
  vcho "Bitsmiths Test Starting..."
  vcho ""

  local idx=0
  local skip
  local is_in
  local ele

  for step in "${_test_steps[@]}"; do
    skip=0

    for ele in "${_arg_skip_steps[@]}"; do
      if [[ "$ele" == "$step" ]]; then
        skip=1
        break
      fi
    done

    if [ "$_arg_run_steps" ]; then
      skip=1

      for ele in "${_arg_run_steps[@]}"; do
        if [[ "$ele" = "$step" ]]; then
          skip=0
          break
        fi
      done
    fi

    if [[ "$skip" -eq 0 ]]; then
      vcho "Running  : $step - ${_test_steps_descr[${idx}]}"

      $step

    else
      vcho "Skipping : $step - ${_test_steps_descr[${idx}]}"
    fi

    idx=$((idx+1))
  done

  echo ""
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -s|--skip)      _arg_skip_steps+=("$2"); shift ;;
        -r|--run)       _arg_run_steps+=("$2"); shift ;;
        -l|--list)      _arg_list=1 ;;
        -v|--verbose)   _arg_verbose=1 ;;
        -h|--help|-\?) _usage_and_exit ;;
        *) echo "Unknown parameter passed: $1"; _usage_and_exit;;
    esac
    shift
done

for x in "${_arg_skip_steps[@]}"; do
  echo " - skip $x"
done

for sig in INT QUIT HUP TERM ALRM USR1; do
  trap "
    _kill_child_processes
    trap - $sig EXIT
    kill -s $sig "'"$$"' "$sig"
done

trap _kill_child_processes EXIT

main
