# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import pytest
import uuid

from bs_lib import common

from bs_audit.audit import Audit
from bs_audit.audit_async import AuditAsync
from bs_audit.base_audit import BaseAudit

import utils


pytestmark = [ pytest.mark.audit ]


@pytest.fixture(scope='module')
def audit_usr_id() -> str:
    return '[test_audit]'


@pytest.fixture(scope='module')
def site_id() -> int:
    return 42


@pytest.fixture(scope='module')
def audit_sync_obj(bs_pod) -> Audit:
    res = Audit(bs_pod.dbcon, 'postgresql')
    return res


@pytest.fixture(scope='module')
async def audit_async_obj(bs_apod):
    res = AuditAsync(bs_apod.dbcon, 'postgresql')
    yield res


@pytest.fixture(scope='module')
async def audit_objects(audit_sync_obj, audit_async_obj):
    fs = utils.MultiTest([audit_sync_obj], [audit_async_obj])

    return fs


def test_audit_base_class():
    baud = BaseAudit()
    dtnow = datetime.datetime.now()
    uid = uuid.uuid4()

    assert baud._text_value('asdf', 'x') == 'asdf'
    assert baud._text_value(1, 'x') == '1'
    assert baud._text_value(True, 'x') == 'True'
    assert baud._text_value(123.23, 'x') == '123.23'
    assert baud._text_value(datetime.date.min, 'x') == ''
    assert baud._text_value(datetime.datetime.min, 'x') == ''
    assert baud._text_value(dtnow, 'x') == dtnow.strftime('%Y-%m-%d %H:%M:%S')
    assert baud._text_value(dtnow.date(), 'x') == dtnow.strftime('%Y-%m-%d')
    assert baud._text_value(dtnow.time(), 'x') == dtnow.strftime('%H:%M:%S')
    assert baud._text_value([1, 2, 3], 'x') == '[1, 2, 3]'
    assert baud._text_value({'foo': 'bar'}, 'x') == '{"foo": "bar"}'
    assert baud._text_value(b'asdf', 'x') == ''
    assert baud._text_value(uid, 'x') == str(uid)
    assert baud._text_value(None, 'x') == ''

    with pytest.raises(Exception):
        assert baud._text_value(object(), 'x')

    with pytest.raises(Exception):
        baud.diff(None, None, None)


def test_setup_data(bs_pod, audit_sync_obj):
    """
    Setup the test data for audit.
    """
    dao = audit_sync_obj._dao
    cfg = dao.dCfg(bs_pod.dbcon)

    if not cfg.try_select_one_deft('test_tbl_parent'):
        cfg.insert_deft('test_tbl_parent', 'id', 'stamp_by|stamp_tm', None, None, None)
        bs_pod.dbcon.commit()

    assert cfg.try_select_one_deft('test_tbl_parent')

    if not cfg.try_select_one_deft('test_tbl_child'):
        cfg.insert_deft('test_tbl_child', 'id', 'stamp_by|stamp_tm', 'test_tbl_parent', 'parent_id', None)
        bs_pod.dbcon.commit()

    assert cfg.try_select_one_deft('test_tbl_child')

    bs_pod.dbcon.commit()


@pytest.mark.asyncio
async def test_audit_cud(audit_objects, site_id, bs_pod, audit_usr_id):

    for ao in audit_objects.all():
        common.sql_run(bs_pod, "delete from audit.col")
        common.sql_run(bs_pod, "delete from audit.aud")
        common.sql_run(bs_pod, "delete from audit.tran")
        bs_pod.commit()

        # --- Test inserts

        dbcon = ao._dbcon
        obj1 = { 'id': 10, 'name': 'bit', 'surname': 'smiths', 'stamp_by': bs_pod.usr_id, 'stamp_tm': datetime.datetime.now() }

        ao.aud(site_id, 'test_audit.py', audit_usr_id)
        ao.diff(None, obj1, 'test_tbl_parent')
        tid = await audit_objects.call(ao, 'commit')

        if audit_objects.is_async(ao):
            await dbcon.commit()
        else:
            dbcon.commit()

        assert tid

        ares = common.sql_fetch(bs_pod, "select * from audit.aud where tran_id = %d" % (tid))

        assert ares
        assert 1 == len(ares)
        ares = ares[0]
        assert ares.get('action')  == 'C'
        assert ares.get('tbl_id')  == 'test_tbl_parent'
        assert ares.get('tbl_key') == str(obj1['id'])

        cres = common.sql_fetch(bs_pod, "select * from audit.col where aud_id = %d" % (ares['id']))

        assert cres
        assert len(cres) == 3

        for cx in cres:
            assert not cx.get('before')
            assert str(obj1[cx['name']]) == cx['after']


        # --- Test updates

        obj1 = { 'id': 10, 'name': 'bit', 'surname': 'smiths', 'stamp_by': bs_pod.usr_id, 'stamp_tm': datetime.datetime.now() }
        obj2 = { 'id': 10, 'name': 'Bit', 'surname': 'Smiths', 'stamp_by': bs_pod.usr_id, 'stamp_tm': datetime.datetime.now() }

        ao.aud(site_id, 'test_audit.py', audit_usr_id)
        ao.diff(obj1, obj2, 'test_tbl_parent')
        tid = await audit_objects.call(ao, 'commit')
        if audit_objects.is_async(ao):
            await dbcon.commit()
        else:
            dbcon.commit()

        assert tid

        ares = common.sql_fetch(bs_pod, "select * from audit.aud where tran_id = %d" % (tid))

        assert ares
        assert 1 == len(ares)
        ares = ares[0]
        assert ares.get('action')  == 'U'
        assert ares.get('tbl_id')  == 'test_tbl_parent'
        assert ares.get('tbl_key') == str(obj1['id'])

        cres = common.sql_fetch(bs_pod, "select * from audit.col where aud_id = %d" % (ares['id']))

        assert cres
        assert len(cres) == 2

        for cx in cres:
            assert str(obj1[cx['name']]) == cx['before']
            assert str(obj2[cx['name']]) == cx['after']


        # --- Test deletes

        obj2 = { 'id': 10, 'name': 'Bit', 'surname': 'Smiths', 'stamp_by': bs_pod.usr_id, 'stamp_tm': datetime.datetime.now() }

        ao.aud(site_id, 'test_audit.py', audit_usr_id)
        ao.diff(obj2, None, 'test_tbl_parent')
        tid = await audit_objects.call(ao, 'commit')
        if audit_objects.is_async(ao):
            await dbcon.commit()
        else:
            dbcon.commit()

        assert tid

        ares = common.sql_fetch(bs_pod, "select * from audit.aud where tran_id = %d" % (tid))

        assert ares
        assert 1 == len(ares)
        ares = ares[0]
        assert ares.get('action')  == 'D'
        assert ares.get('tbl_id')  == 'test_tbl_parent'
        assert ares.get('tbl_key') == str(obj2['id'])

        cres = common.sql_fetch(bs_pod, "select * from audit.col where aud_id = %d" % (ares['id']))

        assert cres
        assert len(cres) == 3

        for cx in cres:
            assert not cx.get('after')
            assert str(obj2[cx['name']]) == cx['before']


@pytest.mark.asyncio
async def test_audit_search(audit_objects, site_id, audit_usr_id):

    for ao in audit_objects.all():
        res = await audit_objects.call(ao, 'search', [ -1 ])
        assert not res

        args = [ site_id, datetime.datetime.now() - datetime.timedelta(hours=1), datetime.datetime.now() ]

        res = await audit_objects.call(ao, 'search', args)
        assert res

        args.append('foo')
        res = await audit_objects.call(ao, 'search', args)
        assert not res

        args.pop()
        args.append('test_tbl_parent')
        res = await audit_objects.call(ao, 'search', args)
        assert res

        rec = res[0]

        args.append(str(99999))
        res = await audit_objects.call(ao, 'search', args)
        assert not res

        args.pop()
        args.append(rec.aud.tbl_key)
        res = await audit_objects.call(ao, 'search', args)
        assert res

        args.append('[fake user]')
        res = await audit_objects.call(ao, 'search', args)
        assert not res

        args.pop()
        args.append(audit_usr_id)
        res = await audit_objects.call(ao, 'search', args)
        assert res

        args.append(['X', 'Y'])
        res = await audit_objects.call(ao, 'search', args)
        assert not res

        args.pop()
        args.append(['D', 'C'])
        res = await audit_objects.call(ao, 'search', args)
        assert res

        rec = res[0]

        args.append(99999)
        res = await audit_objects.call(ao, 'search', args)
        assert not res

        args.pop()
        args.append(rec.aud.tran_id)
        res = await audit_objects.call(ao, 'search', args)
        assert res

        dbcon = ao._dbcon

        if audit_objects.is_async(ao):
            await dbcon.rollback()
        else:
            dbcon.rollback()
