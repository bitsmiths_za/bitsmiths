# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os.path
import pytest

from bs_lib import common
from bs_lib.auto_transaction import AutoTransaction
from bs_lib.auto_transaction_async import AutoTransactionAsync

import utils


pytestmark = [ pytest.mark.bs_lib ]


def test_query_builder():
    from bs_lib import query_builder

    assert query_builder.clean_str_crit("%") == " "
    assert query_builder.clean_str_crit("\n") == " "
    assert query_builder.clean_str_crit("\r") == " "
    assert query_builder.clean_str_crit("\f") == " "
    assert query_builder.clean_str_crit("'") == "''"
    assert query_builder.clean_str_crit("") == ""

    with pytest.raises(Exception):
        assert query_builder.clean_str_crit(utils.random_name(2099))

    assert query_builder.make_pattern('Foo', True, True) == '%Foo%'
    assert query_builder.make_pattern('Foo', True, False) == '%Foo'
    assert query_builder.make_pattern('Foo', False, True) == 'Foo%'
    assert query_builder.make_pattern('*Foo*', True, True) == '%Foo%'
    assert query_builder.make_pattern('', True, True) == '%'

    assert query_builder.dyn_crit('A', 'a', 'status') == " and a.status='A'"
    assert query_builder.dyn_crit('A', 'a', 'status', True) == " and a.status!='A'"
    assert query_builder.dyn_crit('A', 'a', 'status', False, True) == " and a.status ILIKE 'A'"
    assert query_builder.dyn_crit('A', 'a', 'status', True, True) == " and a.status NOT ILIKE 'A'"
    assert query_builder.dyn_crit(1, 'a', 'status') == " and a.status=1"
    assert query_builder.dyn_crit(1, 'a', 'status', True) == " and a.status!=1"
    assert query_builder.dyn_crit('', 'a', 'status') == ""

    assert query_builder.dyn_list(['A', 'B'], 'a', 'status') == " and a.status in ('A','B')"
    assert query_builder.dyn_list(['A', 'B'], 'a', 'status', negative=True) == " and a.status not in ('A','B')"
    assert query_builder.dyn_list([1, 2], 'a', 'status') == " and a.status in (1,2)"
    assert query_builder.dyn_list([1, 2], 'a', 'status', negative=True) == " and a.status not in (1,2)"
    assert query_builder.dyn_list([1, 2], 'a', 'status', " , ", " ") == " and a.status in ( 1 , 2 )"
    assert query_builder.dyn_list([], 'a', 'status') == ""


def test_common_human_size():
    assert common.human_size(1024) == '1 KB'
    assert common.human_size(1024 * 1024) == '1.0 MB'
    assert common.human_size((1024 * 1024) + ((1024 * 200))) == '1.2 MB'
    assert common.human_size(1024 * 1024 * 1024) == '1.0 GB'
    assert common.human_size(1) == '1 byte'


def test_common_hash_passwd():
    res = common.hash_password('test@test.com', 'password123')
    assert res == '3e9c797ead10814a5a510fe5e5235163'


def test_common_email_validation():
    assert common.dest_addr_valid_email(0) is False
    assert common.dest_addr_valid_email('1') is False
    assert common.dest_addr_valid_email('foo') is False
    assert common.dest_addr_valid_email('this_is_some_bullshit') is False
    assert common.dest_addr_valid_email('foo@bar.com') is True


def test_common_sms_validation():
    assert common.dest_addr_valid_sms(0) is False
    assert common.dest_addr_valid_sms('1') is False
    assert common.dest_addr_valid_sms('abc123') is False
    assert common.dest_addr_valid_sms('this_is_some_bullshit') is False
    assert common.dest_addr_valid_sms('+27885210789') is True


def test_common_read_dict():
    with pytest.raises(Exception):
        common.read_dict(None, None)

    with pytest.raises(Exception):
        common.read_dict(1)

    data = {
        'vi' : 123,
        'vf' : 456.78,
        'vs' : 'foobar',
        'vo' : 'active',
        'vn' : None,
    }

    with pytest.raises(Exception):
        common.read_dict(data, '_no_key_', optional=False)

    assert common.read_dict(data, '_no_key_', optional=True) is None

    for key in data:
        assert common.read_dict(data, key) == data[key]

    with pytest.raises(Exception):
        assert common.read_dict(data, 'vn', str, optional=False) is None

    assert common.read_dict(data, 'vn', str, optional=True) is None

    assert common.read_dict(data, 'vi', int) == data['vi']
    assert common.read_dict(data, 'vi', str) == str(data['vi'])

    with pytest.raises(Exception):
        assert common.read_dict(data, 'vs', int)

    with pytest.raises(Exception):
        common.read_dict(data, 'vo', str, req_vals=['_not_', '__found__'])

    assert common.read_dict(data, 'vo', str, req_vals=['active', 'disabled']) == str(data['vo'])


def test_common_md5_checksum():
    assert common.md5_checksum(os.path.expandvars("$LOCAL_PATH/tests/run-tests.sh"))


def test_common_get_logger():
    logger = common.get_logger()
    assert logger

    named_logger = common.get_logger('bs-tests')
    assert named_logger

    list_logger = common.get_logger([None, 'bs-tests'])
    assert list_logger == named_logger

    assert list_logger == common.get_logger(list_logger)
    assert logger == common.get_logger(123)


def test_common_config_loaders():
    fnames = [
        os.path.expandvars("$LOCAL_PATH/tests/stubs/cfgfile.json"),
        os.path.expandvars("$LOCAL_PATH/tests/stubs/cfgfile.yml"),
        os.path.expandvars("$LOCAL_PATH/tests/stubs/cfgfile.text"),
        os.path.expandvars("$LOCAL_PATH/tests/stubs/cfgfile.xml"),
    ]

    cfg = { "Group1" : { "source": "foo", "dest": "bar" }, "Group2" : { "source": "beep", "dest": "boop" } }

    for fname in fnames:
        data = common.read_config_dict_from_file(fname)
        assert data == cfg

        with open(fname, 'rt') as fh:
            data = common.read_dict_from_str(os.path.splitext(fname)[1][1:], fh.read())
            assert data == cfg


    with pytest.raises(Exception):
        common.read_dict_from_str('unknown', '')

    with pytest.raises(Exception):
        common.read_config_dict_from_file('./invalid_filename')

    with pytest.raises(Exception):
        common.read_config_dict_from_file(os.path.expandvars("$LOCAL_PATH/tests/run-tests.sh"))



def test_common_provider_std(provider_std):
    assert provider_std.new_config({'config-file': "$LOCAL_PATH/tests/stubs/cfgfile.json"})
    assert provider_std.new_logger()


    with pytest.raises(Exception):
        provider_std.new_config(None)

    with pytest.raises(Exception):
        provider_std.new_config({'foo': 'bar'})


    with pytest.raises(Exception):
        provider_std.new_db_connector()


def test_common_sql_calls(bs_pod):
    with AutoTransaction(bs_pod) as at:
        res = common.sql_fetch(bs_pod, "select * from fura.config")

        assert res
        assert type(res) == list
        assert len(res) > 1

        res = common.sql_fetch(bs_pod, "select * from fura.config where id = 'token.webkey'", True)

        assert res
        assert type(res) == dict
        assert res.get('id') == 'token.webkey'

        common.sql_run(bs_pod, "update fura.config set tm_stamp = current_timestamp")
        at.commit()
        at.rollback()

    with AutoTransaction(bs_pod) as at:
        res = common.sql_fetch(bs_pod, "select * from fura.config")


@pytest.mark.asyncio
async def test_common_sql_calls_async(bs_apod):
    async with AutoTransactionAsync(bs_apod) as at:
        res = await common.asql_fetch(bs_apod, "select * from fura.config")

        assert res
        assert type(res) == list
        assert len(res) > 1

        res = await common.asql_fetch(bs_apod, "select * from fura.config where id = 'token.webkey'", True)

        assert res
        assert type(res) == dict
        assert res.get('id') == 'token.webkey'

        await common.asql_run(bs_apod, "update fura.config set tm_stamp = current_timestamp")

        await at.rollback()
        await at.commit()

    async with AutoTransactionAsync(bs_apod) as at:
        res = await common.asql_fetch(bs_apod, "select * from fura.config")


@pytest.mark.asyncio
async def test_pod_get_db_locks(bs_pod, bs_apod):
    assert bs_pod.std_db_lock()
    assert bs_pod.nowait_db_lock()
    assert bs_apod.std_db_lock()
    assert bs_apod.nowait_db_lock()


def test_common_dynamic_pluggin_imports():
    obj = common.import_dyn_pluggin('bs_lib', True)

    assert obj[0]
    assert obj[1] is None

    obj = common.import_dyn_pluggin('bs_lib.pod.Pod', False)

    assert obj[0]
    assert obj[1] is None

    obj = common.import_dyn_pluggin('Pod', False)

    assert obj[0] is None
    assert obj[1]

    obj = common.import_dyn_pluggin('bs_lib.not_valid')

    assert obj[0] is None
    assert obj[1]

    with pytest.raises(Exception):
        assert common.import_dyn_pluggin('bs_lib.not_valid', raise_errors = True)


@pytest.mark.asyncio
async def test_interfaces():
    from bs_lib.iprovider import IProvider
    from bs_lib.transactionable import Transactionable
    from bs_lib.transactionable_async import TransactionableAsync

    p = IProvider()
    x = Transactionable()
    y = TransactionableAsync()

    p.new_config()
    p.new_db_connector()
    p.new_logger()
    p.new_pod()

    x.commit()
    await y.commit()

    x.rollback()
    await y.rollback()
