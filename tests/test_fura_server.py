# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import pytest

from bs_fura.braze import AuthType_Couplet
from bs_fura.braze import bAuthPolicy
from bs_fura.braze import bUserLogin

from bs_fura.db.tables import tFunc
from bs_fura.db.tables import tFuncGrp
from bs_fura.db.tables import tRole
from bs_fura.db.tables import tSite
from bs_fura.db.tables import tUsr

import utils


pytestmark = [ pytest.mark.fura_server ]


def _set_fura_pod_and_tok(fs, site: tSite = None):
    fs._pod.usr_id = 'admin'
    fs._pod.site_id = 1 if not site else site.id
    fs._tok_data = { 'usr': 'admin', 'site': fs._pod.site_id, 'role': 'admin' }


@pytest.fixture(scope='module')
def serverimpl_fura(bs_pod):
    from bs_fura.fura_server_impl import FuraServerImpl

    res = FuraServerImpl(bs_pod, 'postgresql')
    res._construct()
    return res


@pytest.fixture(scope='module')
async def serverimpl_fura_async(bs_apod):
    from bs_fura.fura_server_impl_async import FuraServerImplAsync

    res = FuraServerImplAsync(bs_apod, 'postgresql')
    res._construct()
    yield res


@pytest.fixture(scope='module')
async def fura_servers(serverimpl_fura, serverimpl_fura_async):
    fs = utils.MultiTest([serverimpl_fura], [serverimpl_fura_async])

    return fs


@pytest.fixture(scope='module')
async def fura_daos(serverimpl_fura, serverimpl_fura_async):
    fs = utils.MultiTest([serverimpl_fura._dao], [serverimpl_fura_async._dao])

    return fs


@pytest.fixture(scope='module')
def fura_site_code() -> str:
    return 'FURA'


@pytest.mark.asyncio
async def test_read_auth_policy_async(fura_servers, fura_site_code):
    """
    Ensure the basic auth policy call works, as it does not require a login of any kind.
    """
    for fs in fura_servers.all():
        res = await fura_servers.call(fs, 'auth_policy_read', [AuthType_Couplet.key_password, fura_site_code])

        assert res
        assert isinstance(res, bAuthPolicy)
        assert res.auth_type == AuthType_Couplet.key_password
        assert res.descr
        assert res.regex


@pytest.mark.asyncio
async def test_fura_admin_login(fura_servers, fura_site_code):
    """
    Check that we are able to log into the main fura admin account with the dev password.
    After that do a bunch of basic user related server API calls to ensure the basics work
    """
    for fs in fura_servers.all():
        ul  = bUserLogin(fura_site_code, 'admin', AuthType_Couplet.key_password, 'dev')
        res = await fura_servers.call(fs, 'user_login', [ul])

        _set_fura_pod_and_tok(fs)

        assert res == 0

        urec = await fura_servers.call(fs, 'user_rec')

        assert urec
        assert isinstance(urec, tUsr)
        assert urec.site_id == 1
        assert urec.id      == 'admin'
        assert urec.role_id == 'admin'

        urole = await fura_servers.call(fs, 'user_role')

        assert urole
        assert isinstance(urole, tRole)
        assert urole.site_id == 1
        assert urole.id      == 'admin'
        assert urole.status  == tRole.Status_Couplet.key_super_user
        assert urole.level   == 1

        assert await fura_servers.call(fs, 'user_is_super_user')

        slist = await fura_servers.call(fs, 'site_read', ['FURA'])

        assert slist
        assert isinstance(slist, tSite.List)
        assert len(slist)    == 1
        assert slist[0].id   == 1
        assert slist[0].code == 'FURA'

        res = await fura_servers.call(fs, 'user_logout')

        assert res == 0

        with pytest.raises(Exception) as ex1:
            fs._tok_data.clear()
            await fura_servers.call(fs, 'user_role')

        assert str(ex1.value).startswith('Auth user required for RPC call')


@pytest.mark.asyncio
async def test_create_site(fura_servers, fura_site_code):
    """
    Using the fura admin user, create a new site, new roles, and functions.
    """
    idx = 1

    for fs in fura_servers.all():
        _set_fura_pod_and_tok(fs)
        scode = f'BS-TEST-{idx}'
        idx += 1

        srec = tSite(0, scode, f'Bitsmiths Fura Test Site - {idx}', tSite.Status_Couplet.key_active)
        sres = await fura_servers.call(fs, 'site_create', [srec, 'admin@bstesting.co.za', 'devXY123$^&'])

        assert sres


@pytest.mark.asyncio
async def test_site_read_update(fura_servers):
    """
    Ensure we can read and update the site correctly as the admin of this site.
    """
    idx = 1

    for fs in fura_servers.all():
        _set_fura_pod_and_tok(fs)
        scode = f'BS-TEST-{idx}'
        idx += 1

        slist = await fura_servers.call(fs, 'site_read', [scode])

        assert slist
        assert isinstance(slist, tSite.List)
        assert len(slist)    == 1
        assert slist[0].code == scode

        srec = slist[0]

        srec.cc  = 'zz'
        srec.ccy = 'zar'

        urec = await fura_servers.call(fs, 'site_update', [srec])

        assert urec
        assert isinstance(urec, tSite)
        assert urec.id    == srec.id
        assert urec.descr == srec.descr
        assert urec.code  == srec.code
        assert urec.cc    == srec.cc
        assert urec.ccy   == srec.ccy


@pytest.mark.asyncio
async def test_role_crud(fura_servers):
    """
    Test all CRUD calls for roles.
    """
    idx = 1

    for fs in fura_servers.all():
        _set_fura_pod_and_tok(fs)
        scode = f'BS-TEST-{idx}'
        idx += 1

        slist = await fura_servers.call(fs, 'site_read', [scode])
        site_rec = slist[0]
        _set_fura_pod_and_tok(fs, site_rec)

        clist = [
            tRole(site_rec.id, 'power_user', 'Power User',    tRole.Status_Couplet.key_active,  3, 0),
            tRole(site_rec.id, 'data_cap',   'Data Capturer', tRole.Status_Couplet.key_active,  6, 10),
            tRole(site_rec.id, 'aud',        'Audit Clarke',  tRole.Status_Couplet.key_active, 10, 20),
            tRole(site_rec.id, 'remme',      'Remove Me',     tRole.Status_Couplet.key_active, 12, 0),
        ]

        olist = []

        for rec in clist:
            orec = await fura_servers.call(fs, 'role_create', [rec])

            assert orec
            assert isinstance(orec, tRole)
            assert orec.site_id      == rec.site_id
            assert orec.id           == rec.id
            assert orec.descr        == rec.descr
            assert orec.status       == rec.status
            assert orec.level        == rec.level
            assert orec.sess_timeout == rec.sess_timeout
            assert orec.modified_by  == 'admin'

            olist.append(orec)

        olist[2].status       = tRole.Status_Couplet.key_disabled
        olist[2].descr        = 'Audit Admin'
        olist[3].level        = 18
        olist[3].sess_timeout = 60

        ulist = []

        ulist.append(await fura_servers.call(fs, 'role_update', [ olist[2] ]))
        ulist.append(await fura_servers.call(fs, 'role_update', [ olist[3] ]))

        for urec in ulist:
            assert isinstance(urec, tRole)

        for orec in olist:
            rlist = await fura_servers.call(fs, 'role_read', [orec.id])

            assert rlist
            assert isinstance(rlist, tRole.List)
            assert len(rlist) == 1

            rrec = rlist[0]

            assert rrec.site_id      == orec.site_id
            assert rrec.id           == orec.id
            assert rrec.descr        == orec.descr
            assert rrec.status       == orec.status
            assert rrec.level        == orec.level
            assert rrec.sess_timeout == orec.sess_timeout
            assert rrec.modified_by  == orec.modified_by

        alist = await fura_servers.call(fs, 'role_read', [''])

        assert alist
        assert isinstance(alist, tRole.List)
        assert len(alist) == 5

        await fura_servers.call(fs, 'role_delete', [clist[3].id])

        alist = await fura_servers.call(fs, 'role_read', [''])

        assert alist
        assert isinstance(alist, tRole.List)
        assert len(alist) == 4


@pytest.mark.asyncio
async def test_func_reads(fura_servers):
    """
    Test the read calls for functions and function groups.
    """
    for fs in fura_servers.all():
        _set_fura_pod_and_tok(fs)

        flist = await fura_servers.call(fs, 'func_grp_read', ['BS*'])

        assert flist
        assert isinstance(flist, tFuncGrp.List)
        assert len(flist) == 2

        for fg in flist:
            xlist = await fura_servers.call(fs, 'func_grp_read', [fg.id])

            assert xlist
            assert isinstance(flist, tFuncGrp.List)
            assert len(xlist) == 1

            assert xlist[0].id          == fg.id
            assert xlist[0].descr       == fg.descr
            assert xlist[0].modified_by == fg.modified_by

        flist = await fura_servers.call(fs, 'func_read', ['', 'BS-XYZ'])

        assert flist
        assert isinstance(flist, tFunc.List)
        assert len(flist) == 2

        flist = await fura_servers.call(fs, 'func_read', ['', 'BS-TEST'])

        assert flist
        assert isinstance(flist, tFunc.List)
        assert len(flist) == 4

        flist = await fura_servers.call(fs, 'func_read', ['', 'BS*'])

        assert flist
        assert isinstance(flist, tFunc.List)
        assert len(flist) == 6

        for frec in flist:
            xlist = await fura_servers.call(fs, 'func_read', [frec.id, ''])

            assert xlist
            assert isinstance(flist, tFunc.List)
            assert len(xlist) == 1

            assert xlist[0].id           == frec.id
            assert xlist[0].funcgrp_id   == frec.funcgrp_id
            assert xlist[0].descr        == frec.descr
            assert xlist[0].action       == frec.action
            assert xlist[0].oride        == frec.oride
            assert xlist[0].audit_lvl    == frec.audit_lvl
            assert xlist[0].modified_by  == frec.modified_by

        flist = await fura_servers.call(fs, 'func_read', ['BS*', ''])

        assert flist
        assert isinstance(flist, tFunc.List)
        assert len(flist) == 6

        flist = await fura_servers.call(fs, 'func_read', ['BS*', 'FOO'])

        assert isinstance(flist, tFunc.List)
        assert len(flist) == 0

        flist = await fura_servers.call(fs, 'func_read', ['XX', ''])

        assert isinstance(flist, tFunc.List)
        assert len(flist) == 0


@pytest.mark.asyncio
async def test_user_crud(fura_servers):
    """
    Test basic user create read update.
    """
    idx = 1

    for fs in fura_servers.all():
        _set_fura_pod_and_tok(fs)
        scode = f'BS-TEST-{idx}'
        idx += 1

        slist = await fura_servers.call(fs, 'site_read', [scode])
        site_rec = slist[0]

        _set_fura_pod_and_tok(fs, site_rec)

        act   = tUsr.Status_Couplet.key_active
        clist = [
            tUsr(0, 'u1', 'power_user', act, 'Power', 'User',  '', email1='p@u.co.za', usrtype_id='client'),
            tUsr(0, 'u2', 'data_cap',   act, 'Data',  'Cap',   '', email1='d@c.co.za', usrtype_id='client'),
            tUsr(0, 'u3', 'aud',        act, 'Aud',   'Diter', '', email1='a@d.co.za', usrtype_id='client'),
            tUsr(0, 'u4', 'admin',      act, 'Ad',    'Min',   '', email1='a@m.co.za', usrtype_id='client'),
            tUsr(0, 'u5', 'aud',        act, 'Rem',   'Me',    '', email1='r@m.co.za', usrtype_id='client'),
        ]

        olist = []

        for rec in clist:
            orec = await fura_servers.call(fs, 'user_create', [rec])

            assert orec
            assert isinstance(orec, tUsr)

            assert orec.site_id        == site_rec.id
            assert orec.id             == rec.id
            assert orec.role_id        == rec.role_id
            assert orec.status         == rec.status
            assert orec.name_first     == rec.name_first
            assert orec.name_last      == rec.name_last
            assert orec.title          == rec.title
            assert orec.date_activate  == rec.date_activate
            assert orec.date_expire    == rec.date_expire
            assert orec.email1         == rec.email1
            assert orec.email2         == rec.email2
            assert orec.cellno1        == rec.cellno1
            assert orec.cellno2        == rec.cellno2
            assert orec.modified_by    == 'admin'
            assert orec.usrtype_id     == rec.usrtype_id
            assert orec.email1_conf    == rec.email1_conf
            assert orec.email2_conf    == rec.email2_conf
            assert orec.cellno1_conf   == rec.cellno1_conf
            assert orec.cellno2_conf   == rec.cellno2_conf
            assert orec.opt_system     == rec.opt_system
            assert orec.opt_market     == rec.opt_market
            assert orec.opt_fail_login == rec.opt_fail_login

            olist.append(orec)

        olist[1].cellno2      = '0154569871'
        olist[0].email2       = 'foo@BAR.co.au'
        olist[0].status       = tUsr.Status_Couplet.key_disabled

        olist[1].cellno1      = '0114785487'
        olist[1].status       = tUsr.Status_Couplet.key_suspended
        olist[1].title        = 'Mrs'

        olist[2].date_activate = datetime.date.today()
        olist[2].date_expire   = datetime.date.today() + datetime.timedelta(days=100)
        olist[2].cellno2       = '0234434241'
        olist[2].status        = tUsr.Status_Couplet.key_expired
        olist[2].name_first    = 'AUD'
        olist[2].name_first    = 'ITER'

        olist[3].usrtype_id   = 'sys'
        olist[3].date_expire  = datetime.date.today() + datetime.timedelta(days=200)
        olist[3].status       = tUsr.Status_Couplet.key_deleted

        ulist = []

        for rec in olist[:-1]:
            urec = await fura_servers.call(fs, 'user_update', [rec])

            assert urec
            assert isinstance(urec, tUsr)

            assert urec.site_id        == site_rec.id
            assert urec.id             == rec.id
            assert urec.role_id        == rec.role_id
            assert urec.status         == rec.status
            assert urec.status         == act
            assert urec.name_first     == rec.name_first
            assert urec.name_last      == rec.name_last
            assert urec.title          == rec.title
            assert urec.date_activate  == rec.date_activate
            assert urec.date_expire    == rec.date_expire
            assert urec.email1         == rec.email1
            assert urec.email2         == rec.email2
            assert urec.cellno1        == rec.cellno1
            assert urec.cellno2        == rec.cellno2
            assert urec.modified_by    == 'admin'
            assert urec.usrtype_id     == rec.usrtype_id
            assert urec.email1_conf    == rec.email1_conf
            assert urec.email2_conf    == rec.email2_conf
            assert urec.cellno1_conf   == rec.cellno1_conf
            assert urec.cellno2_conf   == rec.cellno2_conf
            assert urec.opt_system     == rec.opt_system
            assert urec.opt_market     == rec.opt_market
            assert urec.opt_fail_login == rec.opt_fail_login

            ulist.append(urec)

        await fura_servers.call(fs, 'user_delete', [clist[-1].id])

        clist.pop()

        for rec in ulist:
            xlist = await fura_servers.call(fs, 'user_read', [rec.id, ''])

            assert xlist
            assert isinstance(xlist, tUsr.List)

            rrec = xlist[0]

            assert rrec.site_id        == site_rec.id
            assert rrec.id             == rec.id
            assert rrec.role_id        == rec.role_id
            assert rrec.status         == rec.status
            assert rrec.name_first     == rec.name_first
            assert rrec.name_last      == rec.name_last
            assert rrec.title          == rec.title
            assert rrec.date_activate  == rec.date_activate
            assert rrec.date_expire    == rec.date_expire
            assert rrec.email1         == rec.email1
            assert rrec.email2         == rec.email2
            assert rrec.cellno1        == rec.cellno1
            assert rrec.cellno2        == rec.cellno2
            assert rrec.modified_by    == 'admin'
            assert rrec.usrtype_id     == rec.usrtype_id
            assert rrec.email1_conf    == rec.email1_conf
            assert rrec.email2_conf    == rec.email2_conf
            assert rrec.cellno1_conf   == rec.cellno1_conf
            assert rrec.cellno2_conf   == rec.cellno2_conf
            assert rrec.opt_system     == rec.opt_system
            assert rrec.opt_market     == rec.opt_market
            assert rrec.opt_fail_login == rec.opt_fail_login

        clist = await fura_servers.call(fs, 'user_read', ['', 'client'])

        assert clist
        assert isinstance(clist, tUsr.List)
        assert len(clist) == 3


@pytest.mark.asyncio
async def test_user_admin_auth_resets(fura_servers):
    """
    Test the admin (super user) can successfully set other user passwords.
    """
    idx = 1

    for fs in fura_servers.all():
        _set_fura_pod_and_tok(fs)
        scode = f'BS-TEST-{idx}'
        idx += 1

        slist = await fura_servers.call(fs, 'site_read', [scode])
        site_rec = slist[0]

        _set_fura_pod_and_tok(fs, site_rec)

        assert await fura_servers.call(fs, 'user_is_super_user')

        ap = AuthType_Couplet.key_password

        await fura_servers.call(fs, 'user_auth_reset_admin', ['u1', ap, 'foo1@bar.zz'])
        _set_fura_pod_and_tok(fs, site_rec)
        await fura_servers.call(fs, 'user_auth_reset_admin', ['u2', ap, 'foo2@bar.zz'])

        _set_fura_pod_and_tok(fs, site_rec)

        # with pytest.raises(xMettle) as ex1:
        #     await fura_servers.call(fs, 'user_auth_reset_admin', ['u3', ap, 'foo3@bar.zz'])

        # assert ex1.value.get_error_code() == bs_fura.ErrCode.RoleDisabled

        # _set_fura_pod_and_tok(fs, site_rec)
        # rlist = await fura_servers.call(fs, 'role_read', ['aud'])

        # assert rlist
        # assert isinstance(rlist, tRole.List)
        # assert len(rlist) == 1

        # rlist[0].status = tRole.Status_Couplet.key_active

        # await fura_servers.call(fs, 'role_update', [rlist[0]])
        # await fura_servers.call(fs, 'user_auth_reset_admin', ['u3', ap, 'foo3@bar.zz'])


@pytest.mark.asyncio
async def test_user_search(fura_daos):
    """
    Test all users search options.
    """
    idx = 1

    for fd in fura_daos.all():
        scode = f'BS-TEST-{idx}'
        site = await fura_daos.call(fd, 'site_get', [scode])

        ulist = await fura_daos.call(fd, 'usr_search', [site.id, 'admin', None, None, None, None, None, None, None, None])
        assert ulist
        assert len(ulist) == 1
        assert ulist[0].id == 'admin'

        ulist = await fura_daos.call(fd, 'usr_search', [site.id, None, ['power_user'], None, None, None, None, None, None, None])  # noqa
        assert len(ulist) == 1
        assert ulist[0].id == 'u1'
        assert ulist[0].role_id == 'power_user'

        ulist = await fura_daos.call(fd, 'usr_search', [site.id, None, None, 'Power', None, None, None, None, None, None])
        assert len(ulist) == 1
        assert ulist[0].id == 'u1'
        assert ulist[0].name_first == 'Power'

        ulist = await fura_daos.call(fd, 'usr_search', [site.id, None, None, None, 'Cap', None, None, None, None, None])
        assert len(ulist) == 1
        assert ulist[0].id == 'u2'
        assert ulist[0].name_first == 'Data'
        assert ulist[0].name_last == 'Cap'

        ulist = await fura_daos.call(fd, 'usr_search', [site.id, None, None, None, None, 'p@u.co.za', None, None, None, None])
        assert len(ulist) == 1
        assert ulist[0].id == 'u1'

        ulist = await fura_daos.call(fd, 'usr_search', [site.id, None, None, None, None, None, None, None, 'client', None])
        assert len(ulist) > 1

        if fura_daos.is_async(fd):
            await fd.pod.rollback()
        else:
            fd.pod.rollback()
