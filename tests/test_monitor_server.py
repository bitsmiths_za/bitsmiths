# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import pytest
import time

from mettle.lib import xMettle

from bs_monitor.monitor_manager     import MonitorManager
from bs_monitor.monitor_server_impl import MonitorServerImpl

from bs_monitor.braze import bBatchQuery
from bs_monitor.braze import bBatchInstQuery
from bs_monitor.braze import bJobInstQuery
from bs_monitor.braze import bLazyLoad

from bs_monitor.db.tables import tBatch
from bs_monitor.db.tables import tBatchItem
from bs_monitor.db.tables import tBatchInst
from bs_monitor.db.tables import tJob
from bs_monitor.db.tables import tJobInst
from bs_monitor.db.tables import tMondate

import utils


pytestmark = [ pytest.mark.monitor_server ]

PYTHON_EXE_PATH = '$LOCAL_PATH/bs-venv/bin/python'
TEST_BATCH_PATH = '$LOCAL_PATH/tests/_test_python3/monitor'


@pytest.fixture(scope='module')
def serverimpl_fura(bs_pod):
    from bs_fura.fura_server_impl import FuraServerImpl

    res = FuraServerImpl(bs_pod, 'postgresql')
    res._construct()
    return res


@pytest.fixture(scope='module')
def serverimpl_monitor(bs_pod):
    from bs_monitor.monitor_server_impl import MonitorServerImpl

    res = MonitorServerImpl(bs_pod, 'postgresql')
    res._construct()
    return res


@pytest.fixture(scope='module')
async def serverimpl_monitor_async(bs_apod):
    from bs_monitor.monitor_server_impl_async import MonitorServerImplAsync

    res = MonitorServerImplAsync(bs_apod, 'postgresql')
    res._construct()
    yield res


@pytest.fixture(scope='module')
async def monitor_servers(serverimpl_monitor, serverimpl_monitor_async):
    fs = utils.MultiTest([serverimpl_monitor], [serverimpl_monitor_async])

    return fs


@pytest.fixture(scope='session')
def mon_manager(bs_pod) -> MonitorManager:
    mm  = MonitorManager(bs_pod,
                         '$LOCAL_PATH/bs-venv/bin/python $LOCAL_PATH/tests/_test_python3/monitor/job-runner.py',
                         5,
                         1,
                         'postgresql')

    mm.initialize()

    return mm


@pytest.mark.asyncio
async def test_server_datetime(monitor_servers):
    """
    Test getting the server datetime.
    """
    dtnow = datetime.datetime.now().replace(microsecond=0)

    for ms in monitor_servers.all():
        sdt = await monitor_servers.call(ms, 'server_date_time')

        assert sdt >= dtnow
        assert dtnow + datetime.timedelta(seconds = 2) >= sdt


@pytest.mark.asyncio
async def test_mondate_crud(monitor_servers):
    """
    Test we can crud the mondate calls.
    """
    for ms in monitor_servers.all():
        res = await monitor_servers.call(ms, 'mondate_read', ['rundate'])

        assert res
        assert isinstance(res, tMondate)
        assert res.id == 'rundate'
        assert res.value

        res = await monitor_servers.call(ms, 'mondate_list', [bLazyLoad(), ''])

        assert res
        assert isinstance(res, tMondate.List)
        assert len(res) >= 2

        for rec in res:
            if rec.id == 'testdate':
                await monitor_servers.call(ms, 'mondate_delete', ['testdate'])
                break

        in_rec = tMondate('testdate', 'This is a test date', datetime.date.today())
        cr_rec = await monitor_servers.call(ms, 'mondate_create', [in_rec])

        assert cr_rec
        assert isinstance(cr_rec, tMondate)
        assert cr_rec.id    == in_rec.id
        assert cr_rec.descr == in_rec.descr
        assert cr_rec.value == in_rec.value

        cr_rec.value -= datetime.timedelta(days=3)
        cr_rec.descr  = 'Test rec updated'

        up_rec = await monitor_servers.call(ms, 'mondate_update', [cr_rec])

        assert up_rec
        assert isinstance(up_rec, tMondate)
        assert up_rec.id    == cr_rec.id
        assert up_rec.descr == cr_rec.descr
        assert up_rec.value == cr_rec.value

        inc_rec = await monitor_servers.call(ms, 'mondate_increment', ['testdate', 5])

        assert inc_rec
        assert isinstance(inc_rec, tMondate)
        assert up_rec.value + datetime.timedelta(days=5) == inc_rec.value

        set_rec = await monitor_servers.call(ms, 'mondate_set_value', ['testdate', datetime.date(2020, 7, 11)])

        assert set_rec
        assert isinstance(set_rec, tMondate)
        assert datetime.date(2020, 7, 11) == set_rec.value

        await monitor_servers.call(ms, 'mondate_delete', [up_rec.id])

        ures = await monitor_servers.call(ms, 'mondate_list', [bLazyLoad(), ''])

        for x in ures:
            assert x.id != 'testdate'


@pytest.mark.asyncio
async def test_purge_initial(monitor_servers):
    """
    Purge any data lying around. Ths will get run agains at the end to ensure it works.
    """
    for ms in monitor_servers.all():
        await monitor_servers.call(ms, 'purge_history', [datetime.date.today() + datetime.timedelta(days=1)])

        res = await monitor_servers.call(ms, 'batch_list', [bLazyLoad(), bBatchQuery()])

        assert res is not None

        for rec in res:
            rec.parent_id = None
            await monitor_servers.call(ms, 'batch_update', [rec])

        for rec in res:
            await monitor_servers.call(ms, 'batch_delete', [rec.id])

        res = await monitor_servers.call(ms, 'job_list', [bLazyLoad(), '', ''])

        assert res is not None

        for rec in res:
            await monitor_servers.call(ms, 'job_delete', [rec.id])


@pytest.mark.asyncio
async def test_job_crud(monitor_servers):
    """
    Test we can crud the job calls.
    """
    for ms in monitor_servers.all():
        res = await monitor_servers.call(ms, 'job_list', [bLazyLoad(), '', ''])

        assert res is not None
        assert isinstance(res, tJob.List)

        for rec in res:
            await monitor_servers.call(ms, 'job_delete', [rec.id])

        in_recs = [
            tJob(0, 'jobcrud_01', 'Testing',       '/asdf/as/df/job/crud', '- ${somearg}', 5),
            tJob(0, 'jobcrud_02', 'Testing',       '${LOCAL_PATH}/python', 'myserver.py', 2),
            tJob(0, 'jobcrud_03', 'Another Group', '/usr/bin/ls', '', 8),
        ]

        out_recs = []

        for irec in in_recs:
            orec = await monitor_servers.call(ms, 'job_create', [irec])

            assert orec
            assert isinstance(orec, tJob)
            assert orec.id
            assert orec.name         == irec.name
            assert orec.group_id     == irec.group_id
            assert orec.program_path == irec.program_path
            assert orec.program_args == irec.program_args
            assert orec.priority     == irec.priority

            out_recs.append(orec)

            rrec = await monitor_servers.call(ms, 'job_read', [orec.id, ''])

            assert rrec
            assert isinstance(rrec, tJob)
            assert rrec.id           == orec.id
            assert rrec.name         == orec.name
            assert rrec.group_id     == orec.group_id
            assert rrec.program_path == orec.program_path
            assert rrec.program_args == orec.program_args
            assert rrec.priority     == orec.priority


        fnd = await monitor_servers.call(ms, 'job_list', [bLazyLoad(), '', 'Testing'])

        assert fnd
        assert len(fnd) == 2

        out_recs[0].priority     = 12
        out_recs[0].name         = 'job test 01'
        out_recs[1].priority     = 6
        out_recs[1].program_args = '${LOCAL_PATH}/code/py3/myserver.py'
        out_recs[2].priority     = 4
        out_recs[2].group_id     = 'Testing'

        for orec in out_recs:
            urec = await monitor_servers.call(ms, 'job_update', [orec])

            assert urec.id           == orec.id
            assert urec.name         == orec.name
            assert urec.group_id     == orec.group_id
            assert urec.program_path == orec.program_path
            assert urec.program_args == orec.program_args
            assert urec.priority     == orec.priority

        fnd = await monitor_servers.call(ms, 'job_list', [bLazyLoad(), '', 'Testing'])

        assert fnd
        assert len(fnd) == 3

        for orec in out_recs:
            await monitor_servers.call(ms, 'job_delete', [orec.id])

        fnd = await monitor_servers.call(ms, 'job_list', [bLazyLoad(), '', 'Testing'])

        assert fnd is not None
        assert len(fnd) == 0


@pytest.mark.asyncio
async def test_batch_crud(monitor_servers):
    """
    Test we can crud the batch calls.
    """
    for ms in monitor_servers.all():
        bstat = tBatch.Status_Couplet()
        bcycl = tBatch.Cycle_Couplet()
        tday  = datetime.datetime.now().replace(microsecond=0)
        res   = await monitor_servers.call(ms, 'batch_list', [bLazyLoad(), bBatchQuery()])

        assert res is not None
        assert isinstance(res, tBatch.List)

        for rec in res:
            rec.parent_id = None
            await monitor_servers.call(ms, 'batch_update', [rec])

        for rec in res:
            await monitor_servers.call(ms, 'batch_delete', [rec.id])


        in_recs = [
            tBatch(0, None, 'Batch 01', 'Test Group', bstat.key_active,   bcycl.key_day,   tday,  3, '023000', None),
            tBatch(0, None, 'Batch 02', 'Test Group', bstat.key_active,   bcycl.key_month, tday,  2, '023000', None),
            tBatch(0, None, 'Batch 03', 'Test Group', bstat.key_disabled, bcycl.key_day,   tday,  5, None, {'tz': -2}),
            tBatch(0, None, 'Batch 04', 'Test Group', bstat.key_disabled, bcycl.key_week,  tday,  4, None, {'foo': 'bar'}),
        ]

        out_recs = []

        for irec in in_recs:
            orec = await monitor_servers.call(ms, 'batch_create', [irec])

            assert orec
            assert isinstance(orec, tBatch)
            assert orec.id
            assert orec.parent_id    == irec.parent_id
            assert orec.name         == irec.name
            assert orec.group_id     == irec.group_id
            assert orec.status       == irec.status
            assert orec.cycle        == irec.cycle
            assert orec.run_date     == irec.run_date
            assert orec.run_interval == irec.run_interval
            assert orec.run_time     == irec.run_time
            assert orec.ext_data     == irec.ext_data

            out_recs.append(orec)

            rrec = await monitor_servers.call(ms, 'batch_read', [orec.id, ''])

            assert rrec
            assert isinstance(rrec, tBatch)
            assert rrec.id           == orec.id
            assert rrec.parent_id    == orec.parent_id
            assert rrec.name         == orec.name
            assert rrec.group_id     == orec.group_id
            assert rrec.status       == orec.status
            assert rrec.cycle        == orec.cycle
            assert rrec.run_date     == orec.run_date
            assert rrec.run_interval == orec.run_interval
            assert rrec.ext_data     == orec.ext_data


        out_recs[0].group_id     = 'Foobar Group'
        out_recs[0].run_interval = 1
        out_recs[1].group_id     = 'Foobar Group'
        out_recs[1].run_interval = 1
        out_recs[1].cycle        = bcycl.key_day
        out_recs[1].parent_id    = out_recs[0].id
        out_recs[1].ext_data     = { 'cust': 'data' }
        out_recs[2].run_interval = 1
        out_recs[2].status       = bstat.key_active
        out_recs[2].parent_id    = out_recs[1].id
        out_recs[3].run_interval = 1
        out_recs[3].name         = 'I have a new name'
        out_recs[3].cycle        = bcycl.key_day
        out_recs[3].parent_id    = out_recs[2].id

        for orec in out_recs:
            urec = await monitor_servers.call(ms, 'batch_update', [orec])

            assert urec
            assert isinstance(urec, tBatch)
            assert urec.id
            assert urec.parent_id    == orec.parent_id
            assert urec.name         == orec.name
            assert urec.group_id     == orec.group_id
            assert urec.status       == orec.status
            assert urec.cycle        == orec.cycle
            assert urec.run_date     == orec.run_date
            assert urec.run_interval == orec.run_interval
            assert urec.ext_data     == orec.ext_data


        lst = await monitor_servers.call(ms, 'batch_list', [bLazyLoad(), bBatchQuery()])

        assert lst
        assert isinstance(res, tBatch.List)
        assert len(lst) == len(out_recs)

        # Ensure we cannot be a parent of ourself
        with pytest.raises(xMettle):
            out_recs[1].parent_id = out_recs[1].id
            await monitor_servers.call(ms, 'batch_update', [out_recs[1]])

        # Ensure we cannot create recursive dependencies
        with pytest.raises(xMettle):
            out_recs[0].parent_id = out_recs[3].id
            await monitor_servers.call(ms, 'batch_update', [out_recs[0]])

        for rec in out_recs:
            rec.parent_id = None
            await monitor_servers.call(ms, 'batch_update', [rec])

        for rec in out_recs:
            await monitor_servers.call(ms, 'batch_delete', [rec.id])

        lst = await monitor_servers.call(ms, 'batch_list', [bLazyLoad(), bBatchQuery()])

        assert lst is not None
        assert isinstance(res, tBatch.List)
        assert len(lst) == 0


@pytest.mark.asyncio
async def test_batchitem_crud(monitor_servers):
    """
    Test we can add items to batches, move then around and the usual crud.
    """
    for ms in monitor_servers.all():
        bstat = tBatch.Status_Couplet()
        bcycl = tBatch.Cycle_Couplet()
        tday  = datetime.datetime.now().replace(microsecond=0)
        res   = await monitor_servers.call(ms, 'batch_list', [bLazyLoad(), bBatchQuery()])

        assert res is not None
        assert isinstance(res, tBatch.List)

        for rec in res:
            rec.parent_id = None
            await monitor_servers.call(ms, 'batch_update', [rec])

        for rec in res:
            await monitor_servers.call(ms, 'batch_delete', [rec.id])

        brec = await monitor_servers.call(ms, 'batch_create',
                                          [tBatch(0, None, 'Items', 'Test Group', bstat.key_active, bcycl.key_day, tday, 1)])

        assert brec

        jlist = await monitor_servers.call(ms, 'job_list', [bLazyLoad(), 'item_tester', ''])

        if jlist:
            jrec = jlist[0]
        else:
            jrec = await monitor_servers.call(ms, 'job_create', [tJob(0, 'item_tester', 'Items',  '/item_test', '', 5)])

        in_recs = [
            tBatchItem(1, brec.id, jrec.id, '--batch 1'),
            tBatchItem(2, brec.id, jrec.id, '--batch 2'),
            tBatchItem(3, brec.id, jrec.id, '--batch 3'),
            tBatchItem(4, brec.id, jrec.id, '--batch 4'),
            tBatchItem(5, brec.id, jrec.id, '--batch 5'),
            tBatchItem(6, brec.id, jrec.id, '--batch 6'),
            tBatchItem(7, brec.id, jrec.id, '--batch 7'),
        ]

        out_recs = []

        for irec in in_recs:
            orec = await monitor_servers.call(ms, 'batchitem_create', [irec])

            assert orec
            assert isinstance(orec, tBatchItem)
            assert orec.id           == irec.id
            assert orec.batch_id     == irec.batch_id
            assert orec.job_id       == irec.job_id
            assert orec.extra_args   == irec.extra_args

            out_recs.append(orec)

            rrec = await monitor_servers.call(ms, 'batchitem_read', [orec.id, orec.batch_id])

            assert rrec
            assert isinstance(rrec, tBatchItem)
            assert rrec.id           == orec.id
            assert rrec.batch_id     == orec.batch_id
            assert rrec.job_id       == orec.job_id
            assert rrec.extra_args   == orec.extra_args

        out_recs[6].extra_args += ' --and more_args'
        urec = await monitor_servers.call(ms, 'batchitem_update', [out_recs[6]])

        assert urec
        assert isinstance(urec, tBatchItem)
        assert urec.extra_args == orec.extra_args

        res = await monitor_servers.call(ms, 'batchitem_list', [brec.id])

        assert res is not None
        assert isinstance(res, tBatchItem.List)
        assert len(res) == len(out_recs)

        await monitor_servers.call(ms, 'batchitem_delete', [7, brec.id])

        srec = await monitor_servers.call(ms, 'batchitem_shift', [1, brec.id, 2])

        assert srec
        assert isinstance(srec, tBatchItem)
        assert srec.id         == 3
        assert srec.extra_args == '--batch 1'


def test_cleanup_for_job_running(serverimpl_monitor):
    """
    Cleanup any test data so we can have a clean database to test the actual running of batch
    jobs and batch instances.
    """
    serverimpl_monitor.purge_history(datetime.date.today() + datetime.timedelta(days=1))

    res = serverimpl_monitor.batch_list(bLazyLoad(), bBatchQuery())

    for rec in res:
        if rec.parent_id is not None:
            serverimpl_monitor.batch_update(rec)

    for rec in res:
        serverimpl_monitor.batch_delete(rec.id)


def test_run_insert_and_run_jobs(serverimpl_monitor, mon_manager):
    """
    Inserts the jobs we want to run as part of our test, then runs a few and ensure the monitor manager
    processes them correctly.
    """
    check_job_list = [
        tJob(0, 'mtest_simple', 'Monitor Testing', PYTHON_EXE_PATH, f'{TEST_BATCH_PATH}/test-batch.py', 10),
        tJob(0, 'mtest_slow',   'Monitor Testing', PYTHON_EXE_PATH, f'{TEST_BATCH_PATH}/test-batch.py sleep1', 10),
        tJob(0, 'mtest_fail',   'Fail Testing',    PYTHON_EXE_PATH, f'{TEST_BATCH_PATH}/test-batch.py fail', 10),
    ]

    dtnow    = datetime.datetime.now()
    job_list = []

    for irec in check_job_list:
        exsts = serverimpl_monitor.job_list(bLazyLoad(), irec.name, '')

        if exsts:
            job_list.append(exsts[0])
        else:
            job_list.append(serverimpl_monitor.job_create(irec))


    serverimpl_monitor.job_schedule(0, 'mtest_simple', dtnow, '--test 01', 10, None)
    serverimpl_monitor.job_schedule(0, 'mtest_simple', dtnow, '--test 02', 10, None)
    serverimpl_monitor.job_schedule(0, 'mtest_fail',   dtnow, '', 10, None)

    _wait_for_jobs_to_finish(serverimpl_monitor, mon_manager, 3)

    items = serverimpl_monitor.jobinst_list(bLazyLoad(0, 3), bJobInstQuery())

    assert items
    assert len(items) == 3

    assert items[0].inst_rec.status == tJobInst.Status_Couplet.key_failed
    assert items[1].inst_rec.status == tJobInst.Status_Couplet.key_completed
    assert items[2].inst_rec.status == tJobInst.Status_Couplet.key_completed

    frec = serverimpl_monitor.jobinst_force_ok(items[0].inst_rec.id, 'Because testing man')

    assert frec
    assert isinstance(frec, tJobInst)
    assert frec.id     == items[0].inst_rec.id
    assert frec.status == tJobInst.Status_Couplet.key_forced_okay


def test_batchinst_running(serverimpl_monitor, mon_manager):
    """
    Test that the batch instance running is working correctly.
    """
    start_date = datetime.datetime(2020, 7, 1, 1, 0, 0)
    bstat      = tBatch.Status_Couplet()
    bcycl      = tBatch.Cycle_Couplet()

    batch = tBatch(0, None, 'run-tester', 'Runner', bstat.key_active, bcycl.key_day, start_date, 1, '010000', None)

    res = serverimpl_monitor.batch_list(bLazyLoad(), bBatchQuery())

    for x in res:
        if x.name == batch.name:
            batch.id = x.id
            batch = serverimpl_monitor.batch_update(batch)

    if not batch.id:
        batch = serverimpl_monitor.batch_create(batch)

    assert batch
    assert isinstance(batch, tBatch)

    bilist = serverimpl_monitor.batchitem_list(batch.id)

    for item in bilist:
        serverimpl_monitor.batchitem_delete(item.id, batch.id)

    job = serverimpl_monitor.job_read(0, 'mtest_simple')

    assert job

    batch_items = [
        tBatchItem(1, batch.id, job.id, '--batch 1'),
        tBatchItem(2, batch.id, job.id, '--batch 2'),
        tBatchItem(3, batch.id, job.id, '--batch 3'),
    ]

    for item in batch_items:
        serverimpl_monitor.batchitem_create(item)

    run_date = start_date

    serverimpl_monitor.mondate_set_value('rundate', run_date.date())


    # Run the batch for 10 days to check its running okay
    for idx in range(0, 10):
        _wait_for_batch_to_finish(serverimpl_monitor, mon_manager, batch.id, run_date)

        print()
        print(idx, '  ', run_date)
        print()

        run_date += datetime.timedelta(days=1)

        serverimpl_monitor.mondate_set_value('rundate', run_date.date())

    brec = serverimpl_monitor.batch_read(batch.id, '')

    assert brec.run_date.date() == datetime.date(2020, 7, 11)


# -- HELPER FUNCTIONS --------------------------------------------------------------------------------------------------


def _wait_for_jobs_to_finish(mon_client: MonitorServerImpl, mon_manager: MonitorManager, last_cnt: int) -> None:
    """
    Wait for monitor jobs to finish.
    """
    waiting  = True
    max_wait = 50

    while waiting:
        max_wait -= 1

        if max_wait < 0:
            raise Exception('Max waits reached while waiting for jobs to complete!')

        mon_manager.run()
        time.sleep(0.1)

        items   = mon_client.jobinst_list(bLazyLoad(0, 3), bJobInstQuery())
        waiting = False

        for rec in items:
            if rec.inst_rec.status not in (tJobInst.Status_Couplet.key_completed, tJobInst.Status_Couplet.key_failed):
                waiting = True


def _wait_for_batch_to_finish(mon_client  : MonitorServerImpl,
                              mon_manager : MonitorManager,
                              batch_id    : int,
                              rundate     : datetime.datetime):
    """
    Wait for a batch to finish.
    """
    waiting  = True
    max_wait = 50

    qry = bBatchInstQuery(datetime.datetime(rundate.year, rundate.month, rundate.day,  0,  0,  0),
                          datetime.datetime(rundate.year, rundate.month, rundate.day, 23, 59, 59),
                          batch_id)

    mon_manager.run()
    time.sleep(0.1)

    while waiting:
        max_wait -= 1

        if max_wait < 0:
            raise Exception(f'Max waits reached while waiting for batch [{batch_id}] to complete!')

        mon_manager.run()
        time.sleep(0.1)

        items   = mon_client.batchinst_list(bLazyLoad(), qry)
        waiting = False

        assert items
        assert len(items) == 1

        if items[0].status != tBatchInst.Status_Couplet.key_completed:
            waiting = True


    jitems = mon_client.jobinst_list(bLazyLoad(0, 10), bJobInstQuery(batchinst_id=items[0].id))

    jitems.reverse()

    for jidx in range(len(jitems)):
        print(jitems[jidx])
        if jidx == 0:
            assert jitems[jidx].inst_rec.parent_id is None
        else:
            assert jitems[jidx].inst_rec.parent_id == jitems[jidx - 1].inst_rec.id
