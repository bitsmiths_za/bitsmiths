# **************************************************************************** #
#                           This file is part of:                              #
#                                BITSMITHS                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/bitsmiths                             #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import pytest
import time

from bs_lib.sentinel_manager import SentinelManager

import utils


pytestmark = [ pytest.mark.sentinel ]


@pytest.fixture(scope='module')
def sentinel_cfg() -> dict:
    batch_path = '$LOCAL_PATH/tests/_test_python3/monitor'

    cfg = {
        'sentinel': {
            'pluggins': [
                {
                    'name'          : 'good-service',
                    'module-path'   : 'sentinel.pluggin.good_service.GoodService',
                    'run-count'     : 1,
                    'proc-interval' : 0.2,
                    'active'        : True
                },
                {
                    'name'          : 'good-pod-service',
                    'module-path'   : 'sentinel.pluggin.good_service.GoodPodService',
                    'run-count'     : 1,
                    'proc-interval' : 0.2,
                    'active'        : True
                },
                {
                    'name'          : 'stubborn-service',
                    'module-path'   : 'sentinel.pluggin.good_service.StubbornService',
                    'run-count'     : 1,
                    'proc-interval' : 0.2,
                    'active'        : True
                },
                {
                    'name'          : 'bad-service',
                    'module-path'   : 'sentinel.pluggin.bad_service.BadService',
                    'run-count'     : 1,
                    'proc-interval' : 0.2,
                    'active'        : True
                },
                {
                    'name'          : 'bad-pod-service',
                    'module-path'   : 'sentinel.pluggin.bad_service.BadPodService',
                    'run-count'     : 1,
                    'proc-interval' : 0.2,
                    'active'        : True
                },
                {
                    'name'          : 'bad-delayed-service',
                    'module-path'   : 'sentinel.pluggin.bad_service.BadDelayedService',
                    'run-count'     : 1,
                    'proc-interval' : 0.2,
                    'active'        : True
                },
            ],

            'processes': [
                {
                    'name'        : 'server-good',
                    'proc-path'   : '$LOCAL_PY',
                    'proc-args'   : f'{batch_path}/test-batch.py forever',
                    'run-count'   : 1,
                    'manage-port' : False,
                    'port'        : None,
                    'active'      : True,
                },
                {
                    'name'        : 'server-good-port',
                    'proc-path'   : '$LOCAL_PY',
                    'proc-args'   : f'{batch_path}/test-batch.py forever',
                    'run-count'   : 1,
                    'manage-port' : True,
                    'port'        : None,
                    'active'      : True,
                },
                {
                    'name'        : 'server-good-port-19002',
                    'proc-path'   : '$LOCAL_PY',
                    'proc-args'   : f'{batch_path}/test-batch.py forever',
                    'run-count'   : 1,
                    'manage-port' : True,
                    'port'        : 19002,
                    'active'      : True,
                },
                {
                    'name'        : 'server-crash',
                    'proc-path'   : '$LOCAL_PY',
                    'proc-args'   : f'{batch_path}/test-batch.py crash',
                    'run-count'   : 1,
                    'manage-port' : False,
                    'port'        : None,
                    'active'      : True,
                },
                {
                    'name'        : 'server-crash-quick',
                    'proc-path'   : '$LOCAL_PY',
                    'proc-args'   : f'{batch_path}/test-batch.py sleep-raise0.2',
                    'run-count'   : 1,
                    'manage-port' : False,
                    'port'        : None,
                    'active'      : True,
                },
            ],
        }
    }

    return cfg


@pytest.fixture(scope='module')
def sentinel_manager(sentinel_cfg, db_connection_strings) -> SentinelManager:
    cfg = {
        'port': 18800,
        'timeout': 1.0,
        'db_url': db_connection_strings['postgresql']['*'],
    }

    prov = utils.ProviderPytest()

    sm = SentinelManager(cfg, prov.new_logger(), prov)

    sm.initialize(sentinel_cfg)

    return sm


def test_shutdown_nothing(sentinel_manager):
    sentinel_manager.shutdown()
    sentinel_manager.kill()

    for proc, pobj in sentinel_manager._processes.items():
        for sproc in pobj['procs']:
            assert not sproc.is_alive()

    for plug, pobj in sentinel_manager._pluggins.items():
        for sthread in pobj['threads']:
            assert not sthread.is_alive()


def test_start_stop_processes(sentinel_manager):
    sentinel_manager.start_services(pluggins=False, processes=True)

    time.sleep(0.2)
    sentinel_manager.monitor_services()

    time.sleep(0.2)
    assert sentinel_manager.monitor_services() > 0
    assert sentinel_manager.shutdown_services(pluggins=False, processes=True, kill=False) > 0

    while sentinel_manager.monitor_services() > 0:
        time.sleep(0.2)
        sentinel_manager.shutdown_services(pluggins=False, processes=True, kill=True)


def test_start_stop_threads(sentinel_manager):
    sentinel_manager.start_services(pluggins=True, processes=False)

    time.sleep(0.2)
    sentinel_manager.monitor_services()

    time.sleep(0.2)
    assert sentinel_manager.monitor_services() > 0
    assert sentinel_manager.shutdown_services(pluggins=True, processes=False, kill=False)

    while sentinel_manager.monitor_services() > 0:
        time.sleep(0.2)
        sentinel_manager.shutdown_services(pluggins=True, processes=False, kill=True)
